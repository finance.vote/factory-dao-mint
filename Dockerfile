FROM node:18-alpine
WORKDIR /app
ENV NODE_ENV=production
RUN npm install sharp
COPY package*.json ./
COPY ./public ./public
COPY .next/standalone ./
COPY .next/static ./.next/static
COPY next.config.js ./

EXPOSE 3000
CMD ["node", "server.js"]