import axios from 'axios';
import classnames from 'classnames';
import TextInput from 'components/Formik/TextInput/TextInput.component';
import { ethInstance } from 'evm-chain-scripts';
import { Form, Formik } from 'formik';
import FormWrapper from 'minting-components/components/FormWrapper';
import { toast } from 'react-toastify';
import utilClasses from 'utils/scss/modules/utils.module.scss';
import * as Yup from 'yup';

import styles from './GenerateCodes.module.scss';

const validationSchema = Yup.object().shape(
  {
    codesAmount: Yup.number()
      .integer('Codes amount must be an integer')
      .min(1, 'Codes amount must be greater than or equal to 1')
      .required('Required'),
    groupKey: Yup.string().required('Required'),
    startRange: Yup.string().when('endRange', {
      is: (endRange) => endRange?.length,
      then: Yup.string().required('Required')
    }),
    endRange: Yup.string().when('startRange', {
      is: (startRange) => startRange?.length,
      then: Yup.string().required('Required')
    })
  },
  ['endRange', 'startRange']
);

const GenerateCodes = () => {
  const onSubmit = async (values) => {
    const { groupKey, codesAmount, startRange, endRange } = values;
    const mintKey = groupKey;

    try {
      const currentAccount = await ethInstance.getEthAccount();
      if (!currentAccount.length) throw new Error('Connect your wallet');

      const requestBody = {
        codesAmount,
        address: currentAccount
      };
      const bodyToSign = {
        codesAmount,
        mintKey
      };

      if (startRange?.length && endRange?.length) {
        bodyToSign.range = { start: startRange, end: endRange };
        requestBody.range = { start: startRange, end: endRange };
      }

      requestBody.signature = await ethInstance.signPersonalMessage(
        JSON.stringify(bodyToSign),
        currentAccount
      );

      await axios.post(`/code-generator/${mintKey}`, requestBody);
      toast.success('Codes have been generated');
    } catch (err) {
      if (err?.message) {
        toast.error(err.message, {
          position: 'top-center'
        });
      }
    }
  };

  return (
    <Formik
      initialValues={{ groupKey: '', codesAmount: '', startRange: '', endRange: '' }}
      onSubmit={onSubmit}
      validateOnChange
      validationSchema={validationSchema}
    >
      {({ errors, values, isValid }) => {
        return (
          <FormWrapper>
            <div className={utilClasses.container}>
              <h1 className={styles.title}>Generate codes for mint</h1>
              <Form>
                <div className={styles.inputContainer}>
                  <TextInput
                    name="groupKey"
                    placeholder="Input group key"
                    label="Group key"
                    className={classnames(
                      errors.groupKey && styles.inputError,
                      styles.input,
                      utilClasses.input
                    )}
                    value={values.groupKey}
                  />
                </div>
                <div className={styles.inputContainer}>
                  <TextInput
                    type="number"
                    name="codesAmount"
                    placeholder="Input codes amount"
                    label="Codes amount"
                    className={classnames(
                      errors.codesAmount && styles.inputError,
                      styles.input,
                      utilClasses.input
                    )}
                    value={values.codesAmount}
                  />
                </div>
                <div className={styles.inputContainer}>
                  <TextInput
                    name="startRange"
                    placeholder="Input start range"
                    label="Start range"
                    className={classnames(
                      errors.startRange && styles.inputError,
                      styles.input,
                      utilClasses.input
                    )}
                    value={values.startRange}
                  />
                </div>
                <div className={styles.inputContainer}>
                  <TextInput
                    name="endRange"
                    placeholder="Input end range"
                    label="End range"
                    className={classnames(
                      errors.endRange && styles.inputError,
                      styles.input,
                      utilClasses.input
                    )}
                    value={values.endRange}
                  />
                </div>
                <div
                  className={
                    (utilClasses.hollowButton, utilClasses.hollowButtonBlack, styles.submitButton)
                  }
                >
                  <button
                    type="submit"
                    disabled={!isValid}
                    className={classnames(
                      utilClasses.hollowButton,
                      utilClasses.hollowButtonBlack,
                      styles.submitButton
                    )}
                  >
                    Generate codes
                  </button>
                </div>
              </Form>
            </div>
          </FormWrapper>
        );
      }}
    </Formik>
  );
};

export default GenerateCodes;
