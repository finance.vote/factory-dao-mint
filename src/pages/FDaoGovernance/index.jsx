import Header from 'components/Header/Header.component';
import MembershipSbts from 'components/MembershipSBTS/MembershipSbts';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import SetYourDao from 'components/SetYourDao/SetYourDao';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Image from 'next/image';
import HowItWorks from '../../components/FDaoPreSeason/HowItWorks';
import fdaoLogo from '../../images/fdaoLogo.png';
import styles from './FDaoGovernance.module.scss';

const discordLink = 'http://discord.gg/factorydao';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#4DFF64'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const FDaoGovernance = ({ groupKey }) => {
  return (
    <main className={styles.main}>
      <Header websiteLink="https://www.factorydao.xyz/" showDiscordLink={false} showTelegramLink />
      <Image
        className={styles.mockLogo}
        src={fdaoLogo}
        width={288}
        height={284}
        alt="fdao dao logo"
      />

      <h1 className={styles.mockDescTop}>Become a FactoryDAO Governor</h1>
      <h2 className={styles.mockDescMid}>
        The FactoryDAO governors will define the launch of the <br />
        protocol, govern the membership and become the DAO leaders of the future. By invite only.
      </h2>

      <MintingController
        groupKey={groupKey}
        isProgressBarVisible={false}
        walletInputLabel="Paste address or click connect wallet"
        customStyles={{ ...styles, highlightIndicators }}
        customTexts={{
          successTitle: 'Welcome to Factory DAO',
          successMessage: `#%TOKEN_ID% now belongs to you`
        }}
        discordLink={discordLink}
        noShift
        MintingButtonComponent={(props) => (
          <DefaultMintingButton
            {...props}
            actionButtonAfterMintVisible
            actionButtonAfterMintText="continue to the voting realm"
            discordLink="https://influence.factorydao.org/factorydaogovernors"
            customStyles={{
              button: styles.button,
              actionButton: styles.actionButton
            }}
          />
        )}
      />
      <HowItWorks />
      <MembershipSbts />
      <SetYourDao name="FactoryDAO" title="Launch your DAO" />
      <PoweredByDao showDiscord={false} showTelegram />
    </main>
  );
};

export default FDaoGovernance;
