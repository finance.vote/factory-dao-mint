import FaqSection from 'components/Gmgn/Faq';
import { GmgnIcons } from 'components/Gmgn/Footer/Footer';
import GenesisBenefits from 'components/Gmgn/GenesisBenefits/GenesisBenefits';
import GmgnHeader, { menu } from 'components/Gmgn/Header/Header';
import Introduce from 'components/Gmgn/Introduce/Introduce';
import StageRanges from 'components/Gmgn/StageRanges/StageRanges';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import MintingStages from 'minting-components/components/MintingStages';
import { useState } from 'react';
import customStyles from './Gmgn.module.scss';

const baseUrl = 'https://www.gmgnsupply.co';
const discordLink = 'https://discord.com/invite/vbUwAN22dj';
const twitterLink = 'https://twitter.com/gmgnsupplyco';
const linkedinLink = 'https://www.linkedin.com/company/gmgn-supply-co/';

const MobileMenu = () => {
  return (
    <>
      <div className={customStyles.mobileMenuContainer}>
        {menu.map(({ text, url }, index) => {
          return (
            <div className={customStyles.mobileMenuItem} key={index}>
              <a href={`${baseUrl}${url}`} key={index}>
                {text}
              </a>
            </div>
          );
        })}
        <div className={customStyles.iconContainer}>
          <GmgnIcons
            discordLink={discordLink}
            twitterLink={twitterLink}
            linkedinLink={linkedinLink}
          />
        </div>
      </div>
    </>
  );
};

const Gmgn = () => {
  const [showMobileMenu, setShowMobileMenu] = useState(false);

  const handleOpenMobileMenu = () => {
    setShowMobileMenu((prev) => !prev);
  };

  return (
    <div className={customStyles.gmgnPage}>
      <GmgnHeader
        handleOpenMobileMenu={handleOpenMobileMenu}
        discordLink={discordLink}
        twitterLink={twitterLink}
        linkedinLink={linkedinLink}
      />
      {showMobileMenu ? (
        <MobileMenu />
      ) : (
        <>
          <Introduce />
          <MintingController
            groupKey="gmgn"
            isFooterControlsEnabled={false}
            customTexts={{
              mobileHeader: 'GMGN',
              symbol: 'GMGN',
              successTitle: `welcome to the club`,
              successMessage: `You now own membership NFT #%TOKEN_ID%`
            }}
            discordLink={discordLink}
            customStyles={customStyles}
            MintingButtonComponent={(props) => (
              <DefaultMintingButton
                {...props}
                customStyles={{
                  button: customStyles.button
                }}
              />
            )}
            StageRangesComponent={StageRanges}
            TopPlaceComponent={<MintingStages groupKey="gmgn" />}
            prefillOnLoad={true}
          />
          <GenesisBenefits />
          <FaqSection />
          <PoweredByDao />
        </>
      )}
    </div>
  );
};

export default Gmgn;
