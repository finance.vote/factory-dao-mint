import Header from 'components/Header/Header.component';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import { ethInstance } from 'evm-chain-scripts';
import AlphaSection from 'identity-components/AlphaSection/AlphaSection';
import financevoteLogo from 'images/fvt/financevote_light.svg';
import Image from 'next/image';
import { memo, useEffect, useState } from 'react';
import IdentityFormMint from '../../identity-components/components/identity';
import styles from './IdentityPage.module.scss';

function IdentityPage() {
  const [currentNetwork, setCurrentNetwork] = useState(
    process.env.NEXT_PUBLIC_DEFAULT_CHAIN_ID || 4
  );
  const checkNetwork = async () => {
    const network = process.env.NEXT_PUBLIC_DEFAULT_CHAIN_ID || (await ethInstance.getChainId());
    setCurrentNetwork(network);
  };

  useEffect(() => {
    checkNetwork();
  }, []);

  return (
    <>
      <Header
        twitterLink="https://twitter.com/financedotvote"
        websiteLink="https://www.finance.vote/"
        discordLink="https://discord.gg/financedotvote"
      />
      <div className={styles.container}>
        <div className={styles.main}>
          <div className={styles.contentWrapper}>
            <Image
              className={styles.financevoteLogo}
              src={financevoteLogo}
              alt="Finance.Vote Logo"
            />
            <div className={styles.description}>
              <div className={styles.title}>
                The FVT subDAO is an experimental cryptoeconomics lab within FactoryDAO.
              </div>
              <br />
              <span>
                FVT Discord will be the home of finance.vote and FactoryDAO Alpha. FVT ID holders
                will have exclusive access to incentivised tests, bounties, educational content and
                DAO tasks to earn FVT.
              </span>
            </div>
            <IdentityFormMint currentNetwork={currentNetwork} />
          </div>
          <AlphaSection />
          <PoweredByDao />
        </div>
      </div>
    </>
  );
}

export default memo(IdentityPage);
