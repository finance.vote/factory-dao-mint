import DialogWhite from 'components/DialogWhite';
import financevoteLogo from 'images/fvt/financevote_dark.svg';
import Image from 'next/image';
import styles from './SuccessMintDialog.module.scss';

const SuccessMintDialog = ({ isVisible, boughtToken }) => {
  return (
    <DialogWhite isOpen={isVisible} width="650px">
      <div className={styles.successDialogContainer}>
        <Image className={styles.financevoteLogo} src={financevoteLogo} alt="Finance.Vote Logo" />
        <div className={styles.title}>
          Congratulations! <br />
          FVT ID {boughtToken} Is Yours
        </div>
        <div className={styles.info}>
          You can now access the FVT app <br />
          to play the game
        </div>
        <div className={styles.buttons}>
          <a href={'https://fvt.app/#/vote'} className={styles.startGame} rel="noopener noreferer">
            start the game
          </a>
        </div>
      </div>
    </DialogWhite>
  );
};

export default SuccessMintDialog;
