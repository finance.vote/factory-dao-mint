import Header from 'components/Header/Header.component';
import sliderIcon from 'images/fvt/financevote_icon.svg';
import financevoteLogo from 'images/fvt/financevote_light.svg';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Image from 'next/image';
import { memo, useState } from 'react';
import AlphaSection from './AlphaSection/AlphaSection';
import styles from './fvts3.module.scss';
import SuccessMintDialog from './SuccessMintDialog/SuccessMintDialog.component';

const PLAY_NOW_LINK = 'https://fvt.app';

const GROUP_KEY = 'fvt';

function MintFVT() {
  const [isSuccessDialogOpen, setIsSuccessDialogOpen] = useState(false);
  return (
    <>
      <Header
        twitterLink="https://twitter.com/financedotvote"
        websiteLink="https://fvt.app/"
        showDiscordLink={false}
        telegramLink="https://discord.gg/financedotvote"
        showTelegramLink
      />
      <div className={styles.container}>
        <div className={styles.main}>
          <div className={styles.contentWrapper}>
            <Image
              className={styles.financevoteLogo}
              src={financevoteLogo}
              alt="Finance.Vote Logo"
            />
            <div className={styles.description}>
              <div className={styles.title}>
                The cryptoeconomic prediction game. Make predictions, earn crypto.
              </div>
              <span>
                Can you predict the future? Vote on a basket of tokens in daily tournaments. Got
                votes on the winning token? You earn a share of the reward pool. Mint your non
                transferrable NFT token below to access the game.
              </span>
            </div>
            <MintingController
              groupKey={GROUP_KEY}
              customStyles={{ ...styles }}
              customTexts={{
                successTitle: `Welcome to FVT-SBT`,
                successMessage: `FVT-SBT #%TOKEN_ID% now belongs to you`
              }}
              playNowLink={PLAY_NOW_LINK}
              sliderIcon={sliderIcon}
              prefillOnLoad={true}
              MintingButtonComponent={(props) => (
                <DefaultMintingButton
                  {...props}
                  customStyles={{
                    button: styles.button,
                    actionButton: styles.actionButton
                  }}
                />
              )}
              isSuccessMintPopup
              CustomSuccessMint={(props) => (
                <SuccessMintDialog
                  {...props}
                  isSuccessDialogOpen={isSuccessDialogOpen}
                  onCloseDialogSuccess={setIsSuccessDialogOpen}
                />
              )}
            />
          </div>
          <AlphaSection />
        </div>
      </div>
    </>
  );
}

export default memo(MintFVT);
