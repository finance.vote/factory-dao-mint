import alphaIcon1 from 'images/community_icon.svg';
import alphaIcon2 from 'images/markets_icon.svg';
import alphaIcon3 from 'images/whitelist_icon.svg';
import Image from 'next/image';
import styles from './AlphaSection.module.scss';

const AlphaConfig = [
  {
    icon: alphaIcon1,
    title: 'Exclusive Gated Community',
    text: 'FVTDAO community alpha, insights and conversation'
  },
  {
    icon: alphaIcon2,
    title: 'Prediction Games',
    text: 'Participate in the cryptoeconomic prediction game to earn tokens.'
  },
  {
    icon: alphaIcon3,
    title: 'FVT Leader Airdrops',
    text: 'Climb to the top of the leaderboard to earn periodic airdrops.'
  }
];

const AlphaItems = () => {
  return AlphaConfig.map((alpha, index) => {
    return (
      <div key={index} className={styles.alphaItem}>
        <Image src={alpha.icon} alt={`alpha_${index}`} />
        <span className={styles.itemTitle}>{alpha.title}</span>
        <p>{alpha.text}</p>
        <span className={styles.additionalText}>{alpha.additionalText}</span>
      </div>
    );
  });
};

const AlphaSection = () => {
  return (
    <div className={styles.sectionWrapper}>
      <div className={styles.sectionContainer}>
        <div className={styles.sectionTitle}>
          <span className={styles.title}>the nft id that keeps on giving (alpha)</span>
          <span className={styles.subTitle}>
            An exclusive run of 100 FVT IDs are available to mint. <br />
            Each ID comes with special privileges.
          </span>
        </div>
        <AlphaItems />
        <div className={styles.lowestSection}>
          <div className={styles.informationBox}>
            <span className={styles.infoTitle}>Tune your DAO through ID Governance</span>
            <span className={styles.infoDescription}>
              FVT ID holders have voting privileges in DAO Governance. Which tokens feature in the
              prediction market game, yield staking incentives, treasury distribution and more.
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AlphaSection;
