import classnames from 'classnames';
import TextInput from 'components/Formik/TextInput/TextInput.component';
import Loader from 'components/Loader';
import Select from 'components/Ui/Select';
import { ethInstance } from 'evm-chain-scripts';
import { Form, Formik } from 'formik';
import uploadMediaFiles from 'helpers/admin';
import FormWrapper from 'minting-components/components/FormWrapper';
import { useState } from 'react';
import { toast } from 'react-toastify';
import utilClasses from 'utils/scss/modules/utils.module.scss';
import * as Yup from 'yup';

import styles from './UploadFiles.module.scss';

const MEDIA_OPTIONS = [
  { value: 'image', label: 'image' },
  { value: 'video', label: 'video' }
];

const validationSchema = Yup.object({
  groupKey: Yup.string().required('Required'),
  files: Yup.lazy((files, data) => {
    const mediaType = data.parent.mediaType?.value;
    const isSingleFile = data.parent.isSingleMediaMint;
    return Yup.mixed()
      .test('filesLength', 'Required', () => files?.length > 0)
      .test(
        'filesEven',
        mediaType === 'image'
          ? 'Should add two files for each image (media file and .json)'
          : 'Should add tree files for each image (media file, .json and profile picture)',
        () => {
          const filesData = files ? Array.from(files) : [];
          let isProfilePicForEachMedia = true;

          const jsonNames = filesData
            .filter((file) => file.name.includes('.json'))
            .map((file) => file.name.split('.')[0]);

          const mediaNames = filesData
            .filter((file) => file.type.includes(mediaType))
            .map((file) => file.name.split('.')[0]);

          if (mediaType === 'video') {
            const profilePicNames = filesData
              .filter((file) => file.type.includes('image'))
              .map((file) => file.name.split('.')[0]);

            // check that each video has profilePic
            isProfilePicForEachMedia = mediaNames.every((fileName) =>
              profilePicNames.includes(fileName)
            );
          }

          // check that each image has .json file with the same name or one img has all required json files
          const areBothDataForEchMedia = mediaNames.every((fileName) =>
            jsonNames.includes(fileName)
          );

          // should be the same count of json files and image files
          const isAllDataLengthCorrect = isSingleFile || jsonNames.length === mediaNames.length;

          return isAllDataLengthCorrect && areBothDataForEchMedia && isProfilePicForEachMedia;
        }
      )
      .nullable(true);
  })
});

const UploadFiles = () => {
  const [parsedFiles, setParsedFiles] = useState(new Array());
  const [isUploading, setIsUploading] = useState(false);

  const onSubmit = async (values) => {
    const { groupKey, mediaType, isSingleMediaMint: isSingleFile } = values;
    setIsUploading(true);
    const mintKey = groupKey;
    const mediaTypeValue = mediaType.value;
    try {
      const currentAccount = await ethInstance.getEthAccount();
      if (!currentAccount.length) throw new Error('Connect your wallet');

      const bodyToSign = {
        groupKey: mintKey,
        mediaType: mediaTypeValue
      };
      const signature = await ethInstance.signPersonalMessage(
        JSON.stringify(bodyToSign),
        currentAccount
      );

      const filesArray = [...parsedFiles];
      var singleMetaIndex = filesArray.findIndex((element) => !!element.media);
      if (singleMetaIndex > -1) {
        const element = filesArray.splice(singleMetaIndex, 1);
        filesArray.splice(0, 0, element[0]);
      }
      await uploadMediaFiles(
        mediaTypeValue,
        mintKey,
        filesArray,
        signature,
        currentAccount,
        isSingleFile
      );
      toast.success('Media files have been uploaded');
    } catch (err) {
      console.log(err);
    } finally {
      setIsUploading(false);
    }
  };

  const onUploadFile = (event, mediaType) => {
    let mediaData = new Array();
    const filesArray = Array.from(event.target.files);
    const fileNames = filesArray.map((file) => file.name.split('.')[0]);
    const uniqueFileNames = [...new Set(fileNames)];

    for (const name of uniqueFileNames) {
      const media = filesArray.find(
        (file) => file.name.split('.')[0] === name && file.type.includes(mediaType)
      );

      let profilePic;
      if (mediaType === 'video') {
        profilePic = filesArray.find(
          (file) => file.name.split('.')[0] === name && file.type.includes('image')
        );
      }

      const jsonData = filesArray.find((file) => file.name === `${name}.json`);

      if (jsonData) {
        const fileReader = new FileReader();
        fileReader.readAsText(jsonData, 'UTF-8');
        fileReader.onload = (e) => {
          const target = e.target;
          const fileBuffer = new Buffer(target?.result, 'binary');
          const meta = fileBuffer.toString('utf8');

          mediaData.push({ id: name, media, meta, profilePic });
        };
      }
    }
    setParsedFiles(mediaData);
  };

  return (
    <Formik
      initialValues={{ groupKey: '', mediaType: MEDIA_OPTIONS[0], files: null }}
      onSubmit={onSubmit}
      validateOnChange
      validationSchema={validationSchema}
    >
      {({ errors, values, setFieldValue, isValid }) => {
        return (
          <FormWrapper>
            <div className={utilClasses.container}>
              <h1 className={styles.title}>Upload media files for mint</h1>
              <Form>
                <div className={styles.inputContainer}>
                  <TextInput
                    name="groupKey"
                    placeholder="Input group key"
                    label="Group key"
                    className={classnames(
                      errors.groupKey && styles.inputError,
                      styles.input,
                      utilClasses.input
                    )}
                    value={values.groupKey}
                  />
                </div>
                <Select
                  id="media-type-selection"
                  className={classnames(
                    styles.select,
                    utilClasses.selectInput,
                    utilClasses.spaceTop
                  )}
                  defaultValue={MEDIA_OPTIONS[0].value}
                  name="mediaType"
                  value={values.mediaType}
                  onChange={(type) => {
                    setFieldValue('mediaType', type);
                  }}
                  label="Select media type"
                  options={MEDIA_OPTIONS}
                />

                <div className={classnames(styles.file, utilClasses.blackInput)}>
                  <label className={styles.fileLabel}>
                    <div className={styles.uploadText}>Upload file</div>
                    <div className={styles.inputContainer}>
                      <input
                        type="file"
                        multiple
                        name="files"
                        className={classnames(
                          errors.files && styles.inputError,
                          styles.input,
                          utilClasses.blackInput
                        )}
                        onChange={(event) => {
                          setFieldValue('files', event.target.files);
                          onUploadFile(event, values.mediaType.value);
                        }}
                      />
                      {errors && <p className={styles.textError}>{errors.files}</p>}
                    </div>
                  </label>
                </div>
                <div className={styles.backendMint}>
                  <label className={classnames(styles.checkbox)}>
                    Single media mint
                    <input
                      type="checkbox"
                      checked={values.isSingleMediaMint}
                      onChange={({ target }) => setFieldValue('isSingleMediaMint', target.checked)}
                    />
                  </label>
                </div>
                {isUploading ? (
                  <div className={styles.loaderContainer}>
                    <Loader maxWidth="200px" />
                  </div>
                ) : (
                  <div
                    className={
                      (utilClasses.hollowButton, utilClasses.hollowButtonBlack, styles.submitButton)
                    }
                  >
                    <button
                      type="submit"
                      disabled={!isValid}
                      className={classnames(
                        utilClasses.hollowButton,
                        utilClasses.hollowButtonBlack,
                        styles.submitButton
                      )}
                    >
                      Upload media
                    </button>
                  </div>
                )}
              </Form>
            </div>
          </FormWrapper>
        );
      }}
    </Formik>
  );
};

export default UploadFiles;
