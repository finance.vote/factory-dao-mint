import classNames from 'classnames';
import Benefits from 'components/MynaEth/Benefits/Benefits';
import HaveASay from 'components/MynaEth/HaveASay/HaveASay';
import MynaHeader from 'components/MynaEth/Header/MynaHeader';
import sliderIcon from 'images/myna/slider_icon_eth.svg';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Link from 'next/link';
import Introduce from '../../components/MynaEth/Introduce/Introduce';
import customStyles from './Myna.module.scss';

const DISCORD_LINK = 'https://discord.gg/serCyEKwZP';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 15px #A3FF5D',
    status: '#00F220',
    fontFamily: 'Poppins'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const MynaPage = () => {
  return (
    <>
      <div className={customStyles.mynaPage}>
        <MynaHeader
          websiteLink={'https://www.mynaaccountants.co/'}
          discordLink={DISCORD_LINK}
          twitterLink={'https://twitter.com/mynaaccountants'}
        />
        <Introduce />
        <MintingController
          groupKey="myna-eth"
          customStyles={{ ...customStyles, highlightIndicators }}
          sliderIcon={sliderIcon}
          prefillOnLoad
          customTexts={{
            mobileHeader: 'Myna',
            symbol: 'MMCETH',
            successTitle: 'welcome to the club',
            successMessage: `You now own membership NFT #%TOKEN_ID%`,
            numberInputLabel:
              'Click mint now, enter a different number or click the random number arrows',
            placeholderText: 'Pick a number',
            status: {
              unavailable: 'This NFT is taken - choose another'
            },
            buttonText: {
              available: 'MINT NOW',
              unavailable: 'MINT NOW',
              mintAgain: 'MINT ANOTHER'
            }
          }}
          discordLink={DISCORD_LINK}
          MintingButtonComponent={(props) => (
            <DefaultMintingButton
              {...props}
              discordLink={DISCORD_LINK}
              actionButtonAfterMintText={!props.isEligibleToMint ? 'JOIN THE DISCORD' : ''}
              customStyles={{
                button: customStyles.button,
                actionButton: !props.isEligibleToMint
                  ? classNames(customStyles.actionButton, customStyles.discordButton)
                  : customStyles.actionButton
              }}
            />
          )}
          tooltipContent={
            <>
              This is the number of the NFT in the series, not the amount of NFTs you are minting.
              <br />
              <br />
              NFTs are minted one at a time.
              <br />
              <br />
              The next available NFT in the series has been selected for you, just click “MINT NOW”
              if you are happy with it.
              <br />
              <br />
              If you would like to pick another random number click the green “random” arrows
              <br />
              <br />
              If you would like a specific number in the series, try typing it in. It may be taken!
              The traffic lights will indicate if it’s:
              <div>
                <span className={customStyles.tooltipBoldText}>Green:</span> Available
              </div>
              <div>
                <span className={customStyles.tooltipBoldText}>Amber:</span> Somebody else is in the
                process of claiming it now
              </div>
              <div>
                <span className={customStyles.tooltipBoldText}>Red:</span> Already claimed
              </div>
            </>
          }
        />
        <Benefits />
      </div>
      <HaveASay />
      <footer className={customStyles.footer}>
        <span>
          powered by{' '}
          <Link
            href="https://www.factorydao.xyz/"
            target="_blank"
            className={customStyles.footerLink}
          >
            FactoryDAO
          </Link>
        </span>
      </footer>
    </>
  );
};

export default MynaPage;
