import Header from 'components/Header/Header.component';
import HowItWorks from 'components/ImagineDaos/HowItWorks';
import Track from 'components/ImagineDaos/Track';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import imagineDaosLogo from 'images/imagineDaos/IMGDAOS_logo.svg';
import plus1Icon from 'images/imagineDaos/plus1.svg';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Image from 'next/image';
import styles from './ImagineDaos.module.scss';

const discordLink = 'http://discord.gg/factorydao';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#4DFF64'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const ImagineDaos = ({ groupKey }) => {
  const afterMintText =
    groupKey === 'imaginedaos-based' ? 'continue to the voting realm' : 'join the gated community';

  const afterMintLink =
    groupKey === 'imaginedaos-based'
      ? 'https://influence.factorydao.org/factorydaogovernors'
      : 'https://influence.factorydao.org/factorydaogovernors';

  return (
    <main className={styles.main}>
      <Header websiteLink="https://www.factorydao.xyz/" showDiscordLink={false} showTelegramLink />
      <Image
        className={styles.logo}
        src={imagineDaosLogo}
        width={316}
        height={239}
        alt="fdao dao logo"
      />

      <h1 className={styles.title}>It&apos;s time to ImagineDAOs</h1>
      <div className={styles.subtitle}>
        <div>
          The first-ever decentralised competition to innovate and redefine DAOs. Ready to transform
          your ideas into reality? Mint your SBT to join the competition. You can win money.
        </div>

        <br />
        <br />

        <div className={styles.second}>
          You can build reputation.You can earn trust. You can create a DAO.
        </div>
        <Image
          className={styles.plusIcon}
          src={plus1Icon}
          height={40}
          width={40}
          alt="image plus color 1"
        />
      </div>

      <MintingController
        groupKey={groupKey}
        isProgressBarVisible={false}
        walletInputLabel="Paste address or click connect wallet"
        customStyles={{ ...styles, highlightIndicators }}
        customTexts={{
          successTitle: 'Welcome to ImagineDAOs',
          successMessage: `ID #%TOKEN_ID% now belongs to you`,
          numberInputLabel: 'Enter a number or click the arrows for the next available ',
          placeholderText: 'Pick a number'
        }}
        discordLink={discordLink}
        noShift
        MintingButtonComponent={(props) => (
          <DefaultMintingButton
            {...props}
            actionButtonAfterMintVisible
            actionButtonAfterMintText={afterMintText}
            discordLink={afterMintLink}
            customStyles={{
              button: styles.button,
              actionButton: styles.actionButton
            }}
          />
        )}
      />
      <HowItWorks />
      <div className={styles.trackBoxes}>
        <Track
          style={{
            background: 'linear-gradient(238deg, #56F28C 3.7%, #FA399F 52.49%, #FC8891 98.65%)'
          }}
          title="based"
          subtitle="The Free Track"
          who="Individuals who are new to the DAO space or want to participate without a financial commitment."
          what={
            <ul>
              <li>
                A unique Soulbound Token (SBT) grants you the right to submit your DAO idea and
                partake in the voting.
              </li>
              <li>Community support.</li>
              <li>Learning materials on how to build DAOs</li>
              <li>A chance to win prizes and glory</li>
            </ul>
          }
          benefits={
            <ul>
              <li>Invaluable experience in DAO conceptualization and proposal submission.</li>
              <li>See how your ideas stack up against others</li>
              <li>Receive feedback from the wider community.</li>
              <li>Refine your understanding of what makes a DAO successful.</li>
            </ul>
          }
          prizes={
            <ul>
              <li>1st place minimum $500</li>
              <li>2nd, 3rd, 4th places share a minimum of $500 based on the vote split</li>
              <li>
                Potential to win more! Sponsors can send tokens to the prize pot throughout the
                competition and contribute to the total rewards.
              </li>
            </ul>
          }
        />
        <Track
          style={{
            background:
              'linear-gradient(62deg, #7C11A0 4.39%, #AB2DA2 41.04%, #B98EDB 76.15%, #7A9BF7 99.54%)'
          }}
          title="based"
          subtitle="The Free Track"
          who="For those who are serious about developing DAO ideas and are willing to invest in gaining direct support & feedback from experts."
          what={
            <ul>
              <li>
                A unique NFT for the price of 0.1 ETH provides you not only with the right to submit
                your DAO idea but also access to exclusive resources.
              </li>
              <li>
                Your NFT is transferable, meaning you can sell your submission rights or your
                submission itself with the potential winnings.
              </li>
              <li>Direct feedback and support from FactoryDAO team.</li>
              <li>
                Exclusive access to an accelerated DAO curriculum crafted by Dr. Nick, a renowned
                expert in the field of decentralised governance.
              </li>
            </ul>
          }
          benefits={
            <ul>
              <li>Elevated competition experience with structured support</li>
              <li>Deeply explore the potential of your ideas through direct mentorship</li>
              <li>Access to a gated group for 1 on 1 collaboration and networking.</li>
              <li>Access to expert judges&apos; feedback and support.</li>
              <li>Learn, grow and network in the DAO ecosystem.</li>
            </ul>
          }
          prizes={
            <ul>
              <li>
                Max prize pot of 10 ETH (determined by the number of entries, 100 spots @0.1 ETH
                each).
              </li>
              <li>
                The number of runners up will be decided by the Factory21 Governors, who will make
                key decisions
              </li>
              <li>
                20% of the pot will go to FactoryDAO public treasury for bounties and community
                growth.
              </li>
              <li>
                Potential to win more! Sponsors can send tokens to the prize pot throughout the
                competition and contribute to the total rewards.
              </li>
            </ul>
          }
        />
      </div>
      <PoweredByDao showDiscord={false} showTelegram />
    </main>
  );
};

export default ImagineDaos;
