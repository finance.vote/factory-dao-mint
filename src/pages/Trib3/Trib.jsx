import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import TribFabricLeft from 'components/Trib/FabricLeft/TribFabricLeft';
import TribFabricRight from 'components/Trib/FabricRight/TribFabricRight';
import TribHeader from 'components/Trib/Header/TribHeader';
import TribInformation from 'components/Trib/Information/TribInformation';
import TribIntroduce from 'components/Trib/Introduce/TribIntroduce';
import TribLastFabric from 'components/Trib/LastFabric/TribLastFabric';
import TribLogo from 'components/Trib/Logo/TribLogo';
import TribRoadmap from 'components/Trib/Roadmap/TribRoadmap';
import TribSeparator from 'components/Trib/Separator/TribSeparator';
import TribSocials, { TRIB3_CONFIG } from 'components/Trib/Social/TribSocials';
import TribVideo from 'components/Trib/Video/TribVideo';
import sliderIcon from 'images/trib3/SliderIconNew.png';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import styles from './Trib.module.scss';

const handleStyle = {
  backgroundColor: '#fff',
  backgroundImage: `url(${sliderIcon.src})`,
  height: '23px',
  width: '23px',
  border: '0',
  top: '0px',
  opacity: '1',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat'
};

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#4DFF64'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const TribPage = () => {
  return (
    <div className={styles.page}>
      <TribHeader />
      <TribLogo />
      <TribIntroduce />
      <TribFabricLeft />
      <TribFabricRight />
      <TribSocials />
      <MintingController
        groupKey="trib3"
        customStyles={{ ...styles, handleStyle, highlightIndicators }}
        customTexts={{
          successTitle: `Welcome to Trib3`,
          successMessage: `Ticket #%TOKEN_ID% now belongs to you`
        }}
        discordLink={TRIB3_CONFIG.DISCORD_LINK}
        MintingButtonComponent={(props) => (
          <DefaultMintingButton
            {...props}
            customStyles={{
              button: styles.button,
              actionButton: styles.actionButton
            }}
          />
        )}
      />
      <TribVideo />
      <TribInformation />
      <TribSeparator />
      <TribRoadmap />
      <TribLastFabric />
      <PoweredByDao />
    </div>
  );
};

export default TribPage;
