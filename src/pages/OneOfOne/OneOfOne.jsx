import Header from 'components/Header/Header.component';
import BidForm from 'components/OneOfOne/BidForm/BidForm';
import ChartPage from 'components/OneOfOne/Chart/ChartPage';
import Footer from 'components/OneOfOne/Footer/Footer';
import NFTAuctionFactory from 'contracts/NFTAuctionFactory.json';
import { ethInstance } from 'evm-chain-scripts';
import { generateEventsSubscription } from 'helpers/eventsSubscriber';
import {
  useAllAuctionEvents,
  useBidEvents,
  useCurrentAuctionId,
  useCurrentPrice,
  useLastAuctionBid,
  useNumSamplesBeforeSale,
  useSecondsPerSample,
  useStructNftContract,
  useStructTokenForSale,
  useUsdPriceOfEth
} from 'queries/oneOfOne';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import styles from './OneOfOne.module.scss';

const chainId = process.env.NEXT_PUBLIC_DEFAULT_CHAIN_ID || ethInstance.getChainId();

const OneOfOnePage = () => {
  const [bidEvents, setBidEvents] = useState();
  const [isNftSold, setIsNftSold] = useState(false);
  const { data: usdPriceOfEth } = useUsdPriceOfEth();
  const { data: secondsPerSample, refetch: refetchSecondsPerSample } = useSecondsPerSample();
  const { data: numSamplesBeforeSale, refetch: refetchNumSamplesBeforeSale } =
    useNumSamplesBeforeSale();

  const onSold = (isSold) => {
    setIsNftSold(isSold);
  };

  const { data: allEvents, isLoading: isLoadingBidOccurred } = useAllAuctionEvents(
    usdPriceOfEth,
    bidEvents,
    secondsPerSample,
    numSamplesBeforeSale,
    onSold,
    isNftSold
  );
  const { data: currentPrice, refetch: refetchCurrentPrice } = useCurrentPrice(
    secondsPerSample,
    isNftSold
  );
  const { data: lastAuctionBid, refetch: refetchLastAuctionBid } = useLastAuctionBid();
  const { data: currentAuctionId, refetch: refetchCurrentAuctionId } = useCurrentAuctionId();
  const { refetch: refetchTokenForSale } = useStructTokenForSale();
  const { refetch: refetchStructNftContract } = useStructNftContract();
  const { data: bidEventsData, isLoading: isLoadingBidEvents } = useBidEvents(
    currentAuctionId,
    chainId
  );

  const onAddBidCallback = async (event) => {
    await Promise.all([refetchCurrentPrice(), refetchLastAuctionBid()]);
    setBidEvents((events) => {
      const newEvents = [...events, event.data];
      return newEvents;
    });
  };

  const addAuctionCallback = async () => {
    setIsNftSold(false);
    await Promise.all([
      refetchCurrentAuctionId(),
      refetchTokenForSale(),
      refetchStructNftContract(),
      refetchCurrentPrice(),
      refetchNumSamplesBeforeSale(),
      refetchSecondsPerSample(),
      refetchLastAuctionBid()
    ]);
  };

  useEffect(() => {
    try {
      const contractAddress = NFTAuctionFactory.networks[chainId].address;
      generateEventsSubscription('BidOccurred', chainId, contractAddress, onAddBidCallback);
      generateEventsSubscription('AuctionCreated', chainId, contractAddress, addAuctionCallback);
    } catch (error) {
      toast.warn('Unexpected error occurred');
    }
  }, []);

  useEffect(() => {
    if (!bidEventsData) return;
    setBidEvents(bidEventsData);
  }, [bidEventsData?.length]);

  return (
    <>
      <Header showLinks={false} />
      <div className={styles.oneOfOnePage}>
        <div className={styles.container}>
          <div className={styles.firstPart}>
            <p className={styles.annotation}> 1 of 1 auction</p>
            <h1 className={styles.title}>
              finance.vote <br /> dao nft ids
            </h1>
            <div className={styles.description}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
              exercitation ullamco laboris nisi
            </div>
            <button className={styles.howTo}> how to participate</button>
            <ChartPage
              isChartLoading={isLoadingBidEvents || isLoadingBidOccurred}
              allEvents={allEvents}
            />
          </div>
          <BidForm
            usdPriceOfEth={usdPriceOfEth}
            currentPrice={currentPrice}
            lastAuctionBid={lastAuctionBid}
            chainId={chainId}
            isNftSold={isNftSold}
          />
        </div>
      </div>
      <Footer />
    </>
  );
};

export default OneOfOnePage;
