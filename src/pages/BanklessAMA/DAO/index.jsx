import BanklessDaoPreludium from 'components/BanklessDao/Preludium';
import Header from 'components/Header/Header.component';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import banklessDaoLogo from 'images/banklessAma/bankless-dao-ama.svg';
import sliderIcon from 'images/banklessAma/sliderIcon.svg';
import discordIcon from 'images/discord.svg';
import globeIcon from 'images/globe.svg';
import twitterIcon from 'images/twitter.svg';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Image from 'next/image';
import HowItWorks from '../../../components/BanklessDao/HowItWorks';
import styles from './BanklessDAO.module.scss';

const CONTENT = [
  { text: 'Connect your wallet' },
  { text: 'Mint your unique voting non-transferable token.' },
  {
    text: `Navigate to the voting realm: <a href="https://influence.factorydao.org/banklessdaoama" style="color: #ffff">influence.factorydao.org/BanklessDAOAMA</a>`
  },
  {
    text: 'Submit your questions.'
  },
  {
    text: 'Share your question on twitter.'
  },
  {
    text: 'Up and down vote other questions submitted.'
  },
  {
    text: 'Once the submission period closes the top 10 questions will be put up for a quadratic vote.'
  },
  { text: 'Top 3 questions will be asked in the AMA.' }
];

const discordLink = 'https://discord.gg/bankless';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#4DFF64'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const CustomLinks = () => {
  return (
    <ul className={styles.navlinks}>
      <li className={styles.linkContainer}>
        <a
          className={styles.link}
          href="https://twitter.com/banklessDAO"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={twitterIcon} alt="Twitter" />
        </a>
      </li>
      <li className={styles.linkContainer}>
        <a
          className={styles.link}
          href="https://www.bankless.community/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={globeIcon} alt="Website" />
        </a>
      </li>
      <li className={styles.linkContainer}>
        <a className={styles.link} href={discordLink} target="_blank" rel="noopener noreferrer">
          <Image src={discordIcon} alt="discord" />
        </a>
      </li>
    </ul>
  );
};
const BanklessDaoBanner = () => {
  return (
    <div className={styles.BanklessDaoBanner}>
      <Image src={banklessDaoLogo} alt="Bankless DAO AMA" />
    </div>
  );
};

const BanklessAMADao = ({ groupKey }) => {
  return (
    <main className={styles.main}>
      <BanklessDaoBanner />
      <Header
        twitterLink="https://twitter.com/banklessDAO"
        websiteLink="https://www.bankless.community/"
        discordLink={discordLink}
        customLinks={<CustomLinks />}
      />
      <BanklessDaoPreludium chainName="on Optimism" />
      <MintingController
        groupKey={groupKey}
        customStyles={{ ...styles, highlightIndicators }}
        customTexts={{
          successTitle: `Welcome to Bankless DAO`,
          successMessage: `#%TOKEN_ID% now belongs to you`
        }}
        discordLink={discordLink}
        sliderIcon={sliderIcon}
        noShift
        MintingButtonComponent={(props) => (
          <DefaultMintingButton
            {...props}
            actionButtonAfterMintText="to the voting realm"
            discordLink="https://influence.factorydao.org/banklessdaoama"
            customStyles={{
              button: styles.button,
              actionButton: styles.actionButton
            }}
          />
        )}
      />
      <HowItWorks content={CONTENT} />
      <PoweredByDao />
    </main>
  );
};

export default BanklessAMADao;
