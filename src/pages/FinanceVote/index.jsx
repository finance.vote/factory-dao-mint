import Header from 'components/Header/Header.component';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import financevoteLogo from 'images/fvt/financevote_light.svg';
import MintingController from 'minting-components/components/MintingController';
import SequentialPriceComponent from 'minting-components/components/SequentialPriceComponent/SequentialPriceComponent';
// import sliderIcon from '../../../images/fvt/financevote_icon.svg';
import classNames from 'classnames';
import sliderIcon from 'images/fvt/financevote_icon.svg';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintTable from 'minting-components/components/MintTable/MintTable';
import { PRICE_TYPES } from 'minting-components/helpers';
import Image from 'next/image';
import React, { memo, useState } from 'react';
import AlphaSection from './AlphaSection/AlphaSection';
import styles from './FinanceVote.module.scss';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#4DFF64'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const DISCORD_LINK = 'https://discord.com/invite/wpm7XaSh';
const PLAY_NOW_LINK = 'https://finance.vote/#/vote';

const GROUP_KEY = 'financedotvote';

const FinanceVote = () => {
  const [globalChoosenSeries, setGlobalChoosenSeries] = useState(null);
  const isSpeedBumpPrice = globalChoosenSeries?.priceType === PRICE_TYPES.SPEEDBUMP;
  return (
    <>
      <Header
        twitterLink="https://twitter.com/financedotvote"
        websiteLink="https://www.finance.vote/"
        discordLink={DISCORD_LINK}
      />
      <div className={styles.container}>
        <div className={styles.main}>
          <div className={styles.contentWrapper}>
            <Image
              className={styles.financevoteLogo}
              src={financevoteLogo}
              alt="Finance.Vote Logo"
            />
            <div className={styles.description}>
              <div className={styles.title}>A soulbound prediction market on Polygon</div>
              <span>
                finance.vote is a predict-to-earn game, where you can earn tokens for making
                accurate predictions about the market. Build reputation on your SBT and prove your
                trading ability.
              </span>
            </div>
            <div className={styles.playNowContainer}>
              <a
                href={PLAY_NOW_LINK}
                rel="noreferer noopener noreferrer"
                className={classNames(styles.actionButton, styles.playNow)}
                target="_blank"
              >
                <span>PLAY NOW</span>
              </a>
            </div>
            <MintingController
              groupKey={GROUP_KEY}
              customStyles={{ ...styles, highlightIndicators }}
              customTexts={{
                successTitle: `Welcome to FVT-SBT`,
                successMessage: `FVT-SBT #%TOKEN_ID% now belongs to you`
              }}
              discordLink={DISCORD_LINK}
              playNowLink={PLAY_NOW_LINK}
              sliderIcon={sliderIcon}
              prefillOnLoad={true}
              SequentialMintComponent={
                isSpeedBumpPrice
                  ? (props) => (
                      <SequentialPriceComponent
                        isDecayPriceVisible={true}
                        {...props}
                        currencySymbol="MATIC"
                        tokenSymbol="FVSBT"
                      />
                    )
                  : () => null
              }
              setGlobalChoosenSeries={setGlobalChoosenSeries}
              MintingButtonComponent={(props) => (
                <DefaultMintingButton
                  {...props}
                  customStyles={{
                    button: styles.button,
                    actionButton: styles.actionButton
                  }}
                />
              )}
            />
          </div>
          {isSpeedBumpPrice ? <MintTable groupKey={GROUP_KEY} /> : null}
          <AlphaSection />
          <PoweredByDao />
        </div>
      </div>
    </>
  );
};

export default memo(FinanceVote);
