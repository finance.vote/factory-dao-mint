import classNames from 'classnames';
import alphaIcon1 from 'images/community_icon.svg';
import alphaIcon2 from 'images/markets_icon.svg';
import alphaIcon3 from 'images/whitelist_icon.svg';
import Image from 'next/image';
import Link from 'next/link';
import styles from './AlphaSection.module.scss';

const AlphaConfig = [
  {
    icon: alphaIcon1,
    title: 'Gain Access to the Game',
    text: 'Only holders of a FVSBT will be able to play the game and have the opportunity of winning tokens. Pay the entrance fee and predict to win'
  },
  {
    icon: alphaIcon2,
    title: 'Earn $FVT for your predictions',
    text: 'FVT Markets dApp will be launching on Polygon soon, participate in the low fee prediction game to earn tokens.',
    additionalText:
      'You will be presented with a list of tokens, use our quadratic voting system to hedge your bets across which assets you think will perform the most in the next 24 hour market window'
  },
  {
    icon: alphaIcon3,
    title: 'Earn Reputation',
    text: 'Your SBTs store your voting history and our dataDAO will produce reports that show how accurate your predictions have been over time. If the DAO makes accurate predictions everyone wins.'
  }
];

const AlphaItems = () => {
  return AlphaConfig.map((alpha, index) => {
    return (
      <div key={index} className={styles.alphaItem}>
        <Image src={alpha.icon} alt={`alpha_${index}`} />
        <span className={styles.itemTitle}>{alpha.title}</span>
        <p>{alpha.text}</p>
        <span className={styles.additionalText}>{alpha.additionalText}</span>
      </div>
    );
  });
};

const AlphaSection = () => {
  return (
    <div className={styles.sectionWrapper}>
      <div className={styles.sectionContainer}>
        <div className={styles.sectionTitle}>
          <span className={styles.title}>A Soulbound Access Token</span>
          <span className={styles.subTitle}>
            The FVT soulbound token is a non-transferable NFT that will build your reputation,
            voting history and token earnings. It is your passport to the FVT predict-to-earn
            ecosystem.
          </span>
        </div>
        <AlphaItems />
        <div className={styles.lowestSection}>
          <div className={styles.informationBox}>
            <span className={styles.infoTitle}>Tune your DAO through NFT ID Governance</span>
            <span className={styles.infoDescription}>
              Want to go a step further an govern the game. Mint a transferable FVT ID governance
              NFT on the Ethereum blockchain and decide the fate of the DAO.
              <br />
              <br />
              <Link href="/fvt">
                <button
                  className={classNames(styles.mintButton, 'mint-identity-button')}
                  type="submit"
                >
                  Mint a Governance ID
                </button>
              </Link>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AlphaSection;
