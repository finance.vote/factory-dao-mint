import EthBerlinPreludium from 'components/EthBerlin/Preludium';
import Header from 'components/Header/Header.component';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import matrixIcon from 'images/ethberlin/matrix-logo-white.svg';
import sliderIcon from 'images/ethberlin/sliderIcon.svg';
import globeIcon from 'images/globe.svg';
import twitterIcon from 'images/twitter.svg';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Image from 'next/image';
import Diamond from '../../components/EthBerlin/Diamond';
import HowItWorks from '../../components/EthBerlin/HowItWorks';
import styles from './EthBerlin.module.scss';

const discordLink = '';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#4DFF64'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const CustomLinks = () => {
  return (
    <ul className={styles.navlinks}>
      <li className={styles.linkContainer}>
        <a
          className={styles.link}
          href={'https://twitter.com/ETHBerlin'}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={twitterIcon} alt="Twitter" />
        </a>
      </li>
      <li className={styles.linkContainer}>
        <a
          className={styles.link}
          href={'https://ethberlin.ooo/'}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={globeIcon} alt="Website" />
        </a>
      </li>
      <li className={styles.linkContainer}>
        <a
          className={styles.link}
          href={'https://matrix.to/#/%23ethberlin:matrix.org'}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={matrixIcon} alt="matrix" />
        </a>
      </li>
    </ul>
  );
};

const customWalletProviders = [1, 2];

const EthBerlin = ({ groupKey }) => {
  return (
    <main className={styles.main}>
      <Header
        twitterLink="https://twitter.com/financedotvote"
        websiteLink="https://www.finance.vote/"
        discordLink={discordLink}
        customLinks={<CustomLinks />}
      />
      <EthBerlinPreludium />
      <MintingController
        groupKey={groupKey}
        customStyles={{ ...styles, highlightIndicators }}
        customTexts={{
          successTitle: `Welcome to ETH Berlin`,
          successMessage: `ETH Berlin #%TOKEN_ID% now belongs to you`
        }}
        discordLink={'https://vote.ethberlin.ooo'}
        sliderIcon={sliderIcon}
        customWalletProviders={customWalletProviders}
        MintingButtonComponent={(props) => (
          <DefaultMintingButton
            {...props}
            actionButtonAfterMintText="to the voting realm"
            customStyles={{
              button: styles.button,
              actionButton: styles.actionButton
            }}
          />
        )}
      />
      <HowItWorks />
      <Diamond />
      <PoweredByDao />
    </main>
  );
};

export default EthBerlin;
