import BenefitsSection from 'components/BenefitsSection/BenefitsSection';
import GoalsSection from 'components/GoalsSection/GoalsSection';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import ToneOfVoiceSection from 'components/ToneOfVoiceItems/ToneOfVoiceSection';
import Container from 'components/Ui/Container/Container.component';
import sliderIcon from 'images/sovryn/slider_icon.svg';
import swLogo from 'images/sovryn/SW_Logo.svg';
import DefaultFooter from 'minting-components/components/DefaultFooter';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Image from 'next/image';
import React from 'react';
import styles from './Sovryn.module.scss';

// const CONFIG = {
//   DEFAULT_CHAIN_ID: 30,
//   GROUP: 'sovryn',
//   API_URL: 'https://sovryn.influencebackend.xyz',
//   API_WS_URL: 'wss://sovryn.influencebackend.xyz',
//   NFT_ADDRESS: '0x1999b48df6Da3fa7810b3cE3fCE3b1da4E819888'
// };

const Sovryn = () => {
  // const unboxingVideoStream = 'a058cff875f7eefc642671fb391d5374';

  const customStyles = {
    ...styles,
    container: styles.mintingContainer,
    // sliderInfo: styles.sliderInfo,
    handleStyle: {
      backgroundColor: '#fff',
      background: `url(${sliderIcon.src}) no-repeat`,
      height: '34px',
      width: '36px',
      border: 0,
      top: '-10px',
      borderRadius: 0,
      opacity: '1'
    }
  };

  const customTexts = {
    mobileHeader: 'SW',
    success: 'SW'
  };

  return (
    <main className={styles.main}>
      <Container className={styles.container}>
        <div className={styles.main}>
          <div className={styles.contentWrapper}>
            <Image className={styles.fdaoLogo} src={swLogo} alt="Sovryn Whispers Logo" />
            <p className={styles.boldDescription}>
              Sovryn and Blockchain Whispers <br className={styles.mobileBr} />
              have joined forces
              <br /> to create <br className={styles.mobileBr} /> SOVRYN WHISPERS
            </p>
            <p className={styles.description}>
              Our mission is to increase engagement numbers and user
              <br /> activity on Sovryn&apos;s
              <br className={styles.mobileBr} /> social media and forum platform
            </p>
            <MintingController
              groupKey="sovryn"
              customStyles={customStyles}
              customTexts={customTexts}
              FooterComponent={(props) => (
                <DefaultFooter
                  {...props}
                  customStyles={{
                    linkClassName: styles.footerButton
                  }}
                />
              )}
              MintingButtonComponent={(props) => (
                <DefaultMintingButton
                  {...props}
                  customStyles={{
                    button: styles.button,
                    eligibilityError: styles.eligibilityError
                  }}
                />
              )}
            />
          </div>
          <GoalsSection />
          <ToneOfVoiceSection />
          <BenefitsSection />
          <PoweredByDao />
        </div>
      </Container>
    </main>
  );
};

export default Sovryn;
