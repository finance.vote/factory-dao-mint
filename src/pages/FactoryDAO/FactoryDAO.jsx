import Header from 'components/Header/Header.component';
import Container from 'components/Ui/Container/Container.component';
import fdaoLogo from 'images/fdaoLogo.png';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Image from 'next/image';
import React from 'react';
import styles from './FactoryDAO.module.scss';

const FactoryDAO = ({ groupKey = 'factorydao_test' }) => {
  // const unboxingVideoStream = '6e8685018bb835f3b28deedd392c8129';

  const customStyles = {
    randomize: styles.randomize,
    walletInput: styles.walletInput,
    upperContainer: styles.upperContainer,
    middleContainer: styles.middleContainer,
    templateImage: styles.templateImage,
    details: styles.details,
    button: styles.button,
    lowerContainer: styles.lowerContainer,
    successDescription: styles.successDescription,
    successTitle: styles.successTitle,
    inputData: styles.inputData,
    connect: styles.connect,
    buttonWrapper: styles.buttonWrapper,
    relativeWrapper: styles.relativeWrapper,
    slider: styles.slider,
    sliderInfo: styles.sliderInfo,
    status: styles.status,
    eligibilityError: styles.eligibilityError,
    notEligibleInfo: styles.notEligibleInfo,
    handleStyle: {
      backgroundColor: '#fff',
      height: '20px',
      width: '20px',
      border: '0',
      top: '2px',
      borderRadius: '50%',
      opacity: '1'
    },
    percentage: styles.percentage,
    video: styles.video,
    tokenVideo: styles.tokenVideo
  };

  const customTexts = {
    mobileHeader: 'FAD',
    success: 'FAD',
    successTitle: `Welcome to FactoryDAO`,
    successMessage: `ID #%TOKEN_ID% now belongs to you`
  };

  return (
    <>
      <Header />
      <main className={styles.main}>
        <Container className={styles.container}>
          <div className={styles.main}>
            <div className={styles.contentWrapper}>
              <Image className={styles.fdaoLogo} src={fdaoLogo} alt="FactoryDAO Mint Logo" />
              <p className={styles.description}>
                FactoryDAO is the place to build, launch and manage your DAO.
                <br />
                The decentralized home for modular DAO infrastructure.
              </p>
              <MintingController
                groupKey={groupKey}
                customStyles={customStyles}
                customTexts={customTexts}
                discordLink={'https://influence.factorydao.org/factorydaodemo'}
                MintingButtonComponent={(props) => (
                  <DefaultMintingButton
                    {...props}
                    actionButtonAfterMintText="continue to the voting realm"
                    customStyles={{
                      actionButton: styles.actionButton,
                      button: styles.button,
                      eligibilityError: styles.eligibilityError
                    }}
                  />
                )}
              />
            </div>
          </div>
        </Container>
      </main>
    </>
  );
};

export default FactoryDAO;
