import { useMemo } from 'react';

export const useMediaQuery = (queriesConfigs) => {
  const queries = {};
  const bindQueries = () => {
    for (const query of queriesConfigs) {
      const handleQueryChange = () => {
        if (queries[query.maxWidth].matches) {
          query.onTrue?.();
        } else {
          query.onFalse?.();
        }
      };
      if (queries[query.maxWidth]?.removeListener) {
        queries[query.maxWidth].removeListener(handleQueryChange);
      }
      queries[query.maxWidth]?.removeEventListener('change', handleQueryChange);
      queries[query.maxWidth] = window.matchMedia(`(max-width: ${query.maxWidth}px)`);
      if (queries[query.maxWidth].addListener) {
        queries[query.maxWidth].addListener(handleQueryChange);
      }
      queries[query.maxWidth].addEventListener &&
        queries[query.maxWidth].addEventListener('change', handleQueryChange);
      handleQueryChange();
    }
  };

  useMemo(bindQueries, []);
};
