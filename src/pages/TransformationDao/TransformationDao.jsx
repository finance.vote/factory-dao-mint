import Header from 'components/Header/Header.component';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import Introduce from 'components/TransformationDao/Introduce/Introduce';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import customStyles from './TransformationDao.module.scss';

const continueToVoting = 'https://influence.factorydao.org/transformationdao';

const handleStyle = {
  backgroundColor: '#71CA9A',
  height: '23px',
  width: '23px',
  border: '0',
  top: '0px',
  opacity: '1',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat'
};

const TransformationDao = ({ groupKey }) => {
  return (
    <>
      <div className={customStyles.transformationDao}>
        <Header customLinks={<div />} />
        <Introduce />
        <MintingController
          groupKey={groupKey}
          customStyles={{ ...customStyles, handleStyle }}
          prefillOnLoad
          customTexts={{
            mobileHeader: 'TRANSFORMATION DAO',
            successTitle: 'Welcome to Transformation DAO',
            successMessage: `ID #%TOKEN_ID% now belongs to you`,
            numberInputLabel:
              'Click mint now, enter a different number or click the random number arrows',
            placeholderText: 'Pick a number',
            buttonText: {
              available: 'MINT NOW',
              unavailable: 'MINT NOW'
            }
          }}
          MintingButtonComponent={(props) => (
            <DefaultMintingButton
              {...props}
              discordLink={continueToVoting}
              actionButtonAfterMintText="continue to the voting realm"
              customStyles={{
                button: customStyles.button,
                actionButton: customStyles.actionButton
              }}
            />
          )}
          tooltipContentClass={customStyles.tooltipContent}
          tooltipContent={
            <>
              A random token number has been selected for you, just click “MINT NOW” if you are
              happy with it.
              <br />
              <br />
              If you would like to pick another random number click the green “random” arrows
              <br />
              <br />
              If you would like a specific number in the series, try typing it in. It may be taken!
              The traffic lights on the right will indicate if it’s:
              <div>
                <span className={customStyles.tooltipBoldText}>Green:</span> Available
              </div>
              <div>
                <span className={customStyles.tooltipBoldText}>Amber:</span> Somebody else is in the
                process of claiming it now
              </div>
              <div>
                <span className={customStyles.tooltipBoldText}>Red:</span> Already claimed
              </div>
            </>
          }
        />
      </div>
      <PoweredByDao hideLinks />
    </>
  );
};

export default TransformationDao;
