import EthZurichHeader from 'components/EthZurich/Header/Header';
import HowItWorks from 'components/EthZurich/HowItWorks';
import Introduction from 'components/EthZurich/Introduction/Introduction';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import sliderIcon from 'images/ethZurich/sliderIcon.svg';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import customStyles from './EthZurich.module.scss';

const continueToVoting = 'https://influence.factorydao.org/ethereumzurich';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 15px #A3FF5D',
    status: '#00F220',
    fontFamily: 'Poppins'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const EthZurichPage = ({ groupKey }) => {
  return (
    <div className={customStyles.mainContainer}>
      <EthZurichHeader
        websiteLink="https://ethereumzuri.ch/"
        twitterLink="https://twitter.com/EthereumZurich"
        telegramLink="https://t.me/ethereumzurich"
      />
      <Introduction />
      <div className={customStyles.purpleMintContainer} />
      <div className={customStyles.zurichMinting}>
        <MintingController
          groupKey={groupKey}
          customStyles={{ ...customStyles, highlightIndicators }}
          sliderIcon={sliderIcon}
          prefillOnLoad
          walletInputLabel="Paste address or click connect wallet"
          customTexts={{
            successTitle: 'Welcome to EthereumzÜri.ch',
            successMessage: `ID #%TOKEN_ID% now belongs to you`,
            numberInputLabel: 'Enter a number or click the arrows',
            placeholderText: 'Enter a number',
            buttonText: {
              available: 'MINT NOW',
              unavailable: 'MINT NOW'
            }
          }}
          MintingButtonComponent={(props) => (
            <DefaultMintingButton
              {...props}
              discordLink={continueToVoting}
              actionButtonAfterMintText="continue to voting realm"
              customStyles={{
                button: customStyles.button,
                actionButton: !props.isEligibleToMint && customStyles.button
              }}
            />
          )}
          tooltipContent={
            <>
              This is the number of the NFT in the series, not the amount of NFTs you are minting.
              <br />
              <br />
              NFTs are minted one at a time.
              <br />
              <br />
              The next available NFT in the series has been selected for you, just click “MINT NOW”
              if you are happy with it.
              <br />
              <br />
              If you would like to pick another random number click the green “random” arrows
              <br />
              <br />
              If you would like a specific number in the series, try typing it in. It may be taken!
              The traffic lights will indicate if it’s:
              <div>
                <span className={customStyles.tooltipBoldText}>Green:</span> Available
              </div>
              <div>
                <span className={customStyles.tooltipBoldText}>Amber:</span> Somebody else is in the
                process of claiming it now
              </div>
              <div>
                <span className={customStyles.tooltipBoldText}>Red:</span> Already claimed
              </div>
            </>
          }
        />
      </div>
      <HowItWorks />
      <PoweredByDao />
    </div>
  );
};

export default EthZurichPage;
