import Benefits from 'components/Myna/Benefits/Benefits';
import MynaHeader from 'components/Myna/Header/MynaHeader';
import Services from 'components/Myna/Services/Services';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import sliderIcon from 'images/myna/slider_icon.svg';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Introduce from '../../components/Myna/Introduce/Introduce';
import customStyles from './Myna.module.scss';

const DISCORD_LINK = 'https://discord.gg/serCyEKwZP';

const MynaPage = () => {
  return (
    <div className={customStyles.mynaPage}>
      <MynaHeader
        websiteLink={'https://www.mynaaccountants.co/'}
        discordLink={DISCORD_LINK}
        twitterLink={'https://twitter.com/mynaaccountants'}
      />
      <Introduce />
      <MintingController
        groupKey="myna"
        customStyles={customStyles}
        sliderIcon={sliderIcon}
        customTexts={{
          mobileHeader: 'Myna',
          symbol: 'MYNA',
          successTitle: `welcome to the club`,
          successMessage: `You now own membership NFT #%TOKEN_ID%`
        }}
        discordLink={DISCORD_LINK}
        MintingButtonComponent={(props) => (
          <DefaultMintingButton
            {...props}
            customStyles={{
              button: customStyles.button,
              actionButton: customStyles.actionButton
            }}
          />
        )}
      />
      <Benefits />
      <Services />
      <PoweredByDao />
    </div>
  );
};

export default MynaPage;
