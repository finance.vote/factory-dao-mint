import EthBrnoPreludium from 'components/EthBrno/Preludium';
import Header from 'components/Header/Header.component';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import sliderIcon from 'images/ethBrno/sliderIcon.svg';

import CustomLinks from 'components/EthBrno/CustomLinks';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import { highlightIndicators } from 'minting-components/helpers';
import HowItWorks from '../../components/EthBrno/HowItWorks';
import styles from './EthBrno.module.scss';

const EthBrno = ({ groupKey }) => {
  return (
    <main className={styles.main}>
      <Header
        twitterLink="https://twitter.com/ethbrno"
        websiteLink="https://ethbrno.cz/"
        customLinks={<CustomLinks styles={styles} />}
      />
      <EthBrnoPreludium />
      <MintingController
        groupKey={groupKey}
        customStyles={{ ...styles, highlightIndicators }}
        customTexts={{
          successTitle: `Welcome to ETH Brno`,
          successMessage: `ID #%TOKEN_ID% now belongs to you`
        }}
        sliderIcon={sliderIcon}
        MintingButtonComponent={(props) => (
          <DefaultMintingButton
            {...props}
            isBackendMint
            actionButtonAfterMintText="continue to the voting realm"
            discordLink="https://vote.ethbrno.cz/"
            customStyles={{
              button: styles.button,
              actionButton: styles.actionButton
            }}
          />
        )}
      />
      <HowItWorks />
      <PoweredByDao />
    </main>
  );
};

export default EthBrno;
