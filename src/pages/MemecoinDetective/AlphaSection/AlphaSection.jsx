import alphaIcon1 from 'images/community_icon.svg';
import alphaIcon2 from 'images/markets_icon.svg';
import alphaIcon3 from 'images/whitelist_icon.svg';
import Image from 'next/image';
import styles from './AlphaSection.module.scss';

const AlphaConfig = [
  {
    icon: alphaIcon1,
    title: 'Gain Access to the Game',
    text: 'Only holders of a FVSBT will be able to play the game and have the opportunity of winning tokens. Pay the entrance fee and predict to win'
  },
  {
    icon: alphaIcon2,
    title: 'Earn $FVT for your predictions',
    text: 'FVT Markets dApp will be launching on Polygon soon, participate in the low fee prediction game to earn tokens.',
    additionalText:
      'You will be presented with a list of tokens, use our quadratic voting system to hedge your bets across which assets you think will perform the most in the next 24 hour market window'
  },
  {
    icon: alphaIcon3,
    title: 'Earn Reputation',
    text: 'Your SBTs store your voting history and our dataDAO will produce reports that show how accurate your predictions have been over time. If the DAO makes accurate predictions everyone wins.'
  }
];

const AlphaItems = () => {
  return AlphaConfig.map((alpha, index) => {
    return (
      <div key={index} className={styles.alphaItem}>
        <Image src={alpha.icon} alt={`alpha_${index}`} />
        <span className={styles.itemTitle}>{alpha.title}</span>
        <p>{alpha.text}</p>
        <span className={styles.additionalText}>{alpha.additionalText}</span>
      </div>
    );
  });
};

const AlphaSection = () => {
  return (
    <div className={styles.sectionWrapper}>
      <div className={styles.sectionContainer}>
        <div className={styles.sectionTitle}>
          <span className={styles.title}>A souldbound detective token</span>
          <span className={styles.subTitle}>
            The souldbound detective token is an SBT that will give you access to this special FVT
            subDAO. You will be given the opportunity to curate memecoins, learn from each other and
            experts and decide how the DAO develops.
          </span>
        </div>
      </div>
    </div>
  );
};

export default AlphaSection;
