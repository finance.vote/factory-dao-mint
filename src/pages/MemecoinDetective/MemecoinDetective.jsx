import Header from 'components/Header/Header.component';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import memeLogo from 'images/memecoin-detective/meme-logo.svg';
import MintingController from 'minting-components/components/MintingController';
import SequentialPriceComponent from 'minting-components/components/SequentialPriceComponent/SequentialPriceComponent';
// import sliderIcon from '../../../images/fvt/financevote_icon.svg';
import sliderIcon from 'images/fvt/financevote_icon.svg';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import { PRICE_TYPES } from 'minting-components/helpers';
import Image from 'next/image';
import React, { memo, useState } from 'react';
import AlphaSection from './AlphaSection/AlphaSection';
import styles from './MemecoinDetective.module.scss';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#4DFF64'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const DISCORD_LINK = 'https://discord.com/invite/wpm7XaSh';

const MemecoinDetective = ({ groupKey }) => {
  const [globalChoosenSeries, setGlobalChoosenSeries] = useState(null);
  const isSpeedBumpPrice = globalChoosenSeries?.priceType === PRICE_TYPES.SPEEDBUMP;
  return (
    <>
      <Header
        twitterLink="https://twitter.com/financedotvote"
        websiteLink="https://www.finance.vote/"
        discordLink={DISCORD_LINK}
      />
      <div className={styles.container}>
        <div className={styles.main}>
          <div className={styles.contentWrapper}>
            <Image className={styles.financevoteLogo} src={memeLogo} alt="Finance.Vote Logo" />
            <div className={styles.description}>
              <div className={styles.title}>Memecoin Detective DAO</div>
              <span>
                This is an experiment in social sentiment analysis, decentralised DD and trends
                spotting. This special FVT subDAO will aim to detect the next big meme coin. Join
                the DAO and earn tokens for participating in this exciting social experiment.
              </span>
            </div>

            <MintingController
              groupKey={groupKey}
              customStyles={{ ...styles, highlightIndicators }}
              customTexts={{
                successTitle: `Welcome to Memecoin Detective DAO`,
                successMessage: `MCDD #%TOKEN_ID% now belongs to you`
              }}
              discordLink={DISCORD_LINK}
              sliderIcon={sliderIcon}
              prefillOnLoad={true}
              SequentialMintComponent={
                isSpeedBumpPrice
                  ? (props) => (
                      <SequentialPriceComponent
                        isDecayPriceVisible={true}
                        {...props}
                        currencySymbol="MATIC"
                        tokenSymbol="FVSBT"
                      />
                    )
                  : () => null
              }
              setGlobalChoosenSeries={setGlobalChoosenSeries}
              MintingButtonComponent={(props) => (
                <DefaultMintingButton
                  {...props}
                  discordLink="https://influence.factorydao.org/memecoindetectives"
                  actionButtonAfterMintText="to the voting realm"
                  customStyles={{
                    button: styles.button,
                    actionButton: styles.actionButton
                  }}
                />
              )}
            />
          </div>
          <AlphaSection />
          <PoweredByDao />
        </div>
      </div>
    </>
  );
};

export default memo(MemecoinDetective);
