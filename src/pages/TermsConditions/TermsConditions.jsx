import Header from 'components/Header/Header.component';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import { useRouter } from 'next/router';
import styles from './TermsConditions.module.scss';

const TermsConditionsPage = () => {
  const router = useRouter();

  return (
    <div className={styles.termsConditionsPage}>
      <Header isClickable={false} showLinks={false} />
      <div className={styles.container}>
        <button onClick={() => router.back()}>Back</button>
        <section>
          <h1>Terms and Conditions</h1>
          <p>
            FactoryDAO WILL NOT BE RESPONSIBLE OR LIABLE TO YOU FOR ANY LOSS AND TAKE NO
            RESPONSIBILITY FOR, AND WILL NOT BE LIABLE TO YOU FOR, ANY USE OF NFTS, CONTENT, AND/OR
            CONTENT LINKED TO OR ASSOCIATED WITH NFTS, INCLUDING BUT NOT LIMITED TO ANY LOSSES,
            DAMAGES, OR CLAIMS.
          </p>
          <p>
            NFTS EXIST ONLY BY VIRTUE OF THE OWNERSHIP RECORD MAINTAINED IN THE ASSOCIATED
            BLOCKCHAIN (E.G., ETHEREUM NETWORK). ANY TRANSFERS OR SALES OCCUR ON THE ASSOCIATED
            BLOCKCHAIN (E.G., ETHEREUM). FACTORYDAO CANNOT EFFECT OR OTHERWISE CONTROL THE TRANSFER
            OF TITLE OR RIGHT IN ANY NFTS OR UNDERLYING OR ASSOCIATED CONTENT OR ITEMS.
          </p>
          <p>
            NO FACTORYDAO PARTY IS RESPONSIBLE OR LIABLE FOR ANY SUSTAINED LOSSES OR INJURY DUE TO
            VULNERABILITY OR ANY KIND OF FAILURE, ABNORMAL BEHAVIOR OF WALLET, BLOCKCHAINS OR ANY
            OTHER FEATURES OF THE NFTS. NO FACTORYDAO PARTY IS RESPONSIBLE FOR LOSSES OR INJURY DUE
            TO LATE REPORTS BY DEVELOPERS OR REPRESENTATIVES (OR NO REPORT AT ALL) OF ANY ISSUES
            WITH THE BLOCKCHAIN SUPPORTING THE NFTS, INCLUDING FORKS, TECHNICAL NODE ISSUES OR ANY
            OTHER ISSUES HAVING LOSSES OR INJURY AS A RESULT.
          </p>
        </section>

        <section>
          <h3>ASSUMPTION OF RISK</h3>
          <p>You accept and acknowledge:</p>
          <p>
            The value of an NFT is subjective. Prices of NFTs are subject to volatility and
            fluctuations in the price of cryptocurrency can also materially and adversely affect NFT
            prices. You acknowledge that you fully understand this subjectivity and volatility and
            that you may lose money.
          </p>
          <p>
            A lack of use or public interest in the creation and development of distributed
            ecosystems could negatively impact the development of those ecosystems and related
            applications, and could therefore also negatively impact the potential utility of NFTs.
          </p>
          <p>
            The regulatory regime governing blockchain technologies, non-fungible tokens,
            cryptocurrency, and other crypto-based items is uncertain, and new regulations or
            policies may materially adversely affect the development of the Service and the utility
            of NFTs.
            <br /> You are solely responsible for determining what, if any, taxes apply to your
            transactions. FactoryDAO is not responsible for determining the taxes that apply to your
            NFTs.
          </p>
          <p>
            You represent and warrant that you have done sufficient research before making any
            decisions to buy, obtain, transfer, or otherwise interact with any NFTs.
            <br /> We do not control the public blockchains that you are interacting with and we do
            not control certain smart contracts and protocols that may be integral to your ability
            to complete transactions on these public blockchains. Additionally, blockchain
            transactions are irreversible and FactoryDAO has no ability to reverse any transactions
            on the blockchain.
          </p>
          <p>
            There are risks associated with using Internet and blockchain based products, including,
            but not limited to, the risk associated with hardware, software, and Internet
            connections, the risk of malicious software introduction, and the risk that third
            parties may obtain unauthorized access to your third-party wallet. You accept and
            acknowledge that FactoryDAO will not be responsible for any communication failures,
            disruptions, errors, distortions or delays you may experience when using any Blockchain
            network, however caused.
          </p>
          <p>
            If you have a dispute with one or more users, you release us from claims, demands, and
            damages of every kind and nature, known and unknown, arising out of or in any way
            connected with such disputes. in entering into this release you expressly waive any
            protections (whether statutory or otherwise) that would otherwise limit the coverage of
            this release to include those claims which you may know or suspect to exist in your
            favor at the time of agreeing to this release.
          </p>
        </section>

        <section>
          <h3>LIMITATION OF LIABILITY</h3>
          <p>
            To the fullest extent permitted by law, you agree that in no event will FactoryDAO or
            its service providers be liable to you or any third party for any lost profit or any
            indirect, consequential, exemplary, incidental, special, or punitive damages arising
            from these terms or the service, or for any damages related to loss of revenue, loss of
            profits, loss of business or anticipated savings, loss of use, loss of goodwill, or loss
            of data, and whether caused by strict liability or tort (including negligence), breach
            of contract, or otherwise, or for any other claim, demand, or damages whatsoever
            resulting from or arising out of or in connection with these terms of the delivery, use,
            or performance of the service. access to, and use of, the service, products or
            third-party sites, and products are at your own discretion and risk, and you will be
            solely responsible for any damage to your computer system or mobile device or loss of
            data resulting therefrom.
          </p>
          <p>
            Notwithstanding anything to the contrary contained herein, in no event shall the maximum
            aggregate liability of factorydao arising out of or in any way related to these terms,
            the access to and use of the service, content, nfts, or any factorydao products or
            services exceed the greater of (a) $100 or (b) the amount received by factorydao for its
            service directly relating to the items that are the subject of the claim. the foregoing
            limitations will apply even if the above stated remedy fails in its essential purpose.
          </p>
          <p>
            Some jurisdictions do not allow the exclusion or limitation of incidental or
            consequential damages, so the above limitation or exclusion may not apply to you. Some
            jurisdictions also limit disclaimers or limitations of liability for personal injury
            from consumer products, so this limitation may not apply to personal injury claims.
          </p>
        </section>

        <section>
          <h3>NONCUSTODIAL</h3>
          <p>
            While FactoryDAO offers a Minting Suite for NFT Collections, it does not buy, sell, or
            ever take custody or possession of any NFT Collections. “Mint” facilitates minting of
            NFT Collections, but neither FactoryDAO nor the dApp are custodians of any NFT
            Collections. The User understands and acknowledges that the Smart Contracts do not give
            FactoryDAO custody, possession, or control of any NFT Collections at any time for the
            purpose of facilitating Transactions on the Platform. You affirm that you are aware and
            acknowledge that FactoryDAO is a non-custodial service provider. FactoryDAO is not a
            party to any agreement between any sellers, buyers, collectors, and other users.
          </p>
        </section>

        <section>
          <h3>THIRD PARTY CONTENT</h3>
          <p>
            Under no circumstances will FactoryDAO be liable in any way for any NFT Collections, Art
            Content, Non-FactoryDAO Artwork, Non-FactoryDAO Content, or any other content or
            materials of any third parties, including, but not limited to, (i) for any errors or
            omissions in any content or materials, (ii) for infringement or violation of
            intellectual property or other rights in relation to such content or materials, (iii)
            for any promises, purported promises, or commitments made by other Projects, or (iv) for
            any loss or damage of any kind incurred as a result of the use of any such content or
            materials or as a result of reliance on any such promises or commitments. You agree that
            you must evaluate, and bear all risks associated with, the use of any content and the
            purchase of any NFTs, including any reliance on the accuracy, completeness, or
            usefulness of such content.
          </p>
        </section>

        <section>
          <h3>WE DO NOT GET INVOLVED WITH DISPUTES</h3>
          <p>
            You agree that you are solely responsible for your interactions with any NFT collections
            minted on the &quot;Mint&quot; dApp, FactoryDAO will have no liability or responsibility
            with respect thereto. FactoryDAO reserves the right, but has no obligation, to become
            involved in any way with disputes between you and any other user of the dApp.
          </p>
        </section>
      </div>
      <PoweredByDao />
    </div>
  );
};

export default TermsConditionsPage;
