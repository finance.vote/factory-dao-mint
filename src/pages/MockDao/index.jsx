import Header from 'components/Header/Header.component';
import MembershipSbts from 'components/MembershipSBTS/MembershipSbts';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import SetYourDao from 'components/SetYourDao/SetYourDao';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Image from 'next/image';
import HowItWorks from '../../components/MockDao/HowItWorks';
import mockLogo from '../../images/mockDao/Frame.svg';
import styles from './MockDao.module.scss';

const discordLink = 'http://discord.gg/factorydao';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#4DFF64'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const MockDao = ({ groupKey }) => {
  return (
    <main className={styles.main}>
      <Header websiteLink="https://www.factorydao.xyz/" />
      <Image
        className={styles.mockLogo}
        src={mockLogo}
        width={238.28}
        height={254.43}
        alt="mock dao logo"
      />
      <h1 className={styles.mockDescTop}>
        MockDAO is a sandbox for DAO Builders to experiment with DAO governance.
      </h1>
      <h3 className={styles.mockDescMid}>
        MockDAO runs on FactoryDAO technology, allowing DAO Builders to launch a free SBT (Soul
        Bound Token) series. SBTs are similar to NFTs but are non-transferable, i.e. they have zero
        value on the secondary market and can not be sent to another wallet.
      </h3>
      <h2 className={styles.mockDescBot}>
        The first DAO to launch on MockDAO is HeritageDAO; an emergent approach to community
        consensus in architecture.
      </h2>

      <MintingController
        groupKey={groupKey}
        isProgressBarVisible={false}
        walletInputLabel="Paste address or click connect wallet"
        customStyles={{ ...styles, highlightIndicators }}
        customTexts={{
          successTitle: `Welcome to MockDAO`,
          successMessage: `#%TOKEN_ID% now belongs to you`
        }}
        discordLink={discordLink}
        noShift
        MintingButtonComponent={(props) => (
          <DefaultMintingButton
            {...props}
            actionButtonAfterMintVisible
            actionButtonAfterMintText="continue to the voting realm"
            discordLink="https://influence.factorydao.org/mockdao-heritagedao"
            customStyles={{
              button: styles.button,
              actionButton: styles.actionButton
            }}
          />
        )}
      />
      <HowItWorks />
      <MembershipSbts name="MockDAO" />
      <SetYourDao name="MockDAO" title="Set up your own MockDAO" />
      <PoweredByDao />
    </main>
  );
};

export default MockDao;
