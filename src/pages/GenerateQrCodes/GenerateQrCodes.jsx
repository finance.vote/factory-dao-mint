import axios from 'axios';
import classnames from 'classnames';
import TextInput from 'components/Formik/TextInput/TextInput.component';
import { ethInstance } from 'evm-chain-scripts';
import { Form, Formik } from 'formik';
import FormWrapper from 'minting-components/components/FormWrapper';
import { toast } from 'react-toastify';
import utilClasses from 'utils/scss/modules/utils.module.scss';
import * as Yup from 'yup';
import styles from './GenerateQrCodes.module.scss';

const validationSchema = Yup.object({
  qrCodeUrl: Yup.string().required('Required'),
  groupKey: Yup.string().required('Required')
});

const GenerateQrCodes = () => {
  const onSubmit = async (values) => {
    const { groupKey, qrCodeUrl } = values;
    const mintKey = groupKey;

    try {
      const currentAccount = await ethInstance.getEthAccount();
      if (!currentAccount.length) throw new Error('Connect your wallet');

      const bodyToSign = {
        qrCodeUrl,
        mintKey
      };
      const requestParam = { address: currentAccount, ...bodyToSign };
      requestParam.signature = await ethInstance.signPersonalMessage(
        JSON.stringify(bodyToSign),
        currentAccount
      );
      const response = await axios.get(`/qr-generator`, {
        params: requestParam,
        responseType: 'blob'
      });
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', `${groupKey} - ${new Date().toISOString()}.zip`);
      document.body.appendChild(link);
      link.click();

      toast.success('QR codes have been generated');
    } catch ({ error }) {
      if (error) {
        toast.error(error, {
          position: 'top-center'
        });
      }
    }
  };

  return (
    <Formik
      initialValues={{ groupKey: '', qrCodeUrl: '' }}
      onSubmit={onSubmit}
      validateOnChange
      validationSchema={validationSchema}
    >
      {({ errors, values, isValid }) => {
        return (
          <FormWrapper>
            <div className={utilClasses.container}>
              <h1 className={styles.title}>Generate QR codes for mint</h1>
              <Form>
                <div className={styles.inputContainer}>
                  <TextInput
                    name="groupKey"
                    placeholder="Input group key"
                    label="Group key"
                    className={classnames(
                      errors.groupKey && styles.inputError,
                      styles.input,
                      utilClasses.input
                    )}
                    value={values.groupKey}
                  />
                </div>
                <TextInput
                  type="string"
                  name="qrCodeUrl"
                  placeholder="Input QR code URL"
                  label="QR code URL"
                  className={classnames(
                    errors.qrCodeUrl && styles.inputError,
                    styles.input,
                    utilClasses.input
                  )}
                  value={values.qrCodeUrl}
                />
                <div
                  className={
                    (utilClasses.hollowButton, utilClasses.hollowButtonBlack, styles.submitButton)
                  }
                >
                  <button
                    type="submit"
                    disabled={!isValid}
                    className={classnames(
                      utilClasses.hollowButton,
                      utilClasses.hollowButtonBlack,
                      styles.submitButton
                    )}
                  >
                    Generate QR codes
                  </button>
                </div>
              </Form>
            </div>
          </FormWrapper>
        );
      }}
    </Formik>
  );
};

export default GenerateQrCodes;
