import AmaluEligibility from 'contracts/AmaluEligibility.json';
import FixedPriceGate from 'contracts/FixedPricePassThruGate.json';
import MerkleEligibility from 'contracts/MerkleEligibility.json';
import MerkleIdentity from 'contracts/MerkleIdentity.json';
import SpeedBumpPriceGate from 'contracts/SpeedBumpPriceGate.json';
import WhitelistEligibility from 'contracts/WhitelistEligibility.json';
import { ethInstance, fromWei, toWei } from 'evm-chain-scripts';
import { PRICE_TYPES } from 'minting-components/helpers';
import { toast } from 'react-toastify';

const priceContracts = {
  fixed: FixedPriceGate,
  speedbump: SpeedBumpPriceGate,
  flexible: FixedPriceGate
};

const contracts = {
  amalu: AmaluEligibility,
  merkle: MerkleEligibility,
  whitelist: WhitelistEligibility
};

export function getContractAbi(type) {
  return contracts[type];
}

function parseGateFieldsForContractType(contractType, gate) {
  let maxWithdrawalsAddress = 0;
  let maxWithdrawalsTotal = 0;
  let totalWithdrawals = 0;
  switch (contractType) {
    case 'amalu':
      maxWithdrawalsAddress = gate[0];
      maxWithdrawalsTotal = gate[1];
      totalWithdrawals = gate[2];
      break;
    case 'merkle':
      maxWithdrawalsAddress = gate[1];
      maxWithdrawalsTotal = gate[2];
      totalWithdrawals = gate[3];
      break;
    case 'whitelist':
      maxWithdrawalsAddress = gate[1];
      break;
  }
  if (maxWithdrawalsAddress.toNumber() === 0) {
    throw Error('Gate probably doesnt exist, as max withdrawals are equal to 0');
  }
  return { maxWithdrawalsAddress, maxWithdrawalsTotal, totalWithdrawals };
}

export async function addPriceGate(
  contractType,
  priceAddress,
  chainId,
  intPrice,
  priceDecay,
  priceIncrease,
  priceIncreaseDenominator,
  beneficiary
) {
  try {
    const ABI = priceContracts[contractType];
    const contract = await ethInstance.getWriteContractByAddress(ABI, priceAddress, chainId);
    let fnArguments;
    switch (contractType) {
      case PRICE_TYPES.AMALU:
        return 1;
      case PRICE_TYPES.FIXED:
      case PRICE_TYPES.FLEXIBLE:
        fnArguments = [toWei(intPrice.toString()), beneficiary];
        break;
      case PRICE_TYPES.SPEEDBUMP:
        fnArguments = [
          toWei(intPrice),
          toWei(priceDecay.toString()),
          toWei(priceIncrease.toString()),
          toWei(priceIncreaseDenominator.toString())
        ];
        break;
    }
    const oldIndex = await contract.numGates();
    const tx = await contract.addGate.apply(null, fnArguments);
    await tx.wait();
    return oldIndex.toNumber() + 1;
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function ensurePriceGateExists(contractType, chainId, contractAddress, priceIndex) {
  try {
    if (contractType === 'amalu') return 0;
    const contract = await ethInstance.getReadContractByAddress(
      priceContracts[contractType],
      contractAddress,
      chainId
    );
    const cost = await contract.getCost(priceIndex);
    return Number(fromWei(cost));
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function ensureEligibilityGateExists(
  contractType,
  chainId,
  eligibilityAddress,
  gateIndex
) {
  try {
    const ABI = getContractAbi(contractType);
    const contract = await ethInstance.getReadContractByAddress(ABI, eligibilityAddress, chainId);
    const gate = await contract.getGate(gateIndex);
    if (!gate) throw Error('Gate doesnt exist');
    return parseGateFieldsForContractType(contractType, gate);
  } catch (error) {
    return Promise.reject(error.code ?? error);
  }
}

export async function addEligibility(
  contractType,
  address,
  merkleRoot,
  maxWithdrawalsAddress,
  maxWithdrawalsTotal
) {
  try {
    const currentNetwork = await ethInstance.getWalletChainId();
    const contract = await ethInstance.getWriteContractByAddress(
      getContractAbi(contractType),
      address,
      currentNetwork
    );
    let fnArguments;

    switch (contractType) {
      case 'merkle':
        fnArguments = [merkleRoot, maxWithdrawalsAddress, maxWithdrawalsTotal];
        break;
      case 'amalu':
        fnArguments = [maxWithdrawalsAddress, maxWithdrawalsTotal];
        break;
      case 'whitelist':
        fnArguments = [address, maxWithdrawalsTotal];
        break;
    }
    if (fnArguments) {
      const oldIndex = await contract.numGates();
      const tx = await contract.addGate.apply(null, fnArguments);
      await tx.wait();
      return oldIndex.toNumber() + 1;
    } else {
      const errorText = 'Unhandled contract type';
      toast.error(errorText);
      return new Error(errorText);
    }
  } catch (err) {
    console.error('Adding gate failed', err);
    return Promise.reject(err);
  }
}

export async function addMint(
  metadataMerkleRoot,
  ipfsHash,
  nftAddress,
  priceGateAddress,
  eligibilityAddress,
  eligibilityIndex,
  priceIndex,
  gateAddress
) {
  try {
    const currentNetwork = await ethInstance.getWalletChainId();
    const contract = await ethInstance.getWriteContractByAddress(
      MerkleIdentity,
      gateAddress,
      currentNetwork
    );
    const oldNumTrees = await contract.numTrees();

    const tx = await contract.addMerkleTree(
      metadataMerkleRoot,
      ipfsHash,
      nftAddress,
      priceGateAddress,
      eligibilityAddress,
      eligibilityIndex,
      priceIndex
    );
    await tx.wait();
    return oldNumTrees.toNumber() + 1;
  } catch (err) {
    console.error('Adding tree to gate contract failed', err);
    return Promise.reject(err);
  }
}
