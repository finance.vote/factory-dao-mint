import axios from 'axios';
import { ethInstance } from 'evm-chain-scripts';

export const updateSeriesAfterCreateMint = async (
  key,
  seriesId,
  eligibilityIndex,
  priceIndex,
  merkleTreeIndex
) => {
  try {
    const account = await ethInstance.getEthAccount();
    const updateBody = {
      id: seriesId,
      eligibilityIndex,
      priceIndex,
      merkleTreeIndex
    };
    const updateSignature = await ethInstance.signPersonalMessage(
      JSON.stringify(updateBody),
      account
    );
    updateBody.signature = updateSignature;
    await axios.post(`/minting/${key}/update-series`, updateBody);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const updateNewSeries = async (
  key,
  seriesId,
  eligibilityIndex,
  priceIndex,
  merkleTreeIndex
) => {
  try {
    const account = await ethInstance.getEthAccount();
    const updateBody = {
      id: seriesId,
      eligibilityIndex,
      priceIndex,
      merkleTreeIndex
    };
    const updateSignature = await ethInstance.signPersonalMessage(
      JSON.stringify(updateBody),
      account
    );
    updateBody.signature = updateSignature;
    await axios.post(`/minting/${key}/update-series`, updateBody);
  } catch (err) {
    return Promise.reject(err);
  }
};

export const keyToOptionParser = (key) => ({
  label: key,
  value: key.toLocaleLowerCase()
});
