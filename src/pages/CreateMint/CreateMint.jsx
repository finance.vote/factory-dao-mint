import axios from 'axios';
import classnames from 'classnames';
import PageLoading from 'components/Ui/PageLoading/PageLoading.component';
import Select from 'components/Ui/Select';
import { ethInstance } from 'evm-chain-scripts';
import networks from 'helpers/networks.json';
import FormWrapper from 'minting-components/components/FormWrapper';
import { GeneratedData } from 'minting-components/components/GeneratedData/GeneratedData';
import { MintForm, SeriesForm } from 'minting-components/components/MintingForms/forms';
import classes from 'pages/CreateMint/CreateMint.module.scss';
import { updateNewSeries, updateSeriesAfterCreateMint } from 'pages/CreateMint/helpers';
import {
  addEligibility,
  addMint,
  addPriceGate,
  ensureEligibilityGateExists,
  ensurePriceGateExists
} from 'pages/CreateMint/helpers/mint';

import {
  parseAdvancedToSimple,
  parseCSVToJSON,
  parseJSONToCSV,
  setElements
} from 'evm-chain-scripts';

import { PRICE_TYPES } from 'minting-components/helpers';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import utilClasses from 'utils/scss/modules/utils.module.scss';

const availableNetworks = Object.keys(networks)
  .filter((network) => networks[network].name)
  .map((network) => ({ label: networks[network].name, value: network }));

const ADDRESS = {
  name: 'address',
  headers: {
    address: {
      index: 0,
      validate: () => {}
    }
  }
};

const METADATA = {
  name: 'metadata',
  headers: {
    tokenId: {
      index: 0,
      validate: () => {}
    },
    uri: {
      index: 1,
      validate: () => {}
    }
  }
};

const CreateMint = () => {
  const [isTreeGenerated, setIsTreeGenerated] = useState(false);
  const [generatedData, setGeneratedData] = useState({});
  const [isAdding, setIsAdding] = useState(false);
  const [chainName, setChainName] = useState(availableNetworks[0].label);
  const [selectedChain, setSelectedChain] = useState(availableNetworks[0].value);
  const [formType, setFormType] = useState(false);
  const [mintChain, setMintChain] = useState(null);

  const checkNetwork = async (whichTry = 0) => {
    try {
      if (whichTry > 10) return;
      const currentNetwork = await ethInstance.getWalletChainId();
      setChainName(networks[currentNetwork]?.name);
    } catch (err) {
      console.log('Check network error:', err);
      return setTimeout(() => checkNetwork(whichTry + 1), 300);
    }
  };
  useEffect(() => {
    (async () => {
      const timeoutId = await checkNetwork();
      return () => clearTimeout(timeoutId);
    })();
    window.ethereum?.on('chainChanged', (chainId) => setChainName(networks[Number(chainId)]?.name));
    setElements([ADDRESS, METADATA]);
  }, []);
  const standardMintSubmit = async (values) => {
    let {
      eligibilityGateExists,
      eligibilityType,
      chainId,
      eligibilityAddress,
      eligibilityIndex,
      priceGateExists,
      priceType,
      priceAddress,
      priceIndex,
      price,
      maxWithdrawalsPerAddress,
      maxWithdrawalsTotal,
      priceDecay,
      priceIncrease,
      priceDenominator,
      priceBeneficiary
    } = values;
    if (eligibilityGateExists) {
      const { maxWithdrawalsAddress, maxWithdrawalsTotal: _maxWithdrawalsTotal } =
        await ensureEligibilityGateExists(
          eligibilityType,
          chainId,
          eligibilityAddress,
          eligibilityIndex
        );
      maxWithdrawalsPerAddress = maxWithdrawalsAddress.toNumber();
      maxWithdrawalsTotal = _maxWithdrawalsTotal.toNumber();
    }

    if (priceGateExists && priceType === 'fixed') {
      const _price = await ensurePriceGateExists(priceType, chainId, priceAddress, priceIndex);
      if (price !== _price) {
        throw Error('Price in Price Gate doesnt match with input value');
      }
      price = _price;
    } else if (!priceGateExists) {
      priceIndex = await addPriceGate(
        priceType,
        priceAddress,
        chainId,
        price,
        priceDecay,
        priceIncrease,
        priceDenominator,
        priceBeneficiary
      );
    }

    return { price, priceIndex, maxWithdrawalsPerAddress, maxWithdrawalsTotal };
  };

  const submitHandler = async (values) => {
    if (isAdding) return;
    if (networks[selectedChain].name !== chainName) {
      toast.warn(
        "You've selected different network than the one selected in your wallet, please change your wallet's network to the correct one and try again.",
        { position: toast.POSITION.TOP_CENTER }
      );
      return ethInstance.ensureCorrectChainId(0, parseInt(selectedChain));
    }
    setIsAdding(true);
    try {
      const chainId = await ethInstance.getWalletChainId();

      let data = { ...values };
      if (values.isBackendMint) {
        data = {
          ...values,
          eligibilityType: 'qrcode',
          gateAddress: 'N/A',
          eligibilityIndex: 1,
          eligibilityAddress: 'N/A',
          addressLeavesElements: [],
          eligibilityGateExists: true,
          maxWithdrawalsTotal: 0,
          priceType: PRICE_TYPES.FIXED,
          priceAddress: 'N/A',
          priceGateExists: false,
          priceIndex: 1,
          priceBeneficiary: 'N/A',
          priceDecay: 'N/A',
          priceIncrease: 'N/A',
          priceDenominator: 'N/A',
          price: 0
        };
      }

      let {
        group,
        nftAddress,
        gateAddress,
        startsAt,
        paused,
        eligibilityType,
        eligibilityAddress,
        eligibilityGateExists,
        eligibilityIndex,
        priceType,
        priceAddress,
        unboxingVideo,
        cloudflareVideoId,
        everyoneAtOnce,
        showNext,
        reveal,
        trafficLights,
        progressBar,
        addressLeavesElements,
        metadataLeavesElements,
        placeholderImage,
        placeholderType,
        mediaExternal,
        minRange,
        maxRange,
        mintType,
        stageType,
        nextStageMinDate,
        addressAdvanced,
        addressTextarea,
        addressTextareaType,
        addressIncludeHeaders,
        metadataAdvanced,
        metadataTextarea,
        metadataTextareaType,
        metadataIncludeHeaders,
        isBackendMint
      } = data;

      let _addressLeavesElements;
      let _metadataLeavesElements;

      const isMint = !!nftAddress;

      if (addressAdvanced && addressTextarea) {
        const { data } = parseAdvancedToSimple(
          addressTextarea,
          addressTextareaType,
          addressIncludeHeaders,
          'address',
          false,
          false
        );
        _addressLeavesElements = data;
      } else {
        _addressLeavesElements = addressLeavesElements.map(({ address }) => ({
          address: address.trim()
        }));
      }

      let metadataToParse;

      if (metadataAdvanced && metadataTextarea) {
        const { data } = parseAdvancedToSimple(
          metadataTextarea,
          metadataTextareaType,
          metadataIncludeHeaders,
          'metadata',
          false,
          false
        );

        metadataToParse = data;
      } else {
        metadataToParse = metadataLeavesElements;
      }

      _metadataLeavesElements = metadataToParse.map(({ tokenId, uri }) => ({
        tokenId: parseInt(tokenId),
        uri: uri.trim()
      }));

      const { price, priceIndex, maxWithdrawalsPerAddress, maxWithdrawalsTotal } = !isBackendMint
        ? await standardMintSubmit({ ...values, chainId })
        : values;

      let seriesBody = {
        series: {
          eligibility: {
            type: eligibilityType,
            address: eligibilityAddress,
            index: eligibilityGateExists && eligibilityIndex
          },
          price: {
            type: priceType,
            address: priceAddress,
            index: priceIndex || data.priceIndex,
            value: (priceType === PRICE_TYPES.FIXED || priceType === PRICE_TYPES.FLEXIBLE) && price
          },
          startsAt: startsAt,
          nextStageMinDate,
          paused: paused,
          range: {
            start: minRange,
            end: maxRange
          }
        },
        addressLeaves: _addressLeavesElements,
        metadataLeaves: _metadataLeavesElements
      };

      let mintBody = {
        ...seriesBody,
        mint: {
          key: group,
          gateAddress,
          chainId,
          nftAddress,
          options: {
            unboxingVideo: {
              active: unboxingVideo,
              cloudflareVideoId,
              everyoneAtOnce
            },
            media: {
              external: mediaExternal,
              placeholderImage:
                placeholderType === 'file' ? placeholderImage?.originalname : placeholderImage,
              placeholderType,
              showNext,
              reveal
            },
            trafficLights,
            progressBar,
            mintType,
            stages: stageType,
            isBackendMint
          }
        }
      };

      const requestBody = isMint ? mintBody : seriesBody;

      const formData = new FormData();

      const account = await ethInstance.getEthAccount();
      requestBody.signature = await ethInstance.signPersonalMessage(
        JSON.stringify(requestBody),
        account
      );

      formData.append('data', JSON.stringify(requestBody));
      if (isMint) {
        if (placeholderType === 'file') {
          if (placeholderImage) {
            formData.append('placeholderImage', placeholderImage);
          } else {
            throw 'Please upload placeholder image or switch type to cloudflare video Id';
          }
        }
      }

      const url = isMint ? `/minting/add-mint` : `/minting/${group.value ?? group}/add-series`;
      const { data: result, status } = await axios.post(url, formData, {
        maxBodyLength: 'Infinity',
        headers: {
          'Content-Type': `multipart/form-data; boundary=${formData._boundary}`
        }
      });

      const { addressHash, metadataHash: metadataRoot, ipfsHash, seriesId } = result;

      if (isMint) {
        const { mintId } = result;
        setGeneratedData({ ipfsHash, mintId, addressHash, metadataRoot });
        setIsTreeGenerated(true);

        requestBody.series.mint = mintId;
      } else {
        gateAddress = result.gateAddress;
        nftAddress = result.nftAddress;
      }

      let eligibilityGateIndex = requestBody.series.eligibility.index;

      if (!eligibilityGateExists) {
        eligibilityGateIndex = await addEligibility(
          eligibilityType,
          eligibilityAddress,
          addressHash,
          maxWithdrawalsPerAddress,
          maxWithdrawalsTotal
        );
      }

      if (!isBackendMint) {
        const merkleTreeIndex = await addMint(
          metadataRoot,
          ipfsHash,
          nftAddress,
          priceAddress,
          eligibilityAddress,
          eligibilityGateIndex,
          priceIndex,
          gateAddress
        );

        if (isMint) {
          await updateSeriesAfterCreateMint(
            group,
            seriesId,
            eligibilityGateIndex,
            priceIndex,
            merkleTreeIndex
          );
        } else {
          await updateNewSeries(
            group.value ?? group,
            seriesId,
            eligibilityGateIndex,
            priceIndex,
            merkleTreeIndex
          );
        }
      }

      if (status !== 200) {
        throw new Error();
      }

      toast.success(`New ${isMint ? 'mint' : 'series'} added!`);
    } catch (e) {
      console.error(e);
    } finally {
      setIsAdding(false);
    }
  };

  const handleTreeInputType = (type, alternativeInputType, values, setValues) => {
    const field = type + 'Textarea';
    const textarea = values[field];
    let fieldData;

    if (alternativeInputType === 'CSV') {
      //from JSON array to CSV
      fieldData = parseJSONToCSV(textarea, type);
    } else {
      //from CSV to JSON array
      const { preparedJSONData } = parseCSVToJSON(textarea, type, values.includeHeaders);
      fieldData = preparedJSONData;
    }

    setValues({
      ...values,
      [field]: fieldData,
      [type + 'IncludeHeaders']: false,
      [type + 'TextareaType']: alternativeInputType
    });
  };
  const findChainNetwork = (selectedChain) =>
    availableNetworks.find((chain) => chain.value === selectedChain);

  const formTypeBasedProps = formType
    ? {
        defaultValue: availableNetworks[0].value,
        value: findChainNetwork(selectedChain),
        onChange: ({ value }) => setSelectedChain(value),
        label: 'Select chain'
      }
    : {
        value: findChainNetwork(mintChain),
        label: 'Selected chain',
        placeholder: 'Select group key to fill chain name',
        isDisabled: true
      };
  useEffect(() => {
    if (mintChain !== null) setSelectedChain(mintChain);
  }, [mintChain]);
  return (
    <>
      {isAdding ? <PageLoading /> : null}
      <FormWrapper>
        <div className={utilClasses.container}>
          <h1 className={classes.title}>Create new mint</h1>
          <h3 className={classes.networkIndicator}>
            You are currently using: <span>{chainName}</span>
          </h3>
          <Select
            id="chain-selection"
            className={classnames(classes.projectSelect, utilClasses.selectInput)}
            options={availableNetworks}
            {...formTypeBasedProps}
          />
          <p>Select Form Type</p>
          <div className={classes.switchButton} data-test="form-type">
            <input
              className={classes.switchButtonCheckbox}
              id="formSwitch"
              type="checkbox"
              onChange={({ target }) => {
                setFormType(target.checked);
                setMintChain(null);
              }}
              data-test="form-type-switch-btn"
            />
            <label className={classes.switchButtonLabel} htmlFor="formSwitch">
              <span className={classes.switchButtonLabelSpan}>Series</span>
            </label>
          </div>
          {formType ? (
            <MintForm
              onSubmit={submitHandler}
              handleTreeInputType={handleTreeInputType}
              isSelected={formType}
            />
          ) : (
            <SeriesForm
              onSubmit={submitHandler}
              handleTreeInputType={handleTreeInputType}
              isSelected={!formType}
              setMintChain={setMintChain}
            />
          )}
        </div>
        {isTreeGenerated && <GeneratedData generatedData={generatedData} />}
      </FormWrapper>
    </>
  );
};

export default CreateMint;
