import Header from 'components/NFTicket/Header';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Image from 'next/image';
import lynxGraphicImg from '../../images/nfticket/lynx_graphic.svg';
import lynxGraphicMobileImg from '../../images/nfticket/lynx_graphic_mobile.svg';
import footerImg from '../../images/nfticket/poweredByFD.svg';
import styles from './NFTicket.module.scss';

const discordLink = 'http://discord.gg/factorydao';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#4DFF64'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const customStyles = {
  ...styles,
  highlightIndicators,
  handleStyle: {
    backgroundColor: '#F47B0A',
    height: '21px',
    width: '21px',
    border: 0,
    top: '0',
    opacity: '1'
  }
};

const NFTicket = ({ groupKey }) => {
  return (
    <>
      <main className={styles.main}>
        <Image src={lynxGraphicImg} alt="nft ticket cat" className={styles.catGraphic} />
        <Image
          src={lynxGraphicMobileImg}
          alt="nft ticket cat"
          className={styles.catGraphicMobile}
        />
        <Header />
        <div className={styles.welcome}>Welcome to the </div>
        <div className={styles.firstLine}>
          <h1>
            Exclusive <span className={styles.ticket}>NFTicket</span>
          </h1>
        </div>
        <h1 className={styles.collection}>Collection</h1>
        <MintingController
          groupKey={groupKey}
          customStyles={customStyles}
          customTexts={{
            successTitle: 'Welcome to the club',
            successMessage: `You now own NFTicket Avatar #%TOKEN_ID%`,
            numberInputLabel:
              'Click mint now, enter a different number or click the random number arrows',
            placeholderText: 'Enter a number',
            buttonText: {
              available: 'MINT NOW',
              unavailable: 'MINT NOW',
              mintAgain: 'MINT ANOTHER'
            }
          }}
          discordLink={discordLink}
          noShift
          MintingButtonComponent={(props) => (
            <DefaultMintingButton
              {...props}
              isAdditionalButtonVisible
              actionButtonAfterMintText="To the Governance portal"
              discordLink="https://influence.factorydao.org/bloxmovenfticket"
              customStyles={{
                button: styles.button,
                actionButton: styles.actionButton
              }}
            />
          )}
          tooltipContent={
            <>
              This is the number of the NFT in the series, not the amount of NFTs you are minting.
              <br />
              <br />
              NFTs are minted one at a time.
              <br />
              <br />
              The next available NFT in the series has been selected for you, just click “MINT NOW”
              if you are happy with it.
              <br />
              <br />
              If you would like to pick another random number click the green “random” arrows
              <br />
              <br />
              If you would like a specific number in the series, try typing it in. It may be taken!
              The traffic lights will indicate if it’s:
              <div>
                <span className={customStyles.tooltipBoldText}>Green:</span> Available
              </div>
              <div>
                <span className={customStyles.tooltipBoldText}>Amber:</span> Somebody else is in the
                process of claiming it now
              </div>
              <div>
                <span className={customStyles.tooltipBoldText}>Red:</span> Already claimed
              </div>
            </>
          }
        />
      </main>
      <div className={styles.nfTicketContainer}>
        <span className={styles.title}>NFTicket</span>
        <div className={styles.content}>
          A unique collection of exclusive, randomly generated avatars that live on the blockchain.
          By owning one of these avatars, you can participate in the governance of our bloXmove DAO
          and receive access to various rewards and membership benefits, powered by NFTicket via
          bloXmove DAO. <br /> <br />
          But it&apos;s not just about owning a cool avatar or governance rights, you will also make
          a difference in the world. Profits generated from the sale are directed to charities of
          your choosing and to the community.
        </div>
      </div>
      <footer className={styles.footer}>
        <Image src={footerImg} alt="FactoryDAO" />
      </footer>
    </>
  );
};

export default NFTicket;
