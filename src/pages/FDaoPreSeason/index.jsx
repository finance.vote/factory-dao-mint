import Header from 'components/Header/Header.component';
import MembershipSbts from 'components/MembershipSBTS/MembershipSbts';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import SetYourDao from 'components/SetYourDao/SetYourDao';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import Image from 'next/image';
import HowItWorks from '../../components/FDaoPreSeason/HowItWorks';
import fdaoLogo from '../../images/fdaoLogo.png';
import styles from './FDaoPreSeason.module.scss';

const discordLink = 'http://discord.gg/factorydao';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#4DFF64'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const FDaoPreSeason = ({ groupKey }) => {
  return (
    <main className={styles.main}>
      <Header websiteLink="https://www.factorydao.xyz/" showDiscordLink={false} showTelegramLink />
      <Image
        className={styles.mockLogo}
        src={fdaoLogo}
        width={288}
        height={284}
        alt="fdao dao logo"
      />
      <h1 className={styles.mockDescTop}>Experiment with DAO governance</h1>
      <h2 className={styles.mockDescMid}>
        FactoryDAO helps DAOs and communities to launch their <br />
        membership using 0 cost NFTs (SBTs)
      </h2>
      <h3 className={styles.mockDescBot}>
        SBTs are similar to NFTs but are non-transferable, they have zero value on the <br />
        secondary market and can not be sent to another wallet.
      </h3>

      <MintingController
        groupKey={groupKey}
        isProgressBarVisible={false}
        walletInputLabel="Paste address or click connect wallet"
        customStyles={{ ...styles, highlightIndicators }}
        customTexts={{
          successTitle: 'Welcome to Factory DAO',
          successMessage: `#%TOKEN_ID% now belongs to you`
        }}
        discordLink={discordLink}
        noShift
        MintingButtonComponent={(props) => (
          <DefaultMintingButton
            {...props}
            actionButtonAfterMintVisible
            actionButtonAfterMintText="continue to the voting realm"
            discordLink="https://influence.factorydao.org/factorydaopreseason"
            customStyles={{
              button: styles.button,
              actionButton: styles.actionButton
            }}
          />
        )}
      />
      <HowItWorks />
      <MembershipSbts />
      <SetYourDao name="FactoryDAO" title="Launch your DAO" />
      <PoweredByDao showDiscord={false} showTelegram={true} />
    </main>
  );
};

export default FDaoPreSeason;
