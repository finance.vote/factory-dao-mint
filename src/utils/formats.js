import numeral from 'numeral';

export const formatNumber = (number, format = '0,0.[000000000000000000]') => {
  const formattedNumber = number && numeral(number).format(format);

  return isNaN(parseFloat(formattedNumber)) ? number : formattedNumber;
};
