import classnames from 'classnames';
import DialogBlack from 'components/DialogBlack';
import { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import utilClasses from 'utils/scss/modules/utils.module.scss';
import classes from './DateTimeSelectModal.module.scss';

const dateFormat = {
  month: 'short',
  year: 'numeric',
  day: 'numeric',
  hour: 'numeric',
  minute: 'numeric',
  hour12: true
};

const DateTimeSelectModalComponent = ({ value, error, onDateChange, defaultText }) => {
  const [isDateModalOpen, setIsDateModalOpen] = useState(false);
  const [buttonText, setButtonText] = useState(
    value ? value.toLocaleString('en-US', dateFormat) : defaultText
  );
  const [mode, setMode] = useState('date'); // dialog mode - date or time select
  const [date, setDate] = useState(new Date());
  const [hours, setHours] = useState('12');
  const [minutes, setMinutes] = useState('00');

  const toggleMode = () => {
    setMode((prevState) => (prevState === 'date' ? 'time' : 'date'));
  };

  const isValidTime = () => {
    return /^\d{1,2}$/.test(hours) && /^\d{1,2}$/.test(minutes);
  };

  const updateButtonText = (date) => {
    setButtonText(date.toLocaleString('en-US', dateFormat));
  };

  const handleDateModalOpen = () => {
    setIsDateModalOpen(true);
  };
  const handleDateModalClose = () => {
    setIsDateModalOpen(false);
  };

  const handleDateChange = (val) => {
    onDateChange(val);
    setDate(val);
    updateButtonText(val);
  };

  const handleTimeSelect = () => {
    date.setHours(hours);
    date.setMinutes(minutes);
    date.setSeconds(0);
    updateButtonText(date);
    handleDateModalClose();
    setMode('date');
    onDateChange(date);
  };

  function updateTime(event, isHours) {
    let val = parseInt(event.target.value);
    if (
      (/^\d{1,2}|^$/.test(event.target.value) && !val) ||
      (val > 0 && val <= (isHours ? 24 : 59))
    ) {
      isHours ? setHours(event.target.value) : setMinutes(event.target.value);
    }
  }

  return (
    <>
      <DialogBlack isOpen={isDateModalOpen} handleClose={handleDateModalClose}>
        <div className={classes.calendarWrapper} data-test="date-picker-modal">
          <label
            className={classnames(classes.calendarLabel, classes.whiteText)}
            htmlFor="startDate"
          >
            Starts at
          </label>
          {mode === 'date' ? (
            <>
              <DatePicker
                id="startDate"
                selected={value}
                onChange={handleDateChange}
                inline={true}
                dateFormat="Pp"
                minDate={new Date()}
                calendarClassName={classes.calendar}
                data-test="date-picker-calendar"
              />
              <button
                className={classnames(
                  utilClasses.hollowButton,
                  utilClasses.hollowButtonBlack,
                  classes.calendarButton
                )}
                disabled={!value}
                onClick={toggleMode}
                data-test="date-picker-next-btn"
              >
                Next
              </button>
            </>
          ) : (
            <>
              <div className={classes.time}>
                <input
                  placeholder="hh"
                  className="input"
                  onChange={(e) => updateTime(e, true)}
                  value={hours}
                  data-test="date-picker-hour"
                />
                <div>:</div>
                <input
                  placeholder="mm"
                  className="input"
                  onChange={(e) => updateTime(e, false)}
                  value={minutes}
                  data-test="date-picker-minutes"
                />
              </div>
              <div className={classes.calendarButtonWrapper}>
                <button
                  className={classnames(
                    utilClasses.hollowButton,
                    utilClasses.hollowButtonBlack,
                    classes.calendarButton
                  )}
                  onClick={toggleMode}
                  data-test="date-picker-back-btn"
                >
                  Back
                </button>
                <button
                  className={classnames(
                    utilClasses.hollowButton,
                    utilClasses.hollowButtonBlack,
                    classes.calendarButton
                  )}
                  disabled={!isValidTime()}
                  onClick={handleTimeSelect}
                  data-test="date-picker-select-btn"
                >
                  Select
                </button>
              </div>
            </>
          )}
        </div>
      </DialogBlack>

      <label htmlFor="startsAt" className={classes.label}>
        Starts at
        <button
          className={classnames(
            utilClasses.hollowButton,
            utilClasses.hollowButtonBlack,
            classes.inputBtn,
            error && classes.inputError
          )}
          onClick={handleDateModalOpen}
          type="button"
          data-test="date-picker-btn"
        >
          {buttonText}
        </button>
      </label>
    </>
  );
};

export default DateTimeSelectModalComponent;
