import classNames from 'classnames';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import { osName } from 'react-device-detect';
import loaderImg from '../../../images/loader.svg';
import styles from './PageLoading.module.scss';

/**
 *
 * Page loading component, slim splash screen, without logo
 *
 * @returns Page loading component
 */

const PageLoading = ({ loader, className }) => {
  const [isOSDev, setIsIOSDev] = useState(false);

  useEffect(() => {
    const isIOS = osName === 'iOS' || osName !== 'MacOS';
    setIsIOSDev(isIOS);
  }, [osName]);

  return (
    <div className={classNames(styles.loading, className)}>
      {!isOSDev ? (
        <Image alt="loader" src={loader || loaderImg} />
      ) : (
        <object>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="180px"
            height="180px"
            viewBox="0 0 100 100"
            preserveAspectRatio="xMidYMid"
          >
            <rect x="37.5" y="37.5" width="7" height="7" fill="#9329d7">
              <animate
                attributeName="fill"
                values="#20f2ec;#9329d7;#9329d7"
                keyTimes="0;0.125;1"
                dur="1s"
                repeatCount="indefinite"
                begin="0s"
                calcMode="discrete"
              ></animate>
            </rect>
            <rect x="46.5" y="37.5" width="7" height="7" fill="#9329d7">
              <animate
                attributeName="fill"
                values="#20f2ec;#9329d7;#9329d7"
                keyTimes="0;0.125;1"
                dur="1s"
                repeatCount="indefinite"
                begin="0.125s"
                calcMode="discrete"
              ></animate>
            </rect>
            <rect x="55.5" y="37.5" width="7" height="7" fill="#9329d7">
              <animate
                attributeName="fill"
                values="#20f2ec;#9329d7;#9329d7"
                keyTimes="0;0.125;1"
                dur="1s"
                repeatCount="indefinite"
                begin="0.25s"
                calcMode="discrete"
              ></animate>
            </rect>
            <rect x="37.5" y="46.5" width="7" height="7" fill="#9329d7">
              <animate
                attributeName="fill"
                values="#20f2ec;#9329d7;#9329d7"
                keyTimes="0;0.125;1"
                dur="1s"
                repeatCount="indefinite"
                begin="0.875s"
                calcMode="discrete"
              ></animate>
            </rect>
            <rect x="55.5" y="46.5" width="7" height="7" fill="#9329d7">
              <animate
                attributeName="fill"
                values="#20f2ec;#9329d7;#9329d7"
                keyTimes="0;0.125;1"
                dur="1s"
                repeatCount="indefinite"
                begin="0.375s"
                calcMode="discrete"
              ></animate>
            </rect>
            <rect x="37.5" y="55.5" width="7" height="7" fill="#9329d7">
              <animate
                attributeName="fill"
                values="#20f2ec;#9329d7;#9329d7"
                keyTimes="0;0.125;1"
                dur="1s"
                repeatCount="indefinite"
                begin="0.75s"
                calcMode="discrete"
              ></animate>
            </rect>
            <rect x="46.5" y="55.5" width="7" height="7" fill="#9329d7">
              <animate
                attributeName="fill"
                values="#20f2ec;#9329d7;#9329d7"
                keyTimes="0;0.125;1"
                dur="1s"
                repeatCount="indefinite"
                begin="0.625s"
                calcMode="discrete"
              ></animate>
            </rect>
            <rect x="55.5" y="55.5" width="7" height="7" fill="#9329d7">
              <animate
                attributeName="fill"
                values="#20f2ec;#9329d7;#9329d7"
                keyTimes="0;0.125;1"
                dur="1s"
                repeatCount="indefinite"
                begin="0.5s"
                calcMode="discrete"
              ></animate>
            </rect>
          </svg>
        </object>
      )}
    </div>
  );
};

export default PageLoading;
