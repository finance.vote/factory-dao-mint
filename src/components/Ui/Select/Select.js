import classnames from 'classnames';
import arrowDown from 'images/arrow-down.svg';
import Image from 'next/image';
import SelectComponent, { components } from 'react-select';
import classes from './Select.module.scss';

const Select = ({
  isDisabled,
  className,
  defaultValue,
  value,
  options = [],
  placeholder = '',
  onChange,
  label,
  id,
  isLabelHidden = false,
  isError,
  error
}) => {
  const optionBackgroundColor = ({ isDisabled, isSelected, isFocused }) => {
    if (isDisabled) return undefined;
    if (isSelected) return '#ffff00';
    if (isFocused) return '#ffff005e';
    return undefined;
  };
  const optionTextColor = ({ isDisabled, isSelected }) => {
    if (isDisabled) return '#777';
    if (isSelected) return 'black';
    return 'white';
  };
  const optionActiveBackgroundColor = ({ isDisabled, isSelected }) => {
    if (isDisabled) return undefined;
    if (isSelected) return '#fafa6e';
    return '#ffff00e5';
  };
  const optionActiveTextColor = ({ isDisabled, isSelected }) => {
    if (isDisabled) return undefined;
    if (isSelected) return 'white';
    return 'black';
  };

  const customDropdownStyles = {
    option: (defaultStyles, state) => {
      return {
        ...defaultStyles,
        backgroundColor: optionBackgroundColor(state),
        color: optionTextColor(state),
        cursor: state.isDisabled ? 'not-allowed' : 'default',

        ':active': {
          ...defaultStyles[':active'],
          backgroundColor: optionActiveBackgroundColor(state),
          color: optionActiveTextColor(state)
        }
      };
    },
    singleValue: (defaultStyles) => ({ ...defaultStyles, color: 'white' }),
    control: (defaultStyles) => ({
      ...defaultStyles,
      backgroundColor: 'black',
      borderRadius: 0,
      minHeight: 46
    }),
    menu: (defaultStyles) => ({ ...defaultStyles, backgroundColor: 'black', zIndex: 4 }),
    menuList: (defaultStyles) => ({
      ...defaultStyles,
      border: '1px solid white',
      padding: 0,
      marginTop: -5
    })
  };
  const Menu = (props) => {
    return (
      <>
        <components.Menu {...props} className="fadeIn">
          <div>{props.children}</div>
        </components.Menu>
      </>
    );
  };

  const CustomIndicatorContainer = ({ options, getValue }) => {
    if (isDisabled) return;
    const selected = getValue();
    const selectedOptionIndex = selected.length
      ? options?.findIndex((option) => option.value === selected[0].value) + 1
      : null;
    const optionsMax = options?.length;

    return (
      <div className={classes.countContainer}>
        {selectedOptionIndex && optionsMax && (
          <span>
            {selectedOptionIndex}/{optionsMax}
          </span>
        )}
        <Image className={classes.selectIcon} alt="Arrow down" src={arrowDown} />
      </div>
    );
  };

  const labelId = `${id}-label`;
  const defaultSelection = options?.find((option) => option.value === defaultValue);

  return (
    <div className={classnames(classes.container, className)} data-test={id}>
      <label
        className={classes.label}
        id={labelId}
        style={{ visibility: isLabelHidden ? 'hidden' : null }}
      >
        {label}
      </label>
      <div className={isError ? classes.inputError : ''}>
        <SelectComponent
          isDisabled={isDisabled}
          aria-labelledby={labelId}
          value={value || defaultSelection}
          placeholder={placeholder}
          styles={customDropdownStyles}
          onChange={onChange}
          options={options}
          components={{
            IndicatorsContainer: CustomIndicatorContainer,
            Menu
          }}
        />
      </div>
      {error && <p className={classes.textError}>{error}</p>}
    </div>
  );
};

export default Select;
