import genesisLogo from 'images/gmgn/genesis-nft-launch-title.png';
import mintingNow from 'images/gmgn/minting_now.svg';
import Image from 'next/image';
import styles from './Introduce.module.scss';

const Introduce = () => {
  return (
    <div className={styles.container}>
      <Image alt="logo" src={genesisLogo} className={styles.logo} />
      <Image alt="minting_now" src={mintingNow} className={styles.mintingNow} />
      {/* <span className={styles.description}>
        This is a limited “Genesis” Mint to reward our early community and your entry point into
        co-owning 100+ Consumer Packaged Goods brands built as a DAO. Get the lowdown below and make
        sure you join the allowlist via our{' '}
        <a href="https://discord.com/invite/vbUwAN22dj" target="_blank" rel="noreferrer">
          Discord
        </a>
        <br />
        <br />
        <span className={styles.bold}>Are you new to gmgn? You’re still early!</span>
        <br />
        <br />
        Imagine if Consumer Packaged Goods (CPG) brands like Nestlé or Unilever started as
        Decentralized Autonomous Organizations (DAO) and you were invited to be a stakeholder from
        the start. This is your invitation.
        <br />
        <br />
        Want to learn more? Check out our <a href="https://gmgn.gitbook.io/litepaper/">
          Litepaper
        </a>{' '}
        or watch the explainer video{' '}
        <a href="https://twitter.com/gmgnsupplyco/status/1573006081994924040?s=20&t=TomJzbQ17WWwLX3rqTUQmA">
          here
        </a>
        .
      </span> */}
    </div>
  );
};

export default Introduce;
