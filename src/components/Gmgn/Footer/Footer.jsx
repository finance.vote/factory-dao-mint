import discordIcon from 'images/discord.svg';
import linkedinIcon from 'images/linkedin.svg';
import twitterIcon from 'images/twitter.svg';
import Image from 'next/image';
import styles from './Footer.module.scss';

export const GmgnIcons = ({ discordLink, twitterLink, linkedinLink }) => {
  return (
    <div className={styles.iconContainer}>
      <div className={styles.linkContainer}>
        <a
          className={styles.link}
          href={discordLink || 'http://discord.gg/factorydao'}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={discordIcon} alt="Discord" />
        </a>
      </div>
      <div className={styles.linkContainer}>
        <a
          className={styles.link}
          href={twitterLink || 'https://twitter.com/FactDAO'}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={twitterIcon} alt="Twitter" />
        </a>
      </div>
      <div className={styles.linkContainer}>
        <a
          className={styles.link}
          href={linkedinLink || '/'}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={linkedinIcon} alt="LinkedIn" />
        </a>
      </div>
    </div>
  );
};

const Footer = (props) => {
  return (
    <div className={styles.container}>
      <div>----------------------------</div>
      <GmgnIcons {...props} />
    </div>
  );
};

export default Footer;
