import classNames from 'classnames';
import faqImg from 'images/gmgn/faq.png';
import mintDemo from 'images/gmgn/Mint_Demo.gif';
import Image from 'next/image';
import { useState } from 'react';
import accordionItems from './AccordionItems.json';
import styles from './FaqSection.module.scss';

const AccordionItem = ({ title, index, description, activeTab, activateTab }) => {
  const isActive = activeTab === index;

  return (
    <button
      className={!isActive ? styles.accordionItem : styles.accordionItemActive}
      onClick={() => activateTab(index)}
    >
      <div className={styles.controls}>
        <span>{title}</span>
        <div className={styles.plus}>
          <div className={styles.line} />
          <div className={classNames(styles.line, !isActive ? styles.vertical : styles.hide)} />
        </div>
      </div>
      <div className={classNames(styles.accordionDescription, isActive ? styles.active : null)}>
        <span>{description}</span>
        {index === 0 ? (
          <div>
            <Image src={mintDemo} alt="mint-demo" />
          </div>
        ) : null}
      </div>
    </button>
  );
};

const Accordion = () => {
  const [activeTab, setActiveTab] = useState(0);

  const activateTab = (index) => {
    setActiveTab((prev) => (index === prev ? -1 : index));
  };

  return accordionItems.map((item, index) => {
    return (
      <AccordionItem
        activeTab={activeTab}
        key={index}
        index={index}
        title={item.title}
        description={item.description}
        activateTab={activateTab}
      />
    );
  });
};

const FaqSection = () => {
  return (
    <div className={styles.container}>
      <Image src={faqImg} alt="faq" />
      <div className={styles.accordionContainer}>
        <Accordion />
      </div>
    </div>
  );
};

export default FaqSection;
