import React from 'react';
import styles from './MintSchedule.module.scss';
const rounds = [
  {
    date: '11am EST: Tier 1 Full Pass',
    whitelist: 'Open to Full Pass Premint winning addresses',
    limit: 'Max Limit of 1 NFT per address'
  },
  {
    date: '12pm EST: Any remaining Tier 1 Full Passes ',
    whitelist: 'Open to Full Pass Premint Waitlist',
    limit: 'Max Limit of 1 NFT per address'
  },
  {
    date: '12:30pm EST: Tier 2 Discount Pass Opens + any unsold Full Pass NFTs remain available',
    whitelist: 'Discount Pass: Open to 250 discount pass PreMint winning addresses',
    limit: 'Max Limit of 1 NFT per address'
  },
  {
    date: '1:30pm EST: Any remaining NFTs (both Full and Discount Passes)',
    whitelist:
      'Open to all gmgn Allow List addresses including those who minted in rounds 1-3 + all community partners (via snapshot or allowlist)',
    limit: 'Max Limit of 10 NFTs per address'
  },
  {
    date: '5pm EST: Public round - any remaining NFTs',
    whitelist: 'Open to non-allow list public wallets'
  }
];

const RoundItem = ({ step, date, whitelist, limit }) => {
  return (
    <div className={styles.roundItem}>
      <span className={styles.roundItemTitle}>Round {step}:</span>
      <span>{date}</span>
      <span>{whitelist}</span>
      {limit ? <span>{limit}</span> : null}
    </div>
  );
};

const MintSchedule = () => {
  return (
    <div className={styles.container}>
      <div className={styles.titleSection}>
        <h1>Mint Schedule</h1>
        <span>
          If a round sells out, the mint will move straight to the next <br /> round bringing these
          timings forward automatically. <br />
          <br />
          <strong>Tier 1: #1-100&nbsp;&nbsp; / &nbsp;&nbsp; Tier 2: #101-350</strong>
        </span>
      </div>
      <div className={styles.roundsContainer}>
        {rounds.map((round, index) => {
          return <RoundItem key={index} step={index + 1} {...round} />;
        })}
        <div className={styles.roundItem}>
          <span className={styles.roundItemTitle}>Golden Ticket Giveaways:</span>
          <span>350-360 in the range have be reserved for community Golden Ticket winners</span>
        </div>
      </div>
    </div>
  );
};

export default MintSchedule;
