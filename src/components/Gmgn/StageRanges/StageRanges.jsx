import React from 'react';
import styles from './StageRanges.module.scss';
const StageRanges = () => {
  return (
    <div className={styles.container}>
      <span>Select a number in the ranges below:</span>
      <span>Tier 1 Full Pass: #1-100 / Price 0.45ETH</span>
      <span>Tier 2 Discount Pass: #101-350 / Price 0.15ETH</span>
      <small>tier 2 opens in round 3</small>
    </div>
  );
};

export default StageRanges;
