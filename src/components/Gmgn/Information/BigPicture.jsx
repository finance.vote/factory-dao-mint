import daoImg from 'images/gmgn/gmgnDAO.png';
import bigPicture from 'images/gmgn/the-big-picture.png';
import Image from 'next/image';
import styles from './BigPicture.module.scss';

const BigPicture = () => {
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <Image src={bigPicture} alt="the-big" />
      </div>
      <div className={styles.infoText}>
        Why is this a big deal? Well,{' '}
        <span className={styles.bold}>
          gmgnDAO Members will get each of the 100+ product subDAO NFTs for FREE{' '}
        </span>
        as they drop in the future. Think about that for a moment and you’ll get the picture (if
        not, see the picture below).
      </div>
      <div>
        <Image className={styles.daoImg} src={daoImg} alt="gmgn-dao-" />
      </div>
    </div>
  );
};

export default BigPicture;
