import React from 'react';
import styles from './GenesisBenefits.module.scss';

import tier1 from 'images/gmgn/tier1_NFT.svg';
import tier2 from 'images/gmgn/tier2_NFT.svg';
import Image from 'next/image';

const Tiers = () => {
  return (
    <>
      <div className={styles.tierItem}>
        <Image alt="tier1" src={tier1} />
        <span className={styles.desc}>
          Drops October 12th <br />
          (100 limited supply)
        </span>
        <ul>
          <li>100% off the gmgnDAO NFT in December</li>
          <li>Earlybird shipment of OnChain Chocolate</li>
          <li>Exclusive chats & IRL events with Web3 OGs</li>
          <li>1 in 10 chance of winning a *Golden Ticket NFT</li>
        </ul>
      </div>
      <div className={styles.tierItem}>
        <Image alt="tier2" src={tier2} />
        <span className={styles.desc}>
          Drops October 12th <br />
          (250 limited supply)
        </span>
        <ul>
          <li>30% off the gmgnDAO NFT in December</li>
          <li>Earlybird shipment of OnChain Chocolate</li>
          <li>Exclusive chats & IRL events with Web3 OGs</li>
          <li>1 in 50 chance of winning a *Golden Ticket NFT</li>
        </ul>
      </div>
    </>
  );
};

const GenesisBenefits = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>
        What does the <br /> Genesis NFT get me?
      </h1>
      <Tiers />
    </div>
  );
};

export default GenesisBenefits;
