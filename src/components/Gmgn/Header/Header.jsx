import classNames from 'classnames';
import Container from 'components/Ui/Container/Container.component';
import discordIcon from 'images/discord.svg';
import logo from 'images/gmgn/gmgn-logo.png';
import linkedinIcon from 'images/linkedin.svg';
import twitterIcon from 'images/twitter.svg';
import Image from 'next/image';
import styles from './Header.module.scss';

const baseUrl = 'https://www.gmgnsupply.co';

export const menu = [
  { text: 'Home', url: baseUrl },
  { text: 'Litepaper', url: 'https://gmgn.gitbook.io/litepaper' },
  { text: 'DAO Advisors', url: baseUrl + '/dao-advisors' },
  { text: 'Press', url: baseUrl + '/press' },
  { text: 'Mint Info', url: baseUrl + '/mint' }
];

const GmgnHeader = ({ twitterLink, linkedinLink, discordLink, handleOpenMobileMenu }) => {
  return (
    <Container className={styles.mainContainer}>
      <header className={styles.header}>
        <div className={styles.leftContainer}>
          {menu.map(({ text, url }, index) => {
            return (
              <a href={url} className={styles.link} key={index}>
                {text}
              </a>
            );
          })}
        </div>
        <div className={styles.centerContainer}>
          <a className={styles.link} href={baseUrl} target="_blank" rel="noopener noreferrer">
            <Image src={logo} alt="gmgn-logo" />
          </a>
        </div>
        <div className={styles.rightContainer}>
          <div
            className={styles.plus}
            onClick={handleOpenMobileMenu}
            onKeyDown={handleOpenMobileMenu}
            role="button"
            tabIndex={0}
          >
            <div className={styles.line} />
            <div className={classNames(styles.line, styles.vertical)} />
          </div>
          <div className={styles.linkContainer}>
            <a
              className={styles.link}
              href={discordLink || 'http://discord.gg/factorydao'}
              target="_blank"
              rel="noopener noreferrer"
            >
              <Image src={discordIcon} alt="Discord" />
            </a>
          </div>
          <div className={styles.linkContainer}>
            <a
              className={styles.link}
              href={twitterLink || 'https://twitter.com/FactDAO'}
              target="_blank"
              rel="noopener noreferrer"
            >
              <Image src={twitterIcon} alt="Twitter" />
            </a>
          </div>
          <div className={styles.linkContainer}>
            <a
              className={styles.link}
              href={linkedinLink || '/'}
              target="_blank"
              rel="noopener noreferrer"
            >
              <Image src={linkedinIcon} alt="LinkedIn" />
            </a>
          </div>
        </div>
      </header>
    </Container>
  );
};

export default GmgnHeader;
