function withMergedStyles(WrappedComponent, styles = {}) {
  return function withStyles(props) {
    const { customStyles } = props;
    const componentStyles = {
      ...styles,
      ...customStyles
    };
    return <WrappedComponent styles={componentStyles} {...props} />;
  };
}
export default withMergedStyles;
