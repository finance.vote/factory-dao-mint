import styles from './BenefitsSection.module.scss';

const paragraphs = [
  'To participate in the decision making.',
  ' To have a voice on what are the most important matters, our DAO should focus.',
  'To prioritize the areas, we need to spend our budget.',
  ' Holding a Sovryn Whisper NFT is a cool thing to show off!'
];

const BenefitsSection = () => {
  return (
    <div className={styles.container}>
      <h3 className={styles.title}>
        NFT <br className={styles.mobileOnly} />
        BENEFITS
      </h3>
      {paragraphs.map((paragraph, index) => {
        return (
          <p className={styles.paragraph} key={index}>
            {paragraph}
          </p>
        );
      })}
    </div>
  );
};

export default BenefitsSection;
