import Container from 'components/Ui/Container/Container.component';
import discordIcon from 'images/discord.svg';
import globeIcon from 'images/globe.svg';
import mintLogo from 'images/mintLogo.svg';
import telegramIcon from 'images/telegram-white.svg';
import twitterIcon from 'images/twitter.svg';
import If from 'minting-components/components/If/If';
import Image from 'next/image';
import Link from 'next/link';
import styles from './Header.module.scss';

/**
 * Header component
 */
const Header = ({
  twitterLink,
  websiteLink,
  discordLink,
  telegramLink = '',
  isClickable = true,
  showLinks = true,
  customLinks = false,
  showTelegramLink = false,
  showDiscordLink = true
}) => {
  // const [isHamburgerOpened, setIsHamburgerOpened] = useState(false);
  // const onClickHamburger = () => {
  //   setIsHamburgerOpened(!isHamburgerOpened);
  //   emitter.emit(isHamburgerOpened ? 'hamburger-close' : 'hamburger-open');
  // };

  return (
    <Container className={styles.mainContainer}>
      <header className={styles.header}>
        <div className={styles.logo}>
          <If condition={isClickable}>
            <div className={styles.link}>
              <Link href="/">
                <div className={styles.image}>
                  <Image alt="mint-logo" src={mintLogo} className={styles.logo} />
                </div>
              </Link>
            </div>
          </If>
          <If condition={!isClickable}>
            <div className={styles.image}>
              <Image alt="mint-logo" src={mintLogo} className={styles.logo} />
            </div>
          </If>
        </div>
        <div className={styles.container}>
          <If condition={showLinks}>
            {customLinks ? (
              customLinks
            ) : (
              <ul className={styles.navlinks}>
                <li className={styles.linkContainer}>
                  <a
                    className={styles.link}
                    href={twitterLink || 'https://twitter.com/FactDAO'}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <Image src={twitterIcon} alt="Twitter" />
                  </a>
                </li>
                <li className={styles.linkContainer}>
                  <a
                    className={styles.link}
                    href={websiteLink || '/'}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <Image src={globeIcon} alt="Website" />
                  </a>
                </li>
                <If condition={showDiscordLink}>
                  <li className={styles.linkContainer}>
                    <a
                      className={styles.link}
                      href={discordLink || 'http://discord.gg/factorydao'}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <Image src={discordIcon} alt="Discord" />
                    </a>
                  </li>
                </If>
                <If condition={showTelegramLink}>
                  <li className={styles.linkContainer}>
                    <a
                      className={styles.link}
                      href={telegramLink || 'https://t.me/factdao'}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <Image src={telegramIcon} alt="Telegram" />
                    </a>
                  </li>
                </If>
              </ul>
            )}
          </If>
        </div>
      </header>
    </Container>
  );
};

export default Header;
