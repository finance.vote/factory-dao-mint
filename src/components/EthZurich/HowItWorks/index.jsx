import basketball from 'images/ethZurich/basketball.svg';
import glasses from 'images/ethZurich/glasses.svg';
import sticker from 'images/ethZurich/sticker.svg';
import Image from 'next/image';
import Link from 'next/link';
import styles from './howItWorks.module.scss';

export default function HowItWorks() {
  return (
    <section className={styles.howItWorks}>
      <span className={styles.howItWorks__title}>
        How it works
        <Image alt="sticker" src={sticker} className={styles.stickerIcon} />
      </span>
      <span className={styles.howItWorks__innerTitle}>During the hackathon </span>
      <ol className={styles.howItWorks__list} style={{ marginBottom: '92px' }}>
        <li>Scan the QR code you received with your wristband at the entrance</li>
        <li>Mint your unique voting non-transferrable token on Optimism</li>
        <li>
          Explore the voting realm{' '}
          <Link
            className={styles.link}
            href="https://vote.ethereumzuri.ch/"
            target="_blank"
            rel="noopener noreferrer"
          >
            vote.ethereumzuri.ch
          </Link>{' '}
          and vote on some fun questions first to get used to the quadratic voting experience
        </li>
      </ol>
      <span className={styles.howItWorks__innerTitle}>
        After project submissions close (Sunday 11am)
      </span>
      <ol className={styles.howItWorks__list}>
        <li>
          Check out all submissions at{' '}
          <Link
            className={styles.link}
            href="https://ethereumzurich.devfolio.co/projects"
            target="_blank"
            rel="noopener noreferrer"
          >
            ethereumzurich.devfolio.co&#47;projects
          </Link>
          <Image alt="basketball" src={basketball} className={styles.basketball} />
          <Image alt="glasses" src={glasses} className={styles.glasses} />
        </li>
        <li>
          Vote for selected projects at{' '}
          <Link
            className={styles.link}
            href="https://vote.ethereumzuri.ch/"
            target="_blank"
            rel="noopener noreferrer"
          >
            vote.EthereumZuri.ch
          </Link>{' '}
          using your unique EthereumZuri.ch non-transferable token
        </li>
        <li>Wait for the big winner reveal at the EthereumZuri.ch Closing Ceremony 3:30 PM</li>
      </ol>
    </section>
  );
}
