import Container from 'components/Ui/Container/Container.component';
import globeIcon from 'images/globe.svg';
import mintLogo from 'images/mintLogo1.svg';
import mobileMintLogo from 'images/mint_mobile.svg';
import telegramIcon from 'images/telegram-white.svg';
import twitterIcon from 'images/twitter.svg';
import Image from 'next/image';
import Link from 'next/link';
import styles from './Header.module.scss';

const EthZurichHeader = ({ twitterLink, websiteLink, telegramLink }) => {
  return (
    <Container className={styles.mainContainer}>
      <header className={styles.header}>
        <div className={styles.logo}>
          <Link href="/" className={styles.link}>
            <div className={styles.image}>
              <Image alt="mint-logo" src={mintLogo} className={styles.logo} />
            </div>
          </Link>
          <Link href="/" className={styles.link}>
            <div className={styles.mobileImage}>
              <Image alt="mint-logo" src={mobileMintLogo} className={styles.logo} />
            </div>
          </Link>
        </div>
        <div className={styles.container}>
          <ul className={styles.navlinks}>
            <li className={styles.linkContainer}>
              <Link
                className={styles.link}
                href={twitterLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image src={twitterIcon} alt="Twitter" />
              </Link>
            </li>
            <li className={styles.linkContainer}>
              <Link
                className={styles.link}
                href={websiteLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image src={globeIcon} alt="Website" />
              </Link>
            </li>
            <li className={styles.linkContainer}>
              <Link
                className={styles.link}
                href={telegramLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image src={telegramIcon} alt="telegram" />
              </Link>
            </li>
          </ul>
        </div>
      </header>
    </Container>
  );
};

export default EthZurichHeader;
