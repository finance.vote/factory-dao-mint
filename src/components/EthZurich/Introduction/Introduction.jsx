import logo from 'images/ethZurich/EZ_Logo.svg';
import greenTunnel from 'images/ethZurich/tunnel.svg';
import ultima from 'images/ethZurich/ultima.svg';
import Image from 'next/image';
import styles from './Introduction.module.scss';

const Introduction = ({ isMintLoading }) => {
  return (
    <div className={styles.container}>
      <div className={styles.logo}>
        <Image src={logo} alt="eth-zurich-logo" />
      </div>
      <div className={styles.title}>ethereum zÜri.ch</div>
      <div className={styles.title}>Conference & Hackathon</div>
      <div className={styles.author}>by UZH Blockchain Center</div>
      <div className={styles.description}>
        <div>Mint Your Non-Transferable Token To Vote For The EthereumZuri.ch</div>
        <div>Main Prize And Decide Which Projects Should Win!</div>
        <div>Every Vote Matters!</div>
        {!isMintLoading ? (
          <>
            <Image alt="ultima" src={ultima} className={styles.ultimaIcon} />
            <Image alt="greenTunnel" src={greenTunnel} className={styles.greenTunnelIcon} />
          </>
        ) : null}
      </div>
    </div>
  );
};

export default Introduction;
