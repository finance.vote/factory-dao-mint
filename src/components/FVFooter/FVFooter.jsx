import Container from 'components/Ui/Container/Container.component';
import mediumIcon from 'images/medium.svg';
import telegramIcon from 'images/telegram.svg';
import twitterIcon from 'images/tweet.svg';
import Image from 'next/image';
import styles from './FVFooter.module.scss';

const FVFooter = () => {
  return (
    <footer className={styles['app-footer']}>
      <Container>
        <div className={styles['app-footer__container']}>
          <div className="">
            2022 All rights reserved &nbsp;
            <a href="https://finance.vote" target="_blank" rel="noreferrer">
              finance.vote
            </a>
          </div>
          <div className={styles['app-footer__links']}>
            <a href="https://t.me/financedotvote" target="_blank" rel="noreferrer">
              <Image src={telegramIcon} alt="Telegram" />
            </a>
            &nbsp;
            <a href="https://twitter.com/financedotvote" target="_blank" rel="noreferrer">
              <Image src={twitterIcon} alt="Twitter" />
            </a>
            &nbsp;
            <a href="https://medium.com/@financedotvote" target="_blank" rel="noreferrer">
              <Image src={mediumIcon} alt="Medium" />
            </a>
          </div>
        </div>
      </Container>
    </footer>
  );
};

export default FVFooter;
