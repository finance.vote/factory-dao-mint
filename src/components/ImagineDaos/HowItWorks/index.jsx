import plus2Icon from 'images/imagineDaos/plus2.svg';
import plusImg from 'images/imagineDaos/plus3.svg';
import Image from 'next/image';
import styles from './HowItWorks.module.scss';

const HowItWorks = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.header}>
        What it&lsquo;s all about
        <Image
          className={styles.plusIcon}
          src={plus2Icon}
          height={40}
          width={40}
          alt="image plus color 2"
        />
      </h1>
      <div className={styles.daoOptions}>
        <div className={styles.innerContainer}>
          <span className={styles.innerDesc}>
            ImagineDAOs is a Web3 enabled competition designed to accelerate and enhance the
            imagination of people thinking in DAOs. Choose either the free or paid experience. You
            will have four weeks to form, tweak and submit your DAO idea, supported by the
            FactoryDAO team.
          </span>
        </div>
        <div className={styles.innerContainer}>
          <span className={styles.innerDesc}>
            ImagineDAOs is more than just a competition; it&apos;s an incubator for ideas and a
            catalyst for change within the DAO ecosystem. Our mission is to unearth and nurture
            groundbreaking DAO concepts that can contribute to a more equitable, transparent, and
            collaborative digital world.
          </span>
        </div>
      </div>
      <div className={styles.choosePathBox}>
        <Image src={plusImg} width={40} height={40} alt="plus image" className={styles.image1} />
        <Image src={plusImg} width={40} height={40} alt="plus image" className={styles.image2} />
        <h2>Choose Your Path</h2>
        <div className={styles.content}>
          Whether you opt for the Free Track or the enhanced experience of the Paid Track,
          ImagineDAOs offers a unique platform to explore, innovate, and contribute to the evolving
          world of DAOs. Each track is designed to ensure that all participants gain meaningful
          experience, knowledge and feedback on their DAO ideas.
          <br />
          <br />
          <span className={styles.boldText}>
            This competition is not just about winning; it&apos;s about pushing the boundaries of
            what&apos;s possible in the DAO space. You can win money. You can build reputation. You
            can earn trust. You can create a DAO.
          </span>
        </div>
      </div>
    </div>
  );
};

export default HowItWorks;
