import plusIcon from 'images/imagineDaos/plus4.svg';
import Image from 'next/image';
import styles from './Track.module.scss';

const Track = ({ title, subtitle, who, what, benefits, prizes, style }) => {
  return (
    <div className={styles.container} style={style}>
      <div className={styles.content}>
        <Image
          className={styles.icon}
          src={plusIcon}
          height={40}
          width={40}
          alt="image plus white"
        />
        <h1 className={styles.header}>
          <span className={styles.title}>{title}</span>
          {subtitle}
        </h1>

        <h2>Who it&apos;s For:</h2>
        {who}
        <h2>What You Get:</h2>
        {what}
        <h2>Experience and Benefits:</h2>
        {benefits}
      </div>
      <div className={styles.prizesBox}>
        <h2>Prizes:</h2>
        {prizes}
      </div>
    </div>
  );
};

export default Track;
