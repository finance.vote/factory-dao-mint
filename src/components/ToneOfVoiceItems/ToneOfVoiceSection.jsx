import tone1 from 'images/sovryn/tone1.svg';
import tone2 from 'images/sovryn/tone2.svg';
import tone3 from 'images/sovryn/tone3.svg';
import tone4 from 'images/sovryn/tone4.svg';
import Image from 'next/image';
import styles from './ToneOfVoiceSection.module.scss';

const items = [
  {
    icon: tone1,
    text: 'Direct and in-your-face'
  },
  {
    icon: tone2,
    text: 'Fast and concise information on key points'
  },
  {
    icon: tone3,
    text: 'Inviting and convincing'
  },
  {
    icon: tone4,
    text: 'Excitement, escapism, status, greed'
  }
];

const ToneOfVoiceSection = () => {
  return (
    <div className={styles.container}>
      <div className={styles.title}>TONE OF VOICE</div>
      {items.map((item, index) => {
        return (
          <div className={styles.item} key={index}>
            <Image src={item.icon} alt={`tone-voice${index + 1}`} />
            <div>
              <p>{item.text}</p>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ToneOfVoiceSection;
