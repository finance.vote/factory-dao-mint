import styles from './Button.module.scss';

const Button = ({ children, onClick, variant, href, newClass }) => {
  return variant === 'link' ? (
    <a className={newClass ? newClass : styles.green} href={href} rel="noopener noreferer">
      {children}
    </a>
  ) : (
    <button onClick={onClick} className={styles.green}>
      {children}
    </button>
  );
};

export default Button;
