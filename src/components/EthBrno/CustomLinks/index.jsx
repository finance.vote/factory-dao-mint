import matrixIcon from 'images/ethberlin/matrix-logo-white.svg';
import globeIcon from 'images/globe.svg';
import twitterIcon from 'images/twitter.svg';
import Image from 'next/image';

const CustomLinks = ({ styles }) => {
  return (
    <ul className={styles.navlinks}>
      <li className={styles.linkContainer}>
        <a
          className={styles.link}
          href="https://twitter.com/ethbrno"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={twitterIcon} alt="Twitter" />
        </a>
      </li>
      <li className={styles.linkContainer}>
        <a
          className={styles.link}
          href="https://ethbrno.cz/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={globeIcon} alt="Website" />
        </a>
      </li>
      <li className={styles.linkContainer}>
        <a
          className={styles.link}
          href="https://matrix.to/#/#ethbrno:gwei.cz"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={matrixIcon} alt="matrix" />
        </a>
      </li>
    </ul>
  );
};

export default CustomLinks;
