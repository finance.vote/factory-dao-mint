import camera from 'images/ethBrno/camera.svg';
import logo from 'images/ethBrno/EthBrno_logo.svg';
import strip from 'images/ethBrno/strip.svg';
import Image from 'next/image';
import styles from './Preludium.module.scss';
const EthBrnoPreludium = () => {
  return (
    <section className={styles.container}>
      <Image alt="ethbrno_logo" src={logo} className={styles.logo} />
      <span className={styles.logoDesc}>privacy & security edition</span>
      <div className={styles.strip}>
        <Image alt="ethbrno_strip" src={strip} className={styles.stripLogo} />
      </div>
      <Image alt="ethbrno_camera" src={camera} className={styles.camera} />
      <span className={styles.description}>
        Mint your non-transferable token to vote for
        <br />
        The ETHBrno² Main prize and decide which
        <br />
        projects should win! Every vote matters!
      </span>
    </section>
  );
};

export default EthBrnoPreludium;
