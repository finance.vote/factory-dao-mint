import city from 'images/ethBrno/city.svg';
import Image from 'next/image';
import React from 'react';
import styles from './how-it-works.module.scss';
const CONTENT_FIRST = [
  { text: 'Scan the QR code you received with your wristband at the entrance' },
  { text: 'Mint your unique voting non-transferrable token on Optimism' },
  {
    text: 'Explore the voting realm <a href="https://vote.ethbrno.cz" target="_blank" style="color: rgba(255, 255, 255, 0.75);">vote.ethbrno.cz</a> and vote on some fun questions first to get used to the quadratic voting experience'
  },
  {
    text: 'Get test tokens on Goerli or Sepolia network using <a href="https://faucet.ethbrno.cz" target="_blank" style="color: rgba(255, 255, 255, 0.75);">faucet.ethbrno.cz</a>'
  }
];
const CONTENT_SECOND = [
  {
    text: 'Check out all submissions at <a href="https://ethbrno.devfolio.co/projects" target="_blank" style="color: rgba(255, 255, 255, 0.75);">ethbrno.devfolio.co/projects</a>'
  },
  {
    text: 'Vote for selected projects at <a href="https://vote.ethbrno.cz" target="_blank" style="color: rgba(255, 255, 255, 0.75);">vote.ethbrno.cz</a> using your unique ETHBrno² non-transferable token'
  },
  {
    text: 'Wait for the big winner reveal at the ETHBrno² Closing Ceremony 4PM'
  }
];
const City = () => {
  return (
    <div className={styles.cityContainer}>
      <Image alt="_city" src={city} className={styles.city} />
    </div>
  );
};
const HowItWorks = () => {
  return (
    <section className={styles.container}>
      <div className={styles.contentBox}>
        <h2 className={styles.title}>How it works</h2>
        <div className={styles.content}>
          <header className={styles.contentHeader}>## During the hackathon</header>
          <ol className={styles.list}>
            {CONTENT_FIRST.map((item, index) => {
              return (
                <li
                  className={styles.listItem}
                  key={index}
                  dangerouslySetInnerHTML={{ __html: item.text }}
                ></li>
              );
            })}
          </ol>
          <header className={styles.contentHeader}>
            ## After project submissions close (Sunday 11am)
          </header>
          <ol className={styles.list}>
            {CONTENT_SECOND.map((item, index) => {
              return (
                <li
                  className={styles.listItem}
                  key={index}
                  dangerouslySetInnerHTML={{ __html: item.text }}
                ></li>
              );
            })}
          </ol>
        </div>
      </div>
      <City />
    </section>
  );
};

export default HowItWorks;
