import styles from './Introduce.module.scss';

const Introduce = () => {
  return (
    <div className={styles.container}>
      <span className={styles.title}>Transformation DAO</span>
      <span className={styles.description}>A DAO for human transformation</span>
    </div>
  );
};

export default Introduce;
