import Button from 'components/Button/Button.component';
import styles from './MintingClosed.module.scss';

const MintingClosed = () => {
  return (
    <div className={styles.card}>
      <h3 className={styles.title}>The Public Mint is Closed</h3>
      <p>When can you mint again?</p>
      <p>When the next Shibaku Mint Party happens!</p>
      <p>
        Join our Discord to keep up to date on developments, mint party dates and particiapte in
        games and competitions
      </p>
      <Button variant="link" href={'https://discord.gg/tribesdao'}>
        Join discord
      </Button>
    </div>
  );
};

export default MintingClosed;
