import loaderImg from 'images/loader.svg';
import Image from 'next/image';

/**
 * Small loader
 */
const Loader = ({ maxWidth }) => (
  <Image style={{ maxWidth: maxWidth || 37 }} src={loaderImg} alt="Loading..." />
);

export default Loader;
