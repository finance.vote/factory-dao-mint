import styles from './HowItWorks.module.scss';

const points = [
  'Mint your free OFF/GRID SBT above to submit your DAO idea and vote.',
  `On June 7th you must submit your DAO idea <a href='https://influence.factorydao.org/offgrid' target='_blank'>here</a>`,
  `On the 7th at 4pm the vote will open <a href='https://influence.factorydao.org/offgrid' target='_blank'>here</a> which will determine the winners.`,
  'The top prize will be $1500 and runners up will share $500 based on their vote weight at the end of the day.',
  'Participants will be sent an “ImagineDAOs” pack that will guide you in your DAO designing and help you come up with your DAO idea.',
  'The brief is entirely open. DAOs are a blank slate for human imagination.'
];

const HowItWorks = () => {
  return (
    <div className={styles.container}>
      <div className={styles.videoArea}>
        <video className={styles.video} loop autoPlay muted src="/DAOs.mp4"></video>
      </div>
      <div className={styles.textArea}>
        <div className={styles.header}>How it works</div>
        <div className={styles.subHeader}>
          You should come to Prague and join us in person, but if you can’t, you can still
          participate in the DAOathon and have a chance of winning a share of $2000 prize.
        </div>
        <div className={styles.points}>
          {points.map((point, index) => {
            return (
              <div className={styles.point} key={index}>
                <div className={styles.index}>{`${index + 1}.`}</div>
                <div dangerouslySetInnerHTML={{ __html: point }}></div>
              </div>
            );
          })}
        </div>
      </div>
      <div></div>
    </div>
  );
};

export default HowItWorks;
