import logo from 'images/offgird/off-grid_logo.png';
import Image from 'next/image';
import styles from './Introduction.module.scss';

const Introduction = () => {
  return (
    <div className={styles.container}>
      <div className={styles.logo}>
        <Image src={logo} alt="off-gird-logo" />
      </div>
      <div className={styles.header}>
        Unleashing the Potential of Decentralized Autonomous Organizations
      </div>
      <div className={styles.text}>
        OFF/GRID is an experiment in autonomy & collective intelligence. <br />
        We will converge in one of the most exciting spaces in Prague for a day and night of DAO
        shenanigans.
        <br />
        Our goal is to learn & share our ideas and practices from across the DAO and cryptospace &
        engage in deep <br />
        Critical discussion & collaboration on how to drive the DAO space forward.
      </div>
    </div>
  );
};

export default Introduction;
