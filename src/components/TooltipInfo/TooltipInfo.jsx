import classNames from 'classnames';
import Image from 'next/image';
import infoIcon from '../../images/info.svg';
import styles from './TooltipInfo.module.scss';

const TooltipInfo = ({
  title,
  children,
  tooltipPositionClass,
  tooltipIcon = infoIcon,
  tooltipContentClass
}) => {
  return (
    <div className={`${styles.tooltipContainer} ${tooltipPositionClass}`}>
      <Image className={styles.infoTooltipIcon} src={tooltipIcon} alt="info-icon" />
      <div className={classNames(styles.infoTooltipContent, tooltipContentClass)}>
        <div className={styles.content}>
          <span className={classNames(styles.title, tooltipContentClass)}>{title}</span>
          <div className={classNames(styles.text, tooltipContentClass)}>{children}</div>
        </div>
        <div className={classNames(styles.arrow, tooltipContentClass)} />
      </div>
    </div>
  );
};

export default TooltipInfo;
