import Container from 'components/Ui/Container/Container.component';
import logo from 'images/nfticket/nfticket.svg';
import Image from 'next/image';
import Link from 'next/link';
import styles from './Header.module.scss';

const Header = () => {
  return (
    <Container className={styles.mainContainer}>
      <header className={styles.header}>
        <div className={styles.logo}>
          <Link href="/" className={styles.link}>
            <div className={styles.image}>
              <Image alt="mint-logo" src={logo} className={styles.logo} />
            </div>
          </Link>
          <Link href="/" className={styles.link}>
            <div className={styles.mobileImage}>
              <Image alt="mint-logo" src={logo} className={styles.logo} />
            </div>
          </Link>
        </div>
        <div className={styles.container}>
          <Link
            variant="link"
            href="https://linktr.ee/bloXmove"
            className={styles.bloxMoveLinksButton}
            target="_blank"
          >
            bloXmove
          </Link>
        </div>
      </header>
    </Container>
  );
};

export default Header;
