import access from 'images/myna/access.svg';
import exclusive from 'images/myna/exclusive.svg';
import newsletters from 'images/myna/newsletters.svg';
import Image from 'next/image';
import styles from './Benefits.module.scss';

const items = [
  {
    name: 'access',
    imgSrc: access,
    description: 'Access to the exclusive Myna Discord Community'
  },
  {
    name: 'exclusive',
    imgSrc: exclusive,
    description: 'Exclusive discounts on events and select NFTs mints'
  },
  {
    name: 'premium',
    imgSrc: newsletters,
    description: 'Premium content: seminars, newsletters and reports'
  }
];
const Benefits = () => {
  return (
    <>
      <div className={styles.container}>
        <span className={styles.title}>benefits</span>
        {items.map((item, index) => {
          return (
            <div key={index} className={styles.benefitsItem}>
              <Image src={item.imgSrc} alt={item.name} />
              <span className={styles.description}>{item.description}</span>
            </div>
          );
        })}
      </div>
      <div className={styles.haveSay}>
        <span className={styles.title}>have a say!</span>
        <span className={styles.descSay}>
          20% of the funds from the mint will be sent to a community safe, holders of the NFTs will
          be able to vote on what community activities it should be spent on, like parties, events
          or merch!
        </span>
      </div>
    </>
  );
};

export default Benefits;
