import Container from 'components/Ui/Container/Container.component';
import mintLogo from 'images/mintLogo1.svg';
import mobileMintLogo from 'images/mint_mobile.svg';
import discordIcon from 'images/myna/discord.svg';
import globeIcon from 'images/myna/globe.svg';
import twitterIcon from 'images/myna/twitter.svg';
import Image from 'next/image';
import Link from 'next/link';
import styles from './Header.module.scss';

const MynaHeader = ({ twitterLink, websiteLink, discordLink }) => {
  return (
    <Container className={styles.mainContainer}>
      <header className={styles.header}>
        <div className={styles.logo}>
          <Link href="/" className={styles.link}>
            <div className={styles.image}>
              <Image alt="mint-logo" src={mintLogo} className={styles.logo} />
            </div>
          </Link>
          <Link href="/" className={styles.link}>
            <div className={styles.mobileImage}>
              <Image alt="mint-logo" src={mobileMintLogo} className={styles.logo} />
            </div>
          </Link>
        </div>
        <div className={styles.container}>
          <ul className={styles.navlinks}>
            <li className={styles.linkContainer}>
              <a
                className={styles.link}
                href={twitterLink || 'https://twitter.com/FactDAO'}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image src={twitterIcon} alt="Twitter" />
              </a>
            </li>
            <li className={styles.linkContainer}>
              <a
                className={styles.link}
                href={websiteLink || '/'}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image src={globeIcon} alt="Website" />
              </a>
            </li>
            <li className={styles.linkContainer}>
              <a
                className={styles.link}
                href={discordLink || 'http://discord.gg/factorydao'}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image src={discordIcon} alt="Discord" />
              </a>
            </li>
            {/* <li className={classNames(styles.linkContainer, styles.mobileOnly)}>
              <button className={styles.link} onClick={onClickHamburger}>
                <Image src={hamburgerIcon} alt="Discord" />
              </button>
            </li> */}
          </ul>
        </div>
      </header>
    </Container>
  );
};

export default MynaHeader;
