import styles from './Services.module.scss';
const Services = () => {
  return (
    <div className={styles.container}>
      <span className={styles.title}>the nft key to professional services</span>
      <span className={styles.description}>
        FactoryDAO in collaboration with Myna will be building the first of it’s kind “Service NFT”
        system. The Service NFT is designed to allow crypto natives the ability to pay for
        professional services in crypto and keep track of the services they’ve paid for all using
        their wallet.
        <br />
        <br /> Holders will be able to subscribe to tax services, book consultations, pay for events
        with discounts and all through one portal. Members will be able to pay in crypto, prove to
        Myna that they have paid and get direct tax advice, all without revealing their identity.
      </span>
      <br />
      <br />
      <span className={styles.summary}>
        Your NFT will become the representation of your membership, service tier, participation and
        more within the Myna Members Club.
      </span>
    </div>
  );
};

export default Services;
