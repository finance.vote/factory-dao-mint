import styles from './TribIntroduce.module.scss';

const TribIntroduce = () => {
  return (
    <div className={styles.container}>
      <div className={styles.infoText}>
        <div>
          TRiB3.0 is a platform for brands, designers, creators, and stylists to explore fresh ideas
          in physical and digital spaces. Our genesis drop of 500 1/1 NFTs has 100 creatives from
          over 40 countries offering 5 digital collectibles each. The TRiBE3.0 community encourages
          our members to develop, grow and share imaginative new projects.
        </div>
        <div>
          We aim to introduce new upcoming designers, alongside established brands to a new WEB3
          audience. Uplifting, encouraging and developing new approaches to fashion in a digital
          landscape.
        </div>
        <div>
          The TRiB3.0 genesis drop is only the beginning of a new and exciting journey into WEB3 for
          collectors and creatives alike. We encourage you to join the TRiB3.0 Fashion Forum and be
          a part of a new digital revolution.
        </div>
      </div>
    </div>
  );
};

export default TribIntroduce;
