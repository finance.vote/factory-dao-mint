import TribTextHeader from '../TextHeader/TribTextHeader';
import styles from './TribInformation.module.scss';

const TribInformation = () => {
  return (
    <div className={styles.container}>
      <div className={styles.headerText}>
        <TribTextHeader text="Key Information" />
      </div>
      <div>
        <div className={styles.infoText}>
          <div>
            In order to gain access to the genesis drop you are required to purchase an Access
            Ticket. This will give you access to choose a 1/1 NFT. You can purchase up to 3 Access
            Tickets per wallet.
          </div>
          <div>
            These will be dropped in 4 volumes with 125 pieces dropped per volume, the first
            dropping as soon as all 500 Access Passes have sold. Subsequent volumes will be released
            one week at a time, so you may wish to wait for the perfect piece. You will be required
            to return to this website to mint your piece.
          </div>
          <div>
            You will be required to burn your Access Ticket to mint your chosen digital collectable
            (Please note gas fees will apply).
          </div>
          <div>
            Holders of a TRiB3.0 NFT will be granted access to the TRiB3.0 Design Forum can expect
            exclusive merchandise, branded air drops, unique IRL events, VR experiences and will
            become members of the TRiB3.0 DAO.
          </div>
          <div>
            TRiB3.0 is an environmentally conscious space; for every sale we will plant 500 trees.
          </div>
        </div>
      </div>
    </div>
  );
};

export default TribInformation;
