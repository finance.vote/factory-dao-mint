import fabricRightMobile from 'images/trib3/fabric-right-mobile-new.png';
import Image from 'next/image';
import styles from './TribVideo.module.scss';

const TribVideo = () => {
  return (
    <div className={styles.container}>
      <Image className={styles.fabricMobile} src={fabricRightMobile} alt="left-fabric-mobile" />
      <iframe
        title="tib3-video"
        src="https://player.vimeo.com/video/734315856?h=2f468cf4cf"
        frameBorder="0"
        allow="autoplay; fullscreen; picture-in-picture"
        allowFullScreen
      />
    </div>
  );
};

export default TribVideo;
