import discordIcon from 'images/trib3/discord.svg';
import globeIcon from 'images/trib3/globe.svg';
import instagramIcon from 'images/trib3/instagram.svg';
import twitterIcon from 'images/trib3/twitter.svg';
import Image from 'next/image';
import styles from './TribSocials.module.scss';

export const TRIB3_CONFIG = {
  DISCORD_LINK: 'https://discord.gg/trib3',
  WEBSITE_LINK: 'https://www.trib3.one/',
  TWITTER_LINK: 'https://twitter.com/TRiB3_NFT',
  INSTAGRAM_LINK: 'https://www.instagram.com/trib3.0/'
};

export const socialItems = [
  {
    link: TRIB3_CONFIG.TWITTER_LINK,
    icon: twitterIcon,
    alt: 'Twitter'
  },
  {
    link: TRIB3_CONFIG.WEBSITE_LINK,
    icon: globeIcon,
    alt: 'Website'
  },
  {
    link: TRIB3_CONFIG.DISCORD_LINK,
    icon: discordIcon,
    alt: 'Discord'
  },
  {
    link: TRIB3_CONFIG.INSTAGRAM_LINK,
    icon: instagramIcon,
    alt: 'Instagram'
  }
];

export const SingleSocial = ({ className, icon, link, alt }) => {
  return (
    <a className={className} href={link} target="_blank" rel="noopener noreferrer">
      <Image src={icon} alt={alt} height={26} width={26} />
    </a>
  );
};

const TribSocials = () => {
  return (
    <div className={styles.container}>
      {socialItems.map(({ icon, link, alt }) => {
        return <SingleSocial className={styles.item} key={alt} icon={icon} link={link} alt={alt} />;
      })}
    </div>
  );
};

export default TribSocials;
