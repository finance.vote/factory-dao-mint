import fabricImg from 'images/trib3/FABRIC_left.svg';
import Image from 'next/image';
import styles from './TribFabricLeft.module.scss';

const TribFabricLeft = () => {
  return (
    <div className={styles.container}>
      <Image className={styles.fabric} src={fabricImg} alt="left-fabric" />
    </div>
  );
};

export default TribFabricLeft;
