import Container from 'components/Ui/Container/Container.component';
import mobileMintLogo from 'images/myna/mint_mobile.svg';

import Image from 'next/image';
import Link from 'next/link';
import { SingleSocial, socialItems } from '../Social/TribSocials';
import styles from './TribHeader.module.scss';

const TribHeader = () => {
  return (
    <Container className={styles.mainContainer}>
      <header className={styles.header}>
        <div className={styles.logo}>
          <Link href="/" className={styles.link}>
            <div className={styles.mobileImage}>
              <Image alt="mint-logo" src={mobileMintLogo} className={styles.logo} />
            </div>
          </Link>
        </div>
        <div className={styles.container}>
          <ul className={styles.navlinks}>
            {socialItems.map(({ icon, link, alt }, index) => {
              return (
                <li className={styles.linkContainer} key={index}>
                  <SingleSocial icon={icon} link={link} alt={alt} className={styles.link} />
                </li>
              );
            })}
          </ul>
        </div>
      </header>
    </Container>
  );
};

export default TribHeader;
