import separatorImg from 'images/trib3/Fabric_separator.png';
import Image from 'next/image';
import styles from './TribSeparator.module.scss';

const TribSeparator = () => {
  return (
    <div className={styles.container}>
      <Image className={styles.separator} src={separatorImg} alt="fabric-separator" />
    </div>
  );
};

export default TribSeparator;
