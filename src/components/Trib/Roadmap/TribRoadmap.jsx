import dot from 'images/trib3/Trib3_dot.png';
import Image from 'next/image';
import TribTextHeader from '../TextHeader/TribTextHeader';
import styles from './TribRoadmap.module.scss';

const roadmapItems = [
  'JULY 29TH – ACCESS TICKET GOES ON SALE',
  '1 WEEK AFTER TICKETS SELL OUT - VOLUME 1 RELEASE',
  'TBC - VOLUME 2 RELEASE',
  'TBC - VOLUME 3 RELEASE',
  'TBC - VOLUME 4 RELEASE',
  'The DAO launches'
];

const RoadmapItem = ({ text }) => {
  return (
    <div className={styles.roadmapItem}>
      <div>
        <Image alt="list-dot" src={dot} />
      </div>
      <div>{text}</div>
    </div>
  );
};

const TribRoadmap = () => {
  return (
    <div className={styles.container}>
      <div className={styles.headerText}>
        <TribTextHeader text="Roadmap" />
      </div>
      {roadmapItems.map((text, index) => {
        return <RoadmapItem key={`roadmap-item-${index}`} text={text} />;
      })}
    </div>
  );
};

export default TribRoadmap;
