import styles from './TribTextHeader.module.scss';

const TribTextHeader = ({ text }) => {
  return <span className={styles.text}>{text}</span>;
};

export default TribTextHeader;
