import fabricImg from 'images/trib3/FABRIC-LAST.png';
import Image from 'next/image';
import styles from './TribLastFabric.module.scss';

const TribLastFabric = () => {
  return (
    <div className={styles.container}>
      <Image className={styles.fabric} src={fabricImg} alt="last-fabric" />
    </div>
  );
};

export default TribLastFabric;
