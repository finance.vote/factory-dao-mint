import tribLogo from 'images/trib3/Trib3_logo.png';
import Image from 'next/image';
import TribTextHeader from '../TextHeader/TribTextHeader';
import styles from './Homepage.module.scss';

const TribLogo = () => {
  return (
    <>
      <div className={styles.wrap}>
        <div>
          <Image className={styles.logo} alt="trib3-logo" src={tribLogo} />
        </div>
        <div>
          <div className={styles.headerText}>
            <TribTextHeader text="" />
          </div>
        </div>
      </div>
    </>
  );
};

export default TribLogo;
