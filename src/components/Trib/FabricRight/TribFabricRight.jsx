import fabricImg from 'images/trib3/FABRIC_right.svg';
import Image from 'next/image';
import styles from './TribFabricRight.module.scss';

const TribFabricRight = () => {
  return (
    <div className={styles.container}>
      <Image className={styles.fabric} src={fabricImg} alt="left-fabric" />
    </div>
  );
};

export default TribFabricRight;
