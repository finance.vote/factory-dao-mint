import discordIcon from 'images/discord.svg';
import twitterIcon from 'images/twitter.svg';
import Image from 'next/image';
import styles from './Footer.module.scss';

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <h3 className={styles.title}>ENTER THE DAO</h3>
      <span className={styles.subtitle}>SHIBAKU grants you access to TribesDAO</span>
      <div className={styles.iconContainer}>
        <a
          className={styles.link}
          href="https://discord.gg/tribesdao"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={discordIcon} alt="Discord" />
        </a>
        <a
          className={styles.link}
          href="https://twitter.com/ShibakuTribe"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={twitterIcon} alt="Twitter" />
        </a>
      </div>
    </footer>
  );
};

export default Footer;
