import classNames from 'classnames';
import NFTAuctionFactory from 'contracts/NFTAuctionFactory.json';
import { ConnectWallet, ethInstance, fromWei } from 'evm-chain-scripts';
import { Form, Formik } from 'formik';
import { useGlobalState } from 'globalState';
import { useEffect, useState } from 'react';
import * as Yup from 'yup';
import NftImage from '../NftImage';

import styles from './BidForm.module.scss';

const validationSchema = Yup.object({
  isAgreement: Yup.bool().oneOf([true], 'Please agree to the terms before place bid')
});

const BidForm = ({ usdPriceOfEth, currentPrice, lastAuctionBid, chainId, isNftSold }) => {
  const [address, setAddress] = useGlobalState('walletAddress');
  const [currentPriceEth, setCurrentPriceEth] = useState(0);
  const [currentPriceUsd, setCurrentPriceUsd] = useState(0);
  const [decay, setDecay] = useState(0);

  const handleLoad = async () => {
    try {
      const ethAccount = await ethInstance.getEthAccount(false);
      setAddress(ethAccount);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    if (!currentPrice) return;
    const currentPriceEthValue = Number(fromWei(currentPrice));
    const currentPriceEth = currentPriceEthValue.toFixed(5) || 0; // additional parseFloat for cutting zeros ex. 12.450000 => 12.45
    const currentEthToUsd = (currentPriceEthValue * usdPriceOfEth || 0).toFixed(2);
    setCurrentPriceEth(currentPriceEth);
    setCurrentPriceUsd(currentEthToUsd);
  }, [currentPrice, usdPriceOfEth]);

  useEffect(() => {
    if (!lastAuctionBid) return;
    const lastBidAmount = lastAuctionBid ? Number(fromWei(lastAuctionBid)) : 0;
    const currentPriceEthValue = Number(fromWei(currentPrice));
    const decay = (lastBidAmount - currentPriceEthValue) / 2 + currentPriceEthValue;
    const decayUsd = (decay * usdPriceOfEth).toFixed(2);
    setDecay(decayUsd);
  }, [lastAuctionBid, currentPrice, usdPriceOfEth]);

  const onSubmit = async () => {
    try {
      const contract = await ethInstance.getContract('write', NFTAuctionFactory);
      const currentAuctionId = await contract.currentAuctionId();
      await contract.bid(currentAuctionId, { value: currentPrice });
    } catch (err) {
      console.log('error', err);
    }
  };

  return (
    <Formik
      initialValues={{ isAgreement: false }}
      onSubmit={onSubmit}
      validateOnBlur
      validateOnChange
      validationSchema={validationSchema}
    >
      {({ errors, touched, getFieldProps }) => {
        return (
          <Form className={styles.bidFormContainer}>
            <NftImage chainId={chainId} />
            <div className={styles.bidForm}>
              <div className={styles.valuesContainer}>
                <div className={styles.singleValue}>
                  <span className={styles.value} name="price">
                    $ {currentPriceUsd}
                  </span>
                  <span className={styles.label}>current price</span>
                </div>
                <div className={styles.singleValue}>
                  <span className={styles.value} name="identityDecayFactor" value={currentPriceEth}>
                    {currentPriceEth} ETH
                  </span>
                  <span className={styles.label}>approx.</span>
                </div>
              </div>
              <span className={styles.priceDecay}>price decay : $ {decay}/min</span>
              <div className={styles.inputGroupContainer}>
                <span className={styles.myBid}>my bid</span>
                <div className={styles.inputGroup}>
                  <span className={styles.bidInput}>{`$ ${currentPriceUsd}`}</span>
                  <span className={styles.price}>{currentPriceEth + ' ETH'}</span>
                </div>
              </div>
              <label className={styles.agreement}>
                <input type="checkbox" {...getFieldProps('isAgreement')} />
                <span className={styles.agreementText}>
                  I understand that by placing a bid I am committing to purchasing this item at the
                  &quot;max spend&quot; entered above
                </span>
              </label>
              {touched.isAgreement && errors.isAgreement && (
                <p className={styles.error}>{errors.isAgreement}</p>
              )}
              <div className={styles.buttonContainer}>
                {!address?.length ? (
                  <ConnectWallet
                    className={classNames(styles.placeBidButton)}
                    onConnect={handleLoad}
                  />
                ) : (
                  <button type="submit" className={styles.placeBidButton} disabled={isNftSold}>
                    Place bid
                  </button>
                )}
              </div>
              {!window.ethereum && (
                <div>
                  <div>
                    <div type="warning">
                      No wallet detected. Please install Metamask in order to mint an identity.
                    </div>
                  </div>
                </div>
              )}
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default BidForm;
