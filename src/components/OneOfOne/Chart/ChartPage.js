import Loader from 'components/Loader';
import dynamic from 'next/dynamic';
import styles from './ChartPage.module.scss';
import { generateChartSet } from './helpers';
import Legend from './Legend';

const AuctionChart = dynamic(() => import('./AuctionChart'), { ssr: false });

const ChartPage = ({ isChartLoading, allEvents }) => {
  const isServer = typeof window === 'undefined';
  const generateDataSets = (allEvents) => {
    if (!allEvents) return;
    const datasets = [];
    datasets.push(generateChartSet(allEvents));
    return datasets;
  };

  return (
    <div className={styles.chartPageContainer}>
      {isChartLoading ? (
        <div className={styles.loader}>
          <Loader maxWidth="200px" />
        </div>
      ) : (
        <>
          {!isServer && <AuctionChart chartData={{ datasets: generateDataSets(allEvents) }} />}
          <Legend />
        </>
      )}
    </div>
  );
};

export default ChartPage;
