const SCSS_VARS = {
  transparent: 'transparent',
  orange: '#e0a855',
  green: '#50f568'
};

export const generateChartSet = (values) => {
  return {
    // label: 'scatterPlot',
    type: 'scatter',
    data: values,
    pointBackgroundColor: (point) => {
      if (point.raw?.event === 'bid') {
        return SCSS_VARS.orange;
      }
      if (point.raw?.event === 'sold') {
        return SCSS_VARS.green;
      }
      return SCSS_VARS.transparent;
    },
    pointStyle: 'rect',
    pointRadius: 10,
    hoverRadius: 10,
    borderColor: (point) => {
      if (point.raw?.event === 'decay') {
        return SCSS_VARS.orange;
      }
      return SCSS_VARS.transparent;
    }
  };
};
