import classnames from 'classnames';
import styles from './Legend.module.scss';

const GridLegendItem = ({ type, label }) => {
  return (
    <div className={styles.legendItem}>
      <div className={classnames(styles.point, styles[type])}></div>
      <span className={styles.text}>{label}</span>
    </div>
  );
};

const Legend = () => {
  return (
    <div className={styles.legend}>
      <GridLegendItem type="bid" label="bid" />
      <GridLegendItem type="decay" label="decay" />
      <GridLegendItem type="sold" label="sold" />
    </div>
  );
};

export default Legend;
