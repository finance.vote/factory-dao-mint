import {
  BarController,
  BarElement,
  CategoryScale,
  Chart,
  LinearScale,
  LineElement,
  LogarithmicScale,
  PointElement,
  ScatterController,
  Tooltip
} from 'chart.js';
import zoomPlugin from 'chartjs-plugin-zoom';
import { memo, useEffect, useRef } from 'react';
Chart.register(
  ScatterController,
  BarController,
  CategoryScale,
  BarElement,
  LinearScale,
  LogarithmicScale,
  PointElement,
  LineElement,
  Tooltip,
  zoomPlugin
);

const AuctionChart = ({ chartData }) => {
  const Ctx = useRef();
  const chart = useRef();
  const auctionEvents = chartData.datasets?.[0].data;
  useEffect(() => {
    const ctx = Ctx.current.getContext('2d');
    chart.current = new Chart(ctx, {
      type: 'scatter',
      data: chartData,
      options
    });
    return () => {
      chart.current.destroy();
    };
  }, [auctionEvents.length, auctionEvents[auctionEvents.length - 1]?.event]);

  const options = {
    responsive: true,
    parsing: {
      xAxisKey: 'x',
      yAxisKey: 'y'
    },
    scales: {
      y: {
        ticks: {
          color: '#8f8f8f',
          font: {
            size: 14,
            family: 'Work Sans'
          },
          // stepSize: 2
          callback: (tickValue) => `$ ${tickValue.toFixed(2)}`
        },
        border: {
          color: 'white'
        },
        grid: {
          display: false
        },
        type: 'linear'
      },
      x: {
        border: {
          color: 'white'
        },
        ticks: {
          display: false
        },
        grid: {
          display: false
        },
        offset: true
      }
    },
    plugins: {
      zoom: {
        pan: {
          enabled: true,
          mode: 'x',
          threshold: 1,
          onPan: ({ chart }) => chart.update('none')
        },
        zoom: {
          wheel: {
            enabled: true
          },
          pinch: {
            enabled: true
          },
          mode: 'x',
          onZoomComplete: ({ chart }) => chart && chart.update('none'),
          onZoomStart({ chart, event }) {
            const zoomLevel = chart.getZoomLevel();
            if (zoomLevel > 2.1 && event.wheelDelta > 0) return false;
          }
        },
        limits: {
          x: { min: 0, max: 'original' }
        }
      }
    },
    layout: {
      padding: 10
    }
  };

  return (
    <div
      className="chart-container"
      style={{ position: 'relative', minHeight: '500px', width: '45vw' }}
    >
      <canvas
        ref={Ctx}
        style={{
          backgroundColor: 'transparent'
        }}
        height="100%"
        width="100%"
      />
    </div>
  );
};

export default memo(AuctionChart, (prev, next) => JSON.stringify(prev) === JSON.stringify(next));
