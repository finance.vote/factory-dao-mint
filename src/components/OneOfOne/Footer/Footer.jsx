import discordIcon from 'images/discord.svg';
import websiteIcon from 'images/globe.svg';
import twitterIcon from 'images/twitter.svg';
import Image from 'next/image';
import styles from './Footer.module.scss';

const FVFooter = () => {
  return (
    <footer className={styles['app-footer']}>
      <div className={styles['app-footer__container']}>
        <div className="">
          part of the &nbsp;
          <a href="https://finance.vote" target="_blank" rel="noreferrer">
            finance.vote
          </a>
          <span className={styles['app_version']}>v. {process.env.NEXT_PUBLIC_VERSION}</span>
          &nbsp; DAO Suite
        </div>
        <div className={styles['app-footer__links']}>
          &nbsp;
          <a href="https://twitter.com/financedotvote" target="_blank" rel="noreferrer">
            <Image src={twitterIcon} alt="Twitter" />
          </a>
          &nbsp;
          <a href="https://www.factorydao.xyz/" rel="noreferer noopener">
            <Image src={websiteIcon} alt="website_icon" />
          </a>
          &nbsp;
          <a href="http://discord.gg/financedotvote" target="_blank" rel="noreferrer">
            <Image src={discordIcon} alt="Discord" />
          </a>
        </div>
      </div>
    </footer>
  );
};

export default FVFooter;
