import Loader from 'components/Loader';
import If from 'minting-components/components/If/If';
import Image from 'next/legacy/image';
import { useFetchNftImage, useStructNftContract, useStructTokenForSale } from 'queries/oneOfOne';
import oneOfOneImg from './../../../images/10f1.png';
import styles from './NftImage.module.scss';

const NftImage = ({ chainId }) => {
  const { data: tokenForSale } = useStructTokenForSale();
  const { data: nftContract } = useStructNftContract();

  const { data: nftMedia, isFetching: isNftMediaFetching } = useFetchNftImage(
    tokenForSale,
    nftContract,
    chainId
  );

  return (
    <>
      <If condition={nftMedia.mediaUrl && !isNftMediaFetching}>
        <div className={styles.images}>
          <div className={styles.oneOfOne}>
            <Image src={oneOfOneImg} alt="oneOfOne" />
          </div>
          {nftMedia.isVideo ? (
            <video
              autoPlay
              muted
              loop
              playsInline
              className={styles.image}
              style={{
                display: isNftMediaFetching ? 'none' : 'block'
              }}
            >
              <source src={nftMedia.mediaUrl} type="video/mp4" />
            </video>
          ) : (
            <div className={styles.image}>
              <Image
                alt="nft"
                src={nftMedia.mediaUrl}
                className={styles.image}
                style={{
                  display: isNftMediaFetching ? 'none' : 'block'
                }}
                layout="fill"
              />
            </div>
          )}
        </div>
      </If>
      <If condition={isNftMediaFetching}>
        <div className={styles.loaderContainer}>
          <Loader maxWidth="200px" />
        </div>
      </If>
    </>
  );
};

export default NftImage;
