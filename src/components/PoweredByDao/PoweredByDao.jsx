import discordIcon from 'images/discord.svg';
import websiteIcon from 'images/globe.svg';
import powLogo from 'images/sovryn/poweredByLogo.png';
import telegramIcon from 'images/telegram-white.svg';
import twitterIcon from 'images/twitter.svg';
import If from 'minting-components/components/If/If';
import Image from 'next/image';
import styles from './PoweredByDao.module.scss';

const PoweredByDao = ({ hideLinks = false, showDiscord = true, showTelegram = false }) => {
  return (
    <div className={styles.container}>
      <Image className={styles.logo} src={powLogo} alt="factory_dao_logo" />
      <h3 className={styles.poweredTitle}>powered by FactoryDAO</h3>
      <p className={styles.description}>
        If you can dream it, you can DAO it <br />
        Build a DAO now!
      </p>
      {!hideLinks && (
        <div className={styles.socials}>
          <a href="https://twitter.com/FactDAO" rel="noreferer noopener">
            <Image src={twitterIcon} alt="twitter_icon" />
          </a>
          <a href="https://www.factorydao.xyz/" rel="noreferer noopener">
            <Image src={websiteIcon} alt="website_icon" />
          </a>
          <If condition={showDiscord}>
            <a href="https://discord.gg/factorydao" rel="noreferer noopener">
              <Image src={discordIcon} alt="discord_icon" />
            </a>
          </If>
          <If condition={showTelegram}>
            <a href="https://t.me/factdao" rel="noreferer noopener">
              <Image src={telegramIcon} alt="telegram_icon" />
            </a>
          </If>
        </div>
      )}
    </div>
  );
};

export default PoweredByDao;
