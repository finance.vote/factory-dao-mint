import withMergedStyles from 'components/withMergedStyles/withMergedStyles';
import If from 'minting-components/components/If/If';
import Image from 'next/image';
import classes from './Preludium.module.scss';

const BanklessDaoPreludium = ({ image, introductionText, styles, chainName = '' }) => {
  const { src, alt } = image || {};
  return (
    <section className={classes.container}>
      <span>
        The BanklessDAO AMA series dives into community
        <br /> and growth strategies of successful organisations.
      </span>
      <If condition={introductionText}>
        <span className={styles.introduction}>{introductionText}</span>
      </If>
      <If condition={image}>
        <div className={classes.LogoElementFi}>
          <Image src={src} alt={alt} />
        </div>
      </If>
      <span className={classes.description}>
        Mint your free Soul Bound NFT Token {chainName}
        <br /> to suggest questions and vote on the top submissions.
      </span>
    </section>
  );
};

export default withMergedStyles(BanklessDaoPreludium, classes);
