import withMergedStyles from 'components/withMergedStyles/withMergedStyles';
import Image from 'next/image';
import classes from './Preludium.module.scss';

const BanklessDaoPreludium = ({ image, introductionText, styles }) => {
  const { src, alt } = image;
  return (
    <section className={classes.container}>
      <span>
        The BanklessDAO AMA series dives into community
        <br /> and growth strategies of successful organisations.
      </span>
      <span className={styles.introduction}>{introductionText}</span>
      <div className={classes.LogoElementFi}>
        <Image src={src} alt={alt} />
      </div>
      <span className={classes.description}>
        Mint your free Soul Bound NFT Token to suggest
        <br /> questions and vote on the top submissions.
      </span>
    </section>
  );
};

export default withMergedStyles(BanklessDaoPreludium, classes);
