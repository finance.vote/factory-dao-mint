import styles from './how-it-works.module.scss';

const QuadraticVotingBtn = () => {
  return (
    <a href="https://youtu.be/A8BEzAqNYl8" target="_blank" rel="noreferrer">
      <button className={styles.quadraticBtn}>
        HOW TO USE QUADRATIC VOTING IN THE INFLUENCE APP
      </button>
    </a>
  );
};

const HowItWorks = ({ content }) => {
  return (
    <section className={styles.container}>
      <h2 className={styles.title}>HOW IT WORKS</h2>
      <div className={styles.content}>
        <ol className={styles.listItem}>
          {content.map((item, index) => {
            return <li key={index} dangerouslySetInnerHTML={{ __html: item.text }}></li>;
          })}
        </ol>
      </div>
      <QuadraticVotingBtn />
    </section>
  );
};

export default HowItWorks;
