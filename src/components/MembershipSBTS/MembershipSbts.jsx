import styles from './MembershipSbts.module.scss';
const MembershipSbts = ({ name = 'FactoryDAO' }) => {
  return (
    <div className={styles.container}>
      <h1 className={styles.header}>DAO membership SBTs</h1>
      <h2 className={styles.desc}>
        There are 4 types of minting available in {name}, all mints are SBT tokens (NFT 721 without
        the transfer options) at 0 cost.
      </h2>
      <div className={styles.containerOptions}>
        <div className={styles.optionContainer}>
          <h1>Unique Code</h1>
          <h2 className={styles.containerOptions__desc}>
            Suitable to low cost chains, DAO members receive a one-time-use unique code which can be
            sent to them directly or can be given out at an event in the form of a QR code.
          </h2>
          <h3 className={styles.containerOptions__toolTip}>
            Mint gas fees: subsidised by FactoryDAO or the project
          </h3>
        </div>
        <div className={styles.optionContainer}>
          <h1>whitelist</h1>
          <h2 className={styles.containerOptions__desc}>
            A project can provide a whitelist of addresses which will be eligible to mint from their{' '}
            {name} SBT series. The associated gas cost will be on the minter (cost depends on the
            chain).
          </h2>
          <h3 className={styles.containerOptions__toolTip}>
            Mint gas fees: paid by the user (cost depends on the chain)
          </h3>
        </div>
        <div className={styles.optionContainer}>
          <h1>Gated</h1>
          <h2 className={styles.containerOptions__desc}>
            The mint can be gated to holders of another token or mix of tokens (fungible or
            non-fungible). A snapshot will be taken when the mint opens and can be updated over
            time.
          </h2>
          <h3 className={styles.containerOptions__toolTip}>
            Mint gas fees: paid by the user (cost depends on the chain)
          </h3>
        </div>
        <div className={styles.optionContainer}>
          <h1>Open</h1>
          <h2 className={styles.containerOptions__desc}>
            The DAO SBT series is open to anyone to mint. Open mints are more prone to Sybil
            attacks. Plan accordingly.
          </h2>
          <h3 className={styles.containerOptions__toolTip}>
            Mint gas fees: paid by the user (cost depends on the chain)
          </h3>
        </div>
      </div>
    </div>
  );
};

export default MembershipSbts;
