import React from 'react';
import styles from 'components/DialogBlack/DialogBlack.module.scss';

const DialogBlack = ({ isOpen, handleClose, children }) => {
  if (!isOpen) return null;
  return (
    <>
      <div className={styles.backdrop} />
      <div className={styles.dialog}>
        <div className={styles.modal}>
          <button className={styles.close} onClick={handleClose} />
          {children}
        </div>
      </div>
    </>
  );
};
export default DialogBlack;
