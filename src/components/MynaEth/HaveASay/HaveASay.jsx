import styles from './HaveASay.module.scss';
const HaveASay = () => {
  return (
    <div className={styles.haveSay}>
      <span className={styles.title}>have a say!</span>
      <span className={styles.descSay}>
        20% of the funds from the mint will be sent to a community safe, holders of the NFTs will be
        able to vote on what community activities it should be spent on, like parties, events or
        merch!
      </span>
    </div>
  );
};

export default HaveASay;
