import accounting from 'images/myna/accounting.gif';
import benefits from 'images/myna/benefits.gif';
import community from 'images/myna/community.gif';
import content from 'images/myna/content.gif';
import discount from 'images/myna/discounts.gif';
import Image from 'next/image';
import styles from './Benefits.module.scss';

const items = [
  {
    name: 'discount',
    imgSrc: discount,
    description: '10% off accounting and forensic fees'
  },
  {
    name: 'benefits',
    imgSrc: benefits,
    description: 'Discounts on events with special guest appearances'
  },
  {
    name: 'accounting',
    imgSrc: accounting,
    description: 'Quarterly AMAs on accounting, personal tax, offshore, forensic and more'
  },
  {
    name: 'community',
    imgSrc: community,
    description: 'Access to the exclusive Myna Discord Community'
  },
  {
    name: 'content',
    imgSrc: content,
    description: 'Premium content: seminars, newsletters and reports'
  }
];
const Benefits = () => {
  return (
    <div className={styles.container}>
      <div className={styles.gridBox}>
        <span className={styles.title}>benefits</span>
        {items.map((item, index) => {
          return (
            <div key={index} className={styles.benefitsItem}>
              <Image src={item.imgSrc} alt={item.name} />
              <span className={styles.description}>{item.description}</span>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Benefits;
