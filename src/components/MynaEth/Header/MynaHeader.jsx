import discordIcon from 'images/myna/discord_eth.svg';
import globeIcon from 'images/myna/globe_eth.svg';
import mynaLogo from 'images/myna/myna_logo_eth.svg';
import twitterIcon from 'images/myna/twitter_eth.svg';
import Image from 'next/image';
import Link from 'next/link';
import styles from './Header.module.scss';

const MynaHeader = ({ twitterLink, websiteLink, discordLink }) => {
  return (
    <header className={styles.header}>
      <div className={styles.logo}>
        <Link href="/" className={styles.link}>
          <div className={styles.image}>
            <Image alt="myna-logo" src={mynaLogo} className={styles.logo} />
          </div>
        </Link>
      </div>
      <div className={styles.container}>
        <ul className={styles.navlinks}>
          <li className={styles.linkContainer}>
            <a className={styles.link} href={twitterLink} target="_blank" rel="noopener noreferrer">
              <Image src={twitterIcon} alt="Twitter" />
            </a>
          </li>
          <li className={styles.linkContainer}>
            <a className={styles.link} href={websiteLink} target="_blank" rel="noopener noreferrer">
              <Image src={globeIcon} alt="Website" />
            </a>
          </li>
          <li className={styles.linkContainer}>
            <a className={styles.link} href={discordLink} target="_blank" rel="noopener noreferrer">
              <Image src={discordIcon} alt="Discord" />
            </a>
          </li>
        </ul>
      </div>
    </header>
  );
};

export default MynaHeader;
