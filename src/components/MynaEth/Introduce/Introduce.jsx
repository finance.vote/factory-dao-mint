import discordIcon from 'images/myna/discord_eth.svg';
import globeIcon from 'images/myna/globe_eth.svg';
import twitterIcon from 'images/myna/twitter_eth.svg';
import Image from 'next/image';
import styles from './Introduce.module.scss';

const Introduce = () => {
  return (
    <div className={styles.container}>
      <span className={styles.title}>Crypto Accounting NFT Members Club</span>
      <span className={styles.description}>
        We make crypto simple. We demystify the process and make sure you’re set up for success, no
        matter how you use cryptocurrency.
      </span>
      <div className={styles.mobileLinks}>
        <a
          className={styles.link}
          href={'https://twitter.com/mynaaccountants'}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={twitterIcon} alt="Twitter" />
        </a>
        <a
          className={styles.link}
          href={'https://www.mynaaccountants.co/'}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={globeIcon} alt="Website" />
        </a>
        <a
          className={styles.link}
          href={'https://discord.gg/9rZGRgZq'}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Image src={discordIcon} alt="Discord" />
        </a>
      </div>
    </div>
  );
};

export default Introduce;
