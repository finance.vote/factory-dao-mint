// import openSeaIcon from 'images/opensea_transparent_white.svg';
import { emitter } from 'emitter';
import closeIcon from 'images/close.svg';
import discordIcon from 'images/discord.svg';
import instagramIcon from 'images/instagram.svg';
import twitterIcon from 'images/twitter.svg';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import styles from './HamburgerMenu.module.scss';
// import { MainContext } from 'context/context';

const HamburgerMenu = () => {
  // const { openSeaURL } = useContext(MainContext);
  const [isOpen, setIsOpen] = useState(false);
  const openHamburger = () => setIsOpen(true);
  const closeHamburger = () => setIsOpen(false);

  // const scrollAndCloseHamburger = () => {
  //   window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
  //   closeHamburger();
  // };

  useEffect(() => {
    emitter.on('hamburger-open', openHamburger);
    emitter.on('hamburger-close', closeHamburger);

    return () => {
      emitter.off('hamburger-open', openHamburger);
      emitter.off('hamburger-close', closeHamburger);
    };
  }, []);

  if (!isOpen) return null;

  return (
    <div className={styles.container}>
      <button className={styles.closeButton} onClick={closeHamburger}>
        <Image src={closeIcon} alt="close" />
      </button>
      <div className={styles.content}>
        <ul className={styles.navlinks}>
          <li className={styles.linkContainer}>
            <a
              className={styles.link}
              href="https://metaverse.tribesdao.com/"
              rel="noopener noreferrer"
            >
              Metaverse
            </a>
          </li>
        </ul>
        <div className={styles.socialLinks}>
          <a
            className={styles.socialLink}
            href="https://twitter.com/ShibakuTribe"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image src={twitterIcon} alt="Twitter" />
          </a>
          <a
            className={styles.socialLink}
            href="https://discord.gg/tribesdao"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image src={discordIcon} alt="Discord" />
          </a>
          <a
            className={styles.socialLink}
            href="https://www.instagram.com/shibaku.tribe/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Image src={instagramIcon} alt="Instagram" />
          </a>
          {/* <a
            className={styles.socialLink}
            href={openSeaURL}
            target="_blank"
            rel="noopener noreferrer">
            <Image src={openSeaIcon} alt="OpenSea" />
          </a> */}
        </div>
      </div>
    </div>
  );
};

export default HamburgerMenu;
