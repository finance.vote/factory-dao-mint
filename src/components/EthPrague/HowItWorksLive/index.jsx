import balls from 'images/ethPrague/balls.svg';
import Image from 'next/image';
import Link from 'next/link';
import styles from './howItWorks.module.scss';

export default function HowItWorks() {
  return (
    <section className={styles.container}>
      <span className={styles.title}>
        How it works
        <Image alt="balls" src={balls} className={styles.balls} />
      </span>
      <ol className={styles.list}>
        <li>Click through the invite link you received</li>
        <li>Connect your wallet or paste your wallet address into the address field</li>
        <li>Mint your unique voting non-transferrable token</li>
        <li>
          Go to the{' '}
          <Link
            className={styles.link}
            href="https://live.ethprague.com"
            target="_blank"
            rel="noopener noreferrer"
          >
            ETH Prague Live Stream
          </Link>{' '}
          to participate in the Live AMA
        </li>
        <li>
          12pm-3pm on June 11th: Vote for the Hackathon Audience favourite{' '}
          <Link
            className={styles.link}
            href="https://influence.factorydao.org/ethprague-audience"
            target="_blank"
            rel="noopener noreferrer"
          >
            here
          </Link>{' '}
          using your unique ETHPrague non-transferable token
        </li>
        <li>Wait for the big winner reveal at the closing ceremony at the Hacker House at 4 PM</li>
      </ol>
    </section>
  );
}
