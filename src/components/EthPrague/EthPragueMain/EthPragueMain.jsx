import EthPragueHeader from 'components/EthPrague/Header';
import Introduction from 'components/EthPrague/Introduction';
import PoweredByDao from 'components/PoweredByDao/PoweredByDao';
import infoIcon from 'images/infoGrey.svg';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import customStyles from './EthPragueMain.module.scss';

const continueToVoting = 'https://influence.factorydao.org/ethprague-audience';

const highlightIndicators = {
  available: {
    background: '#4DFF64',
    shadow: '0px 0px 15px #A3FF5D',
    status: '#00F220',
    fontFamily: 'Poppins'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

const EthPragueMain = ({
  groupKey,
  title,
  description,
  howItWorks,
  afterMintText,
  afterMintLink
}) => {
  return (
    <div className={customStyles.mainContainer}>
      <div className={customStyles.bgSection}>
        <EthPragueHeader
          websiteLink="https://EthPrague.international"
          twitterLink="https://twitter.com/FactDAO"
          telegramLink="https://t.me/factdao"
        />
        <Introduction title={title} description={description} />
        <div className={customStyles.minting}>
          <div className={customStyles.yellowRectangle} />
          <MintingController
            groupKey={groupKey}
            customStyles={{ ...customStyles, highlightIndicators }}
            prefillOnLoad
            walletInputLabel="Paste address or click connect wallet"
            customTexts={{
              successTitle: 'Welcome to ETHPrague',
              successMessage: `ID #%TOKEN_ID% now belongs to you`,
              numberInputLabel:
                'Click claim now, enter a different number or click the random number arrows',
              placeholderText: 'Enter a number',
              buttonText: {
                available: 'MINT NOW',
                unavailable: 'MINT NOW'
              }
            }}
            MintingButtonComponent={(props) => (
              <DefaultMintingButton
                {...props}
                discordLink={afterMintLink || continueToVoting}
                actionButtonAfterMintText={afterMintText || 'continue to voting realm'}
                customStyles={{
                  button: customStyles.button,
                  actionButton: !props.isEligibleToMint && customStyles.button
                }}
              />
            )}
            tooltipIcon={infoIcon}
            tooltipContentClass={customStyles.tooltipContent}
            tooltipContent={
              <>
                A random token number has been selected for you, just click “MINT NOW” if you are
                happy with it.
                <br />
                <br />
                If you would like to pick another random number click the green “random” arrows
                <br />
                <br />
                If you would like a specific number in the series, try typing it in. It may be
                taken! The traffic lights on the right will indicate if it’s:
                <div>
                  <span className={customStyles.tooltipBoldText}>Green:</span> Available
                </div>
                <div>
                  <span className={customStyles.tooltipBoldText}>Amber:</span> Somebody else is in
                  the process of claiming it now
                </div>
                <div>
                  <span className={customStyles.tooltipBoldText}>Red:</span> Already claimed
                </div>
              </>
            }
          />
        </div>
      </div>
      {howItWorks}
      <div className={customStyles.powered}>
        <PoweredByDao />
      </div>
    </div>
  );
};

export default EthPragueMain;
