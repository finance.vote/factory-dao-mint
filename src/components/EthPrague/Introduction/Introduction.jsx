import ball1 from 'images/ethPrague/ball1.svg';
import ball2 from 'images/ethPrague/ball2.svg';
import ball3 from 'images/ethPrague/ball3.svg';
import logo from 'images/ethPrague/logo.svg';
import Image from 'next/image';
import styles from './Introduction.module.scss';

const Introduction = ({ title, description }) => {
  return (
    <div className={styles.container}>
      <div className={styles.logo}>
        <Image src={logo} alt="eth-prague-logo" width={274} height={330} />
      </div>
      <div className={styles.header}>{title}</div>
      <div className={styles.text}>
        {description}
        <Image alt="ball" src={ball1} className={styles.ball1} />
        <Image alt="ball" src={ball2} className={styles.ball2} />
        <Image alt="ball" src={ball3} className={styles.ball3} />
      </div>
    </div>
  );
};

export default Introduction;
