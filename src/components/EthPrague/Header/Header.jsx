import Container from 'components/Ui/Container/Container.component';
import globeIcon from 'images/ethPrague/globe.svg';
import telegramIcon from 'images/ethPrague/telegram.svg';
import twitterIcon from 'images/ethPrague/twitter.svg';
import mintLogo from 'images/mintLogoBlack.svg';
import mobileMintLogo from 'images/mint_mobile.svg';
import Image from 'next/image';
import Link from 'next/link';
import styles from './Header.module.scss';

const twitterLink = 'https://twitter.com/EthPrague';
const websiteLink = 'https://ethprague.com/';
const telegramLink = 'https://t.me/ethprague';

const OffGridHeader = () => {
  return (
    <Container className={styles.mainContainer}>
      <header className={styles.header}>
        <div className={styles.logo}>
          <Link href="/" className={styles.link}>
            <div className={styles.image}>
              <Image alt="mint-logo" src={mintLogo} className={styles.logo} />
            </div>
          </Link>
          <Link href="/" className={styles.link}>
            <div className={styles.mobileImage}>
              <Image alt="mint-logo" src={mobileMintLogo} className={styles.logo} />
            </div>
          </Link>
        </div>
        <div className={styles.container}>
          <ul className={styles.navlinks}>
            <li className={styles.linkContainer}>
              <Link
                className={styles.link}
                href={twitterLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image src={twitterIcon} alt="Twitter" />
              </Link>
            </li>
            <li className={styles.linkContainer}>
              <Link
                className={styles.link}
                href={websiteLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image src={globeIcon} alt="Website" />
              </Link>
            </li>
            <li className={styles.linkContainer}>
              <Link
                className={styles.link}
                href={telegramLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                <Image src={telegramIcon} alt="telegram" />
              </Link>
            </li>
          </ul>
        </div>
      </header>
    </Container>
  );
};

export default OffGridHeader;
