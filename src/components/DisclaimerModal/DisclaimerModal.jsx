import classnames from 'classnames';
import fdaoLogo from 'images/fdaoLogoS.png';
import localforage from 'localforage';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import styles from './DisclaimerModal.module.scss';

let visitedMints;

const DisclaimerModal = () => {
  const [showDisclaimerDialog, setDisclaimerDialog] = useState(false);
  const router = useRouter();
  const { asPath } = router;

  const isDynamicRouteLoaded = !asPath.startsWith('/['); // [mint] - real path is not loaded from the beginning
  const currPage = asPath.length > 1 ? asPath.substring(1) : null;

  useEffect(() => {
    if (!isDynamicRouteLoaded || !currPage) return;

    (async () => {
      visitedMints = await localforage.getItem('disclaimerAccepted');

      if (
        currPage.includes('admin') ||
        currPage.includes('terms-and-conditions') ||
        currPage.includes('fvts3')
      ) {
        setDisclaimerDialog(false);
        return;
      }

      if (!visitedMints || !visitedMints.split(',').includes(currPage)) {
        setTimeout(() => setDisclaimerDialog(true), 500);
      }
    })();
  }, [currPage, isDynamicRouteLoaded]);

  async function handleAcceptTerms() {
    setDisclaimerDialog(false);

    const path = `${visitedMints ? visitedMints + ',' : ''}${currPage}`;
    await localforage.setItem('disclaimerAccepted', path);
  }

  return showDisclaimerDialog ? (
    <>
      <div className={styles.backdrop} />
      <div className={classnames(styles.disclaimerForm, styles.modal)}>
        <div className={styles.modalBody}>
          <h2>Disclaimer</h2>
          <div>
            The FactoryDAO minting platform is free and open source software that helps you mint
            NFTs created by third parties.
          </div>
          <div className={styles.paragraph}>
            We do not make any representations or warranties about this third-party content visible
            through our Service, including any content associated with NFTs displayed on the
            Service, and you bear responsibility for verifying the legitimacy, authenticity, and
            legality of NFTs that you purchase from third-party sellers.
          </div>
          <div className={styles.paragraph}>
            NFTs may be subject to terms directly between buyers and sellers with respect to the use
            of the NFT content and benefits associated with a given NFT.
          </div>
          <div className={styles.paragraph}>
            <button className={styles.btn} onClick={handleAcceptTerms}>
              Agree and continue
            </button>
          </div>
          <div className={styles.paragraph}>
            <Link href="/terms-and-conditions" className="link">
              terms and conditions
            </Link>
          </div>
        </div>
        <div className={styles.modalFooter}>
          <Image src={fdaoLogo} alt="FactoryDao logo" />
        </div>
      </div>
    </>
  ) : null;
};

export default DisclaimerModal;
