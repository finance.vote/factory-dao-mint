import diamond from 'images/ethberlin/ethberlin-diamond.svg';
import Image from 'next/image';
import React from 'react';
import styles from './Diamond.module.scss';

const Diamond = () => {
  return (
    <div className={styles.diamondContainer}>
      <Image alt="_diamond" src={diamond} className={styles.diamond} />
    </div>
  );
};

export default Diamond;
