import logo from 'images/ethberlin/ethberlin-wordmark.svg';
import Image from 'next/image';
import styles from './Preludium.module.scss';
const EthBerlinPreludium = () => {
  return (
    <section className={styles.container}>
      <Image alt="ethberlin_logo" src={logo} className={styles.logo} />
      <span className={styles.powerOf}>to the power of 3</span>
      <span className={styles.description}>
        Mint your non-transferrable token to vote for the
        <br />
        ETHBerlin³ Open Track - Berlin&apos;s Favorite <br />
        and decide which team should win 10,000 DAI!
      </span>
    </section>
  );
};

export default EthBerlinPreludium;
