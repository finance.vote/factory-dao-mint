import styles from './how-it-works.module.scss';
const CONTENT = [
  { text: 'Connect your ETHBerlin³ EtherCard wallet <b>on the Goerli testnet</b>' },
  { text: 'Mint your unique voting non-transferrable token' },
  {
    text: 'Explore the voting realm and vote on some fun questions first to get used to the quadratic voting experience'
  },
  {
    text: 'Check out all submissions on <a style="color: white;" target="_blank" href="https://ethberlin.devfolio.co/projects">ethberlin.devfolio.co/projects</a> and make your mind up about your favorite projects'
  },
  {
    text: 'Check the "Open Track - Berlin\'s Favorite" realm <a style="color: white;" target="_blank" href="https://vote.ethberlin.ooo/2022">here</a> and support your for your favorite projects by allocating votes to them! Voting is open from 12:00 pm - 15:00 pm on Sunday.'
  },
  { text: 'Wait for the big winner reveal at the ETHBerlin³ Closing Ceremony at 4PM.' }
];

const HowItWorks = () => {
  return (
    <section className={styles.container}>
      <h2 className={styles.title}>(h)ow it works</h2>
      <div className={styles.content}>
        {CONTENT.map((item, index) => {
          return (
            <span className={styles.listItem} key={index}>
              <p className={styles.number}>{index + 1}.</p>{' '}
              <span dangerouslySetInnerHTML={{ __html: item.text }}></span>
            </span>
          );
        })}
      </div>
    </section>
  );
};

export default HowItWorks;
