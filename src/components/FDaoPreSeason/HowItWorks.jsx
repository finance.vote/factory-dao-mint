import Link from 'next/link';
import styles from './HowItWorks.module.scss';
const HowItWorks = () => {
  return (
    <>
      <h1 className={styles.header}>What you can do with factoryDAO</h1>
      <div className={styles.daoOptions}>
        <div className={styles.innerContainer}>
          <h2 className={styles.innerHeader}>Build</h2>
          <span className={styles.innerDesc}>
            DAO Builders and their communities can begin discovering the “Art of Governance” by
            utilising the FactoryDAO governance stake, with a range of expressive built-in voting
            strategies including quadratic voting and mixed media voting.
          </span>
        </div>
        <div className={styles.innerContainer}>
          <h2 className={styles.innerHeader}>Govern</h2>
          <span className={styles.innerDesc}>
            The <Link href="https://influence.factorydao.org/">influence</Link> dApp is “Gas Free”
            for Builders to create Votes and Tasks and for users to Vote and log submissions. As a
            DAO, your governance experiments will be gated to the community members who have minted
            from the DAO SBT series.
          </span>
        </div>
      </div>
    </>
  );
};

export default HowItWorks;
