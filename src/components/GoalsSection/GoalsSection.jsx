import goalIcon1 from 'images/sovryn/goal1.svg';
import goalIcon2 from 'images/sovryn/goal2.svg';
import goalIcon3 from 'images/sovryn/goal3.svg';
import Image from 'next/image';
import styles from './GoalsSection.module.scss';

const GoalsConfig = [
  { icon: goalIcon1, text: 'Aspiring Bitcoin maxis and wannabe crypto millionaires' },
  {
    icon: goalIcon2,
    text: 'Individualists with a fascination for social status, new technology and risk'
  },
  { icon: goalIcon3, text: 'Ambition to be free from traditions and obligations' }
];

const GoalItems = () => {
  return GoalsConfig.map((goal, index) => {
    return (
      <div key={index} className={styles.goalItem}>
        <Image src={goal.icon} alt={`goal_${index}`} />
        <p>{goal.text}</p>
      </div>
    );
  });
};

const GoalsSection = () => {
  return (
    <div className={styles.sectionContainer}>
      <div className={styles.sectionTitle}>GOALS</div>
      <GoalItems />
      <div className={styles.lowerInformation}>
        <p>
          The first part of engagement is complete with the creation of sovryns.com, an unofficial
          website dedicated to Sovryn fanatics, or &apos;Sovryns&apos;.
        </p>
      </div>
    </div>
  );
};

export default GoalsSection;
