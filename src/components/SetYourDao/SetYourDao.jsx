import Link from 'next/link';
import styles from './SetYourDao.module.scss';
const SetYourDao = ({ title, name }) => {
  return (
    <div className={styles.container}>
      <div className={styles.innerLeft}>
        <h1 className={styles.innerHeader}>{title}</h1>
        <h2>
          If you would like to use {name} to experiment with your DAO designs and community, please
          reach out to FactoryDAO at{' '}
          <Link href="mailto:https://www.factorydao.xyz/">hello@factorydao.xyz</Link> for more
          information.
        </h2>
      </div>
      <div className={styles.innerRight}>
        <h1>If you already know what DAO setup you want please provide the following:</h1>
        <ul>
          <li>
            <b>Name</b> of your DAO
          </li>
          <li>
            <b>DAO Logo:</b> transparent .png or .svg for dark background
          </li>
          <li>
            <b>Number of SBTs will be issued:</b> min 5-100 max
          </li>
          <li>
            <b>SBT Art:</b> Square format, min 800px, png/jpeg/gif/mp4/gltf
          </li>
          <li>
            <b>Dates of the minting window:</b> From-To or open ended
          </li>
          <li>
            <b>Team addresses:</b> Will have rights to set up votes and tasks
          </li>
          <li>
            <b>Type of mint:</b> Unique code/Whitelist/Gated/Open (if whitelist please provide a csv
            of the addresses)
          </li>
        </ul>
      </div>
    </div>
  );
};

export default SetYourDao;
