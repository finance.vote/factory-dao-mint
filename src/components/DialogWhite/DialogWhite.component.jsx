import React from 'react';
import styles from './DialogWhite.module.scss';

const DialogWhite = ({ isOpen, handleClose, children }) => {
  if (!isOpen) return null;
  return (
    <>
      <div className={styles.backdrop} />
      <div className={styles.dialog}>
        <div className={styles.modal}>
          {handleClose && <button className={styles.close} onClick={handleClose} />}
          {children}
        </div>
      </div>
    </>
  );
};
export default DialogWhite;
