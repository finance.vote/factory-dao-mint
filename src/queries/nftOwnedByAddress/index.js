import VoterID from 'contracts/VoterID.json';
import { ethInstance } from 'evm-chain-scripts';
import { MINTING_STATUSES } from 'minting-components/helpers';
import { useQuery } from 'react-query';

const checkTokensOwnedByUser = async (nftAddress, chainId, address) => {
  try {
    const nftContract = await ethInstance.getReadContractByAddress(VoterID, nftAddress, chainId);
    const ownedCount = await nftContract.balanceOf(address);
    return ownedCount.toNumber();
  } catch (error) {
    console.log(error);
    return 0;
  }
};

export const useCheckTokensOwnedByUser = (nftAddress, chainId, mintingStatus, walletAddress) =>
  useQuery(
    ['ownedBy', nftAddress, chainId, walletAddress],
    async () => await checkTokensOwnedByUser(nftAddress, chainId, walletAddress),
    {
      enabled: !!(
        mintingStatus === MINTING_STATUSES.DONE &&
        nftAddress &&
        chainId &&
        walletAddress
      ),
      initialData: {
        total: '',
        minted: '',
        placeholderText: 'Pick a number',
        percentage: ''
      }
    }
  );
