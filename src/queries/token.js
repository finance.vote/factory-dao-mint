import { ethInstance } from 'evm-chain-scripts';
// import { useContext, useEffect, useState } from 'react';
import { useMutation, useQuery } from 'react-query';
// import { getIdentitiesForAccount } from '../helpers/eventGetters';
import {
  getCurrentRoundId,
  getIdentityDecayFactor,
  getIdentityPrice,
  getNumIdentities,
  getTokenBalance
} from '../helpers/simpleGetters';
import { createIdentity, submitVote } from '../helpers/simpleSetters';
// import { Context } from '../identity-components/context/identity/Store';

// function useIdentitiesQueryWithCallback(action) {
//   const [isLoading, setIsLoading] = useState(true);
//   const [error, setError] = useState(null);
//   const [data, setData] = useState([]);
//   const [dataHandler, handleData] = useState(null);
//   const [state, dispatch] = useContext(Context);

//   useEffect(() => {
//     if (!Object.keys(state.identities).length) {
//       try {
//         action((newData) => {
//           setIsLoading(false);
//           if (newData !== null) {
//             setData((data) => [newData, ...data]);
//             dispatch({
//               type: 'SET_IDENTITY',
//               payload: {
//                 key: newData.tokenId.toString(),
//                 value: newData
//               }
//             });
//             dataHandler && dataHandler(newData);
//           }
//         });
//       } catch (err) {
//         setError(err);
//       }
//     } else {
//       setData(Object.values(state.identities));
//       setIsLoading(false);
//       dataHandler && dataHandler(Object.values(state.identities));
//     }
//   }, [action, dataHandler]);

//   return {
//     isLoading,
//     error,
//     data,
//     handleData
//   };
// }

export const useNumIdentities = () => useQuery('numIdentities', () => getNumIdentities());

// export const useIdentitiesForAccount = () =>
//   useIdentitiesQueryWithCallback(getIdentitiesForAccount);

export const useCurrentRoundId = (tournamentId) =>
  useQuery('currentRoundId', () => getCurrentRoundId(tournamentId));

export const useIdentityPrice = () => useQuery('identityPrice', getIdentityPrice);

export const useIdentityDecayFactor = () => useQuery('identityDecayFactor', getIdentityDecayFactor);

export const useTokenBalance = () => useQuery('tokenBalance', getTokenBalance);

export const useCreateIdentity = () => useMutation(createIdentity);

export const useSubmitVote = () =>
  useMutation((variables) => submitVote(...Object.values(variables)));

export const useChainId = () => useQuery('chainId', () => ethInstance.getChainId());

export const useAccount = () => useQuery('currentAccount', ethInstance.getEthAccount);
