import { useQuery } from 'react-query';
import { getNextTokenToMint } from './nextTokenToMint';

export const useNextTokenToMint = (group, mintType, prefillOnLoad) =>
  useQuery(['nextTokenToMint', group], async () => getNextTokenToMint(group), {
    enabled: !!(group && (mintType === 'sequential' || prefillOnLoad)),
    refetchInterval: group && mintType === 'sequential' ? 10000 : false
  });
