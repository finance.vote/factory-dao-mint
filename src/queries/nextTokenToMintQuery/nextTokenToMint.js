import axios from 'axios';

export const getNextTokenToMint = async (group) => {
  try {
    const {
      data: { token: nextTokenToMint }
    } = await axios.get(`/minting/next/${group}`);
    if (!nextTokenToMint) {
      throw new Error('could not fetch next token to mint');
    }
    return nextTokenToMint;
  } catch (e) {
    console.error(e);
  }
};
