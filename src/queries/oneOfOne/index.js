import {
  fetchNftImage,
  getAllAuctionEvents,
  getAuctionData,
  getBidEvents,
  getCurrentAuctionId,
  getCurrentPrice,
  getStructures,
  getUsdPriceOfEth
} from 'helpers/oneOfOne';
import { useQuery } from 'react-query';

export const useSecondsPerSample = () =>
  useQuery(
    'secondsPerSample',
    async () => {
      const auction = await getAuctionData();
      return auction.secondsPerSample;
    },
    {
      refetchOnWindowFocus: false
    }
  );

export const useLastAuctionBid = () =>
  useQuery(
    'lastAuctionBid',
    async () => {
      const auction = await getAuctionData();
      return auction.lastBid;
    },
    {
      refetchOnWindowFocus: false
    }
  );

export const useStructTokenForSale = () =>
  useQuery(
    'structTokenForSale',
    async () => {
      const structures = await getStructures();
      return parseInt(structures.tokenForSale);
    },
    {
      refetchOnWindowFocus: false
    }
  );

export const useStructNftContract = () =>
  useQuery(
    'structNftContract',
    async () => {
      const structures = await getStructures();
      return structures.nftContract;
    },
    {
      refetchOnWindowFocus: false
    }
  );

export const useNumSamplesBeforeSale = () =>
  useQuery(
    'numSamplesBeforeSale',
    async () => {
      const structures = await getStructures();
      return structures.numSamplesBeforeSale;
    },
    {
      refetchOnWindowFocus: false
    }
  );

export const useCurrentPrice = (secondsPerSample, isNftSold) =>
  useQuery('currentPrice', async () => await getCurrentPrice(), {
    initialData: 0,
    refetchInterval: !isNftSold && parseInt(secondsPerSample) * 1000,
    refetchIntervalInBackground: true,
    refetchOnWindowFocus: false
  });

export const useUsdPriceOfEth = () =>
  useQuery('usdPriceOfEth', async () => await getUsdPriceOfEth(), { initialData: 0 });

export const useAllAuctionEvents = (
  usdPriceOfEth,
  bidEvents,
  secondsPerSample,
  numSamplesBeforeSale,
  onSold,
  isNftSold
) =>
  useQuery(
    ['allAuctionEvents', bidEvents, numSamplesBeforeSale],
    async () => await getAllAuctionEvents(usdPriceOfEth, bidEvents, numSamplesBeforeSale, onSold),
    {
      initialData: [],
      enabled: !!(usdPriceOfEth && bidEvents && secondsPerSample && numSamplesBeforeSale),
      refetchInterval: bidEvents?.length && !isNftSold && parseInt(secondsPerSample) * 1000,
      refetchIntervalInBackground: true,
      refetchOnWindowFocus: false
    }
  );

export const useCurrentAuctionId = () =>
  useQuery('currentAuctionId', async () => await getCurrentAuctionId(), {
    refetchOnWindowFocus: false
  });

export const useBidEvents = (currentAuctionId, chainId) =>
  useQuery(
    ['bidEvents', currentAuctionId],
    async () => await getBidEvents(currentAuctionId, chainId),
    {
      refetchOnWindowFocus: false
    }
  );

export const useFetchNftImage = (nftId, contractAddress, chainId) =>
  useQuery(
    ['nftImage', nftId, contractAddress, chainId],
    async () => fetchNftImage(nftId, contractAddress, chainId),
    {
      initialData: { isVideo: false, mediaUrl: '' },
      enabled: !!(nftId && contractAddress && chainId),
      refetchOnWindowFocus: false
    }
  );
