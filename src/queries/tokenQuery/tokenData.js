import axios from 'axios';

export const getTokenData = async (group) => {
  try {
    const { data: tokenData } = await axios.get(`/minting/stats/${group}`);

    const percentage = parseInt((tokenData.minted / tokenData.total) * 100);
    return { ...tokenData, percentage };
  } catch (e) {
    console.error(e);
  }
};
