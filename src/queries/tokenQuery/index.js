import { useQuery } from 'react-query';
import { getTokenData } from './tokenData';

export const useTokenData = (group, isProgressBarVisible) =>
  useQuery(['TokenData', group], async () => await getTokenData(group), {
    enabled: !!(group && isProgressBarVisible),
    initialData: {
      total: '',
      minted: '',
      placeholderText: 'Pick a number',
      percentage: ''
    }
  });
