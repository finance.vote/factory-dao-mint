import { useQuery } from 'react-query';
import { getRandomToken } from './randomToken';

export const useRandomToken = (group) =>
  useQuery(['randomToken', group], async () => getRandomToken(group), {
    enabled: false,
    initialData: {}
  });
