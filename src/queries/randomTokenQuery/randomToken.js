import axios from 'axios';

export const getRandomToken = async (group) => {
  try {
    const {
      data: { token: randomToken }
    } = await axios.get(`/minting/random/${group}`);
    return randomToken;
  } catch (e) {
    console.error(e);
  }
};
