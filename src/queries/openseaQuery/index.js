import { useQuery } from 'react-query';
import { getBaseOpenseaUrl } from './opensea';

export const useOpenseaUrl = (chainId) =>
  useQuery(['openseaUrl', chainId], () => getBaseOpenseaUrl(chainId), {
    enabled: !!chainId,
    initialData: 'opensea.io'
  });
