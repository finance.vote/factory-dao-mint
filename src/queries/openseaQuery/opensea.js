import NETWORKS from 'helpers/networks.json';

export const getBaseOpenseaUrl = (chainId) => {
  if (chainId === 30 || chainId === 31) {
    return '';
  }

  return NETWORKS[chainId]?.network === 'mainnet' ? 'opensea.io' : 'testnets.opensea.io';
};
