import { useQuery } from 'react-query';

function getPlaceholderText(tokensRange, placeholderText) {
  return tokensRange.end
    ? `${placeholderText} (${tokensRange.start} - ${tokensRange.end || ''})`
    : placeholderText;
}

export const useGetPlaceholderText = (tokensRange, group, placeholderText) =>
  useQuery(
    ['placeholdertext', group, tokensRange.start, tokensRange?.end],
    () => getPlaceholderText(tokensRange, placeholderText),
    {
      enabled: !!(group && tokensRange?.end),
      initialData: placeholderText
    }
  );
