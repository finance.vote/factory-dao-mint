import { useQuery } from 'react-query';
import { AmaluEligibilityCheck, AmaluWithdrawalsCheck } from './amalu';
import { MerkleEligibilityCheck, MerkleWithdrawalsCheck } from './merkle';
import { qrCodeEligibility, qrCodeWithdrawals } from './qrcode';
import { TokenMappingEligibilityCheck, TokenMappingWithdrawalsCheck } from './tokenmapping';
import { WhitelistEligibility, WhitelistWithdrawals } from './whitelist';

const keyToQueryEligibilityMap = {
  amalu: AmaluEligibilityCheck,
  merkle: MerkleEligibilityCheck,
  tokenmapping: TokenMappingEligibilityCheck,
  whitelist: WhitelistEligibility,
  qrcode: qrCodeEligibility
};

const keyToQueryWithdrawalsMap = {
  amalu: AmaluWithdrawalsCheck,
  merkle: MerkleWithdrawalsCheck,
  tokenmapping: TokenMappingWithdrawalsCheck,
  whitelist: WhitelistWithdrawals,
  qrcode: qrCodeWithdrawals
};

const getDataPerKey = async (
  key,
  chainId,
  address,
  eligibilityIndex,
  eligibilityAddress,
  seriesId,
  groupKey,
  seriesConfig,
  code
) => {
  const eligibilityData = await keyToQueryEligibilityMap[key](
    chainId,
    address,
    eligibilityIndex,
    eligibilityAddress,
    seriesId,
    groupKey,
    code
  );
  let timesWithdrawnSum = 0;
  let resultSum = 0;
  const isCodeAvailable = eligibilityData.isEligibleToMint;

  const eligibilitiesStringArr = seriesConfig.map((series) => {
    const eligibility = {
      eligibilityIndex: series.eligibilityIndex,
      eligibilityAddress: series.eligibilityAddress,
      eligibilityType: series.eligibilityType
    };
    return JSON.stringify(eligibility);
  });
  const uniqueEligibilitiesString = [...new Set(eligibilitiesStringArr)]; // removing duplicated eligibility data
  const uniqueEligibilities = uniqueEligibilitiesString.map((string) => JSON.parse(string));

  // total withdrawals from all trees
  for (const eligibility of uniqueEligibilities) {
    if (!eligibility.eligibilityType) continue;
    const withdrawalsData = await keyToQueryWithdrawalsMap[eligibility.eligibilityType](
      chainId,
      address,
      eligibility.eligibilityIndex,
      eligibility.eligibilityAddress,
      seriesId,
      groupKey,
      isCodeAvailable
    );

    timesWithdrawnSum += withdrawalsData.timesWithdrawn;
    resultSum += withdrawalsData.maxWithdrawals;
  }

  const withdrawals = {
    timesWithdrawn: timesWithdrawnSum,
    maxWithdrawals: resultSum
  };
  return {
    ...eligibilityData,
    withdrawals
  };
};

/**
 * Get User Eligibility Status and set Mints available, isEligible
 */
export const useEligibilityQuery = (
  key,
  chainId,
  address,
  eligibilityIndex,
  eligibilityAddress,
  seriesId,
  groupKey,
  seriesConfig,
  code
) => {
  return useQuery(
    [key, chainId, address, eligibilityIndex, eligibilityAddress, seriesId, groupKey, code],
    async () =>
      await getDataPerKey(
        key,
        chainId,
        address,
        eligibilityIndex,
        eligibilityAddress,
        seriesId,
        groupKey,
        seriesConfig,
        code
      ),
    {
      enabled: !!(key && chainId && (code || address) && eligibilityIndex && eligibilityAddress),
      refetchOnWindowFocus: false,
      initialData: {
        addressMerkleProof: [],
        isEligibleToMint: false,
        withdrawals: {}
      }
    }
  );
};
