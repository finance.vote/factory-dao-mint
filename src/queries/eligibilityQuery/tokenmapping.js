import NFTEligibility from 'contracts/NFTEligibility.json';
import { ethInstance } from 'evm-chain-scripts';

export const TokenMappingEligibilityCheck = async (
  chainId,
  address,
  eligibilityIndex,
  eligibilityAddress
) => {
  let addressMerkleProof = [];
  let isEligibleToMint = false;
  try {
    const eligibilityContract = await ethInstance.getReadContractByAddress(
      NFTEligibility,
      eligibilityAddress,
      chainId
    );

    isEligibleToMint = await eligibilityContract.isEligible(
      eligibilityIndex,
      address,
      addressMerkleProof
    );
  } catch (error) {
    console.error(
      error.code ?? error,
      `Eligibility check error occurred: please check account/network you're connected to.`
    );
    throw Error(`You're not eligible, check account and try again`);
  }
  return { addressMerkleProof, isEligibleToMint };
};

export const TokenMappingWithdrawalsCheck = async (
  chainId,
  address,
  eligibilityIndex,
  eligibilityAddress
) => {
  let withdrawals = {};
  try {
    const eligibilityContract = await ethInstance.getReadContractByAddress(
      NFTEligibility,
      eligibilityAddress,
      chainId
    );
    const [, maxWithdrawals, numWithdrawals] = await eligibilityContract.getGate(eligibilityIndex);
    withdrawals = {
      timesWithdrawn: parseInt(numWithdrawals),
      maxWithdrawals: parseInt(maxWithdrawals)
    };
  } catch (error) {
    console.error(
      error.code ?? error,
      `Withdrawals check error occurred: please check account/network you're connected to.`
    );
    throw Error(`You're not eligible, check account and try again`);
  }
  return withdrawals;
};
