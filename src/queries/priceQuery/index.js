import { useQuery } from 'react-query';
import { getFixedCost } from './fixed';
import { getSpeedBumpCost } from './speedBump';

const keyToQueryMap = {
  fixed: getFixedCost,
  speedBump: getSpeedBumpCost
};

export const useCost = (key, seriesPriceValue, merkleContract, treeIndex, gateAddress, chainId) =>
  useQuery(
    [key, treeIndex],
    async () =>
      await keyToQueryMap[key](seriesPriceValue, merkleContract, treeIndex, gateAddress, chainId),
    {
      enabled: !!(key && treeIndex)
    }
  );
