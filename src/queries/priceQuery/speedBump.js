import { ethInstance, fromWei } from 'evm-chain-scripts';

export const getSpeedBumpCost = async (
  seriesPriceValue,
  merkleContract,
  treeIndex,
  gateAddress,
  chainId
) => {
  try {
    const currentNetwork = chainId ?? ethInstance.getChainId();
    const gateContract =
      gateAddress &&
      (await ethInstance.getReadContractByAddress(merkleContract, gateAddress, currentNetwork));
    const cost = await gateContract.getPrice(treeIndex);
    return fromWei(cost);
  } catch (error) {
    console.error(
      error?.code,
      'LOADING PRICE ERROR: ',
      `please check account/network you're connected to.`
    );
    return 0;
  }
};
