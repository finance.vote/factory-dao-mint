import VoterID from 'contracts/VoterID.json';
import { getDefaultSub } from 'helpers/eventsSubscriber';
import { useQuery } from 'react-query';

export async function getMintedEvents(
  chainId,
  abi,
  abiName,
  eventName,
  contractAddress,
  nftBlockNumber
) {
  const events = getDefaultSub();
  const abiEvents = events.getEventsFromAbi(abi);
  const subResponse = await events.getAllEvents({
    project: abiName,
    chainId,
    eventName,
    requestBody: {
      abi: abiEvents,
      params: {
        contractAddress,
        fromBlock: nftBlockNumber,
        query: {
          fromBlock: nftBlockNumber
        }
      },
      setupOnly: true
    }
  });
  if (subResponse.success) {
    return subResponse.allEvents?.reverse() || [];
  } else {
    throw subResponse.error;
  }
}

export const useMintedEvents = (chainId, contractAddress, nftBlockNumber) =>
  useQuery(
    ['mintedEvents', chainId, contractAddress, 'Transfer', nftBlockNumber],
    async () =>
      await getMintedEvents(
        chainId,
        VoterID,
        'VoterID',
        'Transfer',
        contractAddress,
        nftBlockNumber
      ),
    {
      enabled: !!(chainId && contractAddress),
      initialData: []
    }
  );
