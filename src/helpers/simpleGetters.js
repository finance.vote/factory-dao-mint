import { asciiToHex, ethInstance, fromWei, toBN } from 'evm-chain-scripts';
import ChainLinkOracle from '../contracts/ChainLinkOracle2.json';
import Identity from '../contracts/Identity.json';
import FVT from '../contracts/Token.json';
import Trollbox from '../contracts/Trollbox.json';

// versionMap[chainId][tournamentId][version] = { startRoundId, endRoundId}
const versionMap = {
  1: {
    1: {
      1: {
        startRoundId: 1,
        endRoundId: 0
      }
    }
  },
  3: {
    1: {
      1: {
        startRoundId: 1,
        endRoundId: 0
      }
    }
  },
  56: {
    1: {
      1: {
        startRoundId: 1,
        endRoundId: 0
      }
    }
  },
  333: {
    1: {
      1: {
        startRoundId: 1,
        endRoundId: 0
      }
    }
  }
};

export async function getManagement() {
  return {
    trollbox: await ethInstance.isUserManagement(Trollbox),
    identity: await ethInstance.isUserManagement(Identity)
  };
}

export async function isWinnerOracle(tournamentId) {
  const t = await getTournament(tournamentId);
  const account = await ethInstance.getEthAccount();

  return account.toLowerCase() === t.winnerOracle.toLowerCase();
}

export async function getTokenBalance() {
  const account = await ethInstance.getEthAccount();
  const fvt = await ethInstance.getContract('read', FVT);
  const balance = await fvt.balanceOf(account);

  console.log('account', account, 'fvt', fvt, 'balance', balance.toString());
  return fromWei(balance);
}

export async function identityAlreadyVoted(voterId, tournamentId) {
  const trollbox = await ethInstance.getContract('read', Trollbox);
  const currentRound = await trollbox.getCurrentRoundId(tournamentId);
  const lastRoundVoted = await trollbox.getLastRoundVoted(tournamentId, voterId);

  return lastRoundVoted === currentRound;
}

export async function getRoundResolved(tournamentId, roundId) {
  const winner = await getWinner(tournamentId, roundId);

  return winner > 0;
}

export async function getWinner(tournamentId, roundId) {
  let winner;

  try {
    const trollbox = await ethInstance.getContract('read', Trollbox);
    const round = await trollbox.getRound(tournamentId, roundId);

    winner = round[1];
  } catch (err) {
    console.error(err);
  }

  return winner;
}

export async function getLastRoundReward(tournamentId, voterId) {
  const trollbox = await ethInstance.getContract('read', Trollbox);

  const lastRoundReward = await trollbox.getLastRoundReward(tournamentId, voterId);

  return parseInt(lastRoundReward);
}

export async function getNumIdentities() {
  const identity = await ethInstance.getContract('read', Identity);
  const numIdentities = await identity.numIdentities();

  return parseInt(numIdentities);
}

export async function roundAlreadyResolved(tournamentId, roundId) {
  const trollbox = await ethInstance.getContract('read', Trollbox);

  return await trollbox.roundAlreadyResolved(tournamentId, roundId);
}

export async function getRoundResolution(tournamentId) {
  const currentRoundId = await getCurrentRoundId(tournamentId);
  const resolution = {};

  for (let roundId = 1; roundId < currentRoundId - 1; roundId++) {
    resolution[roundId] = await roundAlreadyResolved(tournamentId, roundId);
  }
  return resolution;
}

export async function isSynced(voterId, tournamentId, roundId) {
  const trollbox = await ethInstance.getContract('read', Trollbox);

  console.log('isSynced', voterId, tournamentId, roundId);
  if (!voterId) {
    return null;
  }
  return await trollbox.isSynced(voterId, tournamentId, roundId);
}

export async function getRoundBonus(voterId, tournamentId, roundId) {
  if (!voterId) {
    return null;
  }
  const trollbox = await ethInstance.getContract('read', Trollbox);
  const bonus = await trollbox.getRoundBonus(voterId, tournamentId, roundId);

  return bonus;
}

export async function getVoiceCredits(tournamentId, user) {
  const contract = await ethInstance.getContract('read', Trollbox);
  const v = parseInt(await contract.getVoiceCredits(tournamentId, user));

  if (v === 0) {
    return await getVoiceUBI(tournamentId);
  }
  return v;
}

export async function getVoiceUBI(tournamentId) {
  const contract = await ethInstance.getContract('read', Trollbox);
  const voiceUBI = await contract.getVoiceUBI(tournamentId);

  return parseInt(voiceUBI);
}

export async function getClaimablePowerRound(voterId, tournamentId, roundId) {
  if (!voterId) {
    return null;
  }
  const synced = await isSynced(voterId, tournamentId, roundId);
  const bonus = await getRoundBonus(voterId, tournamentId, roundId);

  console.log(
    'roundId',
    roundId,
    'synced',
    synced,
    'bonus',
    bonus[0].toString(),
    fromWei(bonus[1]).toString()
  );
  return {
    synced,
    roundId,
    vBonus: parseInt(bonus[0]),
    fvtBonus: bonus[1]
  };
}

export async function getClaimablePowerAll(voterId, tournamentId) {
  console.log('getClaimablePowerAll', voterId, tournamentId);
  const currentRoundId = await getCurrentRoundId(tournamentId);
  const lastClaimableRound = currentRoundId - 2;
  const unclaimedRounds = [];
  const deferred = [];

  let vBonus = 0;

  let fvtBonus = toBN(0);

  // TODO: this is linear in the number of rounds...
  for (let roundId = 1; roundId <= lastClaimableRound; roundId++) {
    deferred.push(getClaimablePowerRound(voterId, tournamentId, roundId));
  }

  const results = await Promise.all(deferred);

  results.forEach((result) => {
    if (result && result.fvtBonus.toString() !== '0' && !result.synced) {
      vBonus += result.vBonus;
      fvtBonus = fvtBonus.add(toBN(result.fvtBonus));
      unclaimedRounds.push(result.roundId);
    }
  });

  fvtBonus = parseFloat(fromWei(fvtBonus));

  return {
    unclaimedRounds,
    vBonus,
    fvtBonus
  };
}

export async function getProposedPrice(symbol) {
  const oracle = await ethInstance.getContract('read', ChainLinkOracle);
  const price = await oracle.prices(asciiToHex(symbol));

  return price;
}

export async function getLastProposal() {
  const oracle = await ethInstance.getContract('read', ChainLinkOracle);
  const numProposals = await oracle.numProposals();
  const proposal = await oracle.proposals(numProposals);

  console.log('numProposals', numProposals);
  console.log('proposal', proposal);
  return proposal;
}

export async function getOwner(token) {
  const id = await ethInstance.getContract('read', Identity);

  return await id.ownerOf(token);
}

export async function getUnclaimedTokens(voterId) {
  const trollbox = await ethInstance.getContract('read', Trollbox);
  const tokensWon = await trollbox.tokensWon(voterId);

  console.log('tokensWon', voterId, tokensWon, fromWei(tokensWon));
  return parseFloat(fromWei(tokensWon));
}

export async function getTournament(tournamentId) {
  const contract = await ethInstance.getContract('read', Trollbox);
  const tournament = await contract.tournaments(tournamentId);

  tournament.startTime = parseInt(tournament.startTime);
  tournament.roundLengthSeconds = parseInt(tournament.roundLengthSeconds);
  tournament.tournamentId = parseInt(tournament.tournamentId);
  tournament.currentRoundId = parseInt(await getCurrentRoundId(tournamentId));
  //  tournament.tokenList = await getTokenList()
  tournament.tokenRoundBonus = fromWei(tournament.tokenRoundBonus);

  return tournament;
}

export async function getVoteTotals(tournamentId, roundId, numOptions) {
  // console.log('getVoteTotals', tournamentId, roundId, numOptions)
  const contract = await ethInstance.getContract('read', Trollbox);
  const deferred = [];

  for (let i = 1; i <= numOptions; i++) {
    deferred.push(contract.getVoteTotals(tournamentId, roundId, i));
  }

  const votes = await Promise.all(deferred);

  // console.log('voteNums', tournamentId, roundId, voteNums)
  return votes.map((x) => parseInt(x));
}

export async function getVoteTotal(tournamentId, roundId, option) {
  const contract = await ethInstance.getContract('read', Trollbox);

  const totals = await contract.getVoteTotals(tournamentId, roundId, option);

  return totals.map((x) => parseInt(x));
}

export async function getCurrentRoundId(tournamentId) {
  const contract = await ethInstance.getContract('read', Trollbox);
  const roundId = await contract.getCurrentRoundId(tournamentId);

  return parseInt(roundId);
}

export async function getIdentityDecayFactor() {
  const contract = await ethInstance.getContract('read', Identity);
  const bigPrice = await contract.identityDecayFactor();
  return fromWei(bigPrice);
}

export async function getIdentityPrice() {
  const contract = await ethInstance.getContract('read', Identity);
  const bigPrice = await contract.getIdentityPrice();

  return fromWei(bigPrice);
}

export function getDaysBack(tournament, roundId) {
  if (roundId === 0 || roundId === tournament.currentRoundId - 1) {
    console.log({ tournament });
    const roundStart =
      tournament.startTime + (tournament.currentRoundId - 1) * tournament.roundLengthSeconds;
    const now = Math.floor(new Date().getTime() / 1000);

    return Math.floor((now - roundStart) / 86400);
  } else {
    return 7;
  }
}

// export async function getTokenAddress(chainId) {
//   console.log('FVT', chainId, FVT.networks);
//   return FVT.networks[chainId].address;
// }

export async function getWinnerIndex(tournament, roundId) {
  if (roundId === 0 || roundId === tournament.currentRoundId - 1) {
    return 0;
  } else {
    const winnerIndex = await getWinner(tournament.tournamentId, roundId);

    return parseInt(winnerIndex);
  }
}

export async function getWinnerChainlink() {
  const contract = await ethInstance.getContract('read', ChainLinkOracle);
  const prices = await contract.getWinner();

  console.log('prices', prices);
  return prices;
}
