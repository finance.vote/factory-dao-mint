import axios from 'axios';
const formData = require('form-data');

const sendRequest = async (
  mediaType,
  group,
  value,
  signature,
  currentAccount,
  isSingleFile,
  singleId
) => {
  try {
    const data = new formData();
    data.append('signature', signature);
    data.append('address', currentAccount);
    if (value.media) data.append('media', value.media);
    if (value.profilePic) data.append('profilePic', value.profilePic);
    data.append('metadata', value.meta);
    data.append('isSingleFile', isSingleFile);
    if (isSingleFile) data.append('singleId', singleId);

    const response = await axios.post(`/upload/${mediaType}/${group}/${value.id}`, data, {
      maxBodyLength: 'Infinity',
      headers: {
        'Content-Type': `multipart/form-data; boundary=${data._boundary}`,
        token: 'CUSTOM_JWT'
      }
    });
    return response.data;
  } catch (err) {
    console.log(`Error`, err?.message);
    throw err;
  }
};

export default async function uploadMediaFiles(
  mediaType,
  group,
  files,
  signature,
  currentAccount,
  isSingleFile
) {
  try {
    const startDate = Math.floor(new Date().getTime() / 1000);
    for (let value of files) {
      await sendRequest(
        mediaType,
        group,
        value,
        signature,
        currentAccount,
        isSingleFile,
        files[0].id
      );
    }

    const endDate = Math.floor(new Date().getTime() / 1000);
    console.log(endDate - startDate, 'seconds');
  } catch (err) {
    console.log(err);
    throw err;
  }
}
