import Events from 'servment';

let eventsHelper;
export function getDefaultSub() {
  const subscriber =
    process.env.NEXT_PUBLIC_SUBSCRIBER_URL || 'subscriber-test.influencebackend.xyz';
  if (!eventsHelper) eventsHelper = new Events(subscriber);
  return eventsHelper;
}

export async function getEvents(chainId, abi, abiName, eventName) {
  try {
    const abiNetwork = abi.networks[chainId];
    const contractAddress = abiNetwork.address;
    const fromBlock = abiNetwork.fromBlock;
    const events = getDefaultSub();
    const abiEvents = events.getEventsFromAbi(abi);
    const subResponse = await events.getAllEvents({
      project: abiName,
      chainId,
      eventName,
      requestBody: {
        abi: abiEvents,
        params: {
          contractAddress,
          fromBlock,
          query: {
            fromBlock
          }
        },
        setupOnly: true
      }
    });
    if (subResponse.success) return subResponse.allEvents || [];
    else throw subResponse.error;
  } catch (error) {
    console.error('Cannot get the events.\n', error);
  }
}

export const generateEventsSubscription = (eventName, chainId, contractAddress, callback) => {
  try {
    console.log(
      `Subscribing to ${eventName} event, chainId:${chainId} contract: ${contractAddress}`
    );
    const events = getDefaultSub();
    events.initializeEventListening(
      async (event) => await callback(event),
      chainId,
      contractAddress,
      [eventName]
    );
  } catch (err) {
    console.error(`Error in generate ${eventName} subscription`, err);
  }
};
