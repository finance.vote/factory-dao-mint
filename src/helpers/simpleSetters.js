import { ethInstance, fromWei, toWei } from 'evm-chain-scripts';
// import ReferralProgram from '../contracts/ReferralProgram.json';
import Identity from '../contracts/Identity.json';
import FVT from '../contracts/Token.json';
import Trollbox from '../contracts/Trollbox.json';
import TrollboxProxy from '../contracts/TrollboxProxy.json';
import { getClaimablePowerAll, getClaimablePowerRound } from './simpleGetters';

const emptyHash = '0x0000000000000000000000000000000000000000000000000000000000000000';

export async function updateAccount(voterId, tournamentId, roundId) {
  const trollbox = await ethInstance.getContract('write', Trollbox);
  const account = await ethInstance.getEthAccount();

  await trollbox.updateAccount(voterId, tournamentId, roundId, {
    from: account,
    gas: 130000
  });
}

export async function submitVote(voterId, tournamentId, choices, weights, updateRoundId = 0) {
  const gasLimit = 120000 + 50000 * choices.length;
  const contract = await ethInstance.getContract('write', Trollbox, {
    from: account,
    gas: gasLimit
  });
  const account = await ethInstance.getEthAccount();

  console.log('vote', voterId, tournamentId, choices, weights, updateRoundId);
  // 1 vote costs < 150k, 10 votes costs 550k
  // 400k / 9 ~= 45k => gasPrice = 100k + 45k * choices.length

  console.log('submitVote', { voterId, tournamentId, choices, weights, updateRoundId });
  return await contract.vote(voterId, tournamentId, choices, weights, emptyHash, updateRoundId);
}

export async function createIdentity(maxPrice) {
  const account = await ethInstance.getEthAccount();
  const identity = await ethInstance.getContract('write', Identity, { from: account, gas: 150000 });
  const fvt = await ethInstance.getContract('write', FVT, {
    from: account,
    gas: 50000
  });

  const currentApproval = await fvt.allowance(account, identity.address);

  if (parseFloat(fromWei(currentApproval.toString())) < parseFloat(maxPrice)) {
    const approveTransaction = await fvt.approve(
      identity.address,
      '1000000000000000000000000000000'
    );

    await approveTransaction.wait();
    console.log('finished approving');
  }

  const createId = await identity.createMyIdentity(toWei(maxPrice));
  await createId.wait();
  // await saveNewEvent(maxPrice, account);
  // const rcpt =
  // const ditId = rcpt.events.IdentityCreated.returnValues.token;

  //  if (referer > 0) {
  //    const referral = await ethInstance.getContract('write', ReferralProgram)
  //
  //    console.log('setReferral')
  //    // referer first, referee second
  //    referral.setReferral(referer, ditId).send({ from: account, gas: 100000 })
  //  }

  // return ditId;
}

export async function claimPowerAllRounds(voterIds, setClaimError = null) {
  const gasLimit =
    130000 * packages.voterIds.length + 55000 * packages.voterIdsWithTokens.length + 15000;
  const trollboxProxy = await ethInstance.getContract('write', TrollboxProxy, {
    from: account,
    gas: gasLimit
  });
  const account = await ethInstance.getEthAccount();
  const tournamentIds = [1];
  const packages = {
    voterIds: [],
    tournamentIds: [],
    roundIds: []
  };

  let bonusSumm = 0;

  console.log('voterIds', voterIds);
  for (let j = 0; j < voterIds.length; j++) {
    const voterId = voterIds[j];

    if (voterId) {
      for (let i = 0; i < tournamentIds.length; i++) {
        const tournamentId = tournamentIds[i];
        const roundIdObj = await getClaimablePowerAll(voterId, tournamentId);

        roundIdObj.unclaimedRounds.forEach((roundId) => {
          packages.voterIds.push(voterId);
          packages.tournamentIds.push(tournamentId);
          packages.roundIds.push(roundId);
        });
        bonusSumm += roundIdObj.fvtBonus;
      }
    }
  }

  const trollbox = await ethInstance.getContract('read', Trollbox);
  const fvt = await ethInstance.getContract('read', FVT);
  const balance = await fvt.balanceOf(trollbox._address);

  console.log('voterIds', voterIds);
  packages.voterIdsWithTokens = voterIds.filter((x) => x.unclaimedTokens > 0).map((x) => x.tokenId);
  packages.voterIdsWithTokens.push(...packages.voterIds);
  packages.voterIdsWithTokens = packages.voterIdsWithTokens.filter(
    (el, i, arr) => arr.indexOf(el) === i
  );
  //  packages.voterIdsWithTokens = packages.voterIds.filter((el, i, arr) => arr.indexOf(el) === i)

  console.log('claimPower', Object.values(packages));
  if (packages.voterIds.length > 0) {
    if (balance >= bonusSumm) {
      await trollboxProxy.updateAndWithdraw(...Object.values(packages));
    } else {
      if (setClaimError) {
        setClaimError('not enough FVT');
      }
    }
  }
}

export async function claimPowerOneRound(voterId, roundId, setClaimError = null) {
  const gasLimit = 130000 + 55000 + 15000;
  const trollboxProxy = await ethInstance.getContract('write', TrollboxProxy, {
    from: account,
    gas: gasLimit
  });
  const account = await ethInstance.getEthAccount();
  const tournamentIds = [1];
  const voterIds = [voterId];
  const roundIds = [roundId];

  let bonusSumm = 0;

  if (voterId) {
    for (let i = 0; i < tournamentIds.length; i++) {
      const roundIdObj = await getClaimablePowerRound(voterId, tournamentIds[i], roundId);

      bonusSumm += roundIdObj.fvtBonus;
    }
  }

  const trollbox = await ethInstance.getContract('read', Trollbox);
  const fvt = await ethInstance.getContract('read', FVT);
  const balance = await fvt.balanceOf(trollbox._address);

  if (balance >= bonusSumm) {
    await trollboxProxy.updateAndWithdraw(voterIds, tournamentIds, roundIds, voterIds);
  } else {
    if (setClaimError) {
      setClaimError('not enough FVT');
    }
  }
}
