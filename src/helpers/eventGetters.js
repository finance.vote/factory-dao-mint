import { ethInstance, fromWei, sha3 } from 'evm-chain-scripts';
import ChainLinkOracle from '../contracts/ChainLinkOracle2.json';
import Identity from '../contracts/Identity.json';
import Trollbox from '../contracts/Trollbox.json';
import {
  getClaimablePowerAll,
  getClaimablePowerRound,
  getOwner,
  getTokenList,
  getUnclaimedTokens,
  getVoiceCredits,
  getWinner
} from './simpleGetters';

const transferEventSignature = sha3('Transfer(address,address,uint256)');
const voteEventSignature = sha3(
  'VoteOccurred(uint256,uint256,uint256,uint256[],uint256[],bytes32)'
);
const winnerConfirmedEventSignature = sha3('WinnerConfirmed(uint256,uint256,int[])');

export async function getVotesByIdentity(callback, identityNum, tournamentId = 1) {
  console.log('getVotesByIdentity');
  const filter = { tournamentId: tournamentId, voterId: identityNum };

  await ethInstance.getAllEvents(
    async (event) => {
      console.log('VoteOccurred event', event);
      const winner = await getWinner(tournamentId, event.roundId);

      console.log('VoteOccurred event', event);
      event.winner = winner.toString();

      const winnings = await getClaimablePowerRound(identityNum, tournamentId, event.roundId);

      winnings.fvtBonus = parseFloat(fromWei(winnings.fvtBonus));
      event.winnings = winnings;
      callback(event);
    },
    Trollbox,
    'VoteOccurred',
    filter,
    0,
    'latest',
    voteEventSignature,
    Trollbox.abi[9].inputs
  );
}

export async function getIdentitiesForAccount(callback) {
  console.log('getIdentitiesForAccount');
  const account = await ethInstance.getEthAccount();
  const filter = { to: account };

  await ethInstance.getAllEvents(
    async (event) => {
      const owner = await getOwner(event.tokenId);

      console.log('event', event);
      if (event.tokenId && owner.toLowerCase() === account.toLowerCase()) {
        const pow = await getClaimablePowerAll(event.tokenId, 1);
        const unclaimedTokens = await getUnclaimedTokens(event.tokenId);
        const currentV = await getVoiceCredits(1, event.tokenId);

        event.tokenId = parseInt(event.tokenId);

        event.unclaimedRounds = pow.unclaimedRounds;
        event.unclaimedTokens = unclaimedTokens;
        console.log('event.unclaimedTokens', event.unclaimedTokens);
        event.fvtBalance = unclaimedTokens;
        event.voteMarkets = [
          {
            name: 'DeFi Winners',
            v: currentV,
            rewards: {
              v: pow.vBonus,
              fvt: pow.fvtBonus
            }
          }
        ];
        console.log('pow', pow);
        callback(event);
      } else {
        console.log('owner', owner, 'account', account);
      }
    },
    Identity,
    'Transfer',
    filter,
    0,
    'latest',
    transferEventSignature,
    Identity.abi[10].inputs
  );
}

export async function getHistoricChainlinkPrices(roundId, callback) {
  // console.log('getHistoricPricesForAccount', roundId)
  const filter = { roundId };
  const tournamentId = 1;
  const tokens = await getTokenList(tournamentId, roundId);

  await ethInstance.getAllEvents(
    async (event) => {
      // console.log('historic price event', event)
      const prices = {};

      for (let i = 0; i < tokens.length; i++) {
        const token = tokens[i];

        prices[token.symbol] = {
          value: event.prices[i] / 10 ** token.decimals,
          difference: 0
        };
      }
      // console.log('processed prices', { prices })
      callback(prices);
    },
    ChainLinkOracle,
    'WinnerConfirmed',
    filter,
    0,
    'latest',
    winnerConfirmedEventSignature,
    ChainLinkOracle.abi[9].inputs
  );
}

export async function getHistoricChainlinkPricesWrapper(roundId) {
  return new Promise((resolve) => {
    getHistoricChainlinkPrices(roundId, (successResponse) => {
      resolve(successResponse);
    });
  });
}
