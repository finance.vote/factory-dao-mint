export function shortenWalletAddress(address) {
  return address ? `${address.substr(0, 6)}...${address.substr(-4)}` : '';
}
