import axios from 'axios';
import NFTAuctionFactory from 'contracts/NFTAuctionFactory.json';
import VoterID from 'contracts/VoterID.json';
import { ethInstance, fromWei, ipfsGet } from 'evm-chain-scripts';
import { getEvents } from 'helpers/eventsSubscriber';

const getNftMetadata = async (id, contractAddress, chainId = null) => {
  if (id && id != 0 && contractAddress) {
    try {
      const identity = await ethInstance.getReadContractByAddress(
        VoterID,
        contractAddress,
        chainId
      );

      const uri = await identity.tokenURI(parseInt(id));

      if (!uri) return null;
      let metadata;
      if (uri.startsWith('ipfs') || uri.startsWith('Q')) metadata = await ipfsGet(uri);
      else {
        metadata = await fetch(uri).then((res) => res.json());
      }
      return metadata;
    } catch (e) {
      return null;
    }
  }
};
export const fetchNftImage = async (nftId, contractAddress, currentNetwork) => {
  if (!nftId) return '';

  const metadata = contractAddress
    ? await getNftMetadata(nftId, contractAddress, currentNetwork)
    : null;

  if (!metadata) {
    return {
      mediaUrl: null,
      isVideo: true
    };
  }
  if (metadata.animation_url?.startsWith('ipfs')) {
    return {
      mediaUrl: 'https://ipfs.io/ipfs/' + metadata.image.replace('ipfs://', ''),
      isVideo: true
    };
  } else if (metadata.animation_url) {
    return { mediaUrl: metadata.image, isVideo: true };
  }
  if (metadata.image.startsWith('ipfs')) {
    return 'https://ipfs.io/ipfs/' + metadata.image.replace('ipfs://', '');
  } else {
    return { mediaUrl: metadata.image, isVideo: false };
  }
};

export const getCurrentPrice = async () => {
  try {
    const contract = await ethInstance.getContract('read', NFTAuctionFactory);
    const bigPrice = await contract.getCurrentPrice();
    return bigPrice;
  } catch (error) {
    console.log(error);
    return 0;
  }
};

export const getAuctionData = async () => {
  try {
    const contract = await ethInstance.getContract('read', NFTAuctionFactory);
    const currentAuctionId = await contract.currentAuctionId();
    const currentAuction = await contract.auctions(currentAuctionId);

    return currentAuction;
  } catch (error) {
    console.log(error);
    return 0;
  }
};

export const getCurrentAuctionId = async () => {
  try {
    const contract = await ethInstance.getContract('read', NFTAuctionFactory);
    const currentAuctionId = await contract.currentAuctionId();

    return currentAuctionId;
  } catch (error) {
    console.log(error);
    return 0;
  }
};

export const getUsdPriceOfEth = async () => {
  try {
    const usdPriceOfEth = await axios.get(
      'https://api.coingecko.com/api/v3/simple/price?ids=ethereum&vs_currencies=usd'
    );

    return usdPriceOfEth.data.ethereum.usd;
  } catch (error) {
    console.log(error);
    return 0;
  }
};

export const getBidEvents = async (currentAuctionId, chainId) => {
  try {
    const events = await getEvents(chainId, NFTAuctionFactory, 'NftAuction', 'BidOccurred');
    await getEvents(chainId, NFTAuctionFactory, 'NftAuction', 'AuctionCreated');

    const eventsFromCurrentAuction = events
      ?.filter((event) => parseInt(event.data.auctionId) === parseInt(currentAuctionId))
      .map((event) => event.data);

    return eventsFromCurrentAuction;
  } catch (err) {
    console.error(err);
    throw 'Error while trying to get auction events';
  }
};

const getSamplesSinceLastBid = async () => {
  try {
    const contract = await ethInstance.getContract('read', NFTAuctionFactory);
    const samplesSinceLastBid = await contract.getSamplesSinceLastBid();
    return parseInt(samplesSinceLastBid);
  } catch (error) {
    console.log(error);
    return 0;
  }
};

export const getStructures = async () => {
  try {
    const contract = await ethInstance.getContract('read', NFTAuctionFactory);
    const currentAuctionId = await contract.currentAuctionId();
    const structures = await contract.structures(currentAuctionId);

    return structures;
  } catch (error) {
    console.log(error);
    return 0;
  }
};

const addDecayEvents = (bidAmount, numSamples, allEventsCount, usdPriceOfEth, nextBidPrice) => {
  const newDecayEvents = [];
  for (let i = 0; i < numSamples; i++) {
    const decay = Number(bidAmount) + Number(bidAmount) / (1 << i);

    if (parseInt(decay) === parseInt(nextBidPrice)) {
      break;
    }

    const intFromWei = Number(fromWei(parseInt(decay)));
    newDecayEvents.push({
      x: allEventsCount + i,
      y: intFromWei * usdPriceOfEth,
      yLabel: intFromWei * usdPriceOfEth,
      event: 'decay'
    });
  }
  return newDecayEvents;
};

export const getAllAuctionEvents = async (
  usdPriceOfEth,
  bidEvents,
  numSamplesBeforeSale,
  onSold
) => {
  try {
    const samplesSinceLastBid = await getSamplesSinceLastBid();
    const maxNumSamples = numSamplesBeforeSale;
    const newEvents = [];
    if (maxNumSamples) {
      bidEvents.forEach((event, index) => {
        const intFromWei = parseFloat(fromWei(parseInt(event.bidAmount)));
        const usdValue = intFromWei * usdPriceOfEth;
        newEvents.push({
          x: newEvents.length,
          y: usdValue,
          yLabel: usdValue,
          event: 'bid'
        });

        if (bidEvents.length - 1 === index) {
          if (samplesSinceLastBid >= maxNumSamples) {
            //decays between last bid and sold
            const decayEvents = addDecayEvents(
              event.bidAmount,
              maxNumSamples,
              newEvents.length,
              usdPriceOfEth
            );
            newEvents.push(...decayEvents);
            newEvents.push({
              x: newEvents.length,
              y: usdValue,
              yLabel: usdValue,
              event: 'sold'
            });
            onSold(true);
          } else {
            //decays when is not sold yet
            const decayEvents = addDecayEvents(
              event.bidAmount,
              samplesSinceLastBid,
              newEvents.length,
              usdPriceOfEth
            );
            newEvents.push(...decayEvents);
          }
        } else {
          // decays between bids
          const decayEvents = addDecayEvents(
            event.bidAmount,
            maxNumSamples,
            newEvents.length,
            usdPriceOfEth,
            bidEvents[index + 1].bidAmount
          );
          newEvents.push(...decayEvents);
        }
      });
    }

    return newEvents;
  } catch (err) {
    console.log('error', err);
    return [];
  }
};
