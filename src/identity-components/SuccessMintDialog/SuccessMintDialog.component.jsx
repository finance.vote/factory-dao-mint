import DialogWhite from 'components/DialogWhite';
import financevoteLogo from 'images/fvt/financevote_dark.svg';
import Image from 'next/image';
import styles from './SuccessMintDialog.module.scss';

const SuccessMintDialog = ({ isSuccessDialogOpen, onCloseDialogSuccess, mintedIdentity }) => {
  return (
    <DialogWhite isOpen={isSuccessDialogOpen} handleClose={onCloseDialogSuccess} width="650px">
      <div className={styles.successDialogContainer}>
        <Image className={styles.financevoteLogo} src={financevoteLogo} alt="Finance.Vote Logo" />
        <div className={styles.title}>
          Congratulations! <br />
          FVT ETH ID {mintedIdentity} Is Yours
        </div>
        <div className={styles.info}>
          You can now access the FVT discord to verify your membership to the FVTDAO
        </div>
        <div className={styles.buttons}>
          <a
            href={'https://discord.gg/financedotvote'}
            className={styles.joinDiscord}
            rel="noopener noreferer"
          >
            Join the Discord
          </a>
          <a
            href={'https://opensea.io/collection/fvt'}
            className={styles.opensea}
            rel="noopener noreferer"
          >
            view on opensea
          </a>
        </div>
      </div>
    </DialogWhite>
  );
};

export default SuccessMintDialog;
