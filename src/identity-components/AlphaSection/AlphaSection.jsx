import alphaIcon1 from 'images/community_icon.svg';
import alphaIcon2 from 'images/markets_icon.svg';
import alphaIcon3 from 'images/whitelist_icon.svg';
import Image from 'next/image';
import styles from './AlphaSection.module.scss';

const AlphaConfig = [
  {
    icon: alphaIcon1,
    title: 'Exclusive Gated Community',
    text: 'FVT Discord, home to exclusive FVTDAO and FactoryDAO alpha, insight and conversation'
  },
  {
    icon: alphaIcon2,
    title: 'Prediction Markets',
    text: 'FVT Markets dApp will be launching on Polygon soon, participate in the low fee prediction game to earn tokens.',
    additionalText:
      'ETH FVT ID holders will get a free whitelist spot for the new Polygon ID series.'
  },
  {
    icon: alphaIcon3,
    title: 'Discounted Whitelists',
    text: 'FVT ETH ID holders secure a discount on future FactoryDAO NFT launches'
  }
];

const AlphaItems = () => {
  return AlphaConfig.map((alpha, index) => {
    return (
      <div key={index} className={styles.alphaItem}>
        <Image src={alpha.icon} alt={`alpha_${index}`} />
        <span className={styles.itemTitle}>{alpha.title}</span>
        <p>{alpha.text}</p>
        <span className={styles.additionalText}>{alpha.additionalText}</span>
      </div>
    );
  });
};

const AlphaSection = () => {
  return (
    <div className={styles.sectionWrapper}>
      <div className={styles.sectionContainer}>
        <div className={styles.sectionTitle}>
          <span className={styles.title}>the nft id that keeps on giving (alpha)</span>
          <span className={styles.subTitle}>
            An exclusive run of 1000 FVT ETH IDs are available to mint.
            <br />
            There will only ever be 1,000 available and each ID comes with special privileges.
          </span>
        </div>
        <AlphaItems />
        <div className={styles.lowestSection}>
          <div className={styles.informationBox}>
            <span className={styles.infoTitle}>Tune your DAO through NFT ID Governance</span>
            <span className={styles.infoDescription}>
              FVT ID holders have voting privileges in DAO Governance. Which tokens feature in the
              prediction market game, yield staking incentives, treasury distribution and more.
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AlphaSection;
