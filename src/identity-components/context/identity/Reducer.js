const Reducer = (state, action) => {
  switch (action.type) {
    case 'SET_IDENTITY':
      state.identities[action.payload.key] = action.payload.value;
      return state;
    default:
      return state;
  }
};

export default Reducer;
