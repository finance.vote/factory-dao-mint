import Image from 'next/image';
import React from 'react';
import templateImage from '../../../images/fvt/fvtZero.jpg';

function ArtIdentityImage({ identityNum, chainId }) {
  const url =
    identityNum && chainId && identityNum <= 1000
      ? 'https://gallery.vote/art/chain/' + parseInt(chainId) + '/voter/' + parseInt(identityNum)
      : null;

  return identityNum && chainId && identityNum <= 1000 ? (
    <Image src={url} alt="mint-identity-image" fill />
  ) : (
    <Image alt="default-mint-image" src={templateImage} fill />
  );
}

export default ArtIdentityImage;
