import axios from 'axios';
import classNames from 'classnames';
import PageLoading from 'components/Ui/PageLoading/PageLoading.component';
import { ConnectWallet, ethInstance } from 'evm-chain-scripts';
import { useGlobalState } from 'globalState';
import { createIdentity } from 'helpers/simpleSetters';
import SuccessMintDialog from 'identity-components/SuccessMintDialog/SuccessMintDialog.component';
import moment from 'moment';
import Image from 'next/image';
import {
  useChainId,
  useIdentityDecayFactor,
  useIdentityPrice,
  useNumIdentities,
  useTokenBalance
} from 'queries/token';
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import ReactPaginate from 'react-paginate';
import { toast } from 'react-toastify';
import { shortenWalletAddress } from '../../../helpers/index';
import networks from '../../../helpers/networks.json';
import expandSvg from '../../../images/expand.svg';
import sliderIcon from '../../../images/fvt/financevote_icon.svg';
import loaderImg from '../../../images/loader.svg';
import ArtIdentityImage from './ArtIdentityImage';
import styles from './IdentityFormMintForm.module.scss';

const If = ({ condition, children }) => {
  if (!condition) return null;

  return children;
};

function IdentityFormMint({ currentNetwork }) {
  const [address, setAddress] = useGlobalState('walletAddress');
  const [isSuccessDialogOpen, setIsSuccessDialogOpen] = useState(false);
  const [mintedId, setMintedId] = useState(0);
  const [activePage, setActivePage] = useState(1);
  const { handleSubmit } = useForm();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [mintedEvents, setMintedEvents] = useState([]);
  const [isTableDataLoading, setIsTableDataLoading] = useState(false);
  const identityPrice = useIdentityPrice();
  const identityDecayFactor = useIdentityDecayFactor();
  const {
    data: numIdentities,
    refetch: refetchNumIdentities,
    isLoading: numIdentitiesLoading
  } = useNumIdentities();
  const tokenBalance = useTokenBalance();

  const chainId = useChainId();
  const allIdentitiesCount = 1000;
  const percentage = ((numIdentities / allIdentitiesCount) * 100).toFixed(1);

  const explorer = (str, type = 'address') => {
    return `${networks[currentNetwork]?.explorer}/${type}/${str}`;
  };

  const saveNewEvent = async (price, voterAddress) => {
    let today = moment().format('DD/MM/YYYY');
    const payload = {
      voterAddress: voterAddress,
      date: today,
      price: price,
      fvtId: (numIdentities + 1).toString()
    };
    return await axios.post(`/minting/data`, payload);
  };

  const getMintedEvents = async () => {
    try {
      setIsTableDataLoading(true);
      const { data: tableData } = await axios.get(`/minting/data`);
      setMintedEvents(tableData.sort((el1, el2) => Number(el2.fvtId) - Number(el1.fvtId)));
    } catch (err) {
      console.log('error', err);
      toast.error('Table data could not be loaded', {
        position: 'top-center'
      });
    } finally {
      setIsTableDataLoading(false);
    }
  };

  useEffect(() => {
    (async () => await getMintedEvents())();
  }, []);

  async function onSubmit() {
    try {
      setIsSubmitting(true);
      const price = identityPrice.data;
      await createIdentity(price, saveNewEvent);
      setMintedId(numIdentities + 1);
      await getMintedEvents();
      setIsSuccessDialogOpen(true);
    } catch (error) {
      console.log('error', error);
      toast.error('Something went wrong. Please try again minting an identity.', {
        position: 'top-center'
      });
    } finally {
      setIsSubmitting(false);
    }
  }

  // TODO: is this still necessary? I think useQuery takes care of this now
  useEffect(() => {
    const timeout1 = setTimeout(identityPrice.refetch, 1000);
    const interval1 = setInterval(identityPrice.refetch, 1000);
    const timeout2 = setTimeout(refetchNumIdentities, 1000);
    const interval2 = setInterval(refetchNumIdentities, 1000);

    return () => {
      clearTimeout(timeout1);
      clearInterval(interval1);
      clearTimeout(timeout2);
      clearInterval(interval2);
    };
  }, [identityPrice.refetch, refetchNumIdentities]);

  const onPaginationChange = (pagination) => {
    setActivePage(pagination.selected + 1);
  };

  const handleLoad = async () => {
    try {
      const ethAccount = await ethInstance.getEthAccount(false);
      setAddress(ethAccount);
    } catch (e) {
      console.log(e);
    }
  };

  const onCloseDialogSuccess = () => {
    setIsSuccessDialogOpen(false);
  };

  const isPriceHigherThanTokenBalance = Number(tokenBalance.data) < Number(identityPrice.data);

  return (
    <>
      <SuccessMintDialog
        isSuccessDialogOpen={isSuccessDialogOpen}
        onCloseDialogSuccess={onCloseDialogSuccess}
        mintedIdentity={mintedId}
      />
      <div className={styles.identityFormContainer}>
        <If condition={numIdentitiesLoading || chainId.isLoading || isSubmitting}>
          <div className={styles.loadingContainer}>
            <PageLoading />
          </div>
        </If>
        <div className={styles.image}>
          <ArtIdentityImage identityNum={numIdentities + 1} chainId={chainId.data} />
        </div>
        <form noValidate className={styles.identityForm} onSubmit={handleSubmit(onSubmit)}>
          <>
            <div className={styles.fvtId}>FVT ID {numIdentities + 1 || ''}</div>
            <div className={styles.formRow}>
              <div className={styles.formRow}>
                <div className={styles.valuesContainer}>
                  <div className={styles.singleValue}>
                    <span
                      className={styles.value}
                      name="price"
                      value={`${identityPrice.data || '0.0'} $FVT`}
                    >{`${identityPrice.data || '0.0'} $FVT`}</span>
                    <span className={styles.label}>current price</span>
                  </div>
                  <div className={styles.singleValue}>
                    <span
                      className={styles.value}
                      name="identityDecayFactor"
                      value={identityDecayFactor.data}
                    >
                      {identityDecayFactor.data || '0.0'}
                    </span>
                    <span className={styles.label}>price decay per block</span>
                  </div>
                </div>
              </div>
            </div>
            <>
              <div className={styles.buttonContainer}>
                {!address?.length ? (
                  <ConnectWallet
                    className={classNames(styles.mintButton, 'mint-identity-button')}
                    onConnect={handleLoad}
                  />
                ) : (
                  <button
                    type="submit"
                    disabled={
                      numIdentities >= 1000 || isPriceHigherThanTokenBalance || !address?.length
                    }
                    className={classNames(styles.mintButton, 'mint-identity-button')}
                  >
                    Mint now
                  </button>
                )}
                {isPriceHigherThanTokenBalance && (
                  <div className={styles.buttonInfo}>
                    You do not have sufficient balance to mint
                  </div>
                )}
              </div>

              <If condition={numIdentities}>
                <Slider
                  min={0}
                  className={styles.slider}
                  value={numIdentities}
                  max={1000}
                  disabled
                  handleStyle={{
                    backgroundColor: '#fff',
                    background: `url(${sliderIcon.src}) no-repeat`,
                    height: '26px',
                    width: '26px',
                    filter: 'drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25))',
                    border: 0,
                    top: '-2px',
                    opacity: '1'
                  }}
                  trackStyle={{
                    backgroundColor: '#fff'
                  }}
                  railStyle={{
                    backgroundColor: 'rgba(255, 255, 255, 0.25)'
                  }}
                  style={{
                    backgroundColor: 'transparent',
                    width: '100%'
                  }}
                  dotStyle={{ display: 'none' }}
                  marks={{
                    0: <span className={styles.sliderInfo}>{numIdentities} MINTED</span>,
                    [1000]: <span className={styles.sliderInfo}>{1000 - numIdentities} LEFT</span>
                  }}
                />
                <span className={styles.percentage}>{percentage}%</span>
              </If>
            </>
            {!window.ethereum && (
              <div className={styles.col}>
                No wallet detected. Please install Metamask in order to mint an identity.
              </div>
            )}
          </>
        </form>
        <span className={styles.note}>
          Note: To mint you will need to approve 2 transactions, one to approve token spend and
          another to mint the NFT ID
        </span>

        <div className={styles.mintsTable}>
          <span className={styles.title}>Previous mints</span>
          {!isTableDataLoading ? (
            <>
              <div className={styles.tableContainer}>
                <table>
                  <thead>
                    <tr className={styles.head}>
                      <td>ID NO.</td>
                      <td>MINTED BY</td>
                      <td>PRICE</td>
                      <td>DATE</td>
                    </tr>
                  </thead>
                  <tbody>
                    {mintedEvents?.length ? (
                      mintedEvents.slice((activePage - 1) * 4, activePage * 4).map((event) => (
                        <tr key={event._id}>
                          <td className={styles.firstCol}>{event.fvtId}</td>
                          <td>
                            <div>
                              {shortenWalletAddress(event.voterAddress)}{' '}
                              <a
                                className={styles.link}
                                href={explorer(event.voterAddress)}
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                <Image src={expandSvg} alt="expand" />
                              </a>
                            </div>
                          </td>
                          <td className={styles.price}>{event.price} $FVT</td>
                          <td>{event.date}</td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td className={styles.firstCol} colSpan="5">
                          <p>There aren&apos;t any previous mints here yet!</p>
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
              {mintedEvents?.length ? (
                <ReactPaginate
                  forcePage={activePage - 1}
                  pageCount={Math.ceil(mintedEvents.length / 4)}
                  pageRangeDisplayed={1}
                  marginPagesDisplayed={1}
                  onPageChange={onPaginationChange}
                  breakLabel="/"
                  previousLabel="< previous"
                  nextLabel="next >"
                  containerClassName={styles.paginationContainer}
                  activeClassName={styles.paginationActivePage}
                  pageLinkClassName={styles.paginationLink}
                  disabledClassName={styles.pageDisabled}
                  nextClassName={styles.nextPrevClassName}
                  previousClassName={styles.nextPrevClassName}
                />
              ) : null}
            </>
          ) : (
            <Image src={loaderImg} alt="Table loader" />
          )}
        </div>
      </div>
    </>
  );
}

export default IdentityFormMint;
