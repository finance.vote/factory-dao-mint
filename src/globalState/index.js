import { ethInstance } from 'evm-chain-scripts';
import { useEffect } from 'react';
import { createGlobalState } from 'react-hooks-global-state';

const initialState = {
  walletAddress: '',
  walletInputMintValues: null
};

export const { useGlobalState, setGlobalState, getGlobalState } = createGlobalState(initialState);

const GlobalStateProvider = ({ children }) => {
  const [, setWalletAddress] = useGlobalState('walletAddress');
  const handleAddressChange = async () => {
    const acc = await ethInstance.getEthAccount(false);
    setWalletAddress(acc);
  };
  useEffect(() => {
    document.addEventListener('account_changed', handleAddressChange);
    (async () => {
      const account = await ethInstance.getEthAccount();
      if (account) setWalletAddress(account);
    })();
  }, []);

  return children;
};

export default GlobalStateProvider;
