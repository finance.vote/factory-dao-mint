import DialogWhite from 'components/DialogWhite';
import 'react-datepicker/dist/react-datepicker.css';
import styles from './BackendMintConfirmModal.module.scss';

const BackendMintConfirmModal = ({ selectedToken, isOpen, onModalClose, onConfirmMint }) => {
  const handleModalClose = () => {
    onModalClose();
  };

  const onClickMint = async () => {
    await onConfirmMint();
  };

  return (
    <DialogWhite isOpen={isOpen} handleClose={handleModalClose}>
      <h2 className={styles.header}>Do you want to mint ID #{selectedToken?.value}?</h2>
      <div className={styles.buttonContainer}>
        <button onClick={handleModalClose} className={styles.button}>
          CANCEL
        </button>
        <button onClick={onClickMint} className={styles.button}>
          OK
        </button>
      </div>
    </DialogWhite>
  );
};

export default BackendMintConfirmModal;
