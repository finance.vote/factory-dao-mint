import Image from 'next/image';
import React from 'react';
import styles from './FaqCard.module.scss';

const FaqCard = ({ icon, title, children }) => {
  return (
    <div className={styles.card}>
      <div className={styles.titleContainer}>
        <Image src={icon} alt="FAQ" />
        <span>{title}</span>
      </div>
      <div className={styles.description}>{children}</div>
    </div>
  );
};

export default FaqCard;
