import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

const ProgressBar = ({
  isVisible,
  tokensTotal,
  tokensMinted,
  percentage,
  handleStyle,
  sliderClassName,
  sliderInfoClassName,
  percentageClassName
}) => {
  if (!isVisible) return null;
  return (
    <>
      <Slider
        min={0}
        className={sliderClassName}
        value={tokensMinted}
        max={tokensTotal}
        disabled
        handleStyle={handleStyle}
        trackStyle={{
          backgroundColor: '#fff'
        }}
        railStyle={{
          backgroundColor: 'rgba(255, 255, 255, 0.25)'
        }}
        dotStyle={{ display: 'none' }}
        marks={{
          0: <p className={sliderInfoClassName}>{tokensMinted} MINTED</p>,
          [tokensTotal]: <p className={sliderInfoClassName}>{tokensTotal - tokensMinted} LEFT</p>
        }}
      />
      <span className={percentageClassName}>{percentage}%</span>
    </>
  );
};

export default ProgressBar;
