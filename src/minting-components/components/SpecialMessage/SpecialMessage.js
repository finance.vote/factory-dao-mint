const SpecialMessage = ({ content, className }) => {
  if (!content) return null;
  return <div className={className}>{content}</div>;
};

export default SpecialMessage;
