import axios from 'axios';
import { ConnectWallet, ethInstance } from 'evm-chain-scripts';
import React, { useState } from 'react';
import styles from './EligibilityChecker.module.scss';

const zeroAddr = '0x0000000000000000000000000000000000000000';

const EligibilityChecker = ({
  isOpen = false,
  walletAddress,
  setWalletAddress,
  group,
  merkleContract,
  eligibility,
  onError,
  customStyles,
  title,
  subtitle,
  CHAIN_ID
}) => {
  const [loadingEligibilityData, setLoadingEligibilityData] = useState(false);
  const [buttonText, setButtonText] = useState('Check my eligibility');
  const [isEligible, setIsEligible] = useState();

  const notEligibleStyle = {
    color: customStyles ? customStyles.titleDenied : '#FF3333'
  };
  const eligibleStyle = {
    color: customStyles ? customStyles.titleAccepted : '#00FF22'
  };

  const handleAddressChange = async () => {
    const addr = await ethInstance.getEthAccount();
    setWalletAddress(addr);
  };

  const onEligible = () => {
    setButtonText('you are whitelisted!');
    setIsEligible('whitelisted');
  };

  const onNotEligible = () => {
    setButtonText('you are not whitelisted');
    setIsEligible('blacklisted');
  };

  const onClickCheckEligibility = async () => {
    try {
      setLoadingEligibilityData(true);
      const { data: trees } = await axios.get(`minting/${group}/check/${walletAddress}?button=1`);
      let eligibleToAny = false;
      if (trees?.length) {
        const currentNetwork = CHAIN_ID ?? ethInstance.getChainId();

        for (const tree of trees) {
          const gateContract = await ethInstance.getReadContractByAddress(
            merkleContract,
            tree.gateAddress,
            currentNetwork
          );
          const {
            data: { proof }
          } = await axios.get(`/minting/proof/${tree._id}/${walletAddress}`);
          if (isOpen || proof.length) {
            const contractTree = await gateContract.merkleTrees(tree.treeIndex);
            const eligibilityContract = await ethInstance.getReadContractByAddress(
              eligibility,
              contractTree.eligibilityAddress,
              currentNetwork
            );
            if (eligibilityContract.address !== zeroAddr) {
              const isEligible = await eligibilityContract.isEligible(
                contractTree.eligibilityIndex,
                walletAddress,
                isOpen ? [] : proof
              );
              if (isEligible) {
                eligibleToAny = true;
                break;
              }
            }
          }
        }
      }
      if (eligibleToAny) {
        onEligible();
      } else {
        onNotEligible();
      }
    } catch (e) {
      console.error(e?.code, `please check account/network you're connected to.`);
      onError();
    } finally {
      setLoadingEligibilityData(false);
    }
  };

  const titleText = title ? title : 'Eligibility checker';

  const EligibleText =
    isEligible === 'blacklisted'
      ? 'You’re not in the whitelist'
      : isEligible === 'whitelisted'
      ? 'You’re in the whitelist'
      : titleText;

  const eligibleStyling =
    isEligible === 'blacklisted'
      ? notEligibleStyle
      : isEligible === 'whitelisted'
      ? eligibleStyle
      : {};

  return (
    <div className={customStyles?.wrapper ? customStyles.wrapper : styles.wrapper}>
      <h3 className={styles.title} style={eligibleStyling}>
        {EligibleText}
      </h3>
      {isEligible ? (
        <p className={styles.subtitle}>
          Join our Discord for a chance to earn a place in the premints, daily games and keep up
          with the community.
        </p>
      ) : subtitle ? (
        <p className={styles.subtitle}>{subtitle} </p>
      ) : (
        <>
          <p className={styles.subtitle}>Are you one of the chosen dog coin holders?</p>
          <p className={styles.subtitle}>
            Connect your wallet to check if you are in the whitelist for the Shibaku mass mint
          </p>
        </>
      )}
      {isEligible ? (
        <a
          className={styles.discordLink}
          href={'https://discord.gg/tribesdao'}
          rel="noreferer noopener"
        >
          JOIN DISCORD
        </a>
      ) : (
        <button
          disabled={loadingEligibilityData}
          style={
            isEligible === 'blacklisted' ? notEligibleStyle : isEligible ? eligibleStyle : null
          }
          className={!walletAddress?.length ? styles.hidden : styles.button}
          onClick={onClickCheckEligibility}
        >
          {buttonText}
        </button>
      )}

      <div className={walletAddress?.length ? styles.hidden : styles.connect}>
        <ConnectWallet onConnect={handleAddressChange} />
      </div>
    </div>
  );
};

export default EligibilityChecker;
