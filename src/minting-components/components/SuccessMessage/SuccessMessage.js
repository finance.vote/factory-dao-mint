const generateSuccessMessageFromPattern = (string, tokenId, symbol) => {
  return string.replace('%TOKEN_ID%', `${tokenId}`).replace('%SYMBOL%', symbol);
};

const SuccessMessage = ({
  isVisible,
  boughtToken,
  tokenSymbol,
  successTitle,
  successMessage,
  descriptionClassName,
  titleClassName
}) => {
  if (!isVisible) return null;
  return (
    <>
      <p className={titleClassName}>{successTitle}</p>
      <p className={descriptionClassName}>
        {generateSuccessMessageFromPattern(successMessage, boughtToken, tokenSymbol)}
      </p>
    </>
  );
};
export default SuccessMessage;
