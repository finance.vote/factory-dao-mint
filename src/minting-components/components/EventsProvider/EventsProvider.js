import axios from 'axios';
import { useEffect } from 'react';

const EventsProvider = ({ children, groupKey, handleStatusUpdate }) => {
  useEffect(() => {
    const sse = new EventSource(`${axios.defaults.baseURL}/events/${groupKey}`);
    sse.onmessage = handleStatusUpdate;
    return () => {
      sse.close();
    };
  }, []);
  return children;
};

export default EventsProvider;
