import { MINTING_STATUSES } from 'minting-components/helpers';
import { memo } from 'react';
import Light from './Light';
import styles from './TrafficLights.module.scss';

const LIGHTS = ['green', 'orange', 'red'];

const TrafficLights = ({
  visible,
  selectedToken,
  mintingStatus,
  error,
  highlightedIndicators,
  containerStyles,
  indicatorStyles, // can be { green etc. } or string 'classname'
  isSomeoneCurrentlyMinting
}) => {
  if (!visible) return null;
  const CLASSLIST = {
    indicatorContainer: containerStyles,
    indicators: {
      green: indicatorStyles?.green ?? styles.green,
      orange: indicatorStyles?.orange ?? styles.orange,
      red: indicatorStyles?.red ?? styles.red
    }
  };
  const HIGHLIGHT_CONDITIONS = {
    green:
      (selectedToken?.status === 'available' && !error) || mintingStatus === MINTING_STATUSES.DONE,
    orange:
      selectedToken?.status === 'reserved' && !error && mintingStatus !== MINTING_STATUSES.DONE,
    red:
      (mintingStatus !== MINTING_STATUSES.DONE && selectedToken?.status === 'unavailable') ||
      !!error
  };

  return (
    <div className={CLASSLIST.indicatorContainer}>
      {LIGHTS.map((lightType, index) => {
        return (
          <Light
            type={lightType}
            key={index}
            tokenStatus={selectedToken?.status}
            condition={HIGHLIGHT_CONDITIONS[lightType]}
            highlightedStyles={highlightedIndicators}
            indicatorStyles={CLASSLIST.indicators}
            isSomeoneCurrentlyMinting={isSomeoneCurrentlyMinting}
          />
        );
      })}
    </div>
  );
};

export default memo(TrafficLights);
