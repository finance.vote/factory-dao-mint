import { memo } from 'react';

const LightComponent = ({ className, styles = {} }) => {
  return <div className={className} style={styles} />;
};

const BackgroundMintingLight = ({ type, className, highlightedStyles }) => {
  let styles;
  const { background, shadow } = highlightedStyles['reserved'];
  if (type === 'orange') {
    styles = { backgroundColor: background, boxShadow: shadow };
  }
  // switch (type) {
  //   case 'green':
  //     LightComponent = BasicLight;
  //     break;
  //   case 'red' && tokenStatus !== 'unavailable':
  //     LightComponent = BasicLight;
  //     break;
  //   case 'orange':
  //     LightComponent = HightLightedIndicator;
  //     styles = { backgroundColor: background, boxShadow: shadow };
  //     break;
  //   default:
  //     LightComponent = BasicLight;
  //     break;
  // }
  return <LightComponent className={className} styles={styles} />;
};

const Light = ({
  type,
  tokenStatus,
  condition,
  highlightedStyles,
  indicatorStyles,
  isSomeoneCurrentlyMinting
}) => {
  if (isSomeoneCurrentlyMinting) {
    return (
      <BackgroundMintingLight
        className={indicatorStyles[type]}
        type={type}
        tokenStatus={tokenStatus}
        highlightedStyles={highlightedStyles}
      />
    );
  }
  if (condition) {
    let _highlightedStyles = {};
    switch (type) {
      case 'red':
        _highlightedStyles = highlightedStyles['unavailable'];
        break;
      case 'orange':
        _highlightedStyles = highlightedStyles['reserved'];
        break;
      case 'green':
        _highlightedStyles = highlightedStyles['available'];
        break;
    }
    const { background, shadow } = _highlightedStyles;
    const lightOn = { backgroundColor: background, boxShadow: shadow };
    return <LightComponent className={indicatorStyles[type]} styles={lightOn} />;
  }
  return <LightComponent className={indicatorStyles[type]} />;
};

export default memo(Light);
