import classNames from 'classnames';
import classes from './BackendMint.module.scss';

const BackendMint = ({ isBackendMint, onChange }) => {
  return (
    <div className={classes.backendMint}>
      <label className={classNames(classes.checkbox)}>
        Backend wallet mint (giveaway)
        <input type="checkbox" checked={isBackendMint} onChange={onChange} />
      </label>
    </div>
  );
};

export default BackendMint;
