import { ConnectWallet, ethInstance } from 'evm-chain-scripts';
import { useGlobalState } from 'globalState';
import classnames from 'classnames';
import Container from 'components/Ui/Container/Container.component';
import utilClasses from 'utils/scss/modules/utils.module.scss';
import styles from './FormWrapper.module.scss';

const FormWrapper = ({ children }) => {
  const [, setWalletAddress] = useGlobalState('walletAddress');

  const handleAddressChange = async () => {
    try {
      const address = await ethInstance.getEthAccount();
      if (address) setWalletAddress(address);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Container className={styles.container}>
      <div className={styles.header}>
        <ConnectWallet
          onConnect={handleAddressChange}
          className={classnames(
            utilClasses.hollowButton,
            utilClasses.hollowButtonBlack,
            styles.walletButton
          )}
        />
      </div>
      <div className={styles.formWrapper}>{children}</div>
    </Container>
  );
};

export default FormWrapper;
