import { highlightIndicators, TEXTLIST } from 'minting-components/helpers';
import styles from './defaultLayout.module.scss';

function withLayout(WrappedComponent) {
  return function withPassedStyles(props) {
    const { customStyles, sliderIcon, customTexts, noShift } = props;
    const handleStyle = sliderIcon
      ? {
          backgroundColor: '#fff',
          background: `url(${sliderIcon.src}) no-repeat`,
          height: '28px',
          width: '36px',
          border: 0,
          top: noShift ? 0 : '2px',
          opacity: '1'
        }
      : {
          backgroundColor: '#fff',
          height: '23px',
          width: '23px',
          border: '0',
          top: '0px',
          opacity: '1'
        };

    const componentStyles = {
      handleStyle,
      highlightIndicators,
      ...styles,
      ...customStyles
    };
    return (
      <WrappedComponent
        styles={componentStyles}
        TEXTLIST={{ ...TEXTLIST, ...customTexts }}
        {...props}
      />
    );
  };
}
export default withLayout;
