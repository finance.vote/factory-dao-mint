import Image from 'next/image';
import { forwardRef } from 'react';

const TokenMediaController = forwardRef(
  ({ mimetype, mediaUrl, className, onError, posterUrl, isVisible, placeholderImgUrl }, ref) => {
    if (!isVisible) return null;
    return mimetype === 'image' ? (
      <Image
        style={{ visibility: 'hidden' }}
        alt="mint-identity"
        src={mediaUrl}
        className={className}
        onError={onError}
        fill
        blurDataURL={placeholderImgUrl}
        placeholder="blur"
        onLoad={(event) => {
          event.target.style.visibility = 'visible';
        }}
      />
    ) : (
      <video
        poster={posterUrl}
        className={className}
        style={{ maxWidth: '100%' }}
        preload="auto"
        ref={ref}
        loop
        autoPlay
        src={mediaUrl}
      />
    );
  }
);

TokenMediaController.displayName = 'TokenMediaController';

export default TokenMediaController;
