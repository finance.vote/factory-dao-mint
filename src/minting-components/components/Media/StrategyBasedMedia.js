import axios from 'axios';
import React, { forwardRef, memo, useState } from 'react';
import { useWrappedMintingController } from './Strategies/query';

const StrategyBasedMedia = forwardRef((props, ref) => {
  const baseUrl = axios.defaults.baseURL;
  const { options, mintType, GROUP } = props;
  const {
    media: { showNext, external, reveal }
  } = options;
  const [mediaUrl, setMediaUrl] = useState();
  const [mimetype, setMimetype] = useState('image');
  const [posterUrl, setPosterUrl] = useState();
  const handleTokenImageError = (err) => {
    if (err.target?.src?.includes(`${baseUrl}/minting/media/`)) {
      setMediaUrl(err.target.src);
    } else {
      setMediaUrl(`${baseUrl}/minting/${GROUP}/placeholder`);
    }
  };
  const { data: WrappedMediaController } = useWrappedMintingController(
    showNext,
    external,
    reveal,
    mintType
  );
  if (!WrappedMediaController) return null;
  return (
    <WrappedMediaController
      ref={ref}
      handleTokenImageError={handleTokenImageError}
      baseUrl={baseUrl}
      mediaUrl={mediaUrl}
      posterUrl={posterUrl}
      mimetype={mimetype}
      setMimetype={setMimetype}
      setMediaUrl={setMediaUrl}
      setPosterUrl={setPosterUrl}
      {...props}
    />
  );
});
StrategyBasedMedia.displayName = 'StrategyBasedMedia';
export default memo(StrategyBasedMedia);
