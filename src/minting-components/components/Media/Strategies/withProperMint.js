import axios from 'axios';
import {
  getExternalTokenImage,
  getMediaMimetype,
  MINTING_STATUSES
} from 'minting-components/helpers';
import { forwardRef, useEffect } from 'react';

const withProperMint = (WrappedComponent) => {
  return forwardRef(function WithPassedUrl(props, ref) {
    const {
      baseUrl,
      setMediaUrl,
      setMimetype,
      setPosterUrl,
      GROUP,
      mintingStatus,
      options,
      tokenId,
      tokenUri
    } = props;
    const external = options?.media?.external;

    useEffect(() => {
      if (!tokenId || !tokenUri || mintingStatus !== MINTING_STATUSES.DONE) return;
      const abortController = new AbortController();
      getExternalTokenImage(GROUP, tokenId, tokenUri, external, abortController)
        .then(async (res) => {
          const { mimetype: _res } = await getMediaMimetype(res);
          setMimetype(_res);
          setMediaUrl(res);
        })
        .catch((error) => {
          if (axios.isCancel(error)) return;
          console.error(error);
        });
      if (!external) setPosterUrl(`${baseUrl}/minting/profile-pic/${GROUP}/${tokenId}`);
      return () => abortController.abort();
    }, [tokenId, tokenUri, mintingStatus, props.mediaUrl]);
    return <WrappedComponent ref={ref} {...props} />;
  });
};
export default withProperMint;
