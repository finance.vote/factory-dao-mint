import { MINT_TYPES } from 'minting-components/helpers';
import { useQuery } from 'react-query';
import MediaController from '../MediaController';
import withoutTokenReveal from './withoutTokenReveal';
import withProperMint from './withProperMint';
import withSelectableShowNext from './withSelectableShowNext';

const getWrappedComponent = (showNext, external, reveal, mintType) => {
  const showNextStrategyConditions = showNext && external && reveal; //&& mintType === MINT_TYPES.SELECTABLE;
  const properMintStrategyConditions = !showNext && reveal && mintType === MINT_TYPES.SELECTABLE;
  const onlyPlaceHolderWithoutVideo = !showNext && !reveal && mintType === MINT_TYPES.SELECTABLE;
  let withLogicWrapper;
  switch (true) {
    case showNextStrategyConditions:
      withLogicWrapper = withSelectableShowNext;
      break;
    case properMintStrategyConditions:
      withLogicWrapper = withProperMint;
      break;
    case onlyPlaceHolderWithoutVideo:
      withLogicWrapper = withoutTokenReveal;
      break;
    default:
      withLogicWrapper = withProperMint;
      break;
  }
  return withLogicWrapper(MediaController);
};

export const useWrappedMintingController = (showNext, external, reveal, mintType) =>
  useQuery(
    ['WrappedController', showNext, external, reveal, mintType],
    () => getWrappedComponent(showNext, external, reveal, mintType),
    {
      enabled: !!mintType,
      refetchOnWindowFocus: false
    }
  );
