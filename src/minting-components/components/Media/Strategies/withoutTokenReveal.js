import { MINTING_STATUSES } from 'minting-components/helpers';
import { forwardRef, useEffect } from 'react';

const withoutTokenReveal = (WrappedComponent) => {
  return forwardRef(function WithNoReveal(props, ref) {
    const { baseUrl, GROUP, mintingStatus, setMediaUrl } = props;
    useEffect(() => {
      if (mintingStatus !== MINTING_STATUSES.DONE) return;
      setMediaUrl(`${baseUrl}/minting/${GROUP}/placeholder`);
    }, [mintingStatus]);
    return <WrappedComponent ref={ref} {...props} />;
  });
};
export default withoutTokenReveal;
