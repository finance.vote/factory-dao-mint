import { getMediaMimetype, getNextTokenImage } from 'minting-components/helpers';
import { forwardRef, useEffect } from 'react';

const withSelectableShowNext = (WrappedComponent) => {
  return forwardRef(function WithPassedSelectableShowNext(props, ref) {
    const { setMediaUrl, setMimetype } = props;
    const { GROUP, mintingStatus, options, tokenId } = props;
    const external = options?.media?.external;
    useEffect(() => {
      const controller = new AbortController();
      if (!tokenId) return;
      (async () => {
        const imageUrl = await getNextTokenImage(GROUP, tokenId, external, controller);
        const { mimetype } = await getMediaMimetype(imageUrl, controller);
        setMediaUrl(imageUrl);
        setMimetype(mimetype);
      })();
      return () => controller.abort();
    }, [tokenId, mintingStatus]);
    return <WrappedComponent ref={ref} {...props} />;
  });
};
export default withSelectableShowNext;
