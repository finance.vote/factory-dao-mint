import { Stream } from '@cloudflare/stream-react';
import { forwardRef, memo } from 'react';
import { isMobile } from 'react-device-detect';
import styles from './UnboxingVideo.module.scss';

const UnboxingVideo = forwardRef(({ config, isVisible, hasStopped, onEnd }, ref) => {
  const { active, cloudflareVideoId } = config;
  if (!active) return null;

  return (
    <div
      style={{
        display: isVisible ? 'block' : 'none',
        opacity: hasStopped ? 0 : 1,
        width: '100%',
        height: '100%'
      }}
    >
      <Stream
        className={styles.video}
        preload="auto"
        autoPlay
        muted={isMobile}
        streamRef={ref}
        src={cloudflareVideoId}
        onEnded={onEnd}
      />
    </div>
  );
});

UnboxingVideo.displayName = 'UnboxingVideo';

export default memo(UnboxingVideo);
