import { delayer, getNextTokenImage, MINTING_STATUSES } from 'minting-components/helpers';
import { forwardRef, memo, useEffect, useRef, useState } from 'react';
import PlaceholderImage from './PlaceholderImage/PlaceholderImage';
import TokenMediaController from './TokenMediaController/TokenMediaController';
import UnboxingVideo from './UnboxingVideo/UnboxingVideo';
const MediaController = forwardRef(
  (
    {
      GROUP,
      mediaUrl,
      posterUrl,
      mimetype,
      mintingStatus,
      styles,
      options,
      revealTokenQueue,
      isRevealVideoPlaying,
      handleTokenImageError,
      baseUrl,
      setMediaUrl
    },
    ref
  ) => {
    const {
      media: { reveal, showNext, external },
      unboxingVideo
    } = options;

    const [isUnboxingVideoFadedOut, setIsUnboxingVideoFadedOut] = useState(false);
    const [isUnboxingVideoVisible, setIsUnboxingVideoVisible] = useState(false);
    const unboxingVideoRef = useRef(null);
    const tokenVideoRef = useRef();

    useEffect(() => {
      const unboxAfterMint = mintingStatus === MINTING_STATUSES.DONE && unboxingVideo.active;
      if (!unboxAfterMint) return;
      unboxWithVideo();
    }, [mintingStatus]);

    const onUnboxVideoEnded = () => setIsUnboxingVideoFadedOut(true);

    const unboxWithVideo = () => {
      setIsUnboxingVideoFadedOut(false);
      setIsUnboxingVideoVisible(true);
      if (unboxingVideo.everyoneAtOnce) {
        unboxingVideoRef.current?.pause();
        unboxingVideoRef.current.currentTime = 0;
      }
      unboxingVideoRef.current?.play();
    };

    // CASE EVERYONE SEES VIDEO; START
    const releasedTokenWatcher = () => {
      if (!isRevealVideoPlaying.current && ref.current.length) {
        (async () => {
          const controller = new AbortController();
          isRevealVideoPlaying.current = true;
          const url = await getNextTokenImage(GROUP, ref.current[0], external, controller);
          if (url) {
            const { duration } = unboxingVideoRef.current;
            const durationInMs = duration * 1000;
            setIsUnboxingVideoVisible(true);
            setMediaUrl(url);
            unboxWithVideo();
            isRevealVideoPlaying.current = true;
            await delayer(durationInMs + 7000);
            ref.current.splice(0, 1);
            isRevealVideoPlaying.current = false;
          } else {
            setIsUnboxingVideoFadedOut(true);
            setIsUnboxingVideoVisible(false);
          }
        })();
      }
    };

    useEffect(() => {
      if (!unboxingVideo.active || !unboxingVideo.everyoneAtOnce) return;
      const interval = setInterval(releasedTokenWatcher, 2000);
      return () => clearInterval(interval);
    }, []);

    useEffect(() => {
      const revealsLeftToShow = revealTokenQueue.filter((token) => !ref.current.includes(token));
      for (const token of revealsLeftToShow) {
        ref.current.push(token);
      }
    }, [revealTokenQueue]);
    // CASE EVERYONE SEES VIDEO; END
    const placeholderVisible =
      (showNext && !mediaUrl) ||
      (!showNext && unboxingVideo.active && !isUnboxingVideoVisible) ||
      (!unboxingVideo.active && !mediaUrl) ||
      (mimetype === 'image' && !unboxingVideo.active);

    const isTokenMediaVisible =
      (isUnboxingVideoFadedOut && unboxingVideo.active) ||
      (!unboxingVideo.active && mediaUrl) ||
      (reveal && mediaUrl && isUnboxingVideoFadedOut) ||
      (reveal && mediaUrl && showNext && !isUnboxingVideoVisible);

    const placeholderImgUrl = `${baseUrl}/minting/${GROUP}/placeholder`;

    return (
      <>
        <PlaceholderImage
          url={placeholderImgUrl}
          className={styles.image}
          isVisible={placeholderVisible}
        />
        <TokenMediaController
          className={styles[mimetype]}
          mediaUrl={mediaUrl}
          mimetype={mimetype}
          posterUrl={posterUrl}
          ref={tokenVideoRef}
          onError={handleTokenImageError}
          isVisible={isTokenMediaVisible}
          placeholderImgUrl={placeholderImgUrl}
        />
        <UnboxingVideo
          config={unboxingVideo}
          isVisible={isUnboxingVideoVisible}
          hasStopped={isUnboxingVideoFadedOut}
          onEnd={onUnboxVideoEnded}
          ref={unboxingVideoRef}
        />
      </>
    );
  }
);

MediaController.displayName = 'MediaController';

export default memo(MediaController);
