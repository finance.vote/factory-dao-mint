import { Stream } from '@cloudflare/stream-react';
import { getMediaMimetype } from 'minting-components/helpers';
import Image from 'next/image';
import { useEffect, useRef, useState } from 'react';

const PlaceholderImage = ({ url, className, isVisible }) => {
  const videoRef = useRef();
  const [mimetype, setMimetype] = useState('video');
  const [videoId, setVideoId] = useState('');

  useEffect(() => {
    (async () => {
      const { mimetype, placeholderVideoId } = await getMediaMimetype(url);
      setMimetype(mimetype);
      if (placeholderVideoId) setVideoId(placeholderVideoId);
    })();
  }, []);

  if (!isVisible) return null;

  let Video = null;
  if (videoId) {
    Video = (
      <Stream
        className={className}
        preload="auto"
        autoPlay
        loop
        onCanPlay={() => videoRef.current?.play()}
        muted
        streamRef={videoRef}
        src={videoId}
      />
    );
  }

  return mimetype === 'image' ? (
    <Image alt="placeholder" src={url} className={className} fill />
  ) : (
    Video
  );
};

export default PlaceholderImage;
