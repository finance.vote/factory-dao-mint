import { memo } from 'react';

const PriceController = ({ isVisible, className, withdrawals, currency, price }) => {
  if (!isVisible) return null;
  const { timesWithdrawn, maxWithdrawals } = withdrawals;
  return (
    <span className={className}>
      {maxWithdrawals - timesWithdrawn}/{maxWithdrawals} mints available at
      {` ${price} $${currency}`} per mint
    </span>
  );
};

export default memo(PriceController);
