import axios from 'axios';
import classnames from 'classnames';
import TooltipInfo from 'components/TooltipInfo';
import PageLoading from 'components/Ui/PageLoading/PageLoading.component';
import { ConnectWallet, ethInstance, toWei } from 'evm-chain-scripts';
import { useGlobalState } from 'globalState';
import {
  MINTING_STATUSES,
  MINT_TYPES,
  PRICE_TYPES,
  getTokensRange,
  reserveToken
} from 'minting-components/helpers';
import Image from 'next/image';
import { useEligibilityQuery } from 'queries/eligibilityQuery';
import { useNextTokenToMint } from 'queries/nextTokenToMintQuery';
import { useGetPlaceholderText } from 'queries/placeholderQuery';
import { useRandomToken } from 'queries/randomTokenQuery';
import { useTokenData } from 'queries/tokenQuery';
import React, { useEffect, useRef, useState } from 'react';
import { isMobile } from 'react-device-detect';
import BackendMintConfirmModal from '../BackendMintConfirmModal/BackendMintConfirmModal.component';
import EventsProvider from '../EventsProvider/EventsProvider';
import If from '../If/If';
import StrategyBasedMedia from '../Media/StrategyBasedMedia';
import MintingInfo from '../MintingInfo/MintingInfo';
import PriceController from '../PriceController';
import ProgressBar from '../ProgressBar';
import SpecialMessage from '../SpecialMessage';
import SuccessMessage from '../SuccessMessage';
import TrafficLights from '../TrafficLights/TrafficLights';
import WalletInput from '../WalletInput/WalletInput.component';
import withLayout from '../withLayout/withLayout';

/**
 *
 * Identity minting page
 *
 *
 * @returns The view to show all identity minting
 */
const MintingPage = ({
  mintingConfig,
  seriesConfig,
  styles,
  merkleContract,
  TEXTLIST,
  discordLink,
  playNowLink,
  middleContainerText,
  randomizeBtn,
  randomizeBtnDisabled,
  loader,
  SequentialMintComponent = () => null,
  prefillOnLoad,
  customWalletProviders,
  setGlobalChoosenSeries,
  FooterComponent = () => null,
  MintingButtonComponent = () => null,
  CustomSuccessMint = () => null,
  isSuccessMintPopup = false,
  StageRangesComponent = () => null,
  TopPlaceComponent,
  code,
  walletInputLabel,
  isProgressBarVisible = true,
  tooltipContent = null,
  tooltipIcon,
  tooltipContentClass
}) => {
  const {
    key: groupKey,
    gateAddress,
    chainId,
    currencySymbol: currency,
    options: { trafficLights, progressBar, unboxingVideo, mintType, isBackendMint }
  } = mintingConfig;
  const [choosenSeries, setChoosenSeries] = useState(seriesConfig[0]);
  const [specialMessage, setSpecialMessage] = useState('');
  const {
    id: seriesId,
    eligibilityAddress,
    eligibilityIndex,
    eligibilityType,
    merkleTreeIndex,
    price,
    priceType
  } = choosenSeries;

  const [priceValue, setPriceValue] = useState(0);

  useEffect(() => {
    setGlobalChoosenSeries(choosenSeries);
    setPriceValue(price);
  }, [choosenSeries]);

  const [tokensRange, setTokensRange] = useState({ start: 1, end: '' });
  useEffect(() => {
    setChoosenSeries(seriesConfig[0]);
    const { start, end } = getTokensRange(seriesConfig);
    setTokensRange({ start, end });
    const seriesWithSpecialMessage = seriesConfig.find((series) => series.specialMessage);
    setSpecialMessage(seriesWithSpecialMessage?.specialMessage);
  }, [seriesConfig]);

  const findTokenInSeries = (tokenId, series) => {
    const token = parseInt(tokenId);
    return series?.find((serie) => {
      return serie.range?.end >= token && serie.range?.start <= token;
    });
  };

  const [loading, setLoading] = useState(false);
  const [walletAddress] = useGlobalState('walletAddress');
  const [isMintIdentityButtonDisabled, setIsMintIdentityButtonDisabled] = useState(false);
  const [mintingStatus, setMintingStatus] = useState(MINTING_STATUSES.NONE);
  const [choosenToken, setChoosenToken] = useState();
  const [error, setError] = useState(null);
  const [tokenValue, setTokenValue] = useState();
  const [currentMintingMode] = useState(mintType ?? 'selectable');
  const [revealTokenQueue, setRevealTokenQueue] = useState([]);
  const [isSomeoneCurrentlyMinting, setIsSomeoneCurrentlyMinting] = useState(false);
  const [isEligibleForSpecificNumber, setIsEligibleForSpecificNumber] = useState(true);
  const [walletInputMintValues] = useGlobalState('walletInputMintValues');
  const [isBackendMintModalOpen, setIsBackendMintModalOpen] = useState(false);

  const { data: nextTokenToMint } = useNextTokenToMint(groupKey, mintType, prefillOnLoad);
  const boughtToken = useRef(null);
  const boughtTokenUri = useRef(null);
  const containerRef = useRef();
  const tokensToPlay = useRef([]);
  const isRevealVideoPlaying = useRef(false);

  const {
    data: { addressMerkleProof, isEligibleToMint, withdrawals },
    error: eligibilityError,
    refetch: refetchEligibility
  } = useEligibilityQuery(
    eligibilityType,
    chainId,
    walletAddress,
    eligibilityIndex,
    eligibilityAddress,
    seriesId,
    groupKey,
    seriesConfig,
    code
  );

  const {
    data: { minted: tokensMinted, total: tokensTotal, percentage },
    refetch: refetchTokenData
  } = useTokenData(groupKey, progressBar);

  const { data: placeholderText } = useGetPlaceholderText(
    tokensRange,
    groupKey,
    TEXTLIST.placeholderText
  );

  const { refetch: refetchRandomToken } = useRandomToken(groupKey);

  // FIXME: simplify this
  const tokenId =
    currentMintingMode === MINT_TYPES.SEQUENTIAL && boughtToken?.current
      ? boughtToken.current
      : choosenToken?.value ?? nextTokenToMint?.value;

  const standardMintSubmit = async (tokenToMint, currentNetwork) => {
    const [{ data }, contract] = await Promise.all([
      axios.get(`/minting/metadata-proof/${groupKey}/${seriesId}/${tokenToMint.value}`),
      ethInstance.getWriteContractByAddress(merkleContract, gateAddress, currentNetwork)
    ]);

    const { proof: metadataMerkleProof, uri } = data;
    const isReadyToMint = tokenToMint.status === 'available' || tokenToMint.status === 'reserved';

    const priceToSend = priceType === PRICE_TYPES.FLEXIBLE ? priceValue : price;

    if (isReadyToMint) {
      const gasLimit = await contract.estimateGas.withdraw(
        merkleTreeIndex,
        tokenToMint.value,
        uri,
        addressMerkleProof,
        metadataMerkleProof,
        { from: walletAddress, value: toWei(String(priceToSend)) }
      );

      const tx = await contract.withdraw(
        merkleTreeIndex,
        tokenToMint.value,
        uri,
        addressMerkleProof,
        metadataMerkleProof,
        {
          from: walletAddress,
          value: toWei(String(priceToSend)),
          gasLimit: gasLimit.toNumber() + 5000
        }
      );
      try {
        reserveToken(tokenToMint, walletAddress, tx, groupKey);
      } catch (err) {
        console.error('Error occured during token reservation', err);
      }

      await tx.wait();

      return { uri };
    } else {
      throw Error('Token is unavailable');
    }
  };

  const onClickMintIdentity = async () => {
    try {
      containerRef.current.scrollIntoView();
      setLoading(true);
      let tokenToMint = null;

      if (currentMintingMode === MINT_TYPES.SELECTABLE) {
        tokenToMint = choosenToken;
      } else {
        tokenToMint = nextTokenToMint;
      }

      if (tokenToMint.status === 'unavailable') {
        throw new Error(`Token is ${tokenToMint.status}`);
      }

      setMintingStatus(MINTING_STATUSES.PROCESSING);
      let _uri;

      if (isBackendMint) {
        const { walletAddress } = walletInputMintValues;
        await axios.post(`/minting/${groupKey}/claim`, {
          walletAddress,
          code,
          tokenId: parseInt(tokenToMint.value)
        });
      } else {
        const currentNetwork = chainId ?? ethInstance.getChainId();
        const { uri } = await standardMintSubmit(tokenToMint, currentNetwork);

        _uri = uri;

        boughtToken.current = tokenToMint?.value ?? choosenToken.value;
        boughtTokenUri.current = _uri;

        setMintingStatus(MINTING_STATUSES.DONE);
        refetchEligibility();
        setLoading(false);
      }
    } catch (error) {
      setMintingStatus(MINTING_STATUSES.NONE);
      console.error('error while minting', error);
      const errorCode =
        error?.code && typeof error?.code === 'string'
          ? `ERROR: ${error.code.toLowerCase().replace('_', ' ')}`
          : 'AN ERROR HAS OCCURRED';
      setError(errorCode);
    }
  };

  const onClickMintRandomIdentity = async () => {
    if (!isEligibleToMint)
      throw Error('You are not whitelisted for this stage, could not fetch random token');
    const { data: randomToken } = await refetchRandomToken();
    setChoosenToken(randomToken);
    setTokenValue(randomToken.value);
    getChoosenToken({ target: { value: randomToken.value } });
  };

  useEffect(() => {
    const debouncedSearch = setTimeout(async () => {
      try {
        setError(null);
        let tokenVal = tokenValue ?? nextTokenToMint?.value;
        if (tokenVal && Number.isInteger(Number(tokenVal)) && tokenVal > 0) {
          const response = await axios.get(`/minting/token-status/${groupKey}/${tokenVal}`, {
            validateStatus: (status) => status === 200 || status === 404
          });
          const { data, status } = response;
          if (data?.token) {
            setChoosenToken(data.token);
            const tokenInSeries = findTokenInSeries(data.token.value, seriesConfig);
            if (tokenInSeries) {
              setChoosenSeries(tokenInSeries);
              setIsEligibleForSpecificNumber(true);
            }
          } else if (status === 404) {
            setError('THIS NUMBER IS OUT OF RANGE');
            setChoosenToken(undefined);
          } else {
            throw Error('Unknown error when requesting a random token');
          }
        } else {
          if (tokenVal) {
            throw Error('Invalid input format');
          }
        }
      } catch (error) {
        setError(error?.message);
        setChoosenToken(undefined);
        console.error(error);
      }
    }, 300);
    return () => clearTimeout(debouncedSearch);
  }, [tokenValue, nextTokenToMint, seriesConfig, groupKey]);

  const getChoosenToken = async (event) => {
    try {
      if (!event.target.value) {
        setChoosenToken(null);
      } else {
        const tokenInSeries = findTokenInSeries(event.target.value, seriesConfig);
        if (tokenInSeries) {
          setChoosenSeries(tokenInSeries);
          setIsEligibleForSpecificNumber(true);
        } else {
          setIsMintIdentityButtonDisabled(true);
          setIsEligibleForSpecificNumber(false);
        }
      }
      setTokenValue(event.target.value);
    } catch (e) {
      setChoosenToken(null);
      console.error(e);
    }
  };

  useEffect(() => {
    if (isEligibleToMint) {
      setIsMintIdentityButtonDisabled(choosenToken?.status === 'unavailable');
    }
  }, [choosenToken]);

  const addTokenToPlayQueue = (tokenId) => {
    if (!unboxingVideo.everyoneAtOnce) return;
    return setRevealTokenQueue(revealTokenQueue.concat(tokenId));
  };

  const setCurrentlyMintingWarning = () => {
    if (!unboxingVideo.everyoneAtOnce) return;
    return setIsSomeoneCurrentlyMinting(true);
  };

  const handleBackendMintEvent = async (result) => {
    try {
      const { data, error } = result;

      if (data?.status != 'minted' || error || !data?.uri) {
        throw error;
      }

      boughtToken.current = data?.value ?? choosenToken.value;
      boughtTokenUri.current = data.uri;

      setMintingStatus(MINTING_STATUSES.DONE);
      refetchEligibility();
    } catch (error) {
      setMintingStatus(MINTING_STATUSES.NONE);
      console.error('error while backend minting', error);
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  const handleStatusUpdate = async ({ data }) => {
    const token = JSON.parse(data);

    if (token.isBackendMintEvent) {
      handleBackendMintEvent(token);
    }

    setChoosenToken((prevToken) => {
      if (prevToken) {
        const tokenToUpdate = prevToken.value === token.value;
        if (tokenToUpdate) return token.data;
        return prevToken;
      }
    });

    console.log('Token', token.value, token.status);

    if (token.status === 'unavailable') {
      setIsSomeoneCurrentlyMinting(false);
      addTokenToPlayQueue(token.value);
      await refetchTokenData();
    }

    if (token.status === 'reserved') {
      setCurrentlyMintingWarning();
    }
  };

  const handleBackendMintModalClose = () => {
    setIsBackendMintModalOpen(false);
  };

  const onConfirmMint = async () => {
    setIsBackendMintModalOpen(false);
    await onClickMintIdentity();
  };

  const onClickMint = async () => {
    if (isBackendMint) {
      setIsBackendMintModalOpen(true);
    } else {
      await onClickMintIdentity();
    }
  };

  const isBackendMintInvalid = isBackendMint && !walletInputMintValues?.walletAddress?.length;
  const isNotEligible = !isEligibleToMint && mintingStatus !== MINTING_STATUSES.DONE;

  const isPickNumberDisabled =
    (!isBackendMint && !walletAddress) || isNotEligible || isBackendMintInvalid || loading;

  return (
    <EventsProvider handleStatusUpdate={handleStatusUpdate} groupKey={groupKey}>
      <BackendMintConfirmModal
        selectedToken={choosenToken}
        isOpen={isBackendMintModalOpen}
        onModalClose={handleBackendMintModalClose}
        onConfirmMint={onConfirmMint}
      />
      <div className={styles.container} ref={containerRef}>
        <If condition={loading}>
          <PageLoading loader={loader} className={styles.loader} />
        </If>
        <div className={styles.details}>
          {/* <If condition={isMobile}>
            <Image className={styles.headerImageLogo} src={mobileLogo} alt="logo" />
            <Image className={styles.headerTextLogo} src={mobileTextLogo} alt="text_logo" />
            <p className={styles.mobileHeader}>
              Your uniquely generated {TEXTLIST['mobileHeader']}
              <br />
              is a key to an infinite realm
            </p>
          </If> */}
          {/* <If condition={merkleTreeIndex}> */}
          <div className={styles.upperContainer}>
            <StrategyBasedMedia
              GROUP={groupKey}
              mintingStatus={mintingStatus}
              mintType={mintType}
              options={mintingConfig.options}
              styles={styles}
              revealTokenQueue={revealTokenQueue}
              tokenId={tokenId}
              tokenUri={boughtTokenUri.current}
              ref={tokensToPlay}
              isRevealVideoPlaying={isRevealVideoPlaying}
            />
          </div>
          <If condition={StageRangesComponent}>
            <StageRangesComponent />
          </If>
          <If condition={middleContainerText}>
            <div className={styles.middleContainer}>
              <p dangerouslySetInnerHTML={{ __html: middleContainerText }} />
            </div>
          </If>
          <div className={styles.lowerContainer}>
            <If condition={mintingStatus !== MINTING_STATUSES.DONE && isBackendMint}>
              <WalletInput
                containerClassName={classnames(
                  styles.walletInput,
                  !walletAddress ? styles.walletInputNotConnected : ''
                )}
                label={walletInputLabel}
              />
              <div
                className={classnames(
                  walletAddress ? styles.hidden : [styles.connect, styles.connectWalletBackendMint]
                )}
              >
                <ConnectWallet
                  containerClassName={styles.modal}
                  customWalletProviders={customWalletProviders}
                />
              </div>
            </If>
            <If condition={mintingStatus !== MINTING_STATUSES.DONE && SequentialMintComponent}>
              <SequentialMintComponent
                mintType={mintType}
                tokenSymbol={currency}
                groupKey={groupKey}
              />
            </If>
            {TopPlaceComponent && <TopPlaceComponent />}
            <TrafficLights
              visible={trafficLights && mintingStatus !== MINTING_STATUSES.DONE}
              mintingStatus={mintingStatus}
              error={error}
              highlightedIndicators={styles.highlightIndicators}
              selectedToken={choosenToken}
              containerStyles={styles.indicatorContainer}
              indicatorStyles={styles.indicators}
              isSomeoneCurrentlyMinting={isSomeoneCurrentlyMinting}
            />
            {isSuccessMintPopup ? (
              <CustomSuccessMint
                isVisible={mintingStatus === MINTING_STATUSES.DONE}
                boughtToken={boughtToken.current}
                tokenSymbol={TEXTLIST['symbol']}
                successTitle={TEXTLIST['successTitle']}
                successMessage={TEXTLIST['successMessage']}
                titleClassName={styles.successTitle}
                descriptionClassName={styles.successDescription}
              />
            ) : (
              <SuccessMessage
                isVisible={mintingStatus === MINTING_STATUSES.DONE}
                boughtToken={boughtToken.current}
                tokenSymbol={TEXTLIST['symbol']}
                successTitle={TEXTLIST['successTitle']}
                successMessage={TEXTLIST['successMessage']}
                titleClassName={styles.successTitle}
                descriptionClassName={styles.successDescription}
              />
            )}
            <If
              condition={
                (mintingStatus !== MINTING_STATUSES.DONE &&
                  currentMintingMode === MINT_TYPES.SELECTABLE) ||
                (!currentMintingMode && !walletAddress)
              }
            >
              <SpecialMessage content={specialMessage} className={styles.specialMessage} />
              <If condition={TEXTLIST.numberInputLabel}>
                <div className={styles.numberInputLabel}>{TEXTLIST.numberInputLabel}</div>
              </If>
              <div className={styles.relativeWrapper}>
                <input
                  value={tokenValue ?? choosenToken?.value ?? ''}
                  placeholder={placeholderText}
                  className={styles.inputData}
                  type="text"
                  disabled={isPickNumberDisabled}
                  onChange={getChoosenToken}
                />
                <button
                  disabled={isPickNumberDisabled}
                  className={styles.randomize}
                  onClick={onClickMintRandomIdentity}
                >
                  <Image
                    src={isPickNumberDisabled ? randomizeBtnDisabled : randomizeBtn}
                    alt="randomize"
                  />
                </button>
                {tooltipContent ? (
                  <TooltipInfo
                    title="Number picker:"
                    tooltipPositionClass={styles.tooltipPosition}
                    tooltipIcon={tooltipIcon}
                    tooltipContentClass={tooltipContentClass}
                  >
                    {tooltipContent}
                  </TooltipInfo>
                ) : null}
              </div>
            </If>
            <If condition={priceType === PRICE_TYPES.FLEXIBLE}>
              <div className={styles.relativeWrapper}>
                <input
                  value={priceValue}
                  placeholder="Enter a price"
                  className={styles.inputData}
                  type="number"
                  min={price || 0}
                  onChange={(event) => event.target.value && setPriceValue(event.target.value)}
                />
              </div>
            </If>
            <div className={styles.buttonWrapper}>
              <MintingInfo
                TEXTLIST={TEXTLIST}
                isVisible={mintingStatus !== MINTING_STATUSES.DONE}
                className={styles.status}
                error={error}
                choosenToken={choosenToken}
                highlightedIndicators={styles.highlightIndicators}
              />
              <MintingButtonComponent
                TEXTLIST={TEXTLIST}
                mintingStatus={mintingStatus}
                eligibilityError={eligibilityError?.message}
                isSomeoneCurrentlyMinting={isSomeoneCurrentlyMinting}
                choosenToken={choosenToken}
                onClickMintIdentity={onClickMint}
                error={error}
                discordLink={discordLink}
                playNowLink={playNowLink}
                isEligibleToMint={isEligibleToMint}
                isEligibleForSpecificNumber={isEligibleForSpecificNumber}
                isMintIdentityButtonDisabled={isMintIdentityButtonDisabled}
                isBackendMint={isBackendMint}
                amIMinting={loading}
              />
              {!isBackendMint ? (
                <div className={walletAddress ? styles.hidden : styles.connect}>
                  <ConnectWallet
                    containerClassName={styles.modal}
                    // onConnect={handleAddressChange}
                    customWalletProviders={customWalletProviders}
                  />
                </div>
              ) : null}
            </div>
            {isProgressBarVisible ? (
              <ProgressBar
                isVisible={progressBar && tokensTotal}
                tokensTotal={tokensTotal}
                tokensMinted={tokensMinted}
                percentage={percentage}
                isMobile={isMobile}
                handleStyle={styles.handleStyle}
                sliderClassName={styles.slider}
                sliderInfoClassName={styles.sliderInfo}
                percentageClassName={styles.percentage}
              />
            ) : null}
            <PriceController
              isVisible={withdrawals.maxWithdrawals >= 1}
              className={styles.withdrawals}
              withdrawals={withdrawals}
              currency={currency}
              price={price}
            />
            <If
              condition={
                !isEligibleToMint && walletAddress && mintingStatus !== MINTING_STATUSES.DONE
              }
            >
              <div className={styles.notEligibleInfo}>You are not eligible to mint</div>
            </If>
            <If condition={isEligibleToMint && !isEligibleForSpecificNumber && choosenToken}>
              <div className={styles.notEligibleInfo}>
                You are not eligible to mint NFT with this ID
              </div>
            </If>
            {isBackendMint && choosenSeries.isCodeInvalid ? (
              <span className={styles.error}>Code for this mint is used or invalid</span>
            ) : null}
          </div>
          <FooterComponent
            mintingStatus={mintingStatus}
            boughtToken={boughtToken.current}
            tokenUri={boughtTokenUri.current}
          />
        </div>
      </div>
    </EventsProvider>
  );
};

export default withLayout(MintingPage);
