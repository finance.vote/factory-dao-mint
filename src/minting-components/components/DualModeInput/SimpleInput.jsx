import classnames from 'classnames';
import { Field } from 'formik';
import classes from 'pages/CreateMint/CreateMint.module.scss';
import utilClasses from 'utils/scss/modules/utils.module.scss';

export const MerkleElement = ({
  setFieldValue,
  hashKeys,
  hashTypes,
  values,
  elementsList,
  element,
  deleteItem,
  errors
}) => {
  const errorOccurred =
    errors?.[elementsList]?.[values.findIndex((i) => i.index === element.index)];

  const updateElements = (event, key) => {
    const {
      target: { value }
    } = event;
    const indexToUpdate = values.findIndex((item) => item.index === element.index);
    if (indexToUpdate < 0) return;
    values[indexToUpdate][key] = value;
    values.splice(indexToUpdate, 1, values[indexToUpdate]);
    setFieldValue(elementsList, values);
  };

  const listIndex = values.findIndex((item) => item.index === element.index);

  return (
    <div className={classnames(classes.argumentInputs, 'fadeIn')} key={element.index}>
      {hashKeys.map((key, index) => {
        return (
          <Field
            className={classnames(errorOccurred?.[key] && classes.inputError, utilClasses.input)}
            key={index}
            name={`${elementsList}[${listIndex}].${key}`}
            type={hashTypes[index]}
            placeholder={`${key} - ${hashTypes[index]}`}
            onChange={(e) => updateElements(e, key)}
            data-test={key + 'field'}
          />
        );
      })}
      <button
        className={classes.deleteBtn}
        onClick={() => deleteItem(element.index)}
        type="button"
        data-test="delete-simple-element"
      >
        X
      </button>
    </div>
  );
};

export const SimpleInput = ({
  hashKeys,
  hashTypes,
  setFieldValue,
  merkleElements,
  elementsList,
  errors
}) => {
  const addNewElement = () => {
    const newItems = merkleElements.concat(
      hashKeys.reduce((acc, curr) => ((acc[curr] = ''), acc), {
        index: (merkleElements[merkleElements.length - 1]?.index ?? 0) + 1
      })
    );
    return setFieldValue(elementsList, newItems);
  };

  const deleteItem = (index) => {
    const indexToDelete = merkleElements.findIndex((item) => item.index === index);
    merkleElements.splice(indexToDelete, 1);
    setFieldValue(elementsList, [...merkleElements]);
  };
  return (
    <div className="fadeIn">
      <div className={classes.scrollableWrapper} data-test="simple-input-merkle-elements">
        {merkleElements.map((element) => {
          return (
            <MerkleElement
              key={element.index}
              setFieldValue={setFieldValue}
              hashKeys={hashKeys}
              hashTypes={hashTypes}
              element={element}
              values={merkleElements}
              elementsList={elementsList}
              deleteItem={deleteItem}
              errors={errors}
            />
          );
        })}
      </div>
      <button
        className={classnames(
          utilClasses.hollowButton,
          utilClasses.hollowButtonBlack,
          classes.addElementBtn
        )}
        onClick={addNewElement}
        type="button"
        data-test="simple-input-add-new-element"
      >
        Add new element
      </button>
    </div>
  );
};
