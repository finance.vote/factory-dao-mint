import classnames from 'classnames';
import { parseAdvancedToSimple, parseSimpleToCSV, parseSimpleToJSON } from 'evm-chain-scripts';
import { AdvancedInput } from 'minting-components/components/DualModeInput/AdvancedInput';
import { SimpleInput } from 'minting-components/components/DualModeInput/SimpleInput';
import classes from 'pages/CreateMint/CreateMint.module.scss';
import utilClasses from 'utils/scss/modules/utils.module.scss';

const InputTypes = {
  CSV: {
    inputType: 'CSV',
    switchButtonText: 'Switch to JSON Array',
    placeholderText: 'Input CSV values here..',
    alternativeInputType: 'JSON'
  },
  JSON: {
    inputType: 'JSONArray',
    switchButtonText: 'Switch to CSV',
    placeholderText: 'Input JSON Array here..',
    alternativeInputType: 'CSV'
  }
};

const DualModeInput = ({
  errors,
  values,
  type,
  hash,
  textareaName,
  elementsList,
  setFieldValue,
  handleBlur,
  handleChange,
  treeInputType,
  setTreeInputType,
  setValues
}) => {
  const handleModeChange = (values) => {
    const isAdvancedMode = !values[type + 'Advanced'];
    const field = type + 'Textarea';
    const start = values[`${type}IncludeHeaders`] ? 1 : 0;
    let fieldName = '';
    let fieldValue;

    if (!isAdvancedMode) {
      const { data } = parseAdvancedToSimple(values[field], treeInputType, start, type, true);
      fieldName = type + 'LeavesElements';
      fieldValue = data;
    } else {
      const textAreaElements =
        treeInputType === 'CSV'
          ? parseSimpleToCSV(values[`${type}LeavesElements`], type, start)
          : parseSimpleToJSON(values[`${type}LeavesElements`]);

      fieldName = field;
      fieldValue = textAreaElements;
    }

    setValues({
      ...values,
      [fieldName]: fieldValue,
      [type + 'IncludeHeaders']: false,
      [type + 'Advanced']: isAdvancedMode
    });
  };

  return (
    <div className={classnames(classes.input)}>
      <button
        className={classnames(
          utilClasses.hollowButton,
          utilClasses.hollowButtonBlack,
          classes.inputTypeButton,
          classes.actions
        )}
        onClick={() => handleModeChange(values)}
        type="button"
        data-test="input-switch"
      >
        {values[type + 'Advanced'] ? 'Simple mode' : 'Advanced mode (CSV/JSON)'}
      </button>
      {!values[type + 'Advanced'] ? (
        <SimpleInput
          hashKeys={hash.hashKeys}
          hashTypes={hash.hashTypes}
          setFieldValue={setFieldValue}
          merkleElements={values[type + 'LeavesElements']}
          values={values}
          elementsList={elementsList}
          errors={errors}
          textareaName={textareaName}
          type={type}
        />
      ) : (
        <AdvancedInput
          values={values}
          type={type}
          setValues={setValues}
          textareaName={textareaName}
          textareaConfig={InputTypes[treeInputType]}
          onTreeInputTypeChange={setTreeInputType}
          handleBlur={handleBlur}
          handleChange={handleChange}
          errors={errors}
        />
      )}
    </div>
  );
};

export default DualModeInput;
