import classnames from 'classnames';
import { parseFile } from 'evm-chain-scripts';
import classes from 'pages/CreateMint/CreateMint.module.scss';
import { toast } from 'react-toastify';
import utilClasses from 'utils/scss/modules/utils.module.scss';

export const AdvancedInput = ({
  textareaConfig: { inputType, placeholderText, alternativeInputType, switchButtonText },
  textareaName,
  values,
  type,
  errors,
  handleChange,
  onTreeInputTypeChange,
  handleBlur,
  setValues
}) => {
  const initialValues = values[textareaName] ?? '';

  const importSettings = {
    label: `Import ${inputType === 'CSV' ? 'CSV' : 'JSON'} file`,
    accept: inputType === 'CSV' ? '.csv' : '.json'
  };

  const handleFileUpload = (event) => {
    const callback = ({ parsedData, error }) => {
      if (error) {
        toast.error(error);
      } else {
        setValues({ ...values, [textareaName]: parsedData, [type + 'IncludeHeaders']: false });
      }
    };

    parseFile(event.target, type, inputType, callback, values[`${type}IncludeHeaders`]);
  };

  return (
    <div className="fadeIn">
      {values[`${type}File`] ? null : (
        <textarea
          className={classnames(errors[textareaName] && classes.inputError, classes.textarea)}
          name={textareaName}
          rows={7}
          cols={50}
          placeholder={placeholderText}
          value={initialValues}
          onChange={handleChange}
          data-test="adv-mode-textarea"
        />
      )}
      <div className={classnames(classes.actions, classes.file)}>
        <label htmlFor={`${textareaName}-file`} className={classes.fileLabel}>
          {importSettings.label}
        </label>
        <div className={classes.actionsColumn}>
          <input
            name={`${type}File`}
            id={`${textareaName}-file`}
            className={classes.fileInput}
            onChange={handleFileUpload}
            type="file"
            accept={importSettings.accept}
            data-test={`${textareaName}-file-input`}
          />
          <div className={classes.actionsColumn}>
            {inputType === 'CSV' && (
              <>
                <label className={classnames(classes.checkbox, 'fadeIn')}>
                  Headers included
                  <input
                    name={`${type}IncludeHeaders`}
                    type="checkbox"
                    checked={values[`${type}IncludeHeaders`]}
                    onBlur={handleBlur}
                    onChange={handleChange}
                    data-test={`${type}-include-headers`}
                  />
                </label>
                {values[`${type}IncludeHeaders`] && (
                  <p className={classnames(classes.warning, 'fadeIn')} data-test="headers-warning">
                    Be careful, first line of data are headers and wont be included
                  </p>
                )}
              </>
            )}
          </div>
        </div>

        <div className={classes.actionsColumn}>
          <button
            className={classnames(
              utilClasses.hollowButton,
              utilClasses.hollowButtonBlack,
              classes.inputTypeButton
            )}
            onClick={() => onTreeInputTypeChange(type, alternativeInputType, values, setValues)}
            type="button"
            data-test="adv-mode-switch-btn"
          >
            {switchButtonText}
          </button>
        </div>
      </div>
    </div>
  );
};
