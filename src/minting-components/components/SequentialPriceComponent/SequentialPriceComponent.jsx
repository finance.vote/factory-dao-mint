import { ethInstance } from 'evm-chain-scripts';
import { useEffect, useState } from 'react';
import classes from './SequentialPriceComponent.module.scss';

const SpeedBumpPriceGateAbi = [
  {
    inputs: [
      { internalType: 'address', name: 'from', type: 'address' },
      { internalType: 'uint256', name: 'price', type: 'uint256' },
      { internalType: 'uint256', name: 'paid', type: 'uint256' }
    ],
    name: 'NotEnoughETH',
    type: 'error'
  },
  {
    inputs: [
      { internalType: 'address', name: 'from', type: 'address' },
      { internalType: 'address', name: 'to', type: 'address' },
      { internalType: 'uint256', name: 'amount', type: 'uint256' }
    ],
    name: 'TransferETHFailed',
    type: 'error'
  },
  { inputs: [], name: 'ZeroDenominator', type: 'error' },
  { inputs: [], name: 'ZeroPriceFloor', type: 'error' },
  {
    inputs: [
      { internalType: 'uint256', name: 'priceFloor', type: 'uint256' },
      { internalType: 'uint256', name: 'priceDecay', type: 'uint256' },
      { internalType: 'uint256', name: 'priceIncrease', type: 'uint256' },
      { internalType: 'uint256', name: 'priceIncreaseDenominator', type: 'uint256' },
      { internalType: 'address', name: 'beneficiary', type: 'address' }
    ],
    name: 'addGate',
    outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    stateMutability: 'nonpayable',
    type: 'function'
  },
  {
    inputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    name: 'gates',
    outputs: [
      { internalType: 'uint256', name: 'priceIncreaseFactor', type: 'uint256' },
      { internalType: 'uint256', name: 'priceIncreaseDenominator', type: 'uint256' },
      { internalType: 'uint256', name: 'lastPrice', type: 'uint256' },
      { internalType: 'uint256', name: 'decayFactor', type: 'uint256' },
      { internalType: 'uint256', name: 'priceFloor', type: 'uint256' },
      { internalType: 'uint256', name: 'lastPurchaseBlock', type: 'uint256' },
      { internalType: 'address', name: 'beneficiary', type: 'address' }
    ],
    stateMutability: 'view',
    type: 'function'
  },
  {
    inputs: [{ internalType: 'uint256', name: 'index', type: 'uint256' }],
    name: 'getCost',
    outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    stateMutability: 'view',
    type: 'function'
  },
  {
    inputs: [],
    name: 'numGates',
    outputs: [{ internalType: 'uint256', name: '', type: 'uint256' }],
    stateMutability: 'view',
    type: 'function'
  },
  {
    inputs: [
      { internalType: 'uint256', name: 'index', type: 'uint256' },
      { internalType: 'address', name: 'sender', type: 'address' }
    ],
    name: 'passThruGate',
    outputs: [],
    stateMutability: 'payable',
    type: 'function'
  }
];

async function getIdentityDecayFactor() {
  const contract = await ethInstance.getReadContractByAddress(
    { abi: SpeedBumpPriceGateAbi },
    '0x8f624A8E59D9AC77795D9DC036B3ECA8327f6Ae5',
    137
  );
  const gate = await contract.gates(1);
  const decayFactor = gate.decayFactor / 10 ** 18;
  return decayFactor.toFixed(4);
}

async function getIdentityPrice() {
  const contract = await ethInstance.getReadContractByAddress(
    { abi: SpeedBumpPriceGateAbi },
    '0x8f624A8E59D9AC77795D9DC036B3ECA8327f6Ae5',
    137
  );
  const price = await contract.getCost(1);
  return (price / 10 ** 18).toFixed(2);
}

const SequentialPriceComponent = ({
  // groupKey,
  // mintType,
  // tokenSymbol,
  isDecayPriceVisible,
  customStyles,
  currencySymbol
}) => {
  const styles = { ...classes, ...customStyles };
  // const { data: identityPrice } = useIdentityPrice();
  // const { data: identityDecayFactor } = useIdentityDecayFactor();
  // const { data: nextToken, isSuccess } = useNextTokenToMint(groupKey, mintType);
  const [priceDecay, setPriceDecay] = useState();
  const [price, setPrice] = useState();
  useEffect(() => {
    let isComponentStillAlive = true;
    (async () => {
      const [decay, price] = await Promise.all([getIdentityDecayFactor(), getIdentityPrice()]);
      if (isComponentStillAlive) {
        setPriceDecay(decay);
        setPrice(price);
      }
    })();
    return () => (isComponentStillAlive = false);
  }, []);
  return (
    <>
      {/* {isSuccess ? (
        <div className={styles.fvtId}>
          {tokenSymbol} ID {nextToken?.value || ''}
        </div>
      ) : null} */}
      <div className={styles.formRow}>
        <div className={styles.valuesContainer}>
          <div className={styles.singleValue}>
            <span
              className={styles.value}
              name="price"
              value={`${price || '0'} $${currencySymbol}`}
            >{`${price || '0'} $${currencySymbol}`}</span>
            <span className={styles.label}>current price</span>
          </div>
          {isDecayPriceVisible ? (
            <div className={styles.singleValue}>
              <span className={styles.value} name="identityDecayFactor" value={priceDecay}>
                {priceDecay || '0.0'}
              </span>
              <span className={styles.label}>price decay per block</span>
            </div>
          ) : null}
        </div>
      </div>
    </>
  );
};

export default SequentialPriceComponent;
