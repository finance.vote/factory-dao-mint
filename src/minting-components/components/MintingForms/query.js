import axios from 'axios';
import { useQuery } from 'react-query';

const fetchGroupKeys = async () => {
  try {
    const { data: groupKeysData } = await axios.get('/minting/group-keys');
    return groupKeysData;
  } catch (error) {
    console.error('Could not fetch mint group keys');
  }
};

const fetchAllSeriesForGroupKey = async (groupKey) => {
  try {
    const { data: lastMintMetadata } = await axios.get(`/minting/all-series/${groupKey}`);
    return lastMintMetadata;
  } catch (error) {
    console.error('Could not fetch all series for selected group key');
  }
};

const fetchSeriesMetadata = async (metadataRoot) => {
  try {
    const { data: seriesMetadata } = await axios.get(`/minting/series-metadata/${metadataRoot}`);
    return seriesMetadata;
  } catch (error) {
    console.error('Could not fetch series metadata');
    return [];
  }
};

export const useGroupKeys = () =>
  useQuery(['groupKeys'], async () => await fetchGroupKeys(), {
    refetchOnWindowFocus: false,
    initialData: []
  });

export const useAllSeriesForGroupKey = ({ groupKey }) =>
  useQuery(
    ['allSeriesForGroupKey', groupKey],
    async () => await fetchAllSeriesForGroupKey(groupKey),
    {
      enabled: !!groupKey,
      refetchOnWindowFocus: false,
      initialData: []
    }
  );

export const useSeriesMetadata = ({ metadataRoot }) =>
  useQuery(['seriesMetadata', metadataRoot], async () => await fetchSeriesMetadata(metadataRoot), {
    enabled: !!metadataRoot,
    refetchOnWindowFocus: false,
    initialData: []
  });
