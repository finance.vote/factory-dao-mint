import classnames from 'classnames';
import DateTimeSelectModal from 'components/DateTimeSelectModal';
import Select from 'components/Ui/Select';
import classes from 'pages/CreateMint/CreateMint.module.scss';
import { keyToOptionParser } from 'pages/CreateMint/helpers';
import { useEffect, useState } from 'react';
import utilClasses from 'utils/scss/modules/utils.module.scss';
const PLACEHOLDER_LABEL = {
  file: 'Upload placeholder image here:',
  cloudflare: 'Input cloudflare video id here:'
};

const PLACEHOLDER_OPTIONS = [
  { value: 'file', label: 'File' },
  { value: 'cloudflare', label: 'Cloudflare' }
];

const MintingOptionsFields = ({
  handleBlur,
  handleChange,
  values,
  errors,
  setFieldValue,
  stageTypes
}) => {
  stageTypes = stageTypes.map(keyToOptionParser);

  const [stageValue, setStageValue] = useState(
    stageTypes.find((type) => type.value === values.stageType)
  );

  useEffect(() => {
    if (!values.stages) {
      setFieldValue('stageType', 'none');
    } else {
      if (values.stageType !== 'none') {
        setStageValue(stageTypes.find((type) => type.value === values.stageType));
      } else {
        setFieldValue('stageType', stageTypes[0].value);
      }
    }
  }, [values.stageType, values.stages]);

  const addPlaceholderToValues = ({ target }) => {
    if (values.placeholderType === 'file') {
      setFieldValue('placeholderImage', target.files[0]);
    } else {
      setFieldValue('placeholderImage', target.value);
    }
  };

  return (
    <div className={classes.section}>
      <p className={classes.subTitle}>Minting Options</p>

      <div className={classnames(classes.checkbox, utilClasses.blackInput)}>
        <input
          type="checkbox"
          name="unboxingVideo"
          id="unboxingVideo"
          className={errors.unboxingVideo && classes.inputError}
          value={values.unboxingVideo}
          onBlur={handleBlur}
          onChange={handleChange}
          data-test="unboxing-video-checkbox"
        />
        <label htmlFor="unboxingVideo">Unboxing video</label>
      </div>
      {values.unboxingVideo ? (
        <>
          <label>
            Video ID
            <input
              type="text"
              name="cloudflareVideoId"
              placeholder="Input cloudflare video ID here"
              className={classnames(
                errors.cloudflareVideoId && classes.inputError,
                classes.input,
                utilClasses.input
              )}
              value={values.cloudflareVideoId}
              onBlur={handleBlur}
              onChange={handleChange}
              data-test="unboxing-cloudflare-id"
            />
          </label>

          <div className={classnames(classes.checkbox, utilClasses.blackInput)}>
            <input
              type="checkbox"
              name="everyoneAtOnce"
              id="everyoneAtOnce"
              className={errors.everyoneAtOnce && classes.inputError}
              value={values.everyoneAtOnce}
              onBlur={handleBlur}
              onChange={handleChange}
              data-test="everyone-at-once-checkbox"
            />
            <label htmlFor="everyoneAtOnce">Everyone at once</label>
          </div>
        </>
      ) : null}
      <Select
        id="placeholder-type-selection"
        className={classnames(
          classes.placeholderType,
          utilClasses.selectInput,
          utilClasses.spaceTop
        )}
        defaultValue={PLACEHOLDER_OPTIONS[0].value}
        name="placeholderType"
        value={PLACEHOLDER_OPTIONS.find((type) => type.value === values.placeholderType)}
        onChange={({ value }) => {
          setFieldValue('placeholderImage', '');
          setFieldValue('placeholderType', value);
        }}
        label="Select placeholder type"
        options={PLACEHOLDER_OPTIONS}
      />
      <div className={classnames(classes.file, utilClasses.blackInput)}>
        <label htmlFor="placeholderImage" className={classes.fileLabel}>
          {PLACEHOLDER_LABEL[values.placeholderType]}
        </label>
        <input
          type={values.placeholderType === 'file' ? 'file' : 'text'}
          name="placeholderImage"
          id="placeholderImage"
          className={classnames(
            errors.placeholderImage && classes.inputError,
            classes.input,
            values.placeholderType === 'file' ? utilClasses.blackInput : utilClasses.input
          )}
          placeholder={PLACEHOLDER_LABEL[values.placeholderType]}
          onBlur={handleBlur}
          onChange={addPlaceholderToValues}
          data-test={`placeholder-${values.placeholderType}-input`}
        />
      </div>
      <div className={classnames(classes.checkbox, utilClasses.blackInput)}>
        <input
          type="checkbox"
          name="mediaExternal"
          id="mediaExternal"
          className={errors.mediaExternal && classes.inputError}
          value={values.mediaExternal}
          onBlur={handleBlur}
          onChange={handleChange}
          data-test="externally-hosted"
        />
        <label htmlFor="mediaExternal">Tokens externally hosted</label>
      </div>

      <div className={classnames(classes.checkbox, utilClasses.blackInput)}>
        <input
          type="checkbox"
          name="showNext"
          id="showNext"
          className={errors.showNext && classes.inputError}
          value={values.showNext}
          onBlur={handleBlur}
          onChange={handleChange}
          data-test="show-next-media-checkbox"
        />
        <label htmlFor="showNext">Show next media</label>
      </div>

      <div className={classnames(classes.checkbox, utilClasses.blackInput)}>
        <input
          type="checkbox"
          name="reveal"
          id="reveal"
          className={errors.reveal && classes.inputError}
          value={values.reveal}
          onBlur={handleBlur}
          onChange={handleChange}
          data-test="reveal-checkbox"
        />
        <label htmlFor="reveal">Reveal</label>
      </div>

      <div className={classnames(classes.checkbox, utilClasses.blackInput)}>
        <input
          type="checkbox"
          name="trafficLights"
          id="trafficLights"
          className={errors.trafficLights && classes.inputError}
          value={values.trafficLights}
          onBlur={handleBlur}
          onChange={handleChange}
          data-test="traffic-lights-checkbox"
        />
        <label htmlFor="trafficLights">Show traffic lights</label>
      </div>

      <div className={classnames(classes.checkbox, utilClasses.blackInput)}>
        <input
          type="checkbox"
          name="progressBar"
          id="progressBar"
          className={errors.progressBar && classes.inputError}
          value={values.progressBar}
          onBlur={handleBlur}
          onChange={handleChange}
          data-test="show-progressbar-checkbox"
        />
        <label htmlFor="progressBar">Show progress bar</label>
      </div>
      <div className={classnames(classes.checkbox, utilClasses.blackInput)}>
        <input
          type="checkbox"
          name="stages"
          id="stages"
          className={errors.stages && classes.inputError}
          value={values.stages}
          onBlur={handleBlur}
          onChange={handleChange}
        />
        <label htmlFor="stages">Stages based</label>
      </div>
      {values.stages ? (
        <Select
          id="stage-type-selection"
          className={classnames(classes.projectSelect, utilClasses.selectInput)}
          defaultValue={stageTypes[0].value}
          value={stageValue}
          onChange={({ value }) => setFieldValue('stageType', value)}
          label="Select stage strategy type"
          options={stageTypes}
        />
      ) : null}
      {values.stages && ['simultaneous', 'timebased'].includes(values.stageType) ? (
        <DateTimeSelectModal
          selected={values.nextStageMinDate}
          onDateChange={(value) => {
            setFieldValue('nextStageMinDate', value);
          }}
          value={values.nextStageMinDate}
          error={errors.nextStageMinDate}
          setFieldValue={setFieldValue}
          defaultText={'Select next stage start date'}
        />
      ) : null}
    </div>
  );
};

export default MintingOptionsFields;
