import classnames from 'classnames';
import DateTimeSelectModal from 'components/DateTimeSelectModal';
import Select from 'components/Ui/Select';
import If from 'minting-components/components/If/If';
import { PRICE_TYPES } from 'minting-components/helpers';
import classes from 'pages/CreateMint/CreateMint.module.scss';
import { keyToOptionParser } from 'pages/CreateMint/helpers';
import { useEffect, useState } from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import utilClasses from 'utils/scss/modules/utils.module.scss';

const AdditionalPriceFields = ({ priceType, errors, handleBlur, handleChange, values }) => {
  return (
    <>
      <If condition={priceType === PRICE_TYPES.SPEEDBUMP}>
        <label className={classes.label}>
          Price decay
          <input
            type="number"
            name="priceDecay"
            className={classnames(
              errors.priceDecay && classes.inputError,
              classes.input,
              utilClasses.input
            )}
            value={values.priceDecay}
            onBlur={handleBlur}
            onChange={handleChange}
            data-test="price-decay"
          />
        </label>
        <label className={classes.label}>
          Price Increase
          <input
            type="number"
            name="priceIncrease"
            className={classnames(
              errors.priceIncrease && classes.inputError,
              classes.input,
              utilClasses.input
            )}
            value={values.priceIncrease}
            onBlur={handleBlur}
            onChange={handleChange}
            data-test="price-increase"
          />
        </label>
        <label className={classes.label}>
          Price Denominator
          <input
            type="number"
            name="priceDenominator"
            className={classnames(
              errors.priceDenominator && classes.inputError,
              classes.input,
              utilClasses.input
            )}
            value={values.priceDenominator}
            onBlur={handleBlur}
            onChange={handleChange}
            data-test="price-denominator"
          />
        </label>
      </If>
      <label className={classes.label}>
        Beneficiary
        <input
          type="text"
          name="priceBeneficiary"
          className={classnames(
            errors.priceBeneficiary && classes.inputError,
            classes.input,
            utilClasses.input
          )}
          value={values.priceBeneficiary}
          onBlur={handleBlur}
          onChange={handleChange}
          data-test="price-beneficiary"
        />
      </label>
    </>
  );
};

const SeriesFields = ({
  handleBlur,
  handleChange,
  setFieldValue,
  values,
  errors,
  eligibilityTypes,
  priceTypes,
  setValues,
  isBackendMint
}) => {
  eligibilityTypes = eligibilityTypes.map(keyToOptionParser);

  priceTypes = priceTypes.map(keyToOptionParser);

  const [eligibilityValue, setEligibilityValue] = useState(
    eligibilityTypes.find((type) => type.value === values.eligibilityType)
  );

  const [priceValue, setPriceValue] = useState(
    priceTypes.find((type) => type.value === values.priceType)
  );

  useEffect(() => {
    if (eligibilityValue.value !== values.eligibilityType) {
      setEligibilityValue(eligibilityTypes.find((type) => type.value === values.eligibilityType));
    }
    if (priceValue.value !== values.priceType) {
      setPriceValue(priceTypes.find((type) => type.value === values.priceType));
    }
  }, [values.eligibilityType, values.priceType]);

  const handleDateChange = (value) => {
    setFieldValue('startsAt', value);
  };

  const handlePriceType = (value) => {
    const isAmalu = value === PRICE_TYPES.AMALU;

    setValues({
      ...values,
      priceIndex: isAmalu ? 1 : '',
      priceType: value
    });
  };

  return (
    <>
      <div className={classes.section} data-test="startsAt">
        <DateTimeSelectModal
          selected={values.startsAt}
          onDateChange={handleDateChange}
          value={values.startsAt}
          error={errors.startsAt}
          setFieldValue={setFieldValue}
          defaultText="Select start date"
        />

        <div className={classnames(classes.checkbox, utilClasses.blackInput)} data-test="is-paused">
          <input
            type="checkbox"
            name="paused"
            id="paused"
            className={errors.paused && classes.inputError}
            value={values.paused}
            onBlur={handleBlur}
            onChange={handleChange}
            data-test="is-paused-checkbox"
          />
          <label htmlFor="paused">Is initially paused</label>
        </div>
      </div>

      <div className={classes.section} data-test="eligibility-section">
        <p className={classes.subTitle}>Series Eligibility</p>

        <Select
          id="eligibility-selection"
          className={classnames(classes.projectSelect, utilClasses.selectInput)}
          defaultValue={eligibilityTypes[0].value}
          value={eligibilityValue}
          onChange={({ value }) => setFieldValue('eligibilityType', value)}
          label="Select eligibility type"
          options={eligibilityTypes}
        />

        <If condition={!isBackendMint}>
          <label className={classes.label}>
            Eligibility address
            <input
              type="text"
              name="eligibilityAddress"
              placeholder="Input eligibility address"
              className={classnames(
                errors.eligibilityAddress && classes.inputError,
                classes.input,
                utilClasses.input
              )}
              value={values.eligibilityAddress}
              onBlur={handleBlur}
              onChange={handleChange}
            />
          </label>

          <label className={classes.label}>
            Maximum withdrawals per address
            <input
              type="number"
              name="maxWithdrawalsPerAddress"
              className={classnames(
                errors.maxWithdrawalsPerAddress && classes.inputError,
                classes.input,
                utilClasses.input
              )}
              defaultValue={values.maxWithdrawalsPerAddress}
              onBlur={handleBlur}
              onChange={handleChange}
            />
          </label>

          <label className={classes.label}>
            Maximum total withdrawals
            <input
              type="number"
              name="maxWithdrawalsTotal"
              className={classnames(
                errors.maxWithdrawalsTotal && classes.inputError,
                classes.input,
                utilClasses.input
              )}
              defaultValue={values.maxWithdrawalsTotal}
              onBlur={handleBlur}
              onChange={handleChange}
            />
          </label>
          <div className={classnames(classes.checkbox, utilClasses.blackInput)}>
            <input
              type="checkbox"
              name="eligibilityGateExists"
              id="eligibilityGateExists"
              className={errors.eligibilityGateExists && classes.inputError}
              checked={values.eligibilityGateExists}
              onBlur={handleBlur}
              onChange={handleChange}
            />
            <label htmlFor="eligibilityGateExists">
              Eligibility gate exists (if not - you will need to add new gate)
            </label>
          </div>
          {values.eligibilityGateExists ? (
            <label className={classnames(classes.label, 'fadeIn')}>
              Eligibility index
              <input
                type="number"
                name="eligibilityIndex"
                className={classnames(
                  errors.eligibilityIndex && classes.inputError,
                  classes.input,
                  utilClasses.input
                )}
                value={values.eligibilityIndex}
                onBlur={handleBlur}
                onChange={handleChange}
              />
            </label>
          ) : null}
        </If>
      </div>
      <If condition={!isBackendMint}>
        <div className={classes.section} data-test="price-section">
          <p className={classes.subTitle}>Series Price</p>

          <Select
            id="price-selection"
            className={classnames(classes.projectSelect, utilClasses.selectInput)}
            defaultValue={priceTypes[0].value}
            value={priceValue}
            onChange={({ value }) => handlePriceType(value)}
            label="Select price type"
            options={priceTypes}
          />

          <label className={classes.label}>
            Price address
            <input
              type="text"
              name="priceAddress"
              placeholder="Input price address"
              className={classnames(
                errors.priceAddress && classes.inputError,
                classes.input,
                utilClasses.input
              )}
              value={values.priceAddress}
              onBlur={handleBlur}
              onChange={handleChange}
              data-test="price-address"
            />
          </label>

          <label className={classes.label}>
            Price
            <input
              type="number"
              name="price"
              className={classnames(
                errors.price && classes.inputError,
                classes.input,
                utilClasses.input
              )}
              value={values.price}
              onBlur={handleBlur}
              onChange={handleChange}
              data-test="price-input"
            />
          </label>
          <If condition={values.priceType !== PRICE_TYPES.AMALU}>
            <div className={classnames(classes.checkbox, utilClasses.blackInput)}>
              <input
                type="checkbox"
                name="priceGateExists"
                id="priceGateExists"
                className={errors.priceGateExists && classes.inputError}
                checked={values.priceGateExists}
                onBlur={handleBlur}
                onChange={handleChange}
                data-test="price-gate-exists"
              />
              <label htmlFor="priceGateExists">
                Price gate exists (if not - you will need to add new gate)
              </label>
            </div>
            {values.priceGateExists ? (
              <label className={classes.label}>
                Price index
                <input
                  type="number"
                  name="priceIndex"
                  className={classnames(
                    errors.priceIndex && classes.inputError,
                    classes.input,
                    utilClasses.input
                  )}
                  value={values.priceIndex}
                  onBlur={handleBlur}
                  onChange={handleChange}
                  data-test="price-index"
                />
              </label>
            ) : (
              <AdditionalPriceFields
                priceType={values.priceType}
                handleBlur={handleBlur}
                handleChange={handleChange}
                errors={errors}
                values={values}
              />
            )}
          </If>
        </div>
      </If>
      <div className={classes.section} data-test="token-ranges">
        <p className={classes.subTitle}>Token range</p>
        <div className={classes.inputsWrapper} data-test="token-ranges-wrapper">
          <label className={classes.label}>
            Minimum
            <input
              type="number"
              name="minRange"
              placeholder="Input minimum token range"
              className={classnames(
                errors.minRange && classes.inputError,
                classes.input,
                utilClasses.input
              )}
              value={values.minRange}
              onBlur={handleBlur}
              onChange={handleChange}
              data-test="token-min-range"
            />
          </label>
          <label className={classes.label}>
            Maximum
            <input
              type="number"
              name="maxRange"
              className={classnames(
                errors.maxRange && classes.inputError,
                classes.input,
                utilClasses.input
              )}
              placeholder="Input maximum token range"
              value={values.maxRange}
              onBlur={handleBlur}
              onChange={handleChange}
              data-test="token-max-range"
            />
          </label>
        </div>
      </div>
    </>
  );
};

export default SeriesFields;
