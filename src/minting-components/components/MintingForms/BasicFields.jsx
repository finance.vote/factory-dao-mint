import classnames from 'classnames';
import Select from 'components/Ui/Select';
import classes from 'pages/CreateMint/CreateMint.module.scss';
import utilClasses from 'utils/scss/modules/utils.module.scss';

const mintTypes = [
  { value: 'sequential', label: 'Sequential' },
  { value: 'selectable', label: 'Selectable' }
];

const BasicFields = ({
  errors,
  handleBlur,
  handleChange,
  values,
  setFieldValue,
  isBackendMint
}) => {
  return (
    <div className={classes.section}>
      <label className={classes.label}>
        Group (key)
        <input
          type="text"
          name="group"
          placeholder="Input group key"
          className={classnames(
            errors.group && classes.inputError,
            classes.input,
            utilClasses.input
          )}
          value={values.group}
          onBlur={handleBlur}
          onChange={handleChange}
          data-test="group-key-input"
        />
      </label>

      <label className={classes.label}>
        NFT address
        <input
          type="text"
          name="nftAddress"
          placeholder="Input NFT address"
          className={classnames(
            errors.nftAddress && classes.inputError,
            classes.input,
            utilClasses.input
          )}
          defaultValue={values.nftAddress}
          onBlur={handleBlur}
          onChange={handleChange}
          data-test="nft-address"
        />
      </label>

      {!isBackendMint ? (
        <label className={classes.label}>
          Gate address
          <input
            type="text"
            name="gateAddress"
            placeholder="Input address"
            className={classnames(
              errors.address && classes.inputError,
              classes.input,
              utilClasses.input
            )}
            value={values.gateAddress}
            onBlur={handleBlur}
            onChange={handleChange}
            data-test="gate-address"
          />
        </label>
      ) : null}

      <Select
        id="mint-type-selection"
        className={classnames(classes.projectSelect, utilClasses.selectInput)}
        defaultValue={mintTypes[0].value}
        name="mintType"
        value={mintTypes.find((type) => type.value === values.mintType)}
        onChange={({ value }) => setFieldValue('mintType', value)}
        label="Select mint type"
        options={mintTypes}
      />
    </div>
  );
};

export default BasicFields;
