import axios from 'axios';
import classnames from 'classnames';
import DateTimeSelectModal from 'components/DateTimeSelectModal';
import DialogBlack from 'components/DialogBlack';
import Select from 'components/Ui/Select';
import { isAddress } from 'ethers/lib/utils';
import { parseCsvToArray } from 'evm-chain-scripts';
import { Formik } from 'formik';
import DualModeInput from 'minting-components/components/DualModeInput';
import { PRICE_TYPES } from 'minting-components/helpers';
import dynamic from 'next/dynamic';
import classes from 'pages/CreateMint/CreateMint.module.scss';
import { useState } from 'react';
import { toast } from 'react-toastify';
import utilClasses from 'utils/scss/modules/utils.module.scss';
import * as Yup from 'yup';
import BackendMint from '../BackendMint';
import { useAllSeriesForGroupKey, useGroupKeys, useSeriesMetadata } from './query';
const BasicFields = dynamic(() => import('minting-components/components/MintingForms/BasicFields'));
const SeriesFields = dynamic(() =>
  import('minting-components/components/MintingForms/SeriesFields')
);
const MintingOptionsFields = dynamic(() =>
  import('minting-components/components/MintingForms/MintingOptionsFields')
);

const uriTest = /^(?:https?:\/\/)?(?:[^@/\n]+@)?(?:www\.)?([^:/?\n]+)/;

const commonValidationDefaults = {
  startsAt: Yup.string().required(),
  nextStageMinDate: Yup.string(),
  paused: Yup.boolean(),
  eligibilityType: Yup.string().required(),
  eligibilityAddress: Yup.string().when('isBackendMint', {
    is: true,
    then: Yup.string().notRequired(),
    otherwise: Yup.string().test('isValidAddress', 'Incorrect address', isAddress).required()
  }),
  eligibilityGateExists: Yup.boolean(),
  eligibilityIndex: Yup.number().when('isBackendMint', {
    is: true,
    then: Yup.number().notRequired(),
    otherwise: Yup.number().when('eligibilityGateExists', {
      is: true,
      then: Yup.number().integer().required()
    })
  }),
  maxWithdrawalsPerAddress: Yup.number()
    .min(1)
    .required()
    .test(
      'lessThanTotalWithdrawals',
      'Value must be equal or less than max total withdrawals',
      (value, context) => {
        return Number(value) <= Number(context.from[0].value.maxWithdrawalsTotal);
      }
    ),
  maxWithdrawalsTotal: Yup.number()
    .min(1)
    .required()
    .test(
      'greaterThanWithdrawalsPerAddress',
      'Value must be equal or greater than max withdrawals per address',
      (value, context) => {
        return Number(value) >= Number(context.from[0].value.maxWithdrawalsPerAddress);
      }
    ),
  priceType: Yup.string().required(),
  priceAddress: Yup.string().when('isBackendMint', {
    is: true,
    then: Yup.string().notRequired(),
    otherwise: Yup.string().test('isValidAddress', 'Incorrect address', isAddress).required()
  }),
  price: Yup.number().min(0),
  priceGateExists: Yup.boolean(),
  priceIndex: Yup.mixed().when('isBackendMint', {
    is: true,
    then: Yup.mixed().notRequired(),
    otherwise: Yup.mixed().when('priceGateExists', {
      is: true,
      then: Yup.number().integer().required()
    })
  }),
  priceBeneficiary: Yup.string().when('priceGateExists', {
    is: false,
    then: Yup.string().required()
  }),
  // priceFloor: Yup.number().when('priceGateExists', {
  //   is: false,
  //   then: Yup.number().required()
  // }),
  priceDecay: Yup.number().when(['priceGateExists', 'priceType'], {
    is: (priceGateExists, priceType) => !priceGateExists && priceType === PRICE_TYPES.SPEEDBUMP,
    then: Yup.number().required()
  }),
  priceIncrease: Yup.number().when(['priceGateExists', 'priceType'], {
    is: (priceGateExists, priceType) => !priceGateExists && priceType === PRICE_TYPES.SPEEDBUMP,
    then: Yup.number().required()
  }),
  priceDenominator: Yup.number().when(['priceGateExists', 'priceType'], {
    is: (priceGateExists, priceType) => !priceGateExists && priceType === PRICE_TYPES.SPEEDBUMP,
    then: Yup.number().required()
  }),
  addressTextarea: Yup.mixed().when(['addressAdvanced', 'addressTextareaType'], {
    is: (addressAdvanced, addressTextareaType) => addressAdvanced && addressTextareaType === 'JSON',
    then: Yup.array(
      Yup.object({
        address: Yup.string().test('isValidAddress', 'Incorrect address', (address) =>
          isAddress(address)
        )
      }).default(undefined)
    ).required(),
    otherwise: Yup.mixed().when(['addressAdvanced', 'addressTextareaType'], {
      is: (addressAdvanced, addressTextareaType) =>
        addressAdvanced && addressTextareaType === 'CSV',
      then: Yup.string()
        .test({
          message: () => 'Invalid address exist',
          test: async (value) => {
            try {
              const data = parseCsvToArray(value);
              const result = data.map((address) => isAddress(address[0]));
              if (result.every((valid) => valid)) return true;

              return false;
            } catch (e) {
              return false;
            }
          }
        })
        .required()
    })
  }),
  addressIncludeHeaders: Yup.boolean(),
  metadataTextarea: Yup.mixed().when(['metadataAdvanced', 'metadataTextareaType'], {
    is: (metadataAdvanced, metadataTextareaType) =>
      metadataAdvanced && metadataTextareaType === 'JSON',
    then: Yup.array(
      Yup.object({
        tokenId: Yup.number(),
        uri: Yup.string().matches(/^(?:https?:\/\/)?(?:[^@/\n]+@)?(?:www\.)?([^:/?\n]+)/)
      }).default(undefined)
    ).required(),
    otherwise: Yup.mixed().when(['metadataAdvanced', 'metadataTextareaType'], {
      is: (metadataAdvanced, metadataTextareaType) =>
        metadataAdvanced && metadataTextareaType === 'CSV',
      then: Yup.string()
        .test({
          message: () => 'Invalid address exist',
          test: async (value) => {
            try {
              const numberTest = /^\d+$/;
              const data = parseCsvToArray(value);
              const validToken = data.map((parsedData) => numberTest.test(parsedData[0]));
              const validUri = data.map((parsedData) => uriTest.test(parsedData[1]));

              if (validToken.every((valid) => valid) && validUri.every((valid) => valid))
                return true;

              return false;
            } catch (e) {
              return false;
            }
          }
        })
        .required()
    })
  }),
  metadataIncludeHeaders: Yup.boolean(),
  includeHeaders: Yup.boolean(),
  addressLeavesElements: Yup.array().when('isBackendMint', {
    is: true,
    then: Yup.array().notRequired(),
    otherwise: Yup.array()
      .when('addressAdvanced', {
        is: false,
        then: Yup.array(
          Yup.object({
            index: Yup.number(),
            address: Yup.string().test('isValidAddress', 'Incorrect address', isAddress).required()
          })
        )
      })
      .nullable()
  }),
  metadataLeavesElements: Yup.array()
    .when('metadataAdvanced', {
      is: false,
      then: Yup.array(
        Yup.object({
          index: Yup.number(),
          tokenId: Yup.number().integer().required(),
          uri: Yup.string()
            .matches(/^(?:https?:\/\/)?(?:[^@/\n]+@)?(?:www\.)?([^:/?\n]+)/)
            .required()
        })
      )
    })
    .nullable(),
  minRange: Yup.number().integer(),
  maxRange: Yup.number()
    .integer()
    .test(
      'greaterThanMinRange',
      'Value must be greater or equal than minimum token range',
      (value, context) => {
        return Number(value) >= Number(context.from[0].value.minRange);
      }
    )
};

const commonFormDefaults = {
  startsAt: '',
  nextStageMinDate: '',
  paused: false,
  eligibilityType: 'amalu',
  eligibilityAddress: '',
  eligibilityGateExists: true,
  eligibilityIndex: '',
  maxWithdrawalsPerAddress: 10,
  maxWithdrawalsTotal: 100,
  priceType: PRICE_TYPES.FIXED,
  priceAddress: '',
  price: 0,
  priceGateExists: true,
  priceIndex: '',
  priceBeneficiary: '',
  // priceFloor: '',
  priceDecay: '',
  priceIncrease: '',
  priceDenominator: '',
  addressLeavesElements: [{ index: 0, address: '' }],
  addressAdvanced: false,
  addressTextarea: '',
  addressIncludeHeaders: false,
  addressFile: '',
  metadataLeavesElements: [{ index: 0, tokenId: '', uri: '' }],
  metadataAdvanced: false,
  metadataTextarea: '',
  metadataIncludeHeaders: false,
  metadataFile: '',
  minRange: 0,
  maxRange: '',
  addressTextareaType: 'JSON',
  metadataTextareaType: 'JSON',
  isBackendMint: false
};

const SERIES_FORM_DEFAULTS = {
  group: null,
  addressLeavesHashTypes: ['string'],
  addressLeavesHashKeys: ['address'],
  metadataLeavesHashTypes: ['number', 'string'],
  metadataLeavesHashKeys: ['tokenId', 'uri'],
  validationSchema: Yup.object({
    ...commonValidationDefaults,
    group: Yup.object().required()
  }),
  formDefaults: {
    ...commonFormDefaults
  }
};

const MINTING_FORM_DEFAULTS = {
  group: '',
  addressLeavesHashTypes: ['string'],
  addressLeavesHashKeys: ['address'],
  metadataLeavesHashTypes: ['number', 'string'],
  metadataLeavesHashKeys: ['tokenId', 'uri'],
  validationSchema: Yup.object().shape({
    ...commonValidationDefaults,
    group: Yup.string().required(),
    nftAddress: Yup.string().test('isValidAddress', 'Incorrect address', isAddress).required(),
    gateAddress: Yup.string().when('isBackendMint', {
      is: true,
      then: Yup.string(),
      otherwise: Yup.string().test('isValidAddress', 'Incorrect address', isAddress).required()
    }),
    unboxingVideo: Yup.boolean(),
    cloudflareVideoId: Yup.string(),
    everyoneAtOnce: Yup.boolean(),
    showNext: Yup.boolean(),
    reveal: Yup.boolean(),
    trafficLights: Yup.boolean(),
    progressBar: Yup.boolean(),
    stages: Yup.boolean(),
    stageType: Yup.string().oneOf(['none', 'soldout', 'timebased', 'simultaneous']),
    mintType: Yup.string().oneOf(['selectable', 'sequential']),
    placeholderType: Yup.string().oneOf(['file', 'cloudflare']),
    placeholderImage: Yup.string().when('placeholderType', {
      is: 'file',
      then: Yup.string().required(),
      otherwise: Yup.string()
        .required()
        .test({
          message: () => 'Invalid cloudflare videoId',
          test: async (id) => {
            try {
              if (!id || id.includes('object')) return false;
              if (location.hostname === 'localhost') return true; // CORS would not accept http -> https requests
              await axios.get(`https://iframe.cloudflarestream.com/${id}`);
              return true;
            } catch (e) {
              return false;
            }
          }
        })
    })
  }),
  formDefaults: {
    ...commonFormDefaults,
    nftAddress: '',
    gateAddress: '',
    unboxingVideo: false,
    cloudflareVideoId: '',
    everyoneAtOnce: false,
    showNext: false,
    reveal: false,
    trafficLights: false,
    progressBar: false,
    placeholderImage: '',
    placeholderType: 'file',
    mediaExternal: false,
    mintType: 'sequential',
    stages: false,
    stageType: 'none',
    isBackendMint: false
  }
};

const eligibilityTypes = ['Amalu', 'Merkle', 'Whitelist', 'Tokenmapping'];
const priceTypes = ['Fixed', 'Speedbump', 'Amalu', 'Flexible'];
const DualModeInputFields = ['address', 'metadata'];
const stageTypes = ['soldout', 'timebased', 'simultaneous'];

export const SeriesForm = ({ onSubmit, handleTreeInputType, isSelected, setMintChain }) => {
  const formConfig = SERIES_FORM_DEFAULTS;
  const [treeInputType, setTreeInputType] = useState({ address: 'JSON', metadata: 'JSON' });
  const [lastConfig, setLastConfig] = useState(formConfig.formDefaults);
  const [isLoadDialogOpen, setIsLoadDialogOpen] = useState(false);
  const [selectedSeries, setSelectedSeries] = useState(null);
  const { data: groupKeys } = useGroupKeys();
  const { data: seriesMetadata, isSeriesMetadataFetching } = useSeriesMetadata({
    metadataRoot: selectedSeries?.value
  });
  const { data: allSeriesForGroupKey } = useAllSeriesForGroupKey({
    groupKey: lastConfig.group?.value
  });

  const groupKeysOptions = groupKeys?.map((key) => ({ label: key, value: key }));

  const seriesOptions = allSeriesForGroupKey.map(({ metadataRoot }) => ({
    label: metadataRoot,
    value: metadataRoot
  }));

  const dualModeFields = DualModeInputFields;

  const updateInitialValues = async (group, values) => {
    const { value: key } = group;
    try {
      const {
        data: { price, eligibility, nextStageMinDate, chainId }
      } = await axios.get(`/minting/last-series/${key}`);
      //set chain of mint by selected group key
      setMintChain(chainId?.toString());
      const defaultStartsAt = new Date(nextStageMinDate);
      setLastConfig({
        ...values,
        group,
        eligibilityType: eligibility.type,
        eligibilityAddress: eligibility.address,
        eligibilityIndex: eligibility.index,
        priceType: price.type,
        priceAddress: price.address,
        price: price.value,
        priceIndex: price.index,
        startsAt: nextStageMinDate ? defaultStartsAt : '',
        isStagesBased: !!defaultStartsAt
      });
    } catch (e) {
      toast.warn('Cannot find selected group');
    }
  };

  const handleTreeInputTypeChange = (type, alternativeInputType, values, setValues) => {
    handleTreeInputType(type, alternativeInputType, values, setValues);

    setTreeInputType((prevState) => ({
      ...prevState,
      [type]: alternativeInputType
    }));
  };

  const handleLoadDialogOpen = () => {
    setSelectedSeries(null);
    setIsLoadDialogOpen(true);
  };
  const handleLoadDialogClose = () => {
    setIsLoadDialogOpen(false);
  };
  return (
    <>
      <Formik
        initialValues={lastConfig}
        onSubmit={onSubmit}
        validationSchema={formConfig.validationSchema}
        validateOnMount={true}
        enableReinitialize={true}
      >
        {({
          errors,
          values,
          setFieldValue,
          handleBlur,
          handleChange,
          handleSubmit,
          isValid,
          setValues
        }) => (
          <>
            <div className={classes.seriesDialog} data-test="series-dialog">
              <DialogBlack isOpen={isLoadDialogOpen} handleClose={handleLoadDialogClose}>
                <Select
                  id="series"
                  className={classnames(classes.projectSelect, utilClasses.selectInput)}
                  defaultValue={null}
                  label="Series"
                  value={selectedSeries}
                  onChange={setSelectedSeries}
                  options={seriesOptions}
                  placeholder="Select series"
                />
                <button
                  disabled={!selectedSeries && isSeriesMetadataFetching}
                  className={classnames(utilClasses.hollowButton, utilClasses.hollowButtonBlack)}
                  type="button"
                  data-test="series-dialog-load-btn"
                  onClick={() => {
                    setFieldValue('metadataLeavesElements', [
                      ...seriesMetadata.map(({ tokenId, uri }, index) => ({
                        index: index,
                        tokenId: parseInt(tokenId),
                        uri: uri.trim()
                      }))
                    ]);
                    handleLoadDialogClose();
                  }}
                >
                  Load metadata
                </button>
              </DialogBlack>
            </div>
            <form onSubmit={handleSubmit} className={isSelected ? 'fadeIn' : ''}>
              <Select
                id="group-key"
                className={classnames(classes.projectSelect, utilClasses.selectInput)}
                defaultValue={null}
                label="Group key"
                value={values.group}
                onChange={(group) => {
                  updateInitialValues(group, values);
                  setFieldValue('group', group);
                }}
                options={groupKeysOptions}
                placeholder="Select group key"
                isError={errors.group}
              />
              {values.group ? (
                <button
                  className={classnames(utilClasses.hollowButton, utilClasses.hollowButtonBlack)}
                  type="button"
                  data-test="load-previous-metadata-btn"
                  onClick={() => {
                    handleLoadDialogOpen();
                  }}
                >
                  Load previous metadata
                </button>
              ) : null}
              <div className={classnames(classes.checkbox, utilClasses.blackInput)}></div>
              <BackendMint
                isBackendMint={values.isBackendMint}
                onChange={({ target }) => setFieldValue('isBackendMint', target.checked)}
              />
              {dualModeFields.map((type, index) => {
                if (values.isBackendMint && index === 0) return null;
                return (
                  <DualModeInput
                    key={type}
                    type={type}
                    hash={{
                      hashKeys: formConfig[`${type}LeavesHashKeys`],
                      hashTypes: formConfig[`${type}LeavesHashTypes`]
                    }}
                    errors={errors}
                    values={values}
                    setFieldValue={setFieldValue}
                    textareaName={`${type}Textarea`}
                    elementsList={`${type}LeavesElements`}
                    handleBlur={handleBlur}
                    handleChange={handleChange}
                    treeInputType={treeInputType[type]}
                    setTreeInputType={handleTreeInputTypeChange}
                    setValues={setValues}
                  />
                );
              })}

              <SeriesFields
                errors={errors}
                values={values.isBackendMint ? { ...values, eligibilityType: 'qr-code' } : values}
                setFieldValue={setFieldValue}
                handleBlur={handleBlur}
                handleChange={handleChange}
                eligibilityTypes={values.isBackendMint ? ['qr-code'] : eligibilityTypes}
                priceTypes={priceTypes}
                setValues={setValues}
                isBackendMint={values.isBackendMint}
              />
              {values.isStagesBased ? (
                <DateTimeSelectModal
                  selected={values.nextStageMinDate}
                  onDateChange={(value) => setFieldValue('nextStageMinDate', value)}
                  value={values.nextStageMinDate}
                  error={errors.nextStageMinDate}
                  setFieldValue={setFieldValue}
                  defaultText="Select next stage start date"
                />
              ) : null}
              <button
                className={classnames(
                  utilClasses.hollowButton,
                  utilClasses.hollowButtonBlack,
                  classes.submitButton
                )}
                disabled={!isValid}
                type="submit"
                data-test="submit-series-form-btn"
              >
                Upload to blockchain
              </button>
            </form>
          </>
        )}
      </Formik>
    </>
  );
};

export const MintForm = ({ onSubmit, handleTreeInputType, isSelected }) => {
  const [treeInputType, setTreeInputType] = useState({ address: 'JSON', metadata: 'JSON' });
  const formConfig = MINTING_FORM_DEFAULTS;
  const formDefaults = { ...formConfig.formDefaults };
  const dualModeFields = DualModeInputFields;

  const handleTreeInputTypeChange = (type, alternativeInputType, values, setValues) => {
    handleTreeInputType(type, alternativeInputType, values, setValues);

    setTreeInputType((prevState) => ({
      ...prevState,
      [type]: alternativeInputType
    }));
  };

  return (
    <Formik
      initialValues={formDefaults}
      onSubmit={onSubmit}
      validationSchema={formConfig.validationSchema}
      validateOnMount={true}
      enableReinitialize={true}
    >
      {({
        errors,
        values,
        setFieldValue,
        handleBlur,
        handleChange,
        handleSubmit,
        isValid,
        setValues
      }) => {
        return (
          <form onSubmit={handleSubmit} className={isSelected ? 'fadeIn' : ''}>
            <BackendMint
              isBackendMint={values.isBackendMint}
              onChange={({ target }) => setFieldValue('isBackendMint', target.checked)}
            />
            {dualModeFields.map((type, index) => {
              if (values.isBackendMint && index === 0) return null;
              return (
                <DualModeInput
                  key={type}
                  type={type}
                  hash={{
                    hashKeys: formConfig[`${type}LeavesHashKeys`],
                    hashTypes: formConfig[`${type}LeavesHashTypes`]
                  }}
                  errors={errors}
                  values={values}
                  setFieldValue={setFieldValue}
                  textareaName={`${type}Textarea`}
                  elementsList={`${type}LeavesElements`}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  treeInputType={treeInputType[type]}
                  setTreeInputType={handleTreeInputTypeChange}
                  setValues={setValues}
                />
              );
            })}

            <BasicFields
              errors={errors}
              values={values}
              handleBlur={handleBlur}
              handleChange={handleChange}
              setFieldValue={setFieldValue}
              isBackendMint={values.isBackendMint}
            />

            <SeriesFields
              errors={errors}
              values={values.isBackendMint ? { ...values, eligibilityType: 'qr-code' } : values}
              setFieldValue={setFieldValue}
              handleBlur={handleBlur}
              handleChange={handleChange}
              eligibilityTypes={values.isBackendMint ? ['qr-code'] : eligibilityTypes}
              priceTypes={priceTypes}
              setValues={setValues}
              isBackendMint={values.isBackendMint}
            />

            <MintingOptionsFields
              errors={errors}
              values={values}
              handleBlur={handleBlur}
              handleChange={handleChange}
              setFieldValue={setFieldValue}
              stageTypes={stageTypes}
            />

            <button
              className={classnames(
                utilClasses.hollowButton,
                utilClasses.hollowButtonBlack,
                classes.submitButton
              )}
              disabled={!isValid}
              type="submit"
              data-test="minting-form-submit-btn"
            >
              Upload to blockchain
            </button>
          </form>
        );
      }}
    </Formik>
  );
};
