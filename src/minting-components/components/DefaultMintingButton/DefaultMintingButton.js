import withMergedStyles from 'components/withMergedStyles/withMergedStyles';
import { useGlobalState } from 'globalState';
import { MINTING_STATUSES } from 'minting-components/helpers';
import { useRouter } from 'next/router';
import { useCheckTokensOwnedByUser } from 'queries/nftOwnedByAddress';
import { useEffect, useState } from 'react';
import If from '../If/If';
import classes from './DefaultMintingButton.module.scss';

const getAfterMintButtonStatus = (isEligibleToMint, nftOwned, isBackendMint) => {
  if (isBackendMint) {
    return !isEligibleToMint;
  }
  return !isEligibleToMint && nftOwned > 0;
};

const MintButton = ({ disabled, className, buttonStyle, onClick, btnText }) => {
  return (
    <button disabled={disabled} className={className} style={buttonStyle} onClick={onClick}>
      <span>{btnText}</span>
    </button>
  );
};

const MINT_STYLE = {
  available: '#00B218',
  reserved: '#FF9500',
  unavailable: 'rgba(0, 179, 24, 0.5)',
  done: '#00B218'
};

const AfterMintButton = ({
  TEXTLIST,
  className,
  actionBtnClassName,
  discordLink,
  buttonClassName,
  actionButtonAfterMintVisible,
  playNowLink,
  actionButtonAfterMintText
}) => {
  const router = useRouter();
  const onClickPlayAgain = () => router.reload(); //Mint Identity on click MINT AGAIN button

  const text = 'JOIN THE DISCORD';

  return (
    <div className={className}>
      {actionButtonAfterMintVisible || actionButtonAfterMintText ? (
        <>
          <a
            href={discordLink}
            rel="noreferer noopener noreferrer"
            className={actionBtnClassName}
            target="_blank"
          >
            <span>{actionButtonAfterMintText || text}</span>
          </a>
          <If condition={playNowLink}>
            <a
              href={playNowLink}
              rel="noreferer noopener noreferrer"
              className={actionBtnClassName}
              target="_blank"
            >
              <span>PLAY NOW</span>
            </a>
          </If>
        </>
      ) : (
        <button
          className={buttonClassName}
          style={{ backgroundColor: MINT_STYLE.done }}
          onClick={onClickPlayAgain}
        >
          <span>{TEXTLIST.buttonText.mintAgain}</span>
        </button>
      )}
    </div>
  );
};

const DefaultMintingButton = ({
  TEXTLIST,
  mintingConfig: {
    nftAddress,
    chainId,
    options: { mintType }
  },
  mintingStatus,
  styles,
  eligibilityError,
  isSomeoneCurrentlyMinting,
  choosenToken,
  onClickMintIdentity,
  discordLink,
  playNowLink,
  isEligibleToMint,
  isEligibleForSpecificNumber,
  isMintIdentityButtonDisabled,
  isBackendMint,
  actionButtonAfterMintText,
  isAdditionalButtonVisible,
  amIMinting
}) => {
  const [buttonStyle, setButtonStyle] = useState();
  const [buttonText, setButtonText] = useState();
  const [walletAddress] = useGlobalState('walletAddress');
  const [walletInputMintValues] = useGlobalState('walletInputMintValues');
  const { data: ownedNftCount } = useCheckTokensOwnedByUser(
    nftAddress,
    chainId,
    mintingStatus,
    walletAddress
  );

  const isDisabled =
    !isEligibleToMint ||
    !isEligibleForSpecificNumber ||
    isMintIdentityButtonDisabled ||
    (isBackendMint &&
      (walletInputMintValues?.isWalletAddressInvalid || !walletInputMintValues?.walletAddress)) ||
    (!choosenToken && mintType !== 'sequential');

  const actionButtonAfterMintVisible = getAfterMintButtonStatus(
    isEligibleToMint,
    ownedNftCount,
    isBackendMint
  );

  useEffect(() => {
    let btnStyle;
    let btnText;
    if (amIMinting && (choosenToken?.status === 'reserved' || isSomeoneCurrentlyMinting)) {
      btnStyle = styles.mintButton ? styles.mintButton.available : MINT_STYLE.available;
      btnText = TEXTLIST.buttonText.available;
    } else if (isSomeoneCurrentlyMinting) {
      btnStyle = MINT_STYLE.reserved;
      btnText = TEXTLIST.buttonText.reserved;
    } else {
      btnStyle =
        choosenToken?.status && styles.mintButton
          ? styles.mintButton[choosenToken?.status]
          : MINT_STYLE[choosenToken?.status];
      btnText = TEXTLIST.buttonText[choosenToken?.status] || TEXTLIST.buttonText.available;
    }

    setButtonText(btnText);
    setButtonStyle({ backgroundColor: btnStyle });
  }, [
    isSomeoneCurrentlyMinting,
    choosenToken,
    mintingStatus,
    amIMinting,
    styles.mintButton,
    TEXTLIST.buttonText
  ]);

  // hide when wallet is not connected
  if (!walletAddress && !isBackendMint) return null;
  return mintingStatus === MINTING_STATUSES.DONE ? (
    <>
      <div className={styles.buttons}>
        {/* 'mint again' button */}
        {isAdditionalButtonVisible && isEligibleToMint && (
          <AfterMintButton
            TEXTLIST={TEXTLIST}
            className={styles.buttonsAfterMint}
            buttonClassName={styles.button}
            actionBtnClassName={styles.actionButton}
            discordLink={discordLink}
            playNowLink={playNowLink}
          />
        )}
        <AfterMintButton
          TEXTLIST={TEXTLIST}
          className={styles.buttonsAfterMint}
          buttonClassName={styles.button}
          actionBtnClassName={styles.actionButton}
          discordLink={discordLink}
          actionButtonAfterMintVisible={actionButtonAfterMintVisible}
          playNowLink={playNowLink}
          actionButtonAfterMintText={actionButtonAfterMintText}
        />
      </div>
    </>
  ) : (
    <>
      <MintButton
        disabled={isDisabled}
        onClick={onClickMintIdentity}
        buttonStyle={buttonStyle}
        choosenToken={choosenToken}
        className={styles.button}
        btnText={buttonText}
      />
      {isSomeoneCurrentlyMinting ? (
        <p className={styles.warning}>
          Caution! Another user is currently minting.
          <br /> The price could increase if they mint before you, check the price is still what you
          are willing to pay.
          <br /> You could lose the gas race. Continue at your own risk
        </p>
      ) : eligibilityError ? (
        <p className={styles.eligibilityError}>{eligibilityError}</p>
      ) : null}
    </>
  );
};

export default withMergedStyles(DefaultMintingButton, classes);
