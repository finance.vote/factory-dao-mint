import withMergedStyles from 'components/withMergedStyles/withMergedStyles';
import { useAllSeriesForGroupKey } from '../MintingForms/query';
import classes from './MintingStages.module.scss';

// const MintingStage = ({ stage, styles, live }) => {
//   const { stage: stageNumber } = stage;
//   const text = live ? 'open' : 'closed';
//   return (
//     <div className={classnames(styles.mintingStage, live ? null : styles.inactive)}>
//       <h1>Stage {stageNumber}</h1>
//       <span>Status: {text}</span>
//       {live ? null : <div className={styles.paused}>&times;</div>}
//     </div>
//   );
// };

const MintingStages = ({ groupKey = 'test', styles }) => {
  const { data } = useAllSeriesForGroupKey({ groupKey });
  if (!data?.length) return null;

  const uniqueSeries = data.reduce((prev, curr) => {
    return { ...prev, [curr.startsAt]: 1 };
  }, {});
  let liveStage = Object.keys(uniqueSeries).filter((date) => {
    const isPast = new Date(date) < new Date();
    return isPast;
  }).length;

  return (
    <div className={styles.container}>
      <span>Round {liveStage > 5 ? 5 : liveStage}</span>&nbsp;/ 5
    </div>
  );
};
export default withMergedStyles(MintingStages, classes);
