import axios from 'axios';
import profilePicBtn from 'images/upload.svg';
import { MINTING_STATUSES } from 'minting-components/helpers';
import Image from 'next/image';
import { useOpenseaUrl } from 'queries/openseaQuery';
import { useEffect, useState } from 'react';
import { isMobile } from 'react-device-detect';
import If from '../If/If';
import { useExternalTokenImage } from '../MintingController/query';
import styles from './DefaultFooter.module.scss';

const OpenseaLink = ({ openseaBaseUrl, nftAddress, boughtToken, className }) => {
  if (!openseaBaseUrl) return null;
  return (
    <a
      href={`https://${openseaBaseUrl}/assets/${nftAddress}/${boughtToken}`}
      target="_blank"
      rel="noreferrer"
      className={className}
    >
      <span>view on opensea</span>
    </a>
  );
};

const DefaultFooter = ({ mintingConfig, mintingStatus, boughtToken, tokenUri, customStyles }) => {
  const { key: groupKey, chainId, options, nftAddress } = mintingConfig;
  const {
    media: { external, reveal }
  } = options;

  const { data: externalUrl } = useExternalTokenImage(external, groupKey, boughtToken, tokenUri);
  const { data: openseaBaseUrl } = useOpenseaUrl(chainId);

  const isVisible = mintingStatus === MINTING_STATUSES.DONE && !isMobile;

  const [externalBlobUrl, setExternalBlobUrl] = useState('');

  const wrapperStyles = customStyles?.wrapperClassName ?? styles.wrapper;
  const linkStyles = customStyles?.linkClassName ?? styles.link;
  const internalUrl = `${axios.defaults.baseURL}/minting/profile-pic/${groupKey}/${boughtToken}`;

  useEffect(() => {
    if (external && externalUrl && !externalBlobUrl.length) {
      (async () => {
        const result = await axios.get(externalUrl, { responseType: 'blob' });
        const fileUrl = window.URL.createObjectURL(result.data);
        setExternalBlobUrl(fileUrl);
      })();
    }
  }, [external, externalUrl]);

  const openExternalMediaInNewTab = () => {
    const tab = window.open();
    tab.target = '_blank';
    tab.location.href = externalUrl;
  };

  return isVisible ? (
    <div className={wrapperStyles}>
      <OpenseaLink
        openseaBaseUrl={openseaBaseUrl}
        className={linkStyles}
        boughtToken={boughtToken}
        nftAddress={nftAddress}
      />
      <If condition={reveal}>
        <a
          href={external ? externalBlobUrl + '' : internalUrl}
          target="_blank"
          className={linkStyles}
          download={`${groupKey}-${boughtToken}`}
          rel="noreferrer"
          onClick={external ? openExternalMediaInNewTab : null}
        >
          <Image src={profilePicBtn} alt="profile pic" />
          <span>profile pic</span>
        </a>
      </If>
    </div>
  ) : null;
};

export default DefaultFooter;
