import { toChecksumAddress } from 'evm-chain-scripts';
import { useGlobalState } from 'globalState';
import loaderImg from 'images/loader.svg';
import debounce from 'lodash.debounce';
import Image from 'next/image';
import { useCallback, useEffect, useState } from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import { useEnsDomainAddress } from '../MintingController/query';

const WalletInput = ({ containerClassName, label }) => {
  const [, setWalletInputMintValues] = useGlobalState('walletInputMintValues');
  const [walletAddress] = useGlobalState('walletAddress');
  const [walletInputValue, setWalletInputValue] = useState('');
  const [debouncedEnsDomain, setDebouncedEnsDomain] = useState('');
  const [error, setError] = useState('');
  const [isEns, setIsEns] = useState(false);
  const { data: addressFromEns, isFetching: isAddressFromEnsFetching } =
    useEnsDomainAddress(debouncedEnsDomain);

  useEffect(() => {
    if (isEns) {
      let error = '';
      if (addressFromEns) {
        setWalletInputValue(addressFromEns);
      } else {
        error = 'Invalid wallet address';
      }

      setWalletInputMintValues({
        walletAddress: addressFromEns || '',
        isWalletAddressInvalid: !!error
      });
      setError(error);
    }
  }, [addressFromEns, isEns]);

  useEffect(() => {
    return () => {
      debouncedSave.cancel();
    };
  }, []);

  useEffect(() => {
    setWalletInputValue(walletAddress);
    setWalletInputMintValues({
      walletAddress,
      isWalletAddressInvalid: false
    });
  }, [walletAddress]);

  const onChangeDebouncedEnsDomain = (ens) => {
    setDebouncedEnsDomain(ens);
  };

  const debouncedSave = useCallback(
    debounce((ens) => onChangeDebouncedEnsDomain(ens), 500),
    []
  );

  const onChange = async (value) => {
    setWalletInputValue(value);

    //validation
    if (!value) {
      setError('Required');
      setWalletInputMintValues({
        walletAddress: '',
        isWalletAddressInvalid: true
      });
    } else {
      try {
        toChecksumAddress(value);
        setWalletInputMintValues({
          walletAddress: value,
          isWalletAddressInvalid: false
        });
        setError('');
        setIsEns(false);
      } catch (err) {
        setIsEns(true);
        debouncedSave(value);
      }
    }
  };

  const walletLabel = label || 'Paste your wallet address';
  return (
    <form className={containerClassName}>
      <span>{walletLabel}</span>
      <div>
        <input
          type="text"
          value={walletInputValue}
          placeholder="Paste wallet address here"
          onChange={(e) => onChange(e.target.value)}
          disabled={isAddressFromEnsFetching}
        />
        {isAddressFromEnsFetching ? <Image src={loaderImg} alt="loader" /> : null}
      </div>
      {!isAddressFromEnsFetching && error && <span style={{ color: 'red' }}>{error}</span>}
    </form>
  );
};

export default WalletInput;
