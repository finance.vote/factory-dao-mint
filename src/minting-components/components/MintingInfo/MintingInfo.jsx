const ErrorMessage = ({ className, color, error }) => {
  return (
    <p className={className} style={{ color: color }}>
      {error}
    </p>
  );
};

const InfoMessage = ({ choosenToken, className, color, TEXTLIST }) => {
  return choosenToken ? (
    <p className={className} style={{ color: color }}>
      {TEXTLIST.status[choosenToken?.status]}
    </p>
  ) : null;
};

const MintingInfo = ({
  isVisible,
  choosenToken,
  error,
  highlightedIndicators,
  className,
  TEXTLIST
}) => {
  if (!isVisible) return null;
  return error ? (
    <ErrorMessage
      color={highlightedIndicators['unavailable']?.status}
      className={className}
      error={error}
    />
  ) : (
    <InfoMessage
      color={highlightedIndicators[choosenToken?.status]?.status}
      choosenToken={choosenToken}
      className={className}
      TEXTLIST={TEXTLIST}
    />
  );
};

export default MintingInfo;
