import { ethInstance, fromEthDate, fromWei } from 'evm-chain-scripts';
import { shortenWalletAddress } from 'helpers';
import expandSvg from 'images/expand.svg';
import moment from 'moment';
import Image from 'next/image';
import { useMintedEvents } from 'queries/mintedEventsQuery';
import { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
import networks from '../../../helpers/networks.json';
import loaderImg from '../../images/loader.svg';

import { useMintConfigQuery } from '../MintingController/query';
import styles from './MintTable.module.scss';

const DateField = ({ blockNumber, chainId, setLoadingStatus }) => {
  const [date, setDate] = useState('');

  useEffect(() => {
    let isComponentStillAlive = true;
    (async () => {
      const block = await ethInstance.getBlock(blockNumber, chainId);
      if (isComponentStillAlive) {
        setDate(fromEthDate(block.timestamp));
        setLoadingStatus(false);
      }
    })();
    return () => (isComponentStillAlive = false);
  }, []);
  return date ? <td>{moment(date).format('DD/MM/YYYY')}</td> : <td>N/A</td>;
};

const PriceField = ({ tx, currency, chainId, setLoadingStatus }) => {
  const [price, setPrice] = useState('');
  useEffect(() => {
    let isComponentStillAlive = true;
    (async () => {
      const eth = await ethInstance.readWeb3(chainId).getTransaction(tx);
      const price = fromWei(eth.value.toString());
      if (isComponentStillAlive) {
        setPrice(price);
        setLoadingStatus(false);
      }
    })();
    return () => (isComponentStillAlive = false);
  }, []);
  return (
    <td className={styles.price}>
      {price} ${currency}
    </td>
  );
};

const MintTable = ({ groupKey = 'ttest' }) => {
  const [activePage, setActivePage] = useState(1);

  const { data: mintingConfig } = useMintConfigQuery(groupKey);
  const { data: mintedEvents, isLoading: isTableDataLoading } = useMintedEvents(
    mintingConfig?.chainId,
    mintingConfig?.nftAddress,
    mintingConfig?.nftBlockNumber
  );
  const [isPriceLoading, setIsPriceLoading] = useState(true);
  const [isDateLoading, setIsDateLoading] = useState(true);
  const explorer = (str, type = 'address') => {
    return `${networks[mintingConfig?.chainId ?? 1]?.explorer}/${type}/${str}`;
  };
  const onPaginationChange = (pagination) => {
    setIsPriceLoading(true);
    setIsDateLoading(true);
    setActivePage(pagination.selected + 1);
  };

  return (
    <div className={styles.mintsTable}>
      <span className={styles.title}>Previous mints</span>
      {!isTableDataLoading ? (
        <>
          <div className={styles.tableContainer}>
            <table style={{ display: isPriceLoading || isDateLoading ? 'none' : 'table' }}>
              <thead>
                <tr className={styles.head}>
                  <td>ID NO.</td>
                  <td>MINTED BY</td>
                  <td>PRICE</td>
                  <td>DATE</td>
                </tr>
              </thead>
              <tbody>
                {mintedEvents?.length ? (
                  mintedEvents.slice((activePage - 1) * 4, activePage * 4).map((event) => {
                    return (
                      <tr key={event._id}>
                        <td className={styles.firstCol}>{event.data?.tokenId}</td>
                        <td>
                          <div>
                            {shortenWalletAddress(event.data?.to)}{' '}
                            <a
                              className={styles.link}
                              href={explorer(event.data?.to)}
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              <Image src={expandSvg} alt="expand" />
                            </a>
                          </div>
                        </td>
                        <PriceField
                          currency={mintingConfig?.currencySymbol}
                          tx={event.transactionHash}
                          chainId={mintingConfig?.chainId}
                          setLoadingStatus={setIsPriceLoading}
                        />
                        <DateField
                          blockNumber={event.blockNumber}
                          chainId={mintingConfig?.chainId}
                          setLoadingStatus={setIsDateLoading}
                        />
                      </tr>
                    );
                  })
                ) : (
                  <tr>
                    <td className={styles.firstCol} colSpan="5">
                      <p>There aren&apos;t any previous mints here yet!</p>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
            <Image
              style={{
                display: isPriceLoading || isDateLoading ? 'block' : 'none',
                margin: '45px auto 46px'
              }}
              src={loaderImg}
              alt="Table loader"
            />
          </div>
          {mintedEvents?.length ? (
            <ReactPaginate
              forcePage={activePage - 1}
              pageCount={Math.ceil(mintedEvents.length / 4)}
              pageRangeDisplayed={1}
              marginPagesDisplayed={1}
              onPageChange={onPaginationChange}
              breakLabel="/"
              previousLabel="< previous"
              nextLabel="next >"
              containerClassName={styles.paginationContainer}
              activeClassName={styles.paginationActivePage}
              pageLinkClassName={styles.paginationLink}
              disabledClassName={styles.pageDisabled}
              nextClassName={styles.nextPrevClassName}
              previousClassName={styles.nextPrevClassName}
            />
          ) : null}
        </>
      ) : (
        <Image src={loaderImg} alt="Table loader" />
      )}
    </div>
  );
};

export default MintTable;
