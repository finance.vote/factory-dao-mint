import { useMemo } from 'react';

export const useMarkers = (tooltips, imageContainerRef, originalImageWidth) => {
  let domImageScale = 1;

  // The wrapper should have the same size as
  // the image.
  if (imageContainerRef.current) {
    const imageBox = imageContainerRef.current.getBoundingClientRect();
    domImageScale = imageBox.width / originalImageWidth;
  }

  const computeMarkers = () =>
    tooltips.map((tooltip) => {
      const titleTransformDirection = tooltip.side === 'left' ? '-100%' : '1%';
      return {
        ...tooltip,
        x: `${tooltip.x * domImageScale}px`,
        y: `${tooltip.y * domImageScale}px`,
        titleWidth: `${(tooltip.titleWidth || 167) * domImageScale}px`,
        titleTransform: `translate(calc(${titleTransformDirection} + 10px), 0)`,
        titleTextAlign: tooltip.side
      };
    });

  const markers = useMemo(computeMarkers, [tooltips, domImageScale, originalImageWidth]);

  return {
    markers
  };
};
