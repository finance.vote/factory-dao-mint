import Image from 'next/image';
import React, { useEffect, useRef, useState } from 'react';
import ArrowLeftIcon from '../../images/arrow-left.svg';
import FlipIcon from '../../images/flip.svg';
import styles from './NftGuide.module.scss';
import { useMarkers } from './useMarkers';

/**
 * "tooltips" prop should be an array with
 * the following structure:
 *   front: [
 *     {
 *       title: 'Toolitp / Marker Title',
 *       description: ['Detailed description', 'Line 2', 'Etc'],
 *       x: 10,
 *       y: 200,
 *       side: 'left'
 *     },
 *   ],
 *   back: [
 *     { ...same as above }
 *   ]
 */
const NftGuide = ({ tooltips, nftImageFront, nftImageBack, nftImageWidth }) => {
  const [mobileMode, setMobileMode] = useState(false);
  const [shouldDisplayMarkers, setShouldDisplayMarkers] = useState(true);
  const [visibleDescription, setVisibleDescription] = useState();
  const [visibleNftFace, setVisibleNftFace] = useState('front');
  const nftImageContainerRef = useRef(null);
  const { markers } = useMarkers(tooltips[visibleNftFace], nftImageContainerRef, nftImageWidth);

  useEffect(() => {
    const mobileQuery = window.matchMedia('(max-width: 768px)');
    const handleQueryChange = () => {
      if (mobileQuery.matches) {
        setMobileMode(true);
        if (!visibleDescription && markers[0]) {
          setVisibleDescription(markers[0]);
        }
      } else {
        setMobileMode(false);
        setVisibleDescription(markers[0]);
      }
    };
    // Safari < 14 hack
    if (mobileQuery.addListener) {
      mobileQuery.addListener(handleQueryChange);
    } else {
      mobileQuery.addEventListener && mobileQuery.addEventListener('change', handleQueryChange);
    }
    handleQueryChange();
    return () => {
      // Safari < 14 hack
      if (mobileQuery.removeListener) {
        mobileQuery.removeListener(handleQueryChange);
      } else {
        mobileQuery.removeEventListener('change', handleQueryChange);
      }
    };
  }, []);

  const Description = visibleDescription ? (
    <div className={styles.box}>
      <p className={styles.title}>{visibleDescription.title}</p>
      {visibleDescription.description.map((line, i) => (
        <p key={i} className={styles.description}>
          {line}
        </p>
      ))}
    </div>
  ) : null;

  const imageClasses = {
    front: `${styles.image} ${visibleNftFace === 'back' ? styles.rotated : ''}`,
    back: `${styles.image} ${visibleNftFace === 'front' ? styles.rotated : ''}`
  };

  const markerClasses = (marker, index) => {
    let isMarkerDescriptionVisible = false;
    if (mobileMode) {
      if (marker?.title === visibleDescription?.title) {
        isMarkerDescriptionVisible = true;
      }
    }
    if (index === 0) {
      return `${styles.marker} ${styles.activeMarker}`;
    }
    const classes = `
      ${shouldDisplayMarkers ? styles.marker : styles.hidden}
      ${isMarkerDescriptionVisible ? styles.forceMarkerTitleVisible : null}
    `;
    return classes;
  };
  const clearFocus = () => {
    const el = document.getElementsByClassName(styles.activeMarker);
    if (el[0]) return (el[0].classList = styles.marker);
  };

  const highlightFirstMarker = (idx) => {
    return idx === 0
      ? (document.getElementById(`m-${idx}`).classList = `${styles.marker} ${styles.activeMarker}`)
      : null;
  };

  const onRotateNft = () => {
    setShouldDisplayMarkers(false);
    setVisibleNftFace(visibleNftFace === 'front' ? 'back' : 'front');
    setTimeout(() => {
      setShouldDisplayMarkers(true);
      highlightFirstMarker(0);
    }, 350);
  };

  useEffect(() => {
    setVisibleDescription(markers[0]);
  }, [markers]);

  const triggerTooltipShow = (marker) => {
    clearFocus();
    return setVisibleDescription(marker);
  };

  const onPaginatedTooltipChange = (direction) => {
    if (!visibleDescription || !markers) {
      return;
    }
    clearFocus();
    const currMarkerShowingIndex = markers.findIndex(
      (marker) => marker.title === visibleDescription.title
    );
    let nextIndex =
      direction === 'previous' ? currMarkerShowingIndex - 1 : currMarkerShowingIndex + 1;
    if (nextIndex < 0) {
      nextIndex = markers.length - 1;
    }
    if (nextIndex >= markers.length) {
      nextIndex = 0;
    }
    if (nextIndex === 0) {
      highlightFirstMarker(0);
    }
    setVisibleDescription(markers[nextIndex]);
  };

  return (
    <div className={styles.componentWrapper}>
      <div className={styles.containerInfo}>
        <h3>SHIBAKU ATTRIBUTES</h3>
        <div className={styles.texts}>
          <p>
            Shibakus are metaverse keys. Each feature of the Shibaku grants the holder special
            powers.
          </p>
          <p>
            The configuration of your attributes, regardless of rarity, can become valuable over
            time.
          </p>
          <p>Every Shibaku is important. Every Shibaku is necessary. Some will become gods.</p>
        </div>
      </div>
      <div className={styles.container}>
        <div className={styles.descriptionContainer}>{Description}</div>
        <div className={styles.nftImageContainer}>
          <div className={styles.mapContainer} ref={nftImageContainerRef}>
            {markers.map((marker, i) => (
              <button
                key={i}
                id={`m-${i}`}
                className={markerClasses(marker, i)}
                style={{ top: marker.y, left: marker.x }}
                onMouseOver={() => triggerTooltipShow(marker)}
                onFocus={() => triggerTooltipShow(marker)}
              >
                <span
                  className={styles.title}
                  style={{
                    width: marker.titleWidth,
                    transform: marker.titleTransform
                  }}
                >
                  <span className={styles.text} style={{ textAlign: marker.titleTextAlign }}>
                    {marker.title}
                  </span>
                </span>
              </button>
            ))}
            <Image alt="NFT Front" className={imageClasses.front} src={nftImageFront} />
            <Image alt="NFT Back" className={imageClasses.back} src={nftImageBack} />
          </div>
        </div>
        <button className={styles.flipButton} onClick={onRotateNft}>
          <span className={styles.iconContainer}>
            <FlipIcon />
          </span>
          <p className={styles.text}>flip</p>
        </button>
        <Pagination onClick={onPaginatedTooltipChange} />
      </div>
    </div>
  );
};

const Pagination = ({ onClick }) => {
  return (
    <div className={styles.paginationContainer}>
      <button className={styles.button} onClick={() => onClick('previous')}>
        <ArrowLeftIcon />
      </button>
      <button className={styles.button} onClick={() => onClick('next')}>
        <ArrowLeftIcon />
      </button>
    </div>
  );
};

export default NftGuide;
