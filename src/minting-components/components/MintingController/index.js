import PageLoading from 'components/Ui/PageLoading/PageLoading.component';
import MerkleIdentity from 'contracts/MerkleIdentity.json';
import { useGlobalState } from 'globalState';
import randomizeBtn from 'images/randomize.svg';
import randomizeBtnDisabled from 'images/randomize_disabled.svg';
import { useRouter } from 'next/router';
import MintingPage from '../MintingPage';
import { useMintConfigQuery, useSeriesQuery } from './query';

const MintingController = ({
  groupKey = null,
  customStyles,
  sliderIcon,
  customTexts,
  discordLink,
  playNowLink,
  SequentialMintComponent,
  prefillOnLoad = false, //TODO: decide if should be in db model
  customWalletProviders,
  walletInputLabel,
  isProgressBarVisible,
  tooltipContent,
  tooltipIcon,
  tooltipContentClass,
  setGlobalChoosenSeries = () => null,
  FooterComponent = () => null,
  MintingButtonComponent = () => null,
  CustomSuccessMint = () => null,
  StageRangesComponent = () => null,
  TopPlaceComponent = () => null,
  isSuccessMintPopup = false
}) => {
  const router = useRouter();
  const code = router.query['code'];

  const [walletAddress] = useGlobalState('walletAddress');
  const { data: mint, isLoading } = useMintConfigQuery(groupKey);
  const { data: series, isLoading: seriesLoading } = useSeriesQuery(
    groupKey,
    walletAddress,
    code,
    mint?.options?.isBackendMint
  );

  if (isLoading || seriesLoading || !(mint && Object.keys(mint).length)) {
    return <PageLoading />;
  } else {
    return (
      <MintingPage
        isSuccessMintPopup={isSuccessMintPopup}
        merkleContract={MerkleIdentity}
        mintingConfig={mint}
        seriesConfig={series}
        randomizeBtn={randomizeBtn}
        randomizeBtnDisabled={randomizeBtnDisabled}
        customStyles={customStyles}
        sliderIcon={sliderIcon}
        customTexts={customTexts}
        discordLink={discordLink}
        playNowLink={playNowLink}
        SequentialMintComponent={SequentialMintComponent}
        prefillOnLoad={prefillOnLoad}
        customWalletProviders={customWalletProviders}
        setGlobalChoosenSeries={setGlobalChoosenSeries}
        FooterComponent={(props) => <FooterComponent {...props} mintingConfig={mint} />}
        MintingButtonComponent={(props) => (
          <MintingButtonComponent {...props} mintingConfig={mint} />
        )}
        CustomSuccessMint={(props) => <CustomSuccessMint {...props} mintingConfig={mint} />}
        StageRangesComponent={StageRangesComponent}
        TopPlaceComponent={TopPlaceComponent}
        code={code}
        walletInputLabel={walletInputLabel}
        isProgressBarVisible={isProgressBarVisible}
        tooltipContent={tooltipContent}
        tooltipIcon={tooltipIcon}
        tooltipContentClass={tooltipContentClass}
      />
    );
  }
};

export default MintingController;
