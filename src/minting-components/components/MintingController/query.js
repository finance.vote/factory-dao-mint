import axios from 'axios';
import { getWalletAddressFromENS } from 'evm-chain-scripts';
import { getExternalTokenImage } from 'minting-components/helpers';
import { useQuery } from 'react-query';
import { toast } from 'react-toastify';

const fetchMintConfig = async (group) => {
  try {
    const { data: config } = await axios.get(`/minting/config/${group}`);
    return config;
  } catch (error) {
    console.error('Could not fetch mint config');
  }
};

const fetchAvailableSeries = async (groupKey, address, code, isBackendMint) => {
  const defaultSeries = {
    id: '',
    eligibilityAddress: '',
    eligibilityIndex: '',
    eligibilityType: '',
    merkleTreeIndex: '',
    price: '',
    priceType: ''
  };
  try {
    const params = code ? { code } : {};
    const { data: series } = await axios.get(`/minting/${groupKey}/check/${address}`, {
      params
    });

    if (series.success === false) {
      let isCodeInvalid = false;
      if (isBackendMint) {
        isCodeInvalid = true;
        toast.error('Code for this mint is used or invalid', {
          position: 'top-center'
        });
      }
      return [{ ...defaultSeries, isCodeInvalid }];
    }
    return series;
  } catch (error) {
    console.log(error);
    return [defaultSeries];
  }
};

const fetchEnsDomainAddress = async (value) => {
  try {
    const addressFromEns = await getWalletAddressFromENS(value);
    return addressFromEns;
  } catch (error) {
    console.error('Could not fetch address from ENS domain');
    return;
  }
};

export const useSeriesQuery = (groupKey, address, code, isBackendMint) =>
  useQuery(
    ['SERIES', groupKey, address, code, isBackendMint],
    async () => await fetchAvailableSeries(groupKey, address, code, isBackendMint),
    {
      enabled: !!((groupKey && address) || (groupKey && code)),
      refetchOnWindowFocus: false,
      initialData: [
        {
          id: '',
          eligibilityAddress: '',
          eligibilityIndex: '',
          eligibilityType: '',
          merkleTreeIndex: '',
          price: '',
          priceType: ''
        }
      ]
    }
  );

export const useMintConfigQuery = (groupKey) =>
  useQuery(['CONFIG', groupKey], async () => await fetchMintConfig(groupKey), {
    enabled: !!groupKey,
    refetchOnWindowFocus: false,
    initialData: {}
  });

export const useExternalTokenImage = (external, groupKey, tokenId, uri) =>
  useQuery(
    ['EXTERNAL_TOKEN_IMG', external, groupKey, tokenId, uri],
    async () => await getExternalTokenImage(groupKey, tokenId, uri, true, new AbortController()),
    {
      enabled: !!(external && groupKey && groupKey && uri),
      refetchOnWindowFocus: false
    }
  );

export const useEnsDomainAddress = (value) =>
  useQuery(['ENS_DOMAIN_ADDRESS', value], async () => await fetchEnsDomainAddress(value), {
    enabled: !!value,
    refetchOnWindowFocus: false,
    initialData: ''
  });
