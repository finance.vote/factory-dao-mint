import classes from 'pages/CreateMint/CreateMint.module.scss';

export const GeneratedData = ({ generatedData }) => {
  return (
    <div className={classes.generatedTableContainer}>
      <table className={classes.generatedTable}>
        <tbody>
          <tr>
            <th>Generated IPFS hash:</th>
            <td>{generatedData.ipfsHash}</td>
          </tr>
          <tr>
            <th>Generated Mint ObjectId:</th>
            <td>{generatedData.mintId}</td>
          </tr>
          <tr>
            <th>Address hash:</th>
            <td>{generatedData.addressHash}</td>
          </tr>
          <tr>
            <th>Metadata hash:</th>
            <td>{generatedData.metadataRoot}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
