import axios from 'axios';

export const MINTING_STATUSES = {
  NONE: 'none',
  PROCESSING: 'processing',
  ERROR: 'error',
  DONE: 'done'
};

export const MINT_TYPES = {
  SELECTABLE: 'selectable',
  SEQUENTIAL: 'sequential'
};

export const PRICE_TYPES = {
  FIXED: 'fixed',
  SPEEDBUMP: 'speedbump',
  AMALU: 'amalu',
  FLEXIBLE: 'flexible'
};

export const TEXTLIST = {
  mobileHeader: 'Test',
  symbol: 'TEST',
  successTitle: `It's yours!`,
  successMessage: `%SYMBOL% #%TOKEN_ID% now belongs to you`,
  placeholderText: 'Enter a number',
  numberInputLabel: '',
  status: {
    available: 'AVAILABLE',
    reserved: 'MINT IN PROGRESS',
    unavailable: 'ALREADY MINTED - ENTER ANOTHER NUMBER'
  },
  buttonText: {
    available: 'MINT',
    reserved: 'TAKE A RISK',
    unavailable: 'MINT',
    mintAgain: 'MINT AGAIN'
  }
};

export const highlightIndicators = {
  available: {
    background: '#00b318',
    shadow: '0px 0px 10px 5px #25B038',
    status: '#00b318'
  },
  reserved: {
    background: '#FF9500',
    shadow: '0px 0px 10px 5px #FF9500',
    status: '#FF9500'
  },
  unavailable: {
    background: '#FF3333',
    shadow: '0px 0px 10px 5px #FF3333',
    status: '#FF3333'
  }
};

export const delayer = (ms) => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), ms);
  });
};
export const getExternalTokenImage = async (
  group,
  token,
  tokenUri,
  externalService,
  abortController
) => {
  if (group === 'ttest' || group === 'trib3') {
    return `${location.protocol}//${location.host}/NFT-TICKET.mp4`;
  }
  const prefix = 'https://factorydao.infura-ipfs.io/ipfs/';
  const internalServiceLink = `${axios.defaults.baseURL}/minting/media/${group}/${token}`;
  try {
    if (!externalService) return internalServiceLink;
    if (!tokenUri.includes('http')) {
      const _uri = tokenUri.replace('ipfs://', '');
      const {
        data: { image, image_url, animation_url }
      } = await axios.get(`${prefix}${_uri}`, { signal: abortController.signal });
      const link = animation_url ?? image ?? image_url;
      return link?.replace('ipfs://', prefix);
    } else {
      const { data } = await axios.get(tokenUri, { signal: abortController.signal });
      const { image, image_url, animation_url } = data;
      return animation_url ?? image ?? image_url ?? tokenUri;
    }
  } catch (error) {
    if (!tokenUri.includes('http')) {
      const uri = `${prefix}${tokenUri}`;
      const {
        data: { image, image_url, animation_url }
      } = await axios.get(uri, { signal: abortController.signal });
      const link = animation_url ?? image ?? image_url;
      return link?.replace('ipfs://', prefix) ?? image_url ?? internalServiceLink;
    }
    if (!abortController.signal.aborted) {
      console.log(error);
    }
  }
};
export const getNextTokenImage = async (group, token, external, abortController) => {
  //Assume its not hosted in ipfs - performance reasons for showNext;
  const internalServiceLink = `${axios.defaults.baseURL}/minting/media/${group}/${token}`;
  if (!external) return internalServiceLink;
  try {
    const { data } = await axios.get(internalServiceLink, { signal: abortController.signal });
    return await getExternalTokenImage(group, token, data, true, abortController);
  } catch (error) {
    if (!abortController.signal.aborted) {
      console.log(error);
    }
  }
};
export const getRevealedTokenMediaUrl = async (tokenId, group, seriesId) => {
  const {
    data: { uri }
  } = await axios.get(`/minting/metadata-proof/${group}/${seriesId}/${tokenId}`);
  return await getExternalTokenImage(group, tokenId, uri, true);
};

export const reserveToken = async ({ value: tokenId }, address, tx, groupKey) => {
  return await axios.post(`/minting/${groupKey}/reserve/${tokenId}`, tx);
};

export const getTokensRange = (series) => {
  let _min;
  let _max;
  if (series.length > 1) {
    const { min, max } = series.reduce(
      ({ min, max }, curr) => {
        const minimum = min > curr.range.start ? curr.range.start : min;
        const maximum = max < curr.range.end ? curr.range.end : max;
        return { min: minimum, max: maximum };
      },
      { min: 1, max: 0 }
    );
    _min = min;
    _max = max;
  } else {
    _min = series[0].range?.start;
    _max = series[0].range?.end;
  }
  return { start: _min, end: _max };
};

export const getMediaMimetype = async (url) => {
  if (!url) return 'image';
  try {
    const { headers, data } = await axios.get(url);
    const mimetype = headers['content-type'];
    return {
      mimetype: mimetype.includes('image') ? 'image' : 'video',
      placeholderVideoId: data.placeholderVideoId
    };
  } catch (err) {
    console.log(err);
    return { mimetype: 'image' };
  }
};

//FOR TOKEN STATUSES TESTING PURPOSES

// useEffect(() => {
//   (async () => {
//     const reserved = {
//       data: JSON.stringify({
//         data: { status: 'reserved', value: 3 },
//         status: 'reserved',
//         value: 3
//       })
//     };
//     await delayer(10000);
//     await handleStatusUpdate(reserved);
//     await delayer(5000);
//     const unavailable = {
//       data: JSON.stringify({
//         data: { status: 'unavailable', value: 3 },
//         status: 'unavailable',
//         value: 3
//       })
//     };
//     await handleStatusUpdate(unavailable);
//   })();
// }, []);
