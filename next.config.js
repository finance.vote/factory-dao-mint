const nextConfig = {
  output: 'standalone',
  reactStrictMode: true,
  swcMinify: true,
  images: {
    //todo make it unified
    remotePatterns: [
      {
        protocol: 'https',
        hostname: '**',
        pathname: '**'
      }
    ]
  },
  webpack: (config, { isServer }) => {
    if (!isServer) {
      config.resolve.fallback = {
        net: false,
        tls: false,
        fs: false
      };
    }

    return config;
  }
};

const withTM = require('next-transpile-modules')(['evm-chain-scripts']);

module.exports = withTM(nextConfig);
