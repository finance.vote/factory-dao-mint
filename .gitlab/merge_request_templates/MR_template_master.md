## MR acceptance checklist

This checklist encourages us to confirm any changes have been analyzed to reduce risks in quality, performance, reliability, security, and maintainability.\
Before submitting this MR into the main branch, please make sure:

* [ ] Your code builds clean without any errors or warnings
* [ ] MR title contains a feature name
* [ ] You've checked 'Squash commits' box
* [ ] You've commented on the most complex parts of the code
* [ ] You've evaluated the [MR acceptance list](https://neti-soft.atlassian.net/l/cp/oFQZYEt0) for this MR.



## What does this MR do?

_Describe in detail what your merge request does and why._\
_What has been done/edited/deleted?_

| **Task number** |
| :---: |
| **FV-000** | 


## Where should the reviewer start? 

_These are strongly recommended to assist reviewers and reduce the time to merge your change._


## How to set up and validate locally (optional)

_Numbered steps to set up and validate the change are strongly suggested._

<!-- 
Example below:
 
1. Enable the invite modal
   ```ruby
   Feature.enable(:invite_members_group_modal)
   ```
2. In rails console enable the experiment fully
   ```ruby
   Feature.enable(:member_areas_of_focus)
   ```
3. Visit any group or project member pages such as `http://127.0.0.1:3000/groups/flightjs/-/group_members`
4. Click the `invite members` button.
-->





