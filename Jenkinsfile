pipeline {
  environment {
    registryUrl =  "${env.DOCKER_REGISTRY_URL}"
    registry = "${env.DOCKER_REGISTRY_NAME}" 
    tag =  "latest"//"${env.GIT_TAG_NAME}"
    registryCredential = 'registry-netisoft'
    serviceName = "${env.SERVICE_NAME}" 
    webhookUrl = "${env.DEPLOY_WEBHOOK_URL}"
    dockerImage = ''
  }

  agent none

  post {
    failure {
      updateGitlabCommitStatus name: 'build', state: 'failed'
    }
    success {
      updateGitlabCommitStatus name: 'build', state: 'success'
    }
  }

  stages {
    stage('Notify Gitlab') {
      steps {
        echo 'Notify GitLab'
        updateGitlabCommitStatus name: 'build', state: 'pending'
      }
    }

    // stage('Git checkout') {
    //   agent any
    //   steps {
    //     script {
    //       checkout([$class: 'GitSCM', branches: [[name: "develop"]],
    //         userRemoteConfigs: [[url: "${env.gitlabSourceRepoHttpUrl}",
    //                               credentialsId: "2d35f615-f265-4b3d-9d60-16d79f6fa3b7"]]
    //       ])
    //     }
    //   }
    // }

    stage('Install dependencies') { 
      agent {
        docker {
          image 'netisoft/node:18.12.0'  
        }    
      }
      steps {
        script{
          dir("${env.WORKSPACE}"){
            sh "rm -rf node_modules/.babel-plugin-polyfill-corejs2-*"
            sh "npm install"
          }
        }
      }
    }

    stage('Build frontend') {
      agent {
        docker {
            image 'netisoft/node:18.12.0'
        }
      }
      steps {
        script{
          dir("${env.WORKSPACE}"){
              sh "npm run build"
          }
        }
      }
    }

    stage("Build docker image") {
      agent any
      steps {
        script {
          println "  \n - registry url: ${registryUrl} \n - registry (image) name: ${registry} \n - webhook url: ${webhookUrl} \n - service name: ${serviceName} \n - image tag: ${tag}"
          dir("${env.WORKSPACE}"){
            dockerImage = docker.build registry + ":$tag"
          }
        }
      }
    }

    stage("Pushing image to registry") {
      agent any
      when {
        expression {env.GIT_BRANCH ==~ /(origin\/(test))/}
      }
      steps {
        script {
          docker.withRegistry(registryUrl, registryCredential ) {
            dockerImage.push()
          }
        }
      }
    }

    stage("Deploy to swarm environment") {
      when {
        expression {env.GIT_BRANCH ==~ /(origin\/(test))/}
      }
      agent any
      steps {
        script {
          def response = httpRequest httpMode: 'POST', url: webhookUrl
          if(response.status >= 400){
            throw new Exception("Unable to deploy $serviceName. Response code: ${response.status}")
          }
        }
      }
    }

    // stage('Deploy to IPFS') {
    //   when {
    //     expression {env.GIT_BRANCH ==~ /(origin\/(test))/}
    //   }
    //   agent {
    //     docker {
    //         image 'netisoft/node:16.15.0'
    //     }    
    //   }
    //   steps {
    //     script {
    //       dir("${env.WORKSPACE}"){
    //           sh "npm run deploy"
    //       }
    //     }
    //   }
    // }

    // stage("Deploy to swarm environment") {
    //   agent any
    //   when {
    //     expression {env.GIT_ACTION_TYPE != 'tag'}
    //   }
    //   steps {
    //     script {
    //       def body = "{ \"push_data\": { \"tag\": \"$tag\" },\"repository\": { \"repo_name\": \"$registry\" } }"
    //       def response = httpRequest contentType: 'APPLICATION_JSON', httpMode: 'POST', requestBody: body, url: webhookUrl
    //       if(response.status >= 400){
    //         throw new Exception("Unable to deploy $serviceName. Response code: ${response.status}")
    //       }
    //     }
    //   }
    // }
    
  }
}
