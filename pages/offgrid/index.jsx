import OffGridPage from 'pages/OffGrid';

export default function OffGrid() {
  return <OffGridPage groupKey="offgrid" />;
}
