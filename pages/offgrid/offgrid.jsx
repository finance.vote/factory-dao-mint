export async function getServerSideProps(context) {
  const code = context.query.code;
  return {
    redirect: {
      destination: `/offgrid?code=${code}`,
      permanent: false
    }
  };
}

export default function OffGrid() {
  return null;
}
