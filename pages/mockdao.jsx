import MockDao from 'pages/MockDao';

export default function MockDaoPage() {
  return <MockDao groupKey="mockdao" />;
}
