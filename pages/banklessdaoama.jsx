import BanklessDaoAMA from 'pages/BanklessAMA/DAO';

export default function BanklessDaoAMAComponent() {
  return <BanklessDaoAMA groupKey="banklessdaoama" />;
}
