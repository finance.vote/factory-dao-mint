import DefaultFooter from 'minting-components/components/DefaultFooter';
import DefaultMintingButton from 'minting-components/components/DefaultMintingButton';
import MintingController from 'minting-components/components/MintingController';
import MintingStages from 'minting-components/components/MintingStages';
import { useRouter } from 'next/router';

const DynamicTestMint = () => {
  const router = useRouter();
  const { mint } = router.query;

  return (
    <>
      <MintingController
        groupKey={mint}
        FooterComponent={DefaultFooter}
        MintingButtonComponent={DefaultMintingButton}
      />
      {mint?.options?.stages && mint?.options?.stages !== 'none' ? (
        <MintingStages groupKey={mint} />
      ) : null}
    </>
  );
};

export default DynamicTestMint;
