import MemecoinDetective from 'pages/MemecoinDetective/MemecoinDetective';

export default function MemecoinDetectivePage() {
  return <MemecoinDetective groupKey="memecoin-detective" />;
}
