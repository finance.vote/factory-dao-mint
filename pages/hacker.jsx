import EthPragueMain from 'components/EthPrague/EthPragueMain';
import HowItWorks from 'components/EthPrague/HowItWorksHacker';

const HackerPage = () => {
  return (
    <EthPragueMain
      title={
        <>
          ETHPrague
          <br />
          Hacker ID
        </>
      }
      description="Mint Your Non-Transferable Token To Vote For The ETHPrague Hackathon: Hackers Favourite And Decide Which Project Should Win! Every Vote Matters!"
      groupKey="hacker"
      howItWorks={<HowItWorks />}
      afterMintLink="https://influence.factorydao.org/ethprague-hacker"
    />
  );
};

export default HackerPage;
