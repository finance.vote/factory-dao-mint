import axios from 'axios';
import GlobalStateProvider from 'globalState';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import { useEffect, useState } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './index.scss';
const DisclaimerModal = dynamic(() => import('components/DisclaimerModal/DisclaimerModal'));
const PageLoading = dynamic(() => import('components/Ui/PageLoading/PageLoading.component'));
const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  const [isBrowser, setIsBrowser] = useState(false);

  useEffect(() => {
    setIsBrowser(true);
  }, []);

  return (
    <>
      <Head>
        <title>FactoryDAO</title>
      </Head>
      <ToastContainer limit={2} />
      <QueryClientProvider client={queryClient}>
        <div>
          {isBrowser ? (
            <GlobalStateProvider>
              <DisclaimerModal />
              <Component {...pageProps} />
            </GlobalStateProvider>
          ) : (
            <PageLoading />
          )}
        </div>
      </QueryClientProvider>
    </>
  );
}

export default MyApp;

axios.defaults.baseURL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:8080';

axios.interceptors.response.use(
  (request) => {
    return request;
  },
  async (error) => {
    if (error.response?.config?.responseType === 'blob') {
      const _error = JSON.parse(await error.response.data.text());
      throw _error;
    }
    if (axios.isCancel(error)) throw Error(error);
    const status = error.response?.status;
    const ethersError = error.response?.data?.error?.code;
    let errorData =
      error.response?.data?.error ??
      error.response?.error ??
      error.response?.data?.error_description ??
      error.message;
    if (ethersError) {
      errorData = `${errorData.code}, ${errorData.argument}, ${errorData.value}`;
    }
    let msg;
    switch (status) {
      case 401:
        msg = '401 Unauthorized';
        break;
      case 400:
        msg = 'Bad request';
        break;
      case 500:
        msg = 'Technical error';
        break;
      default:
        msg = `'Unexpected error, status: ${status}:`;
        break;
    }
    if (console && console.error) console.error(msg, errorData);

    if (errorData && !error?.config?.url?.includes('/reserve/')) {
      toast.clearWaitingQueue();
      toast.error(
        <p className="toast__centered">
          Error occured.
          <br />
          Check console for more details
        </p>,
        { position: 'top-center' }
      );
    }
    throw Error(errorData);
  }
);
