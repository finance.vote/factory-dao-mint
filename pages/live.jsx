import EthPragueMain from 'components/EthPrague/EthPragueMain';
import HowItWorks from 'components/EthPrague/HowItWorksLive';

const LiveVisitorPage = () => {
  return (
    <EthPragueMain
      title={
        <>
          ETHPrague
          <br />
          online&nbsp;viewer&nbsp;ID
        </>
      }
      description="Mint Your Non-Transferable Token To Participate in Live Stream AMA and to Vote For The ETHPrague Hackathon Audience Favourite! Every Vote Matters!"
      groupKey="ethprglive"
      afterMintText="continue to live stream AMA"
      afterMintLink="https://live.ethprague.com"
      howItWorks={<HowItWorks />}
    />
  );
};

export default LiveVisitorPage;
