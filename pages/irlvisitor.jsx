import EthPragueMain from 'components/EthPrague/EthPragueMain';
import HowItWorks from 'components/EthPrague/HowItWorksIrlVisitor';

const IrlVisitorPage = () => {
  return (
    <EthPragueMain
      title={
        <>
          ETHPrague
          <br />
          irl visitor ID
        </>
      }
      description="Mint Your Non-Transferable Token To Vote For The ETHPrague Hackathon Audience Favourite And Decide Which Project Should Win! Every Vote Matters!"
      groupKey="irlvisitor"
      howItWorks={<HowItWorks />}
    />
  );
};

export default IrlVisitorPage;
