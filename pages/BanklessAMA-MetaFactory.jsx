import BanklessAMAMetaFactory from 'pages/BanklessAMA/MetaFactory';

export default function BanklessAMAMetaFactoryComponent() {
  return <BanklessAMAMetaFactory groupKey="banklessama-metafactory" />;
}
