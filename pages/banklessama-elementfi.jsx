import BanklessAMAElementFi from 'pages/BanklessAMA/ElementFi';

export default function BanklessAMAElementfiComponent() {
  return <BanklessAMAElementFi groupKey="banklessama-elementfi" />;
}
