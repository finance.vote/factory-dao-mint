import dynamic from 'next/dynamic';
import Image from 'next/image';
import EthBrno from 'pages/EthBrno';
// import Gmgn from 'pages/Gmgn';
import FVTS3 from 'pages/FVTS3';
import MynaEth from 'pages/MynaEth';
import fdaoLogo from '../public/images/fdaoLogo.png';

const EthBerlin = dynamic(() => import('pages/EthBerlin'));

const Homepage = () => {
  if (location.hostname === 'mint.ethberlin.ooo') {
    return <EthBerlin groupKey="ethberlin" />;
  }
  if (location.hostname === 'mint.ethbrno.cz') {
    return <EthBrno groupKey="ethbrno" />;
  }
  if (location.hostname === 'nft.mynaaccountants.co') {
    return <MynaEth groupKey="myna-eth" />;
  }
  if (location.hostname === 'mint.fvt.app') {
    return <FVTS3 />;
  }

  return (
    <div
      style={{
        width: '100vw',
        height: '100vh',
        backgroundColor: 'black',
        display: 'flex',
        placeItems: 'center',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <Image src={fdaoLogo} alt="FactoryDAO" />
    </div>
  );
};

export default Homepage;
