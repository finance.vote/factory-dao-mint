import EthBrno from 'pages/EthBrno';

export function getServerSideProps({ req }) {
  if (req.headers.host === 'mint.ethbrno.cz') {
    let destinationUrl = '/';
    if (req.url.includes('code=')) {
      destinationUrl += `?code=${req.url.replace(/.*code=(.*)&*/, '$1')}`;
    }
    return {
      redirect: {
        destination: destinationUrl,
        permanent: false
      }
    };
  }
  return {
    props: {}
  };
}

export default function EthBrnoPage() {
  return <EthBrno groupKey="ethbrno" />;
}
