import EthZurich from 'pages/EthZurich';

export default function EthZurichPage() {
  return <EthZurich groupKey="sbt" />;
}
