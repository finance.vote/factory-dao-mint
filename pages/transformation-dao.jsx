import TransformationDao from 'pages/TransformationDao';

export default function TransformationDaoPage() {
  return <TransformationDao groupKey="trfdao" />;
}
