import MintingController from 'minting-components/components/MintingController';

export default function EthBerlinPage() {
  return (
    <MintingController
      groupKey="test_pass"
      customTexts={{
        successTitle: `Welcome to demo FactoryDao Test pass`,
        successMessage: `NFT #%TOKEN_ID% now belongs to you`
      }}
    />
  );
}
