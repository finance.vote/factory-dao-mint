import FactoryDAO from 'pages/FactoryDAO';

export default function FactoryDAOPage() {
  return <FactoryDAO groupKey="factorydao-demo-test" />;
}
