#!/bin/bash

###
### Example usage: ./scripts/build.sh 1.0.4
###

git tag $1
git push origin $1
npm run build
docker build -t netisoft/minting-frontend:$1 .
docker push netisoft/minting-frontend:$1